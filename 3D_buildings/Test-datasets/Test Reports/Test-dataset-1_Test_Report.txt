----------------------Error Report------------------------------
Snapping Tolerance:  0.000010
Folds tolerance:  0.000100
Distnace to plane Tolerance:  0.000010
Number of floating points error:  0
No duplicate points are present in the dataset.
Planarity Error! Healing required!
Total number of polygons:  7
Total number of non-planar polygons:  1
Repair tolerance:  0.0000001
----------------------Repair Report------------------------------
GML id of non-planar polygon:  								<gml:Polygon gml:id="GML_6286ffa9-3811-4796-a92f-3fd037c8e668">
Number of vertices before repair (non-planar polygon):  5
Number of split faces after Repair Process:  2
Split face contains (vertices):  3
Split face contains (vertices):  4
------------- Total number of repaired faces-------------:  1
-----------------Repair successfully executed!-----------------
