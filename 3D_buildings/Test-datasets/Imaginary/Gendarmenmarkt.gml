﻿<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<!-- Written by virtualcityDATABASE Importer/Exporter, version "1.2-postgis-b53" -->
<!-- virtualcitySYSTEMS GmbH, Germany, http://www.virtualcitysystems.de/ -->
<CityModel xmlns="http://www.opengis.net/citygml/1.0" xmlns:tran="http://www.opengis.net/citygml/transportation/1.0" xmlns:wtr="http://www.opengis.net/citygml/waterbody/1.0" xmlns:gml="http://www.opengis.net/gml" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:grp="http://www.opengis.net/citygml/cityobjectgroup/1.0" xmlns:luse="http://www.opengis.net/citygml/landuse/1.0" xmlns:frn="http://www.opengis.net/citygml/cityfurniture/1.0" xmlns:app="http://www.opengis.net/citygml/appearance/1.0" xmlns:tex="http://www.opengis.net/citygml/texturedsurface/1.0" xmlns:xal="urn:oasis:names:tc:ciq:xsdschema:xAL:2.0" xmlns:bldg="http://www.opengis.net/citygml/building/1.0" xmlns:dem="http://www.opengis.net/citygml/relief/1.0" xmlns:veg="http://www.opengis.net/citygml/vegetation/1.0" xmlns:gen="http://www.opengis.net/citygml/generics/1.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opengis.net/citygml/landuse/1.0 http://schemas.opengis.net/citygml/landuse/1.0/landUse.xsd http://www.opengis.net/citygml/cityfurniture/1.0 http://schemas.opengis.net/citygml/cityfurniture/1.0/cityFurniture.xsd http://www.opengis.net/citygml/appearance/1.0 http://schemas.opengis.net/citygml/appearance/1.0/appearance.xsd http://www.opengis.net/citygml/texturedsurface/1.0 http://schemas.opengis.net/citygml/texturedsurface/1.0/texturedSurface.xsd http://www.opengis.net/citygml/transportation/1.0 http://schemas.opengis.net/citygml/transportation/1.0/transportation.xsd http://www.opengis.net/citygml/waterbody/1.0 http://schemas.opengis.net/citygml/waterbody/1.0/waterBody.xsd http://www.opengis.net/citygml/building/1.0 http://schemas.opengis.net/citygml/building/1.0/building.xsd http://www.opengis.net/citygml/relief/1.0 http://schemas.opengis.net/citygml/relief/1.0/relief.xsd http://www.opengis.net/citygml/vegetation/1.0 http://schemas.opengis.net/citygml/vegetation/1.0/vegetation.xsd http://www.opengis.net/citygml/generics/1.0 http://schemas.opengis.net/citygml/generics/1.0/generics.xsd http://www.opengis.net/citygml/cityobjectgroup/1.0 http://schemas.opengis.net/citygml/cityobjectgroup/1.0/cityObjectGroup.xsd">

  <cityObjectMember>
    <bldg:Building gml:id="BLDG_00030002007d1d3b">
      <gml:boundedBy>
        <gml:Envelope srsName="urn:ogc:def:crs,crs:EPSG::25833,crs:EPSG::5783" srsDimension="3">
          <gml:lowerCorner>390814.613281724 5819136.82401951 33.7799987792969</gml:lowerCorner>
          <gml:upperCorner>390843.043772875 5819160.85232523 63.3166664365738</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <creationDate>2014-09-17</creationDate>
      <externalReference>
        <informationSystem>LOCATION</informationSystem>
        <externalObject>
          <name>00030002007d1d3b</name>
        </externalObject>
      </externalReference>
      <gen:stringAttribute name="GE_LoD2_zOffset">
        <gen:value>Auto|13.753612520703093|06.10.2014 12:39:44</gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="EIG_KL_ST">
        <gen:value>1</gen:value>
      </gen:intAttribute>
      <gen:intAttribute name="EIG_KL_PV">
        <gen:value>3</gen:value>
      </gen:intAttribute>
      <gen:doubleAttribute name="H_First_Min">
        <gen:value>42.1651688</gen:value>
      </gen:doubleAttribute>
      <gen:doubleAttribute name="H_First_Max">
        <gen:value>63.3166695</gen:value>
      </gen:doubleAttribute>
      <gen:doubleAttribute name="H_Trauf_Min">
        <gen:value>42.1651688</gen:value>
      </gen:doubleAttribute>
      <gen:doubleAttribute name="H_Trauf_Max">
        <gen:value>63.3166695</gen:value>
      </gen:doubleAttribute>
      <gen:intAttribute name="TexVersion">
        <gen:value>1</gen:value>
      </gen:intAttribute>
      <gen:stringAttribute name="Kachel">
        <gen:value>2400021000</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="LAND">
        <gen:value>11-11</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="RBEZ">
        <gen:value>0-0</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="KREIS">
        <gen:value>00-00</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="GMDE">
        <gen:value>001-001</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STR">
        <gen:value>41659-06092</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="HNR">
        <gen:value>54-63</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ADZ">
        <gen:value>-</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="LFD">
        <gen:value>01-01</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="OAR">
        <gen:value>2121</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="FOLIE">
        <gen:value>011</gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="ANZ_LOC">
        <gen:value>1</gen:value>
      </gen:intAttribute>
      <bldg:function>2121</bldg:function>
      <bldg:roofType>1130</bldg:roofType>
      <bldg:measuredHeight uom="urn:ogc:def:uom:UCUM::m">29.41666</bldg:measuredHeight>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="BLDG_176549">
          <gml:boundedBy>
            <gml:Envelope srsName="urn:ogc:def:crs,crs:EPSG::25833,crs:EPSG::5783" srsDimension="3">
              <gml:lowerCorner>390814.613461288 5819136.8242848 42.1651710445045</gml:lowerCorner>
              <gml:upperCorner>390843.043772816 5819160.85232442 63.3166664365738</gml:upperCorner>
            </gml:Envelope>
          </gml:boundedBy>
          <creationDate>2014-09-17</creationDate>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="GEOM_629167">
              <gml:surfaceMember>
                <gml:Polygon gml:id="GEOM_629173">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GEOM_629173_0_">
                      <gml:posList srsDimension="3">390839.209210454 5819139.44550583 63.3166664365738 390839.696530781 5819139.47881548 63.3176664365738 390840.170663427 5819139.59624111 63.3166664365738 390840.617202114 5819139.7942148 63.3166664365738 390841.022578989 5819140.06672121 63.3166664365738 390841.37447688 5819140.40548037 63.3166664365738 390841.66220355 5819140.80019927 63.3166664365738 390841.877016569 5819141.23888457 63.3166664365738 390842.012388953 5819141.70820703 63.3166664365738 390842.064207479 5819142.19390654 63.3166664365738 390842.030897669 5819142.68122535 63.3166664365738 390841.913471625 5819143.15535654 63.3166664365738 390841.715497276 5819143.60189386 63.3166664365738 390841.442989973 5819144.00726951 63.3166664365738 390841.104229714 5819144.35916635 63.3166664365738 390840.709509555 5819144.64689217 63.3166664365738 390840.27082287 5819144.86170458 63.3166664365738 390839.80149893 5819144.9970766 63.3166664365738 390839.315797907 5819145.04889503 63.3166664365738 390838.828477579 5819145.01558539 63.3166664365738 390838.354344929 5819144.89815976 63.3166664365738 390837.907806238 5819144.70018608 63.3166664365738 390837.502429357 5819144.42767967 63.3166664365738 390837.150531458 5819144.08892051 63.3166664365738 390836.862804782 5819143.6942016 63.3166664365738 390836.647991758 5819143.25551631 63.3166664365738 390836.512619371 5819142.78619384 63.3166664365738 390836.460800843 5819142.30049433 63.3166664365738 390836.494110653 5819141.81317551 63.3166664365738 390836.611536701 5819141.33904432 63.3166664365738 390836.809511055 5819140.89250699 63.3166664365738 390837.082018364 5819140.48713134 63.3166664365738 390837.42077863 5819140.1352345 63.3166664365738 390837.815498795 5819139.84750867 63.3166664365738 390838.254185486 5819139.63269627 63.3166664365738 390838.723509429 5819139.49732425 63.3166664365738 390839.209210454 5819139.44550583 63.3166664365738</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="GEOM_629172">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GEOM_629172_0_">
                      <gml:posList srsDimension="3">390815.735762982 5819148.44079687 42.1751710445045 390828.431339684 5819149.51173184 42.1651710445045 390827.70056851 5819159.18413052 42.1651710445045 390826.89937534 5819159.11338061 42.1651710445045 390815.007335345 5819158.0628411 42.1651710445045 390815.470013012 5819151.95041647 42.1651710445045 390815.571595428 5819150.61467096 42.1651710445045 390815.709759365 5819148.78419027 42.1651710445045 390815.735762982 5819148.44079687 42.1751710445045</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="GEOM_629171">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GEOM_629171_0_">
                      <gml:posList srsDimension="3">390839.472417208 5819141.30019703 62.9663112147753 390842.642310525 5819139.2217528 56.9908344201612 390842.463589271 5819141.51830449 56.9908344201612 390841.765001736 5819150.49509156 56.9908344201612 390840.997396194 5819160.35875657 56.9908344201612 390838.009768074 5819160.09510961 62.9663112147753 390838.773829664 5819150.27698417 62.9663112147753 390839.472417208 5819141.30019703 62.9663112147753</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="GEOM_629170">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GEOM_629170_0_">
                      <gml:posList srsDimension="3">390838.009768153 5819160.09510972 62.97 390827.677697949 5819159.18271467 62.4813211653933 390828.429504075 5819149.52207117 62.4813211653933 390839.472417287 5819141.30019715 62.97 390838.773829742 5819150.27698428 62.97 390838.009768153 5819160.09510972 62.97</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="GEOM_629169">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GEOM_629169_0_">
                      <gml:posList srsDimension="3">390829.12810201 5819140.54529915 62.9663112147753 390816.404460692 5819139.61676499 62.9663112147753 390816.578267026 5819137.31967496 56.9908344201612 390829.306695696 5819138.24855855 56.9908344201612 390839.651010884 5819139.00345649 56.9908344201612 390842.642310525 5819139.2217528 56.9908344201612 390839.472417208 5819141.30019703 62.9663112147753 390829.12810201 5819140.54529915 62.9663112147753</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="GEOM_629168">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GEOM_629168_0_">
                      <gml:posList srsDimension="3">390839.472417287 5819141.30019715 62.97 390828.429504075 5819149.52207117 62.4813211653933 390815.724575615 5819148.59490288 62.4813211653933 390816.40446077 5819139.61676511 62.97 390829.128102089 5819140.54529927 62.97 390839.472417287 5819141.30019715 62.97</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="BLDG_176547">
          <gml:boundedBy>
            <gml:Envelope srsName="urn:ogc:def:crs,crs:EPSG::25833,crs:EPSG::5783" srsDimension="3">
              <gml:lowerCorner>390814.61328226 5819136.8240198 33.7799987792969</gml:lowerCorner>
              <gml:upperCorner>390843.043148584 5819160.85140044 34.0800018310547</gml:upperCorner>
            </gml:Envelope>
          </gml:boundedBy>
          <creationDate>2014-09-17</creationDate>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="GEOM_629110">
              <gml:surfaceMember>
                <gml:Polygon gml:id="GEOM_629113">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GEOM_629113_0_">
                      <gml:posList srsDimension="3">390839.695902067 5819139.47788488 33.8699989318848 390839.20858174 5819139.44457522 33.8699989318848 390838.722880716 5819139.49639365 33.8699989318848 390838.253556773 5819139.63176567 33.8699989318848 390837.814870082 5819139.84657807 33.8699989318848 390837.420149917 5819140.13430389 33.8699989318848 390837.081389651 5819140.48620073 33.8699989318848 390836.808882343 5819140.89157639 33.8699989318848 390836.610907989 5819141.33811371 33.8699989318848 390836.493481942 5819141.8122449 33.8699989318848 390836.460172131 5819142.29956372 33.8699989318848 390836.511990658 5819142.78526323 33.8699989318848 390836.647363045 5819143.2545857 33.8699989318848 390836.86217607 5819143.693271 33.8699989318848 390837.149902746 5819144.0879899 33.8699989318848 390837.501800644 5819144.42674907 33.8699989318848 390837.907177524 5819144.69925547 33.8699989318848 390838.353716216 5819144.89722915 33.8699989318848 390838.827848866 5819145.01465478 33.8699989318848 390839.315169193 5819145.04796442 33.8699989318848 390839.800870216 5819144.99614599 33.8699989318848 390840.270194156 5819144.86077397 33.8699989318848 390840.708880841 5819144.64596157 33.8699989318848 390841.103600999 5819144.35823574 33.8699989318848 390841.442361258 5819144.0063389 33.8699989318848 390841.714868561 5819143.60096325 33.8699989318848 390841.912842909 5819143.15442593 33.8699989318848 390842.030268954 5819142.68029474 33.8699989318848 390842.063578764 5819142.19297593 33.8699989318848 390842.011760237 5819141.70727643 33.8699989318848 390841.876387854 5819141.23795396 33.8699989318848 390841.661574835 5819140.79926867 33.8699989318848 390841.373848165 5819140.40454977 33.8699989318848 390841.021950274 5819140.0657906 33.8699989318848 390840.616573399 5819139.79328419 33.8699989318848 390840.170034713 5819139.59531051 33.8699989318848 390839.695902067 5819139.47788488 33.8699989318848</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="GEOM_629112">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GEOM_629112_0_">
                      <gml:posList srsDimension="3">390828.431167059 5819149.51147631 34.0800018310547 390815.735590359 5819148.44054135 34.0800018310547 390815.709586742 5819148.78393475 34.0800018310547 390815.571422806 5819150.61441544 34.0800018310547 390815.46984039 5819151.95016095 34.0800018310547 390815.007162722 5819158.06258558 34.0800018310547 390826.899202716 5819159.11312509 34.0800018310547 390827.700395885 5819159.183875 34.0800018310547 390828.431167059 5819149.51147631 34.0800018310547</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="GEOM_629111">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GEOM_629111_0_">
                      <gml:posList srsDimension="3">390827.677085156 5819159.18180761 33.7799987792969 390838.00914492 5819160.09418722 33.7799987792969 390840.99690062 5819160.35802303 33.7799987792969 390841.764506162 5819150.49435802 33.7799987792969 390842.463093696 5819141.51757095 33.7799987792969 390842.64181495 5819139.22101927 33.7799987792969 390839.65051531 5819139.00272295 33.7799987792969 390829.306200127 5819138.24782502 33.7799987792969 390816.577771464 5819137.31894142 33.7799987792969 390816.40383755 5819139.61584261 33.7799987792969 390815.723962829 5819148.59399582 33.7799987792969 390828.428891281 5819149.52116411 33.7799987792969 390827.677085156 5819159.18180761 33.7799987792969</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="BLDG_176548">
          <gml:boundedBy>
            <gml:Envelope srsName="urn:ogc:def:crs,crs:EPSG::25833,crs:EPSG::5783" srsDimension="3">
              <gml:lowerCorner>390814.61328226 5819136.8240198 33.7799987792969</gml:lowerCorner>
              <gml:upperCorner>390843.043772816 5819160.85232442 63.3166664365738</gml:upperCorner>
            </gml:Envelope>
          </gml:boundedBy>
          <creationDate>2014-09-17</creationDate>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="GEOM_629114">
              <gml:surfaceMember>
                <gml:Polygon gml:id="GEOM_629141">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GEOM_629141_0_">
                      <gml:posList srsDimension="3">390836.511990658 5819142.78526323 33.8699989318848 390836.512619371 5819142.78619384 63.3166664365738 390836.647991758 5819143.25551631 63.3166664365738 390836.647363045 5819143.2545857 33.8699989318848 390836.511990658 5819142.78526323 33.8699989318848</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="GEOM_629140">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GEOM_629140_0_">
                      <gml:posList srsDimension="3">390836.460172131 5819142.29956372 33.8699989318848 390836.460800843 5819142.30049433 63.3166664365738 390836.512619371 5819142.78619384 63.3166664365738 390836.511990658 5819142.78526323 33.8699989318848 390836.460172131 5819142.29956372 33.8699989318848</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="GEOM_629139">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GEOM_629139_0_">
                      <gml:posList srsDimension="3">390836.493481942 5819141.8122449 33.8699989318848 390836.494110653 5819141.81317551 63.3166664365738 390836.460800843 5819142.30049433 63.3166664365738 390836.460172131 5819142.29956372 33.8699989318848 390836.493481942 5819141.8122449 33.8699989318848</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="GEOM_629138">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GEOM_629138_0_">
                      <gml:posList srsDimension="3">390836.610907989 5819141.33811371 33.8699989318848 390836.611536701 5819141.33904432 63.3166664365738 390836.494110653 5819141.81317551 63.3166664365738 390836.493481942 5819141.8122449 33.8699989318848 390836.610907989 5819141.33811371 33.8699989318848</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="GEOM_629137">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GEOM_629137_0_">
                      <gml:posList srsDimension="3">390836.808882343 5819140.89157639 33.8699989318848 390836.809511055 5819140.89250699 63.3166664365738 390836.611536701 5819141.33904432 63.3166664365738 390836.610907989 5819141.33811371 33.8699989318848 390836.808882343 5819140.89157639 33.8699989318848</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="GEOM_629136">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GEOM_629136_0_">
                      <gml:posList srsDimension="3">390837.081389651 5819140.48620073 33.8699989318848 390837.082018364 5819140.48713134 63.3166664365738 390836.809511055 5819140.89250699 63.3166664365738 390836.808882343 5819140.89157639 33.8699989318848 390837.081389651 5819140.48620073 33.8699989318848</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="GEOM_629135">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GEOM_629135_0_">
                      <gml:posList srsDimension="3">390837.420149917 5819140.13430389 33.8699989318848 390837.42077863 5819140.1352345 63.3166664365738 390837.082018364 5819140.48713134 63.3166664365738 390837.081389651 5819140.48620073 33.8699989318848 390837.420149917 5819140.13430389 33.8699989318848</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="GEOM_629134">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GEOM_629134_0_">
                      <gml:posList srsDimension="3">390837.814870082 5819139.84657807 33.8699989318848 390837.815498795 5819139.84750867 63.3166664365738 390837.42077863 5819140.1352345 63.3166664365738 390837.420149917 5819140.13430389 33.8699989318848 390837.814870082 5819139.84657807 33.8699989318848</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="GEOM_629133">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GEOM_629133_0_">
                      <gml:posList srsDimension="3">390838.253556773 5819139.63176567 33.8699989318848 390838.254185486 5819139.63269627 63.3166664365738 390837.815498795 5819139.84750867 63.3166664365738 390837.814870082 5819139.84657807 33.8699989318848 390838.253556773 5819139.63176567 33.8699989318848</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="GEOM_629132">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GEOM_629132_0_">
                      <gml:posList srsDimension="3">390838.722880716 5819139.49639365 33.8699989318848 390838.723509429 5819139.49732425 63.3166664365738 390838.254185486 5819139.63269627 63.3166664365738 390838.253556773 5819139.63176567 33.8699989318848 390838.722880716 5819139.49639365 33.8699989318848</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="GEOM_629131">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GEOM_629131_0_">
                      <gml:posList srsDimension="3">390839.20858174 5819139.44457522 33.8699989318848 390839.209210454 5819139.44550583 63.3166664365738 390838.723509429 5819139.49732425 63.3166664365738 390838.722880716 5819139.49639365 33.8699989318848 390839.20858174 5819139.44457522 33.8699989318848</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="GEOM_629130">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GEOM_629130_0_">
                      <gml:posList srsDimension="3">390828.431167059 5819149.51147631 34.0800018310547 390828.431339684 5819149.51173184 42.1651710445045 390815.735762982 5819148.44079687 42.1751710445045 390815.735590359 5819148.44054135 34.0800018310547 390828.431167059 5819149.51147631 34.0800018310547</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="GEOM_629129">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GEOM_629129_0_">
                      <gml:posList srsDimension="3">390827.700395885 5819159.183875 34.0800018310547 390827.70056851 5819159.18413052 42.1651710445045 390828.431339684 5819149.51173184 42.1651710445045 390828.431167059 5819149.51147631 34.0800018310547 390827.700395885 5819159.183875 34.0800018310547</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="GEOM_629128">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GEOM_629128_0_">
                      <gml:posList srsDimension="3">390826.899202716 5819159.11312509 34.0800018310547 390826.89937534 5819159.11338061 42.1651710445045 390827.70056851 5819159.18413052 42.1651710445045 390827.700395885 5819159.183875 34.0800018310547 390826.899202716 5819159.11312509 34.0800018310547</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="GEOM_629127">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GEOM_629127_0_">
                      <gml:posList srsDimension="3">390815.007162722 5819158.06258558 34.0800018310547 390815.007335345 5819158.0628411 42.1651710445045 390826.89937534 5819159.11338061 42.1651710445045 390826.899202716 5819159.11312509 34.0800018310547 390815.007162722 5819158.06258558 34.0800018310547</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="GEOM_629126">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GEOM_629126_0_">
                      <gml:posList srsDimension="3">390815.46984039 5819151.95016095 34.0800018310547 390815.470013012 5819151.95041647 42.1651710445045 390815.007335345 5819158.0628411 42.1651710445045 390815.007162722 5819158.06258558 34.0800018310547 390815.46984039 5819151.95016095 34.0800018310547</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="GEOM_629125">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GEOM_629125_0_">
                      <gml:posList srsDimension="3">390815.571422806 5819150.61441544 34.0800018310547 390815.571595428 5819150.61467096 42.1651710445045 390815.470013012 5819151.95041647 42.1651710445045 390815.46984039 5819151.95016095 34.0800018310547 390815.571422806 5819150.61441544 34.0800018310547</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="GEOM_629124">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GEOM_629124_0_">
                      <gml:posList srsDimension="3">390815.709586742 5819148.78393475 34.0800018310547 390815.709759365 5819148.78419027 42.1651710445045 390815.571595428 5819150.61467096 42.1651710445045 390815.571422806 5819150.61441544 34.0800018310547 390815.709586742 5819148.78393475 34.0800018310547</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="GEOM_629123">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GEOM_629123_0_">
                      <gml:posList srsDimension="3">390815.735590359 5819148.44054135 34.0800018310547 390815.735762982 5819148.44079687 42.1751710445045 390815.709759365 5819148.78419027 42.1651710445045 390815.709586742 5819148.78393475 34.0800018310547 390815.735590359 5819148.44054135 34.0800018310547</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="GEOM_629122">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GEOM_629122_0_">
                      <gml:posList srsDimension="3">390842.463589271 5819141.51830449 56.9908344201612 390842.642310525 5819139.2217528 56.9908344201612 390842.64181495 5819139.22101927 33.7799987792969 390842.463093696 5819141.51757095 33.7799987792969 390841.764506162 5819150.49435802 33.7799987792969 390840.99690062 5819160.35802303 33.7799987792969 390840.997396194 5819160.35875657 56.9908344201612 390841.765001736 5819150.49509156 56.9908344201612 390842.463589271 5819141.51830449 56.9908344201612</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="GEOM_629121">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GEOM_629121_0_">
                      <gml:posList srsDimension="3">390829.306695696 5819138.24855855 56.9908344201612 390816.578267026 5819137.31967496 56.9908344201612 390816.577771464 5819137.31894142 33.7799987792969 390829.306200127 5819138.24782502 33.7799987792969 390839.65051531 5819139.00272295 33.7799987792969 390842.64181495 5819139.22101927 33.7799987792969 390842.642310525 5819139.2217528 56.9908344201612 390839.651010884 5819139.00345649 56.9908344201612 390829.306695696 5819138.24855855 56.9908344201612</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="GEOM_629120">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GEOM_629120_0_">
                      <gml:posList srsDimension="3">390838.00914492 5819160.09418722 33.7799987792969 390838.009768074 5819160.09510961 62.9663112147753 390840.997396194 5819160.35875657 56.9908344201612 390840.99690062 5819160.35802303 33.7799987792969 390838.00914492 5819160.09418722 33.7799987792969</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="GEOM_629119">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GEOM_629119_0_">
                      <gml:posList srsDimension="3">390828.428891281 5819149.52116411 33.7799987792969 390828.429504075 5819149.52207117 62.4813211653933 390827.677697949 5819159.18271467 62.4813211653933 390827.677085156 5819159.18180761 33.7799987792969 390828.428891281 5819149.52116411 33.7799987792969</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="GEOM_629118">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GEOM_629118_0_">
                      <gml:posList srsDimension="3">390827.677085156 5819159.18180761 33.7799987792969 390827.677697949 5819159.18271467 62.4813211653933 390838.009768153 5819160.09510972 62.97 390838.009768074 5819160.09510961 62.9663112147753 390838.00914492 5819160.09418722 33.7799987792969 390827.677085156 5819159.18180761 33.7799987792969</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="GEOM_629117">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GEOM_629117_0_">
                      <gml:posList srsDimension="3">390816.40383755 5819139.61584261 33.7799987792969 390816.404460692 5819139.61676499 62.9663112147753 390816.40446077 5819139.61676511 62.97 390815.724575615 5819148.59490288 62.4813211653933 390815.723962829 5819148.59399582 33.7799987792969 390816.40383755 5819139.61584261 33.7799987792969</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="GEOM_629116">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GEOM_629116_0_">
                      <gml:posList srsDimension="3">390815.723962829 5819148.59399582 33.7799987792969 390815.724575615 5819148.59490288 62.4813211653933 390828.429504075 5819149.52207117 62.4813211653933 390828.428891281 5819149.52116411 33.7799987792969 390815.723962829 5819148.59399582 33.7799987792969</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="GEOM_629115">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GEOM_629115_0_">
                      <gml:posList srsDimension="3">390816.577771464 5819137.31894142 33.7799987792969 390816.578267026 5819137.31967496 56.9908344201612 390816.404460692 5819139.61676499 62.9663112147753 390816.40383755 5819139.61584261 33.7799987792969 390816.577771464 5819137.31894142 33.7799987792969</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="GEOM_629166">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GEOM_629166_0_">
                      <gml:posList srsDimension="3">390839.695902067 5819139.47788488 33.8699989318848 390839.696530781 5819139.47881548 63.3176664365738 390839.209210454 5819139.44550583 63.3166664365738 390839.20858174 5819139.44457522 33.8699989318848 390839.695902067 5819139.47788488 33.8699989318848</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="GEOM_629165">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GEOM_629165_0_">
                      <gml:posList srsDimension="3">390840.170034713 5819139.59531051 33.8699989318848 390840.170663427 5819139.59624111 63.3166664365738 390839.696530781 5819139.47881548 63.3176664365738 390839.695902067 5819139.47788488 33.8699989318848 390840.170034713 5819139.59531051 33.8699989318848</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="GEOM_629164">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GEOM_629164_0_">
                      <gml:posList srsDimension="3">390840.616573399 5819139.79328419 33.8699989318848 390840.617202114 5819139.7942148 63.3166664365738 390840.170663427 5819139.59624111 63.3166664365738 390840.170034713 5819139.59531051 33.8699989318848 390840.616573399 5819139.79328419 33.8699989318848</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="GEOM_629163">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GEOM_629163_0_">
                      <gml:posList srsDimension="3">390841.021950274 5819140.0657906 33.8699989318848 390841.022578989 5819140.06672121 63.3166664365738 390840.617202114 5819139.7942148 63.3166664365738 390840.616573399 5819139.79328419 33.8699989318848 390841.021950274 5819140.0657906 33.8699989318848</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="GEOM_629162">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GEOM_629162_0_">
                      <gml:posList srsDimension="3">390841.373848165 5819140.40454977 33.8699989318848 390841.37447688 5819140.40548037 63.3166664365738 390841.022578989 5819140.06672121 63.3166664365738 390841.021950274 5819140.0657906 33.8699989318848 390841.373848165 5819140.40454977 33.8699989318848</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="GEOM_629161">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GEOM_629161_0_">
                      <gml:posList srsDimension="3">390841.661574835 5819140.79926867 33.8699989318848 390841.66220355 5819140.80019927 63.3166664365738 390841.37447688 5819140.40548037 63.3166664365738 390841.373848165 5819140.40454977 33.8699989318848 390841.661574835 5819140.79926867 33.8699989318848</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="GEOM_629160">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GEOM_629160_0_">
                      <gml:posList srsDimension="3">390841.876387854 5819141.23795396 33.8699989318848 390841.877016569 5819141.23888457 63.3166664365738 390841.66220355 5819140.80019927 63.3166664365738 390841.661574835 5819140.79926867 33.8699989318848 390841.876387854 5819141.23795396 33.8699989318848</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="GEOM_629159">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GEOM_629159_0_">
                      <gml:posList srsDimension="3">390842.011760237 5819141.70727643 33.8699989318848 390842.012388953 5819141.70820703 63.3166664365738 390841.877016569 5819141.23888457 63.3166664365738 390841.876387854 5819141.23795396 33.8699989318848 390842.011760237 5819141.70727643 33.8699989318848</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="GEOM_629158">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GEOM_629158_0_">
                      <gml:posList srsDimension="3">390842.063578764 5819142.19297593 33.8699989318848 390842.064207479 5819142.19390654 63.3166664365738 390842.012388953 5819141.70820703 63.3166664365738 390842.011760237 5819141.70727643 33.8699989318848 390842.063578764 5819142.19297593 33.8699989318848</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="GEOM_629157">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GEOM_629157_0_">
                      <gml:posList srsDimension="3">390842.030268954 5819142.68029474 33.8699989318848 390842.030897669 5819142.68122535 63.3166664365738 390842.064207479 5819142.19390654 63.3166664365738 390842.063578764 5819142.19297593 33.8699989318848 390842.030268954 5819142.68029474 33.8699989318848</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="GEOM_629156">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GEOM_629156_0_">
                      <gml:posList srsDimension="3">390841.912842909 5819143.15442593 33.8699989318848 390841.913471625 5819143.15535654 63.3166664365738 390842.030897669 5819142.68122535 63.3166664365738 390842.030268954 5819142.68029474 33.8699989318848 390841.912842909 5819143.15442593 33.8699989318848</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="GEOM_629155">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GEOM_629155_0_">
                      <gml:posList srsDimension="3">390841.714868561 5819143.60096325 33.8699989318848 390841.715497276 5819143.60189386 63.3166664365738 390841.913471625 5819143.15535654 63.3166664365738 390841.912842909 5819143.15442593 33.8699989318848 390841.714868561 5819143.60096325 33.8699989318848</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="GEOM_629154">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GEOM_629154_0_">
                      <gml:posList srsDimension="3">390841.442361258 5819144.0063389 33.8699989318848 390841.442989973 5819144.00726951 63.3166664365738 390841.715497276 5819143.60189386 63.3166664365738 390841.714868561 5819143.60096325 33.8699989318848 390841.442361258 5819144.0063389 33.8699989318848</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="GEOM_629153">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GEOM_629153_0_">
                      <gml:posList srsDimension="3">390841.103600999 5819144.35823574 33.8699989318848 390841.104229714 5819144.35916635 63.3166664365738 390841.442989973 5819144.00726951 63.3166664365738 390841.442361258 5819144.0063389 33.8699989318848 390841.103600999 5819144.35823574 33.8699989318848</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="GEOM_629152">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GEOM_629152_0_">
                      <gml:posList srsDimension="3">390840.708880841 5819144.64596157 33.8699989318848 390840.709509555 5819144.64689217 63.3166664365738 390841.104229714 5819144.35916635 63.3166664365738 390841.103600999 5819144.35823574 33.8699989318848 390840.708880841 5819144.64596157 33.8699989318848</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="GEOM_629151">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GEOM_629151_0_">
                      <gml:posList srsDimension="3">390840.270194156 5819144.86077397 33.8699989318848 390840.27082287 5819144.86170458 63.3166664365738 390840.709509555 5819144.64689217 63.3166664365738 390840.708880841 5819144.64596157 33.8699989318848 390840.270194156 5819144.86077397 33.8699989318848</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="GEOM_629150">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GEOM_629150_0_">
                      <gml:posList srsDimension="3">390839.800870216 5819144.99614599 33.8699989318848 390839.80149893 5819144.9970766 63.3166664365738 390840.27082287 5819144.86170458 63.3166664365738 390840.270194156 5819144.86077397 33.8699989318848 390839.800870216 5819144.99614599 33.8699989318848</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="GEOM_629149">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GEOM_629149_0_">
                      <gml:posList srsDimension="3">390839.315169193 5819145.04796442 33.8699989318848 390839.315797907 5819145.04889503 63.3166664365738 390839.80149893 5819144.9970766 63.3166664365738 390839.800870216 5819144.99614599 33.8699989318848 390839.315169193 5819145.04796442 33.8699989318848</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="GEOM_629148">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GEOM_629148_0_">
                      <gml:posList srsDimension="3">390838.827848866 5819145.01465478 33.8699989318848 390838.828477579 5819145.01558539 63.3166664365738 390839.315797907 5819145.04889503 63.3166664365738 390839.315169193 5819145.04796442 33.8699989318848 390838.827848866 5819145.01465478 33.8699989318848</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="GEOM_629147">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GEOM_629147_0_">
                      <gml:posList srsDimension="3">390838.353716216 5819144.89722915 33.8699989318848 390838.354344929 5819144.89815976 63.3166664365738 390838.828477579 5819145.01558539 63.3166664365738 390838.827848866 5819145.01465478 33.8699989318848 390838.353716216 5819144.89722915 33.8699989318848</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="GEOM_629146">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GEOM_629146_0_">
                      <gml:posList srsDimension="3">390837.907177524 5819144.69925547 33.8699989318848 390837.907806238 5819144.70018608 63.3166664365738 390838.354344929 5819144.89815976 63.3166664365738 390838.353716216 5819144.89722915 33.8699989318848 390837.907177524 5819144.69925547 33.8699989318848</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="GEOM_629145">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GEOM_629145_0_">
                      <gml:posList srsDimension="3">390837.501800644 5819144.42674907 33.8699989318848 390837.502429357 5819144.42767967 63.3166664365738 390837.907806238 5819144.70018608 63.3166664365738 390837.907177524 5819144.69925547 33.8699989318848 390837.501800644 5819144.42674907 33.8699989318848</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="GEOM_629144">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GEOM_629144_0_">
                      <gml:posList srsDimension="3">390837.149902746 5819144.0879899 33.8699989318848 390837.150531458 5819144.08892051 63.3166664365738 390837.502429357 5819144.42767967 63.3166664365738 390837.501800644 5819144.42674907 33.8699989318848 390837.149902746 5819144.0879899 33.8699989318848</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="GEOM_629143">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GEOM_629143_0_">
                      <gml:posList srsDimension="3">390836.86217607 5819143.693271 33.8699989318848 390836.862804782 5819143.6942016 63.3166664365738 390837.150531458 5819144.08892051 63.3166664365738 390837.149902746 5819144.0879899 33.8699989318848 390836.86217607 5819143.693271 33.8699989318848</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="GEOM_629142">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GEOM_629142_0_">
                      <gml:posList srsDimension="3">390836.647363045 5819143.2545857 33.8699989318848 390836.647991758 5819143.25551631 63.3166664365738 390836.862804782 5819143.6942016 63.3166664365738 390836.86217607 5819143.693271 33.8699989318848 390836.647363045 5819143.2545857 33.8699989318848</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
    </bldg:Building>
  </cityObjectMember>
</CityModel>