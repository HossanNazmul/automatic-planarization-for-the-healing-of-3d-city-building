#define _SCL_SECURE_NO_WARNINGS //to avoid some VS 2014 compiler warning for CGAL transform parameter

//#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Simple_cartesian.h>
#include <CGAL/Surface_mesh.h>
#include <fstream>
#include <sstream>
#include <map>
#include <boost/foreach.hpp>


using namespace std;

//typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Simple_cartesian<double> K;
typedef CGAL::Surface_mesh<K::Point_3> Surface_mesh;
typedef Surface_mesh::Vertex_index vertex_index;
typedef Surface_mesh::Face_index face_index;

void data_structure()
{
	cout << " --test 1-- " << endl;

	Surface_mesh mesh;
	vertex_index u1 = mesh.add_vertex(K::Point_3(0, 0, 0));
	vertex_index v1 = mesh.add_vertex(K::Point_3(1, 0, 0));
	vertex_index w1 = mesh.add_vertex(K::Point_3(1, 1, 0));
	vertex_index x1 = mesh.add_vertex(K::Point_3(0, 1, 0));
	vertex_index z1 = mesh.add_vertex(K::Point_3(0.5, 0.5, 0));

	face_index face1 = mesh.add_face(v1, w1, z1);
	face_index face2 = mesh.add_face(x1, z1, w1);
	face_index face3 = mesh.add_face(x1, u1, z1);
	face_index face4 = mesh.add_face(u1, v1, z1);


	{//here I find the wrong face_index which is not existed
		std::cout << "faces around face " << face1 << std::endl;
		CGAL::Face_around_target_circulator<Surface_mesh> vbegin(mesh.halfedge(face1), mesh), done(vbegin);

		do {
			std::cout << *vbegin++ << std::endl;
		} while (vbegin != done);
	}

	BOOST_FOREACH(face_index f_index, faces_around_face(mesh.halfedge(face1), mesh)) {
		//face_index ff = faces_around_face(mesh.halfedge(face1),mesh);
		std::cout << "Adjacent faces: " << f_index << std::endl;
		BOOST_FOREACH(vertex_index vd, vertices_around_face(mesh.halfedge(f_index), mesh)) {
			std::cout << " Vertex: " << mesh.point(vd) << endl; //error for the wrong face_index
																//std::cout << vd << std::endl;
		}
	}

	/* 	{
	std::cout << "vertices around vertex " << mesh.point(v1) << std::endl;
	CGAL::Vertex_around_target_circulator<Surface_mesh> vbegin(mesh.halfedge(v1), mesh), done(vbegin);

	do {
	std::cout << *vbegin++ << std::endl;
	} while (vbegin != done);
	}

	{
	std::cout << "vertices around face " << face1 << std::endl;
	CGAL::Vertex_around_face_circulator<Surface_mesh> vbegin(mesh.halfedge(face1), mesh), done(vbegin);

	do {
	std::cout << *vbegin++ << std::endl;
	} while (vbegin != done);
	} */
}

void add_face(const string face, Surface_mesh& sm, std::map<K::Point_3, Surface_mesh::Vertex_index>& point_to_vertex)
{
	std::stringstream ss;
	ss << face;
	std::vector<Surface_mesh::Vertex_index> vertices;
	while (ss)
	{
		K::Point_3 p;
		ss >> p;
		if (!ss) break;
		std::pair<std::map<K::Point_3, Surface_mesh::Vertex_index>::iterator, bool> insert_res;

		insert_res = point_to_vertex.insert(std::make_pair(p, Surface_mesh::Vertex_index()));
		//if (insert_res.second)
		insert_res.first->second = sm.add_vertex(p);
		vertices.push_back(insert_res.first->second);
	}
	for (std::map<K::Point_3, Surface_mesh::Vertex_index>::iterator it = point_to_vertex.begin();
		it != point_to_vertex.end();it++) {
	}

	if (vertices.front() == vertices.back())
		vertices.pop_back();

	sm.add_face(vertices);
}


int main()
{
	data_structure(); //simple test dataset 1

					  //dataset 2
	const string face1 = "458875.12345678900 5438350.1234567890 112.12345678900 458875.12345678900 5438355.12345678900 112.12345678900 458885.12345678900 5438355.12345678900 112.12345678900 458885.12345678900 5438350.12345678900 112.12345678900 458875.12345678900 5438350.12345678900 112.12345678900";
	const string face2 = "458885.12345678900 5438355.12345678900 115.12345678900 458875.12345678900 5438355.12345678900 115.12345678900 458875.12345678900 5438352.12345678905 117.12345678900 458885.12345678900 5438352.12345678905 117.12345678900 458885.12345678900 5438355.12345678900 115.12345678900";
	const string face3 = "458875.12345678900 5438350.12345678900 115.12345678900 458885.12345678900 5438350.12345678900 115.12345678900 458885.12345678900 5438352.12345678905 117.12345678900 458875.12345678900 5438352.12345678905 117.12345678900 458875.12345678900 5438350.12345678900 115.12345678900";
	const string face4 = "458875.12345678900 5438350.12345678900 112.12345678900 458885.12345678900 5438350.12345678900 112.12345678900 458885.12345678900 5438350.12345678900 115.12345678900 458875.12345678900 5438350.12345678900 115.12345678900 458875.12345678900 5438350.12345678900 112.12345678900";
	const string face5 = "458885.12345678900 5438355.12345678900 112.12345678900 458875.12345678900 5438355.12345678900 112.12345678900 458875.12345678900 5438355.12345678900 115.12345678900 458885.12345678900 5438355.12345678900 115.12345678900 458885.12345678900 5438355.12345678900 112.12345678900";
	const string face6 = "458885.12345678900 5438350.12345678900 112.12345678900 458885.12345678900 5438355.12345678900 112.12345678900 458885.12345678900 5438355.12345678900 115.12345678900 458885.12345678900 5438352.12345678905 117.12345678900 458885.12345678900 5438350.12345678900 115.12345678900 458885.12345678900 5438350.12345678900 112.12345678900";
	const string face7 = "458875.12345678900 5438355.12345678900 112.12345678900 458875.12345678900 5438350.12345678900 112.12345678900 458875.12345678900 5438350.12345678900 115.12345678900 458875.12345678900 5438352.12345678905 117.12345678900 458875.12345678900 5438355.12345678900 115.12345678900 458875.12345678900 5438355.12345678900 112.12345678900";

	Surface_mesh sm;
	std::map<K::Point_3, Surface_mesh::Vertex_index> point_to_vertex;

	add_face(face1, sm, point_to_vertex);
	add_face(face2, sm, point_to_vertex);
	add_face(face3, sm, point_to_vertex);
	add_face(face4, sm, point_to_vertex);
	add_face(face5, sm, point_to_vertex);
	add_face(face6, sm, point_to_vertex);
	add_face(face7, sm, point_to_vertex);


	std::vector<face_index> faces;
	BOOST_FOREACH(face_index fi, sm.faces()) {
		//std::cout << "iteratining through face index: " << fi << std::endl;
		faces.push_back(fi);
	}
	cout << " --test 2-- " << endl;
	for (std::vector<face_index>::iterator it = faces.begin(); it != faces.end(); ++it) {
		std::cout << "face: " << *it << endl;
		BOOST_FOREACH(face_index f, faces_around_face(sm.halfedge(*it), sm)) {
			cout << "Adjacent faces: " << f << endl; //the result is wrong
		}

	}

	cout << "number of faces: " << sm.number_of_faces() << endl;
	cout << "total number of Vertices: " << sm.number_of_vertices() << endl;
	cout << "no of HalfEdges: " << sm.number_of_halfedges() << endl;


	std::getchar();
}

