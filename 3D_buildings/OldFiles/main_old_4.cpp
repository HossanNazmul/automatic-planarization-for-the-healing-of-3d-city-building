/*
*  
*  main.cpp  updated
*  Created on: Jan 2, 2017
*  Author: Md. Nazmul Hossan
*  Description: This is the main program that contains main() function.
*  Git repository:  https://HossanNazmul@bitbucket.org/HossanNazmul/automatic-planarization-for-the-healing-of-3d-city-building.git
*/
#define _CRT_SECURE_NO_WARNINGS
#define _SCL_SECURE_NO_WARNINGS //to avoid some VS 2014 compiler warning for CGAL transform parameter

// headers
#include "definitions.h"
#include "functions.h"
#include "Building.h"
#include "LinearRing.h"
#include "CityGMLOutput.h"
//#include"SM_to_Nef.cpp"
//#include "Print.h"
using namespace std;
//global variables
string filepath = "C:/dev/CGAL-4.10/build-examples/Repair_3D_Buildings/3D_buildings/data/";
Surface_Mesh polygon_mesh;
vector<std::pair<face_index, std::pair<string, vector<Point_3>>>> face_gmlids_rings; //stores face index with gmlid and linear rings
std::vector<std::pair<Point_3, Point_3>> dup_points; //used in finding and repairing floating point number error
std::vector<Point_3> duplicate_points;
std::vector<vertex_index> non_planar_vertices;
std::map<vertex_index, Point_3> vertex_map;
std::map<vertex_index, Vector_3> intersection_points;

//std::map<Point_3, vertex_index> point_to_vertex; //a std::map that contains unique points (no duplicate points)
std::vector<face_index> faces;
std::vector<face_points_pair> face_vectices; //face and incident vertices //typedef std::pair<face_index, std::vector<Point_3>> face_points_pair;
std::vector< std::pair<face_index, std::vector<vertex_index>>> non_planar_face_vectices;
int no_of_non_planar_plane = 0;
int face_count = 0;
vector<string> GMLID;
vector<string> origin_building;
//functions
bool check_LR_validity(vector<Point_3>& linearRingPoints);
face_index add_face(vector<Point_3>& pts, Surface_Mesh& pmesh, std::map<Point_3, vertex_index>& point_to_vertex);
void check_planarization_error(Surface_Mesh& pmesh);
vector<double> check_collinearity(vector<double>& linearRingCoords);
bool check_planarity_on_faces(vector<double>& linear_rings, face_index& fi, Surface_Mesh& pmesh);
bool floating_point_number_error(Surface_Mesh& pmesh, double& tol);
void data_structure();
void write_off_format(Surface_Mesh& m);
Plane_3 plane_equation(Surface_Mesh& mesh, face_index& fi);
bool is_collinear(Point_3& P, Point_3& Q, Point_3& R);
void healing(Surface_Mesh& mesh);
Surface_Mesh repair_floating_point_number_error(Surface_Mesh& pmesh);
void polyhedron_check();


int main()
{
	data_structure();


	Building building;
	LinearRing LR;
	vector<double> linearRingCoords;
	//std::string fileName = filepath + "test_data_set_1.gml";
	std::string fileName = filepath + "building.gml";
	//std::string fileName = filepath + "building_repaired_by_intersection_point.gml";
	//std::string fileName = filepath + "FMP2.gml";
	//std::string fileName = filepath + "test_data_set_3_dup_4.gml";
	//std::string fileName = filepath + "test_data_set_1_1np.gml";
	//std::string fileName = filepath + "test_data_set_3_consecutive_points.gml";
	//std::string fileName = filepath + "test_data_set_3_dup_3_CityGMLwriter_2.gml";
	//std::string fileName = filepath + "Museumsinsel - Plus2.gml";
	//90336 vertices with FME and 12707 vertices with CGAL for Museumsinsel - Plus2.gml

	building.readGMLFile(fileName);


	vector<string> Geom = building.buildingGeom;
	// In L_rings string= GML ID and vector double is ring coords
	vector<std::pair<string, vector<double>>> L_rings = LR.getLinearRings(Geom);


	LR.Writevectors();


	std::cout << "size of LR_Rings: " << L_rings.size() << endl;
	std::cout << "size of Polygon GML_ID: " << LR.PolygonID.size() << endl;
	std::cout << "size of LinearRing GML_ID: " << LR.LinearRingID.size() << endl;
	//if (L_rings.size() == LR.PolygonID.size()) {
	//	GMLID = LR.PolygonID;
	//}
	//else {
	//	GMLID = LR.LinearRingID;
	//}

	origin_building = building.cityObjectMember;

	std::map<Point_3, vertex_index> point_to_vertex;
	
	//iterate through Vector<string> _surfaces and its Linear_Rings 
	for (unsigned int index = 0; index < L_rings.size(); index++) {
		GMLID.push_back(L_rings[index].first);
		linearRingCoords = L_rings[index].second;
		vector<Point_3> linearRingPoints;
		for (unsigned int i = 0; i < linearRingCoords.size(); i = i + 3)
		{
			Point_3 p(linearRingCoords[i], linearRingCoords[i + 1], linearRingCoords[i + 2]);
			linearRingPoints.push_back(p);
		}
		//check if the Linear ring is valid 
		check_LR_validity(linearRingPoints);
		
		std::cout << ">>>>>>>>>>>>Processing Linear Ring <<<<<<<<<<<<<<< : " << L_rings[index].first << endl;
		face_index face_id = add_face(linearRingPoints, polygon_mesh, point_to_vertex);
		face_count++;
		//check_planarity_on_faces(linearRingCoords);
		//std::cout << ">>>>>>>>>>>>Processing GMLID <<<<<<<<<<<<<<< : " << GMLID [index] << endl;
	
		face_gmlids_rings.push_back(std::make_pair(face_id, std::make_pair(L_rings[index].first, linearRingPoints)));
	}

	//for (unsigned int i = 0; i < face_gmlids_rings.size(); i++) {
	//	std::cout << "face_index: " << face_gmlids_rings[i].first << endl;
	//	std::cout << "gml id: " << face_gmlids_rings[i].second.first << endl;
	//	std::cout << "LinearRing Points: "<< face_gmlids_rings[i].second.second.size() << endl;
	//	for (unsigned int j = 0; j < face_gmlids_rings[i].second.second.size(); j++) {
	//		std::cout << face_gmlids_rings[i].second.second[j]<<"\n";
	//	}
	//}

	write_off_format(polygon_mesh);

	//polyhedron_check();
	//iterates through all faces of the Surface_Mesh
	check_planarization_error(polygon_mesh);

	std::cout << no_of_non_planar_plane << " - NON-PLANER polygons and (" << non_planar_vertices.size() << ") non planar vertices has been found" << endl;
	std::cout << "+++ Total " << polygon_mesh.number_of_faces() << " Faces ++++ " << endl;
	std::cout << "Total number of Vertices: " << polygon_mesh.number_of_vertices() << endl;
	std::cout << "Number of HalfEdges: " << polygon_mesh.number_of_halfedges() << endl;

	// showing contents of a std::map
	//std::cout << "mymap contains:\n";
	//for (std::map<vertex_index, Point_3>::iterator it = vertex_map.begin(); it != vertex_map.end(); ++it) {
	//	std::cout << it->first << " => " << it->second << '\n';
	//}


	if(non_planar_vertices.size()!=0)
	{
		//	//check if there is any duplicate points 
		//	//decision pts or pts2? //pts - Andreas - adjacent faces 
		double snap_tolerence = 0.001; //in meter
		bool has_floating_point_error = floating_point_number_error(polygon_mesh, snap_tolerence); //decision pts or pts2? // less iteratin

		Surface_Mesh newMesh;
		if (has_floating_point_error) {
			std::cout << "Error: Duplicate points are present in the dataset! Repairing in progress....." << endl;
			newMesh = repair_floating_point_number_error(polygon_mesh);
			//reset status
			no_of_non_planar_plane = 0;
			non_planar_vertices.clear();

			check_planarization_error(newMesh);

			std::cout << no_of_non_planar_plane << " - NON-PLANER polygons and (" << non_planar_vertices.size() << ") non planar vertices has been found" << endl;
			std::cout << "+++ Total " << newMesh.number_of_faces() << " Faces ++++ " << endl;
			std::cout << "Total number of Vertices: " << newMesh.number_of_vertices() << endl;
			std::cout << "Number of HalfEdges: " << newMesh.number_of_halfedges() << endl;

		}
		else {

			std::cout << "No duplicate points are present in the data-set." << endl;
		}

		//polyhedron_check();
		healing(polygon_mesh);
		//Nef_polyhedron nef(polygon_mesh);
		//std::cout << nef << endl;
	}
	else {
		std::cout << "There is no planarization errors in the data" << endl;
	}




	std::cout << ">>>>>>>>>>>> Press any key to exit <<<<<<<<<<<<<<<  " << endl;
	std::getchar();
	return 0;
}

//--------------All member functions starts from here ------------------------------
bool check_LR_validity(vector<Point_3>& linearRingPoints) {
	//check if the linear ring consist of a min. 4 points (first point is identical to the last point)
	bool valid = true;
	if (linearRingPoints.size() < 4) {
		std::cout << "ERROR: The linear ring contains less than three points" << endl;
		valid = false;
	}
	//check if the first point is identical to the last point of the linear ring
	if (linearRingPoints.at(0) != linearRingPoints.at(linearRingPoints.size() - 1)) {
		std::cout << "ERROR: first and the last points of Linear Rings are NOT identical" << endl;
		valid = false;
	}

	//All points of the sequence besides start and end point are different//no self intersection

	//two edges can intersect only in one start-/end point.  //to account for rounding error

	return valid;
	
}

face_index add_face(vector<Point_3>& pts, Surface_Mesh& pmesh, std::map<Point_3, vertex_index>& point_to_vertex2)
{
	//std::map<Kernel::Point_3, Surface_Mesh::Vertex_index> point_to_vertex2;
	std::vector<Surface_Mesh::Vertex_index> vertices;

	for (unsigned int i = 0; i < pts.size(); i++)
	{
		Point_3 p(pts[i]);

		std::pair<std::map<Kernel::Point_3, Surface_Mesh::Vertex_index>::iterator, bool> insert_res
			= point_to_vertex2.insert(std::make_pair(p, Surface_Mesh::Vertex_index()));

		if (insert_res.second) {
			insert_res.first->second = pmesh.add_vertex(p);
		}
		vertices.push_back(insert_res.first->second);


	}
	if (vertices.front() == vertices.back())
		vertices.pop_back();
	//insert by range
	face_index fi = pmesh.add_face(vertices);

	//faces.push_back(fi);

	//BOOST_FOREACH(vertex_index vd, polygon_mesh.vertices()) {
	//	std::cout << "iteratining through vertex index: " << vd << std::endl;
	//}
	return fi;
}

void check_planarization_error(Surface_Mesh& pmesh)
{
	// True= Planar face; False= Non planar face
	bool is_planar;
	vector<face_index> non_planar_faces;
	BOOST_FOREACH(face_index fi, pmesh.faces()) {
		std::cout << "Checking faces: " << fi << std::endl;
		//vector of face indexes
		faces.push_back(fi);

		std::vector<double> face_x; //contains coordinates of all vertices around a particular face
									//iterate through vertices around face
		vector<Point_3> pt;
		BOOST_FOREACH(vertex_index vi, vertices_around_face(pmesh.halfedge(fi), pmesh)) {
			//std::cout << " Vertices: " << vi << " Coords: " << std::setprecision(15) << polygon_mesh.point(vi) << endl;
			Point_3 P = pmesh.point(vi);
			pt.push_back(P);
			//convertion bcoz of Exact_predicates_exact_constructions_kernel
			face_x.push_back(CGAL::to_double(P.x()));
			face_x.push_back(CGAL::to_double(P.y()));
			face_x.push_back(CGAL::to_double(P.z()));
		}
		//check planarity for the current face
		is_planar = check_planarity_on_faces(face_x, fi, pmesh);
		//Contains non_planar_faces as face_index
		if (!is_planar) {
			non_planar_faces.push_back(fi);
		}

		face_vectices.push_back(std::make_pair(fi, pt));
		pt.clear();
	}

	if (non_planar_faces.empty()) {
		std::cout << "------------------ No Planarization Error! ------------------!" << endl;
	}
	else {
		std::cout << "Planarization Error! Further processing required!" << endl;
	}

	//-------TODO------------
	//Healing process should be started from here?
	for (unsigned int i = 0; i < non_planar_faces.size(); i++) {
		//std::cout << "Non Planar faces :  -----> : " << non_planar_faces[i] << endl;
		BOOST_FOREACH(face_index fi2, faces_around_face(pmesh.halfedge(non_planar_faces[i]), pmesh)) {
			if (fi2 != pmesh.null_face()) {
				//std::cout << "Adjacent faces: " << fi2 << std::endl;
				//vertices around that adjacent face
				//BOOST_FOREACH(vertex_index vd, vertices_around_face(polygon_mesh.halfedge(non_planar_faces[i]), polygon_mesh)) {
				//	//std::cout << "Face: " << fi2  << " Vertex: " << polygon_mesh.point(vd) << endl;
				//}			
			}
		}
	}
	//TODO! now next step is to find the adjacent faces of the non-planar faces
}

bool check_planarity_on_faces(vector<double>& linearRingCoords, face_index& fi, Surface_Mesh& pmesh) {
	// True= Planar face; False= Non planar face
	bool is_planar = true;
	//number of non-planar faces
	int plane_no = 0;
	//if condition is partialy implemented
	double threshold = 0.00001; //0.00001 m = 0.01 mm - 13 non planar polygons for building.gml

	////option 2
	//vector<double> non_collinear_points = check_collinearity(linearRingCoords);
	////-todo- Linear least squares plane fitting 
	////first 3 non-collinear points that defines the plane
	//Point_3 non_collinear_point1(non_collinear_points[0], non_collinear_points[1], non_collinear_points[2]);
	//Point_3 non_collinear_point2(non_collinear_points[3], non_collinear_points[4], non_collinear_points[5]);
	//Point_3 non_collinear_point3(non_collinear_points[6], non_collinear_points[7], non_collinear_points[8]);

	////Calculate Plane equation //optional -> normalize the normal vector
	//Plane_3 plane(non_collinear_point1, non_collinear_point2, non_collinear_point3);

	//option 1
	//find least squares plane fitting 
	Plane_3 plane = plane_equation(pmesh, fi); 	//Linear least squares plane fitting part
	//linear_least_squares_fitting_3(pts.begin(), pts.end(), plane, CGAL::Dimension_tag<0>());
	//std::cout << "least square plane fitting() = " << "(" << plane.a() << "," << plane.b() << "," << plane.c() << "," << plane.d() << ")" << endl;

	//Vector_3 normal = plane.orthogonal_vector();
	//std::cout << "(Normal) = " << normal << endl;
	////Plane parameters
	double A, B, C, D;
	A = CGAL::to_double(plane.a());
	B = CGAL::to_double(plane.b());
	C = CGAL::to_double(plane.c());
	D = CGAL::to_double(plane.d());

	vector<vertex_index> non_planar_vertex;
	//make it a new method
	BOOST_FOREACH(vertex_index vi, vertices_around_face(pmesh.halfedge(fi), pmesh)) {
		//Set newPoint and check if it is on the plane
		Point_3 point_of_interest = pmesh.point(vi);

		//squared distance between the point and the fitted plane
		double dist2plane = sqrt(CGAL::to_double(squared_distance(point_of_interest, plane)));
		//std::cout << "the dist value is : " << dist << endl;
		//checkPlanar = AX+BY+CZ+D (Plane equation)
		//double checkPlanar = (A* CGAL::to_double(point_of_interest.x())) + (B*CGAL::to_double(point_of_interest.y())) + (C*CGAL::to_double(point_of_interest.z())) + D;
		//std::cout<<"the checkPlanar value is : "<<checkPlanar<<endl;

		//if checkPlanar = 0 (>D) then it's on the plane otherwise not and threshold is a user defined value
		if (dist2plane > threshold) {
			std::cout << std::setprecision(15) << "the point (" << point_of_interest.x() << " " << point_of_interest.y() << " " << point_of_interest.z() << ") is NOT on the Plane" << endl;
			std::cout << std::setprecision(15) << "distance2plane: " << dist2plane << endl;
			std::cout << "----------------------------------------" << endl;
			plane_no++;
			//if (vi != pmesh.null_vertex()) {
			non_planar_vertices.push_back(vi);
			//}
			//mymap.insert(std::pair<char, int>('a', 100));
			vertex_map.insert(std::pair<vertex_index, Point_3>(vi, point_of_interest));
			//non_planar_vertex
			non_planar_vertex.push_back(vi);
			//a plane can have more than 1 non planar points
			//number of non planar polygons
			if (plane_no == 1) {
				std::cout << " Name of non planar face: " << fi << endl;
				no_of_non_planar_plane++;
				is_planar = false;
			}
		}
		else {
			//std::cout << "++++++++++++++++++++++++++++++++++++++++" << endl;
			//std::cout << std::setprecision(15)<< "the point (" << point_of_interest.x() << " " << point_of_interest.y() << " " << point_of_interest.z() << ") is on the Plane" << endl;
		}
	}

	if (!is_planar) {
		non_planar_face_vectices.push_back(std::make_pair(fi, non_planar_vertex));
		non_planar_vertex.clear();
	}


	return is_planar;
}

//--- old module -check_collinearity- NOT USED -- delete me later--
vector<double> check_collinearity(vector<double>& linearRingCoords) {
	vector<double> non_collinear_points;

	Point_3 X(linearRingCoords[0], linearRingCoords[1], linearRingCoords[2]);
	Point_3 Y(linearRingCoords[3], linearRingCoords[4], linearRingCoords[5]);
	Point_3 Z(linearRingCoords[6], linearRingCoords[7], linearRingCoords[8]);

	if (CGAL::collinear(X, Y, Z)) {
		std::cout << "---------- The points are colliear and can't define the plane -------------" << endl;
		for (unsigned int j = 9; j < linearRingCoords.size();j = j + 3) {
			Point_3 Z(linearRingCoords[j], linearRingCoords[j + 1], linearRingCoords[j + 2]);

			if (CGAL::collinear(X, Y, Z)) {
				//check if it is again co-linear
				std::cout << "choosing another point" << endl;
			}
			else {
				j = (unsigned int)linearRingCoords.size(); //(unsigned int) - due to convertion waring

				non_collinear_points.push_back(CGAL::to_double(X.x()));non_collinear_points.push_back(CGAL::to_double(X.y()));non_collinear_points.push_back(CGAL::to_double(X.z()));
				non_collinear_points.push_back(CGAL::to_double(Y.x()));non_collinear_points.push_back(CGAL::to_double(Y.y()));non_collinear_points.push_back(CGAL::to_double(Y.z()));
				non_collinear_points.push_back(CGAL::to_double(Z.x()));non_collinear_points.push_back(CGAL::to_double(Z.y()));non_collinear_points.push_back(CGAL::to_double(Z.z()));
			}
		}
	}

	else {
		non_collinear_points.push_back(CGAL::to_double(X.x()));non_collinear_points.push_back(CGAL::to_double(X.y()));non_collinear_points.push_back(CGAL::to_double(X.z()));
		non_collinear_points.push_back(CGAL::to_double(Y.x()));non_collinear_points.push_back(CGAL::to_double(Y.y()));non_collinear_points.push_back(CGAL::to_double(Y.z()));
		non_collinear_points.push_back(CGAL::to_double(Z.x()));non_collinear_points.push_back(CGAL::to_double(Z.y()));non_collinear_points.push_back(CGAL::to_double(Z.z()));
	}

	return non_collinear_points;
} //end of check_collinearity function

  //floating point number error in input file
bool floating_point_number_error(Surface_Mesh& pmesh, double& tol) {
	
	bool _floating_point_error = false;
	//duplicate points
	std::vector<Point_3> base_pts, target_pts;
	BOOST_FOREACH(vertex_index vi, pmesh.vertices()) {
		target_pts.push_back(pmesh.point(vi));
	}

//---------------- start -- Approach 01: non_planar points to all points ---------------
	//used when non_planar points to all points
	//std::cout << "mymap contains:\n";
	for (std::map<vertex_index, Point_3>::iterator it = vertex_map.begin(); it != vertex_map.end(); ++it) {
		//std::cout << it->first << " => " << it->second << '\n';
		base_pts.push_back(it->second);
	}

	//approach one//compare non planar vertex with all other vertices
	for (unsigned int i = 0; i < base_pts.size(); i++) {
		
		Point_3 base_point(base_pts[i]);
		for (unsigned int j = 0; j < target_pts.size(); j++) {
			
			if (base_point != target_pts[j]) {
				double dist = sqrt(CGAL::to_double(squared_distance(base_point, target_pts[j])));
				//std::cout << "dist2plane: " << std::setprecision(15) << dist << endl;

				if (dist < tol) {
					_floating_point_error = true;
					dup_points.push_back(std::make_pair(base_point, target_pts[j]));
					std::cout << "Error: duplicate points are present in the data file" << endl;
					std::cout << std::setprecision(15) << "basepoint: " << base_point << " targetpoint: " << target_pts[j] << endl;
					std::cout << "dist2plane: " << std::setprecision(15) << dist << endl;
				}
			}
		}
	}

//---------------- end -- Approach 01: non_planar points to all points ---------------

//---------------- start -- Approach 02: non_planar point to all points from adjacent faces -------------
	//check performance for adjacent faces
	//std::vector<Point_3> adj_face_vertices;
	//BOOST_FOREACH(vertex_index vi, polygon_mesh.vertices()) {
	//	pts.push_back(polygon_mesh.point(vi));
	//}
	//for (unsigned int i = 0; i < faces.size(); i++) {
	//	BOOST_FOREACH(face_index fi, faces_around_face(polygon_mesh.halfedge(faces[i]), polygon_mesh)) {
	//		//std::cout << "Adjacent faces: " << fi << std::endl;
	//		//vertices around that adjacent face
	//		BOOST_FOREACH(vertex_index vd, vertices_around_face(polygon_mesh.halfedge(faces[i]), polygon_mesh)) {
	//			//std::cout << "Face: " << fi2  << " Vertex: " << polygon_mesh.point(vd) << endl;
	//			//to check type1error? computationally expensive
	//			pts.push_back(polygon_mesh.point(vd));
	//		}
	//	}
//---------------- end -- Approach 02: non_planar point to all points from adjacent faces ---------------	

//---------------- start -- Approach 03: each point to all points ---------------	
	//int it1 = 0, it2 = 0;
	//std::cout << "size of vertices: " << target_pts.size() << endl;
	//for (unsigned int i = 0; i < target_pts.size(); i++) {
	//	it1++;

	//	Point_3 base_point(target_pts[i]);
	//	//Point_3 base_point(390840.99690062, 5819160.35802303, 33.7799987792969);
	//	for (unsigned int j = 1; j < target_pts.size(); j++) {
	//		it2++;
	//		if (base_point != target_pts[j]) {
	//			double dist = sqrt(CGAL::to_double(squared_distance(base_point, target_pts[j])));
	//			//std::cout << "dist2plane: " << std::setprecision(15) << dist << endl;

	//			if (dist < tol) {
	//				_floating_point_error = true;
	//				dup_points.push_back(std::make_pair(base_point, target_pts[j]));
	//				std::cout << "Type1 error is present in the data file" << endl;
	//				std::cout << std::setprecision(15) << "basepoint: " << base_point << " targetpoint: " << target_pts[j] << endl;
	//				std::cout << "dist2plane: " << std::setprecision(15) << dist << endl;
	//			}
	//		}
	//	}
	//}
	////algorithm efficiency test //all points or points from adjacest
	//std::cout << "it1: " << it1 << " it2: " << it2 << endl;
//---------------- end -- Approach 03: each point to all points ---------------
	
	
	//remove duplicate entries
	for (unsigned int i = 0; i < dup_points.size(); i++) {
		Point_3 p1(dup_points[i].first);
		Point_3 p2(dup_points[i].second);
		for (unsigned int j = 1; j < dup_points.size();j++) {
			if (p1 == dup_points[j].second && p2 == dup_points[j].first) {
				//std::cout << "erasing points " << dup_points[j].first << " " << dup_points[j].second << endl;
				//erase doubly occured points
				dup_points.erase(dup_points.begin() + j);
			}
		}
	}

	std::cout << "Number of duplicate points: " << dup_points.size() << endl;

	return _floating_point_error;
}

Surface_Mesh repair_floating_point_number_error(Surface_Mesh& pmesh) {
	std::vector<std::pair<Point_3, Point_3>> _erroneous_points;
	bool _is_successful = true;
	std::vector<std::pair<vertex_index, int>> vertex_occurance;
	std::vector<vertex_index> dup_vertex;
	std::vector<Point_3> unique_pts;
	std::vector<vertex_index> point2vertex;

	for (unsigned int i = 0; i < dup_points.size(); i++) {

		BOOST_FOREACH(vertex_index v, pmesh.vertices()) {
			if (dup_points[i].first == pmesh.point(v)) {
				point2vertex.push_back(v);
				std::cout << "duplicate_points first: " << v << " :" << pmesh.point(v) << endl;
			}
			if (dup_points[i].second == pmesh.point(v)) {
				point2vertex.push_back(v);
				std::cout << "duplicate_points second: " << v << " :" << pmesh.point(v) << endl;
			}
		}
	}
	int counter = 0;
	for (unsigned int i = 0; i < point2vertex.size(); i++) {
		std::cout << "vertices around duplicate_points vertex " << point2vertex[i] << std::endl;
		CGAL::Vertex_around_target_circulator<Surface_Mesh> vbegin(pmesh.halfedge(point2vertex[i]), pmesh), done(vbegin);
		do {
			if (*vbegin != pmesh.null_vertex()) {
				counter++;
			}
			std::cout << "adjacent vertex: " << *vbegin++ << endl;
		} while (vbegin != done);

		std::cout << "point2vertex[i]: " << point2vertex[i] << " occurance; " << counter << endl;
		vertex_occurance.push_back(std::make_pair(point2vertex[i], counter));
		counter = 0;
	}

	for (unsigned int i = 0; i < vertex_occurance.size(); i = i + 2) {
		//std::cout << vertex_occurance[i].first << " =>first => second: " << vertex_occurance[i].second << endl;
		if (vertex_occurance[i].second > vertex_occurance[i + 1].second) {
			dup_vertex.push_back(vertex_occurance[i + 1].first);
			std::cout << vertex_occurance[i + 1].first << " is possible duplicate point! Replace it" << endl;
		}
		else if (vertex_occurance[i].second < vertex_occurance[i + 1].second) {
			dup_vertex.push_back(vertex_occurance[i].first);
			std::cout << vertex_occurance[i].first << " is possible duplicate point! Replace it" << endl;
		}
		else if (vertex_occurance[i + 1].second == vertex_occurance[i].second) {
			dup_vertex.push_back(vertex_occurance[i+1].first); //chose any one of them doesn't matter if i or i+1
			std::cout << "same number of occurance! no further processing! or //chose any one of those and replace other one or //find intersection of three planes " << endl;
			//_is_successful = false;
		}
		else {
			std::cout << "difficult to decide! try to planarize the whole face! no further processing here!" << endl;
			_is_successful = false;
		}
	}
	std::vector< std::pair< vertex_index, std::vector<face_index> >> vertex2incident_faces; //row
	if (!dup_vertex.empty()) {
		//vertex2point
		for (unsigned int i = 0; i < dup_vertex.size(); i++) {
			BOOST_FOREACH(vertex_index v, pmesh.vertices()) {
				//if (v != pmesh.null_vertex()) {
					//std::cout << "null vertex \n";
					if (dup_vertex[i] == (v)) {
						for (unsigned int j = 0; j < dup_points.size();j++) {
							if (pmesh.point(v) == dup_points[j].first) {
								std::cout << dup_points[j].first << " is to be replaced by: " << dup_points[j].second << endl;
								//_erroneous_points(erroneous point, right point)
								_erroneous_points.push_back(std::make_pair(dup_points[j].first, dup_points[j].second));
							}
							else if (pmesh.point(v) == dup_points[j].second) {
								std::cout << dup_points[j].second << " is to be replaced by: " << dup_points[j].first << endl;
								_erroneous_points.push_back(std::make_pair(dup_points[j].second, dup_points[j].first));
							}
						}

						std::vector<face_index> fa; //column
						CGAL::Face_around_target_circulator<Surface_Mesh> vbegin(pmesh.halfedge(dup_vertex[i]), pmesh), done(vbegin);
						do {
							//avoid null faces
							if (*vbegin != pmesh.null_face()) {
								std::cout << *vbegin << " - is the incident face " << std::endl;
								fa.push_back(*vbegin);
							}
							*vbegin++;
						} while (vbegin != done);

						vertex2incident_faces.push_back(std::make_pair(dup_vertex[i], fa));
						fa.clear();
					}
				//}
			}
		}
	}
	//replacing part 
	vector<face_index> faces2update;

	for (unsigned int i = 0; i < vertex2incident_faces.size(); i++) {

		std::cout << "vertex2incident_faces first index: " << vertex2incident_faces[i].first << endl;
		//iterate over 2nd pair index <vector>
		for (unsigned int j = 0; j < vertex2incident_faces[i].second.size(); j++) {

			std::cout << "vertex2incident_faces second vectors: " << vertex2incident_faces[i].second[j] << endl;

			for (unsigned int k = 0; k < face_vectices.size(); k++) {
				std::cout << "first index: " << face_vectices[k].first << endl;
				if (vertex2incident_faces[i].second[j] == face_vectices[k].first) {
					for (unsigned int ik = 0; ik < face_vectices[k].second.size(); ik++) {
						std::cout << "second vectors: " << face_vectices[k].second[ik] << endl;

						if (_erroneous_points[i].first == face_vectices[k].second[ik]) {
							//polygon_mesh.remove_vertex(vd);
							//vertex_index v = polygon_mesh.add_vertex(_erroneous_points[i].second);
							face_vectices[k].second.at(ik) = _erroneous_points[i].second;
							faces2update.push_back(face_vectices[k].first);
							//coords.push_back(_erroneous_points[i].second);
							std::cout << "Replacing : " << _erroneous_points[i].first << " by => " << _erroneous_points[i].second << endl;

						}
					}
				}
			}

		}
	}

	Surface_Mesh NewMesh;
	std::map<Point_3, vertex_index> point_to_vertex2;
	std::vector<Point_3> NewPoints;

	for (unsigned int i = 0; i < face_vectices.size(); i++) {
		std::cout << "face_vectices first index: " << face_vectices[i].first << endl;
		//iterate over 2nd pair index <vector>
		//polygon_mesh.remove_face(face2new_vertices[i].first);
		for (unsigned int j = 0; j < face_vectices[i].second.size(); j++) {
			
			//if(face_vectices[i].first!=pmesh.null_face()){
			
			std::cout << "face_vectices second vectors: " << face_vectices[i].second[j] << endl;
			NewPoints.push_back(face_vectices[i].second[j]);


			//}
		}
		//NewPoints.pop_back();
		add_face(NewPoints, NewMesh, point_to_vertex2);
		NewPoints.clear();
	}

	//if more than one duplicate points in one face
	for (unsigned int i = 0; i < faces2update.size(); i++) {
		std::cout << "faces2update: " << faces2update[i] << endl;
		//face_index fi = faces2update[i];
		for (unsigned int j = 1;j < faces2update.size();j++) {
			if (i != j) {
				if (faces2update[i] == faces2update[j]) {
					faces2update.erase(faces2update.begin() + j);
				}
			}
		}
	}

	CityGMLOutput cityGMLWriter;
	//write_selected_linear_rings(face_vectices, faces2update, filepath);
	vector<std::pair<std::string, std::string>> gmlid_linearRings;

	gmlid_linearRings = cityGMLWriter.new_linear_rings(face_vectices, faces2update, GMLID);
	cityGMLWriter.writeToCityGML(origin_building, gmlid_linearRings, filepath);

	write_off_format(NewMesh);

	
	if (_is_successful) {
		std::cout << "Number of repaired points: " << _erroneous_points.size() <<" and no of updated faces: "<< faces2update.size() << endl;
		std::cout << "--------------------All duplicate points are replaced successfully!--------------------" << endl;
	}
	else
		std::cout << "xxxxxxxxxxx -- Error: Repair failed! -- xxxxxxxxxxx" << endl;


	return NewMesh;
}

void data_structure()
{
	//Nef_polyhedron nef(input);

	//std::string sm_file = "C:/dev/CGAL-4.9/build-example/3D_building_model/Inputfiles/mesh-2.off";
	//std::ifstream istrm(sm_file, std::ios::binary);
	//CGAL::OFF_to_nef_3(istrm, nef_1);

	//Polyhedron p;
	//nef_1.convert_to_polyhedron(p);
	//std::cout << p << std::endl;
	std::cout << " --data_structure() method-- " << endl;
	
	
	std::vector<Point_3> pt;
	//test_set 1
	//pt.push_back(Point_3(390842.642310525, 5819139.2217528, 56.9908344201612));
	//pt.push_back(Point_3(390842.463589271, 5819141.51830449, 56.9908344201612));
	//pt.push_back(Point_3(390841.765001736, 5819150.49509156, 56.9908344201612));
	//pt.push_back(Point_3(390838.773829664, 5819150.27698417, 62.9663112147753));


	//test_set 2
	pt.push_back(Point_3(390816.40383755, 5819139.61584261, 33.7799987792969));
	pt.push_back(Point_3(390816.404460692, 5819139.61676499, 62.9663112147753));
	pt.push_back(Point_3(390816.40446077, 5819139.61676511, 62.97));
	pt.push_back(Point_3(390815.723962829, 5819148.59399582, 33.7799987792969));

	
	std::cout << "distance: " << sqrt(CGAL::to_double(squared_distance(pt.at(2), pt.at(3)))) << endl;
	
	
	std::map<Point_3, vertex_index> p2v;
	Surface_Mesh dmesh;

	add_face(pt, dmesh, p2v);
	face_index fi;
	BOOST_FOREACH (face_index ff, dmesh.faces()) {
		fi = ff;
		cout << "faces \n";
	}
	
	
	BOOST_FOREACH(vertex_index v, vertices_around_face(dmesh.halfedge(fi), dmesh)) {
		halfedge_index hi = dmesh.halfedge(v);
		halfedge_index hi_opp = dmesh.opposite(hi);
		vertex_index vi_next = dmesh.target(hi_opp);
		halfedge_index hi_next = dmesh.next(hi);
		halfedge_index hi_prev = dmesh.prev(hi);
		vertex_index vi_prev = dmesh.target(hi_next);
		Point_3 P(dmesh.point(v));
		Point_3 Q(dmesh.point(dmesh.target(hi_opp)));
		Point_3 R(dmesh.point(dmesh.target(hi_next)));
		std::cout << std::setprecision(15) << P << Q << R << endl;
		std::cout << "v: " << v << " vi_next " << vi_next << " vi_prev " << vi_prev << endl;
		std::cout << "hi: " << hi << " Next halfedge " << hi_next << " Prev halfedge " << hi_prev << " Opposite halfedge " << hi_opp << endl;

		if (CGAL::collinear(P, Q, R)) {
			std::cout << "collinear" << endl;
		}

		Plane_3 plane(P, Q, R);
		std::cout << std::setprecision(15) << "plane defining points: " << "(" << P << "," << Q << "," << R << ")" << endl;
		std::cout << "plane fitting() = " << "(" << plane.a() << "," << plane.b() << "," << plane.c() << "," << plane.d() << ")" << endl;
		std::cout << std::setprecision(15) << "dist to P: "<< sqrt(CGAL::to_double(squared_distance(P, plane))) << endl;
		std::cout << std::setprecision(15) << "dist to Q: " << sqrt(CGAL::to_double(squared_distance(Q, plane))) << endl;
		std::cout << std::setprecision(15) << "dist to R: " << sqrt(CGAL::to_double(squared_distance(R, plane))) << endl;
		std::cout << std::setprecision(15) << "dist to : " << sqrt(CGAL::to_double(squared_distance(pt.at(3), plane))) << endl;
		Plane_3 pln;
		double plane_quality = linear_least_squares_fitting_3(pt.begin(), pt.end(), pln, CGAL::Dimension_tag<0>());
		std::cout << "LS plane fitting() = " << "(" << pln.a() << "," << pln.b() << "," << pln.c() << "," << pln.d() << ")" << "(1=Best fit,0=NOT best) : " << plane_quality << endl;
		std::cout << std::setprecision(15) << "LS dist to P: " << sqrt(CGAL::to_double(squared_distance(P, pln))) << endl;
		std::cout << std::setprecision(15) << "LS dist to Q: " << sqrt(CGAL::to_double(squared_distance(Q, pln))) << endl;
		std::cout << std::setprecision(15) << "LS dist to R: " << sqrt(CGAL::to_double(squared_distance(R, pln))) << endl;
		std::cout << std::setprecision(15) << "dist to : " << sqrt(CGAL::to_double(squared_distance(pt.at(3), pln))) << endl;


	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	Surface_Mesh mesh;

	//SM_to_Nef SmToNef;
	//Nef_polyhedron n = SmToNef.get_Nef();

	vertex_index u1 = mesh.add_vertex(Kernel::Point_3(0, 0, 0));
	vertex_index v1 = mesh.add_vertex(Kernel::Point_3(1, 0, 0));
	vertex_index w1 = mesh.add_vertex(Kernel::Point_3(1, 1, 0));
	vertex_index x1 = mesh.add_vertex(Kernel::Point_3(0, 1, 0));
	vertex_index z1 = mesh.add_vertex(Kernel::Point_3(0.5, 0.5, 0));

	face_index face1 = mesh.add_face(u1, v1, w1, x1);
	//face_index face2 = mesh.add_face(x1, z1, w1);
	//face_index face3 = mesh.add_face(x1, u1, z1);
	//face_index face4 = mesh.add_face(u1, v1, z1);


	  
	BOOST_FOREACH(vertex_index v, vertices_around_face(mesh.halfedge(face1), mesh)) {
		halfedge_index hi = mesh.halfedge(v);
		halfedge_index hi_opp = mesh.opposite(hi);
		halfedge_index hi_nnext = mesh.next(hi_opp);
		vertex_index vi_nnext = mesh.target(hi_nnext);
		halfedge_index hi_nnnext = mesh.next(hi_nnext);
		vertex_index vi_nnnext = mesh.target(hi_nnnext);
		vertex_index vi_next = mesh.target(hi_opp);
		halfedge_index hi_next = mesh.next(hi);
		halfedge_index hi_prev = mesh.prev(hi);
		vertex_index vi_prev = mesh.target(hi_next);
		Point_3 P(mesh.point(v));
		Point_3 Q(mesh.point(mesh.target(hi_opp)));
		Point_3 R(mesh.point(mesh.target(hi_nnext)));
		Point_3 Next_vertex(mesh.point(mesh.target(hi_nnnext)));
		std::cout << P << Q << R << Next_vertex << endl;
		std::cout << "v: " << v << " vi_next " << vi_next<<" vi_nnext "<< vi_nnext << " vi_nnnext "<< vi_nnnext << endl;
		std::cout << "hi: "<<hi <<" Next halfedge "<<hi_next<<" Prev halfedge "<< hi_prev << " Opposite halfedge " << hi_opp << endl;
		

		if (CGAL::collinear(P, Q, R)) {
			std::cout << "collinear" << endl;
		}
			
	}

	{
		std::cout << "vertices around vertex " << v1 << std::endl;
		CGAL::Vertex_around_target_circulator<Surface_Mesh> vbegin(mesh.halfedge(v1), mesh), done(vbegin);
		do {
			std::cout << *vbegin++ << std::endl;
		} while (vbegin != done);
	}

	BOOST_FOREACH(vertex_index vd, vertices_around_target(mesh.halfedge(face1), mesh)) {
		std::cout << "Face: " << face1 << " Vertex: " << vd << " " << mesh.point(vd) << endl;
	}
	
	//Surface_Mesh sm;
	//std::string sm_file = "C:/dev/CGAL-4.10/build-examples/3D_buildings/Inputfiles/box.off";
	//std::ifstream istrm(sm_file, std::ios::binary);
	//istrm >> sm;

	//vector<vertex_index> sm_vertex;
	//BOOST_FOREACH(vertex_index v, sm.vertices()) {
	//	sm_vertex.push_back(v);
	//}

	//vector<face_index> sm_faces;
	//BOOST_FOREACH(face_index fi, sm.faces()) {
	//	sm_faces.push_back(fi);
	//}

	//for (unsigned int i = 0; i < sm_vertex.size();i++) {
	//	{
	//		std::cout << "vertices around vertex " << sm_vertex[i] << sm.point(sm_vertex[i]) << std::endl;
	//		CGAL::Vertex_around_target_circulator<Surface_Mesh> vbegin(sm.halfedge(sm_vertex[i]), sm), done(vbegin);
	//		do {
	//			std::cout << *vbegin++ << sm.point(*vbegin++) << std::endl;
	//		} while (vbegin != done);
	//	}
	//}
	//std::cout << sm.number_of_faces() << endl;


}

void write_off_format(Surface_Mesh& m) {
	std::string fileName = filepath + "sm.off";
	std::ofstream out(fileName);
	out << std::setprecision(15) << m;
	out.close();
}

Plane_3 plane_equation(Surface_Mesh& mesh, face_index& fi) {
	//Linear least squares plane fitting part
	vector<Point_3> points, pts; //later replace this with face_x 
	//Plane_3 plane;
	BOOST_FOREACH(vertex_index vi, vertices_around_face(mesh.halfedge(fi), mesh)) {
		pts.push_back(mesh.point(vi));
	}
	
	if (pts.size() < 3) {
		std::cout << "Error: Linear ring contains less than three points" << endl;
	}

	//if(pts.size()==3){
	//to use first three points to define the plane
	for (int i = 0; i < 1;i++) {
		Point_3 X(pts[i].x(), pts[i].y(), pts[i].z());
		Point_3 Y(pts[i+1].x(), pts[i+1].y(), pts[i+1].z());
		Point_3 Z(pts[i+2].x(), pts[i+2].y(), pts[i+2].z());
		if (CGAL::collinear(X, Y, Z)) {

			Point_3 Z (pts[i + 3].x(), pts[i + 3].y(), pts[i + 3].z());
			if (CGAL::collinear(X, Y, Z)) {
				//check if it is again co-linear
				std::cout << " collinear --- choose another point" << endl;
			}
			else
			{
				points.push_back(X);points.push_back(Y);points.push_back(Z);
			} 
		}
		else {
			points.push_back(X);points.push_back(Y);points.push_back(Z);
		}
	}

	//------------------- first 3 non collinear points defines the plane------------------------------
	Plane_3 plane(points.at(0), points.at(1), points.at(2));
	std::cout << std::setprecision(15) <<"plane defining points: "<< "(" << points.at(0) << "," << points.at(1) << "," << points.at(2) << ")" << endl;
	//plane = pln;
	std::cout << "plane fitting() = " << "(" << plane.a() << "," << plane.b() << "," << plane.c() << "," << plane.d() << ")"<< endl;

	//}
	//else {

		//double plane_quality = linear_least_squares_fitting_3(pts.begin(), pts.end(), plane, CGAL::Dimension_tag<0>());
		//std::cout << "plane fitting() = " << "(" << plane.a() << "," << plane.b() << "," << plane.c() << "," << plane.d() << ")" << "(1=Best fit,0=NOT best) : " << plane_quality << endl;
	//}
	
	//------------------- linear least squares plane fitting -------------------------
	
	return plane;
}

bool is_collinear(Point_3& P, Point_3& Q, Point_3& R) {
	//Linear least squares plane fitting part
	vector<Point_3> pts; //later replace this with face_x 
	bool collinear;

	//if(pts.size()==3){
	//to use first three points to define the plane
	if (CGAL::collinear(P, Q, R)) {
				//check if it is again co-linear
		std::cout << " collinear --- choose another point" << endl;
		collinear = true;
	}
	else
	{
		collinear = false;
	}
	

	//------------------- first 3 non collinear points defines the plane------------------------------
	//Plane_3 plane(points.at(0), points.at(1), points.at(2));
	

	return collinear;
}

void healing(Surface_Mesh& mesh2) {

	Surface_Mesh mesh;
	face_index face;
	std::map<Point_3, vertex_index> point_to_vertex;
	std::vector<Point_3> points;
	std::vector<Point_3> rest_points;
	std::vector<Point_3> new_face;
	std::vector<std::vector<Point_3>> new_faces;
	double tol = 0.00001;
	
	//to find the gmlid
	for (unsigned int i = 0; i < face_gmlids_rings.size(); i++) {
		if (non_planar_face_vectices[0].first == face_gmlids_rings[i].first) {
			std::cout << "face_index: " << face_gmlids_rings[i].first << endl;
			std::cout << "gml id: " << face_gmlids_rings[i].second.first << endl;
			std::cout << "LinearRing Points: " << face_gmlids_rings[i].second.second.size() << endl; //as it is in input file
			for (unsigned int j = 0; j < face_gmlids_rings[i].second.second.size(); j++) {
				std::cout << face_gmlids_rings[i].second.second[j] << "\n";
			}
		}
	}

	for (unsigned int i = 0; i < 1; i++) {
		BOOST_FOREACH(vertex_index v, vertices_around_face(mesh2.halfedge(non_planar_face_vectices[0].first), mesh2)) {
			points.push_back(mesh2.point(v));
		}

		add_face(points, mesh, point_to_vertex);
	}

	unsigned int last_point_index = 0;
	bool first_iteration = true;
	for (unsigned int i = 0; i < points.size(); i = i+3) {
		
		if (first_iteration && last_point_index < (points.size()-1)) {

			Point_3 P(points.at(0));
			Point_3 Q(points[i + 1]);
			Point_3 R(points[i + 2]);

			Plane_3 plane(P, Q, R);

			if (CGAL::collinear(P, Q, R)) {
				std::cout << "the points are collinear \n";
			}
			std::cout << std::setprecision(15) << "plane fitting() = " << "(" << plane.a() << "," << plane.b() << "," << plane.c() << "," << plane.d() << ")" << endl;

			//Plane_3 plane2;
			//double quality = linear_least_squares_fitting_3(pts.begin(), pts.end(), plane2, CGAL::Dimension_tag<0>());
			//std::cout << "least square plane fitting() = " << "(" << plane2.a() << "," << plane2.b() << "," << plane2.c() << "," << plane2.d() << ")" << "fitting quality: " << quality << endl;

			//new_face.push_back(P);new_face.push_back(Q);new_face.push_back(R);

			double dist2plane = sqrt(CGAL::to_double(squared_distance(points[i + 3], plane)));
			std::cout << std::setprecision(15) << "dist2plane: " << dist2plane << endl;
			//double dist2plane2 = sqrt(CGAL::to_double(squared_distance(points[j], plane2)));
			//std::cout << "dist2plane2: " << dist2plane2 << endl;

			last_point_index = i + 3;

			if (dist2plane < tol) {
				//std::cout << "points in plane: " << points[j] << endl;
				//points.erase(points.begin() + j);
				new_face.push_back(P);
				new_face.push_back(Q);
				new_face.push_back(R);
				new_face.push_back(points[i + 3]);

				for (unsigned int j = 3; j < points.size() - 3; j++) {
					double d2p = sqrt(CGAL::to_double(squared_distance(points[j + 1], plane)));
					if (d2p < tol) {
						std::cout << std::setprecision(15) << "d2p: " << d2p << endl;
						new_face.push_back(points[j + 1]);
						last_point_index = j + 1;
					}
					else {
						j = (unsigned int)points.size() - 3; //(unsigned int) - due to convertion waring
					}
				}
				
				new_faces.push_back(new_face);
				new_face.clear();
			}
			else {
				new_face.push_back(P);
				new_face.push_back(Q);
				new_face.push_back(R);

				new_faces.push_back(new_face);
				new_face.clear();


				for (unsigned int k = last_point_index; k < points.size()- last_point_index; k++) {
					Point_3 P2(points.at(0));
					Point_3 Q2(points[last_point_index-1]);
					Point_3 R2(points[last_point_index]);
					Plane_3 plane2(P2, Q2, R2);
					
					double d2p2 = sqrt(CGAL::to_double(squared_distance(points[k + 1], plane2)));
					if (d2p2 < tol) {
						std::cout << std::setprecision(15) << "d2p: " << d2p2 << endl;
						new_face.push_back(points[k + 1]);
					}
					else {
						k = (unsigned int)points.size(); //(unsigned int) - due to convertion waring
					}
				}

				new_faces.push_back(new_face);
				new_face.clear();
			}


			for (unsigned int k = 0; k < new_face.size(); k++) {
				std::cout << "new_face_points: " << new_face[k] << endl;
			}

			first_iteration = false;

		} //end of first iteration loop



		if (!first_iteration && last_point_index < (points.size() - 1)) {

			Point_3 P(points.at(0));
			Point_3 Q(points[last_point_index-1]);
			Point_3 R(points[last_point_index]);


			Plane_3 plane(P, Q, R);

			if (CGAL::collinear(P, Q, R)) {
				std::cout << "the points are collinear \n";
			}
			std::cout << std::setprecision(15) << "plane fitting() = " << "(" << plane.a() << "," << plane.b() << "," << plane.c() << "," << plane.d() << ")" << endl;

			//Plane_3 plane2;
			//double quality = linear_least_squares_fitting_3(pts.begin(), pts.end(), plane2, CGAL::Dimension_tag<0>());
			//std::cout << "least square plane fitting() = " << "(" << plane2.a() << "," << plane2.b() << "," << plane2.c() << "," << plane2.d() << ")" << "fitting quality: " << quality << endl;

			//new_face.push_back(P);new_face.push_back(Q);new_face.push_back(R);

			double dist2plane = sqrt(CGAL::to_double(squared_distance(points[last_point_index + 1], plane)));
			std::cout << std::setprecision(15) << "dist2plane: " << dist2plane << endl;
			//double dist2plane2 = sqrt(CGAL::to_double(squared_distance(points[j], plane2)));
			//std::cout << "dist2plane2: " << dist2plane2 << endl;

			last_point_index = last_point_index + 1;

			if (dist2plane < tol) {
				//std::cout << "points in plane: " << points[j] << endl;
				//points.erase(points.begin() + j);
				new_face.push_back(P);
				new_face.push_back(Q);
				new_face.push_back(R);
				new_face.push_back(points[last_point_index]);

				for (unsigned int j = last_point_index; j < points.size(); j++) {
					double d2p = sqrt(CGAL::to_double(squared_distance(points[j + 1], plane)));
					if (d2p < tol) {
						std::cout << std::setprecision(15) << "d2p: " << d2p << endl;
						new_face.push_back(points[j + 1]);
						last_point_index = j + 1;
					}
					else {
						j = (unsigned int)points.size() - 3; //(unsigned int) - due to convertion waring
					}
				}

			}
			else {
				new_face.push_back(P);
				new_face.push_back(Q);
				new_face.push_back(R);

				for (unsigned int k = last_point_index; k < points.size(); k++) {
					Point_3 P2(points[last_point_index - 2]);
					Point_3 Q2(points[last_point_index - 1]);
					Point_3 R2(points[last_point_index]);
					Plane_3 plane2(P2, Q2, R2);

					double d2p2 = sqrt(CGAL::to_double(squared_distance(points[k + 1], plane2)));
					if (d2p2 < tol) {
						std::cout << std::setprecision(15) << "d2p: " << d2p2 << endl;
						new_face.push_back(points[k + 1]);
					}
					else {
						k = (unsigned int)points.size(); //(unsigned int) - due to convertion waring
					}
				}
			}

			new_faces.push_back(new_face);

			for (unsigned int k = 0; k < new_face.size(); k++) {
				std::cout << "new_face_points: " << new_face[k] << endl;
			}
			new_face.clear();
		}


































	} //end of the points i for loop



	Surface_Mesh repaired_mesh;
	std::map<Point_3, vertex_index> point2vertex;
	for (unsigned int i = 0; i < new_faces.size();i++) {
		std::cout << "faces:" << i << " Vertices: " << endl;
		for (unsigned int j = 0; j < new_faces[i].size(); j++) {
			std::cout << new_faces[i][j] << endl;
		}
		std::cout << "\n";
		add_face(new_faces[i], repaired_mesh, point2vertex);
	}

	check_planarization_error(repaired_mesh);
















	BOOST_FOREACH(face, mesh.faces()) {
		BOOST_FOREACH(vertex_index v, vertices_around_face(mesh.halfedge(face), mesh)) {

			if (first_iteration){
				halfedge_index hi = mesh.halfedge(v);
				halfedge_index hi_opp = mesh.opposite(hi);
				halfedge_index hi_nnext = mesh.next(hi_opp);
				vertex_index vi_nnext = mesh.target(hi_nnext);
				halfedge_index hi_nnnext = mesh.next(hi_nnext);
				vertex_index vi_nnnext = mesh.target(hi_nnnext);
				vertex_index vi_next = mesh.target(hi_opp);
				halfedge_index hi_next = mesh.next(hi);
				halfedge_index hi_prev = mesh.prev(hi);
				vertex_index vi_prev = mesh.target(hi_next);
				Point_3 P(mesh.point(v));
				Point_3 Q(mesh.point(mesh.target(hi_opp)));
				Point_3 R(mesh.point(mesh.target(hi_nnext)));
				Point_3 Next_vertex(mesh.point(mesh.target(hi_nnnext)));
				std::cout << P << Q << R << Next_vertex << endl;
				std::cout << "v: " << v << " vi_next " << vi_next << " vi_nnext " << vi_nnext << " vi_nnnext " << vi_nnnext << endl;

				//find least squares plane fitting 
				Plane_3 plane(P, Q, R); 
				if (CGAL::collinear(P, Q, R)) {
					std::cout << "the points are collinear \n";
				}
				std::cout << std::setprecision(15) << "plane fitting() = " << "(" << plane.a() << "," << plane.b() << "," << plane.c() << "," << plane.d() << ")" << endl;
				
				//Plane_3 plane2;
				//double quality = linear_least_squares_fitting_3(pts.begin(), pts.end(), plane2, CGAL::Dimension_tag<0>());
				//std::cout << "least square plane fitting() = " << "(" << plane2.a() << "," << plane2.b() << "," << plane2.c() << "," << plane2.d() << ")" << "fitting quality: " << quality << endl;
			
				//new_face.push_back(P);new_face.push_back(Q);new_face.push_back(R);
				
				double dist2plane = sqrt(CGAL::to_double(squared_distance(Next_vertex, plane)));
				std::cout << std::setprecision(15) << "dist2plane: " << dist2plane << endl;
				//double dist2plane2 = sqrt(CGAL::to_double(squared_distance(points[j], plane2)));
				//std::cout << "dist2plane2: " << dist2plane2 << endl;

				new_face.push_back(P);
				new_face.push_back(Q);
				new_face.push_back(R);

				if (dist2plane < tol) {
					//std::cout << "points in plane: " << points[j] << endl;
					new_face.push_back(Next_vertex);
					//points.erase(points.begin() + j);
				}
				else {
					
				}

				new_faces.push_back(new_face);
				
				for (unsigned int k = 0; k < new_face.size(); k++) {
					std::cout << "new_face_points: " << new_face[k] << endl;
				}
				new_face.clear();
				first_iteration = false;
			}	
		}
	}




	
   

	for (unsigned int i = 0; i < non_planar_face_vectices.size(); i++) {
		std::cout << "Non planar faces: " << non_planar_face_vectices[i].first << endl;
		std::cout << "Non planar vertices: ";
		for (unsigned int j = 0; j < non_planar_face_vectices[i].second.size(); j++) {
			std::cout << non_planar_face_vectices[i].second[j] << ", ";
		}
		std::cout << "\n";
	}



	//Test if the a point is incident to exactly three faces 
	//precondition to find intersection of three plane
	int no_of_incident_vertices; 
	bool count = true;
	bool has_3incident_face = false;
	for (unsigned int i = 0; i < non_planar_vertices.size(); i++) {

		std::cout << "vertices around vertex " << non_planar_vertices[i] << " --  " << mesh.point(non_planar_vertices[i]) << std::endl;
		CGAL::Vertex_around_target_circulator<Surface_Mesh> vbegin(mesh.halfedge(non_planar_vertices[i]), mesh), done(vbegin);
		no_of_incident_vertices = 0;
		do {
			if(*vbegin!=mesh.null_vertex()){
			std::cout << *vbegin++ << " ---- " << mesh.point(*vbegin++) << std::endl;
			no_of_incident_vertices++;
			}
		} while (vbegin != done);

		if (no_of_incident_vertices == 3) {
			if (count) {
				has_3incident_face = true;
			}

		}
		else {
			has_3incident_face = false;
			count = false;
		}
	}

	if (has_3incident_face) {
		std::cout << "+++++ Each non planar vertex is incident to exactly three faces +++++" << endl;


		//adjacent faces to a vertex 
		for (unsigned int k = 0; k < non_planar_vertices.size(); k++) {
			vector<face_index> adj_faces;
			vector<Plane_3> adj_planes;
			std::cout << "faces around vertex " << non_planar_vertices[k] << " --  " << mesh.point(non_planar_vertices[k]) << std::endl;
			CGAL::Face_around_target_circulator<Surface_Mesh> vbegin(mesh.halfedge(non_planar_vertices[k]), mesh), done(vbegin);
			do {
				//std::cout << *vbegin++ << " ---- " << std::endl;
				adj_faces.push_back(*vbegin++);
				adj_planes.push_back(plane_equation(mesh, *vbegin++));

			} while (vbegin != done);

			// find intersection point of three planes
			//std::cout << "size of adj_faces " << adj_faces.size() << endl;
			//std::cout << "size of adj_planes " << adj_planes.size() << endl;
			for (unsigned int i = 0; i < adj_planes.size(); i = i + 3) {
				Vector_3 n1, n2, n3;
				n1 = adj_planes[i].orthogonal_vector();
				n2 = adj_planes[i + 1].orthogonal_vector();
				n3 = adj_planes[i + 2].orthogonal_vector();
				double d1, d2, d3;
				d1 = -(adj_planes[i].d()); //d = -D 
				d2 = -(adj_planes[i + 1].d());
				d3 = -(adj_planes[i + 2].d());
				Vector_3 cross_n23 = CGAL::cross_product(n2, n3);
				Vector_3 cross_n31 = CGAL::cross_product(n3, n1);
				Vector_3 cross_n12 = CGAL::cross_product(n1, n2);
				Vector_3 mul1 = (d1 * cross_n23);
				Vector_3 mul2 = (d2 * cross_n31);
				Vector_3 mul3 = (d3 * cross_n12);
				Vector_3 part1 = (mul1 + mul2 + mul3);
				double dot_n23 = CGAL::to_double(CGAL::scalar_product(n1, cross_n23));
				if (dot_n23 == 0) {
					std::cout << "Two planes are parallel!! Determinant is zero! => n1.(n2*n3)!=0)" << endl;
				}
				else
				{
					Vector_3 pt = (part1 / dot_n23);
					std::cout << mesh.point(non_planar_vertices[k])<<" can be replaced by intersection point: " << pt << endl;
					intersection_points.insert(std::pair<vertex_index, Vector_3>(non_planar_vertices[k], pt));
				}

			}

		}
	}
	else
	{
		std::cout << "----- Warning: Non planar vertex is NOT incident to exactly three faces. ----" << endl;
	}


	// showing contents of a std::map
	//std::cout << "mymap contains:\n";
	//for (std::map<vertex_index, Vector_3>::iterator it = intersection_points.begin(); it != intersection_points.end(); ++it) {
	//	std::cout << it->first << " => " << it->second << '\n';
	//}


	//for (unsigned int i = 0; i < faces.size(); i++) {
	//	BOOST_FOREACH(face_index fi, faces_around_face(polygon_mesh.halfedge(faces[i]), polygon_mesh)) {
	//		//std::cout << "Adjacent faces: " << fi << std::endl;
	//		//vertices around that adjacent face
	//		BOOST_FOREACH(vertex_index vd, vertices_around_target(polygon_mesh.halfedge(faces[i]), polygon_mesh)) {
	//			std::cout << "Face: " << faces[i] << " Vertex: " << polygon_mesh.point(vd) << endl;
	//		}
	//	}
	//	//check if there is any duplicate points 
	//	//decision pts or pts2? //pts - Andreas - adjacent faces 
	//	//bool error2 = floating_point_number_error(tolerence, pts2); //decision pts or pts2? // less iteratin
	//	//if (error1)												
	//	//	std::cout << "error type 1 is present? " << error1 << endl;
	//}

}

void polyhedron_check() {
	//from .OFF surface mesh 
	Polyhedron P;
	std::string fileName = filepath + "sm.off";
	std::ifstream in(fileName);
	in >> P;

	//CGAL_precondition(P.is_valid());
	//std::cout << P << endl;
	std::cout << "no of faces: " << P.size_of_facets() << endl;
	std::cout << "no of vertices: " << P.size_of_vertices() << endl;
	if (P.is_closed()) {
		std::cout << "Polyhedron is closed" << endl;
	}
	if (P.is_valid()) {
		std::cout << "Polyhedron is valid" << endl;
	}
	if (P.is_pure_trivalent()) {
		std::cout << "all vertices have exactly three incident edges" << endl;
	}
	if (P.is_pure_bivalent()) {
		std::cout << "all vertices have exactly two incident edges" << endl;
	}
	if (P.is_pure_triangle()) {
		std::cout << "all facets are triangles" << endl;
	}

	//CGAL_postcondition(P.is_valid());
}
//unused codes
//-----------------------------------------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------------------------------------