/*
*  main.cpp
*  Created on: Jan 2, 2017
*  Author: Md. Nazmul Hossan
*  Description: This is the main program that contains main() function. 
*/
#define _CRT_SECURE_NO_WARNINGS
#define _SCL_SECURE_NO_WARNINGS //to avoid some VS 2014 compiler warning for CGAL transform parameter

// headers
#include "definitions.h"
#include "Building.h"
#include "LinearRing.h"
//#include"SM_to_Nef.cpp"
//#include "Print.h"


//global variables
Surface_Mesh polygon_mesh, pm;
std::vector<vertex_index> non_planar_vertices;
std::map<vertex_index, Point_3> vertex_map;
std::map<vertex_index, Vector_3> intersection_points;
std::vector<Point_3> all_vertices;
std::map<Point_3, vertex_index> point_to_vertex; //a std::map that contains unique points (no duplicate points)
std::vector<face_index> faces;
int no_of_non_planar_plane = 0;
int face_count=0;

//functions
void add_face(vector<double>  &linearRingCoords);
void check_faces();
vector<double> check_collinearity(vector<double> &linearRingCoords);
bool check_planarity(vector<double> &linear_rings, face_index &fi);
bool error_type1(double &tol, vector<Point_3> &pts);
void data_structure();
void write_off_format(Surface_Mesh &m);
Plane_3 plane_equation(Surface_Mesh &mesh, face_index &fi);
void healing(Surface_Mesh &mesh);
void polyhedron_check();
void cgal2citygml(vector<string> &rings, vector<string> &gml_id);


using namespace std;

int main()
{
	//use of CGAL templetes
	//data_structure();
	//CityGML to Mesh structure
	Building building;
	LinearRing LR;
	vector<double> linearRingCoords;
	//getGeometries.readGMLFile("C:/dev/CGAL-4.9/build-example/3D_building_model/Inputfiles/building.gml");
	//getGeometries.readGMLFile("C:/dev/CGAL-4.9/build-example/3D_building_model/Inputfiles/Museumsinsel - Plus2.gml"); //90336 vertices with FME and 12707 vertices with CGAL
	//getGeometries.readGMLFile("C:/dev/CGAL-4.9/build-example/3D_building_model/Inputfiles/test_data_set_1_collinearity_test.gml");
	//getGeometries.readGMLFile("C:/dev/CGAL-4.9/build-example/3D_building_model/Inputfiles/test_data_set_1.gml");
	building.readGMLFile("C:/dev/CGAL-4.9/build-example/3D_building_model/Inputfiles/test_data_set_1_1np_v2.gml");

	vector<string> Geom = building.buildingGeom;
	vector<string> L_rings = LR.getLinearRing(Geom);
	vector<string> GMLID;

	int iterate = 1;
		
	cout << "size of LR_Rings: " << L_rings.size() << endl;
	cout << "size of Polygon GML_ID: " << LR.PolygonID.size() << endl;
	cout << "size of LinearRing GML_ID: " << LR.LinearRingID.size() << endl;
	if (L_rings.size() == LR.PolygonID.size()) {
		GMLID = LR.PolygonID;
		}
	else {
		GMLID = LR.LinearRingID;
	}

	//for (unsigned int index = 0; index < L_rings.size(); index++) {
	//	cout << L_rings[index] << endl;
	//}
		//iterate through Vector<string> _surfaces and its Linear_Rings 
		for (unsigned int index = 0; index < L_rings.size(); index++) {
			linearRingCoords = LR.getRingCoords(L_rings, index);
			//cout << ">>>>>>>>>>>>Processing Linear Ring <<<<<<<<<<<<<<< : " << face_count << endl;
			add_face(linearRingCoords);
			face_count++;
			//check_planarity(linearRingCoords);
			//cout << ">>>>>>>>>>>>Processing GMLID <<<<<<<<<<<<<<< : " << GMLID [index] << endl;
		}
		//LR.Writevectors();

		//iterate = iterate + 1;
	//} //end of while loop
	write_off_format(polygon_mesh);
	//iterates through all faces of the Surface_Mesh
	check_faces();

	cout << no_of_non_planar_plane <<" - NON-PLANER polygons and ("<< non_planar_vertices.size() << ") non planar vertices has been found" << endl;
	cout << "+++ Total "<< polygon_mesh.number_of_faces() <<" Faces ++++ " << endl;
	cout << "Total number of Vertices: " << polygon_mesh.number_of_vertices() << endl;
	cout << "Number of HalfEdges: " << polygon_mesh.number_of_halfedges() << endl;


	healing(polygon_mesh);

	
	//Nef_polyhedron nef(polygon_mesh);
	//cout << nef << endl;

	//for (std::map<Kernel::Point_3, Surface_Mesh::Vertex_index>::iterator it = point_to_vertex.begin();
	//	it != point_to_vertex.end();it++) {
	//	//cout << "content of point_to_vertex " << it->first << " and " << it->second << endl;
	//}

	cout << ">>>>>>>>>>>> Press any key to exit <<<<<<<<<<<<<<<  " << endl;
	std::getchar();
	return 0;
}

//--------------All member functions starts from here ------------------------------
void add_face(vector<double> &linearRingCoords)
{
	//std::map<Kernel::Point_3, Surface_Mesh::Vertex_index> point_to_vertex2;
	std::vector<Surface_Mesh::Vertex_index> vertices;

	for (unsigned int i = 0; i<linearRingCoords.size(); i = i + 3)
	{
		Point_3 p(linearRingCoords[i], linearRingCoords[i + 1], linearRingCoords[i + 2]);

		std::pair<std::map<Kernel::Point_3, Surface_Mesh::Vertex_index>::iterator, bool> insert_res
			= point_to_vertex.insert(std::make_pair(p, Surface_Mesh::Vertex_index()));
		if (insert_res.second){
			//all_vertices.push_back(p);
			insert_res.first->second = polygon_mesh.add_vertex(p);
		}
			vertices.push_back(insert_res.first->second);


	}
	if (vertices.front() == vertices.back()) 
		vertices.pop_back();
		//insert by range
		polygon_mesh.add_face(vertices);

	//faces.push_back(fi);

	//BOOST_FOREACH(vertex_index vd, polygon_mesh.vertices()) {
	//	std::cout << "iteratining through vertex index: " << vd << std::endl;
	//}
}

void check_faces()
{
	// True= Planar face; False= Non planar face
	bool is_planar;
	vector<face_index> non_planar_faces;
	BOOST_FOREACH(face_index fi, polygon_mesh.faces()) {
		std::cout << "Checking faces: " << fi << std::endl;
		//vector of face indexes
		faces.push_back(fi);

		std::vector<double> face_x; //contains coordinates of all vertices around a particular face
									//iterate through vertices around face
		BOOST_FOREACH(vertex_index vi, vertices_around_face(polygon_mesh.halfedge(fi), polygon_mesh)) {
			//std::cout << " Vertices: " << vi << " Coords: " << std::setprecision(15) << polygon_mesh.point(vi) << endl;
			Point_3 P = polygon_mesh.point(vi);
			//convertion bcoz of Exact_predicates_exact_constructions_kernel
			face_x.push_back(CGAL::to_double(P.x()));
			face_x.push_back(CGAL::to_double(P.y()));
			face_x.push_back(CGAL::to_double(P.z()));
		}
		//check planarity for the current face
		is_planar = check_planarity(face_x, fi);
		//Contains non_planar_faces as face_index
		if (!is_planar) {
			non_planar_faces.push_back(fi);
		}
	}

	//-------TODO------------
	//Healing process should be started from here?
	for (unsigned int i = 0; i < non_planar_faces.size(); i++) {
		//cout << "Non Planar faces :  -----> : " << non_planar_faces[i] << endl;
		BOOST_FOREACH(face_index fi2, faces_around_face(polygon_mesh.halfedge(non_planar_faces[i]), polygon_mesh)) {
			if (fi2 != polygon_mesh.null_face()) {
				//std::cout << "Adjacent faces: " << fi2 << std::endl;
				//vertices around that adjacent face
				//BOOST_FOREACH(vertex_index vd, vertices_around_face(polygon_mesh.halfedge(non_planar_faces[i]), polygon_mesh)) {
				//	//std::cout << "Face: " << fi2  << " Vertex: " << polygon_mesh.point(vd) << endl;
				//}			
			}
		}
	}
	//TODO! now next step is to find the adjacent faces of the non-planar faces
}

bool check_planarity(vector<double> &linearRingCoords, face_index& fi) {
	// True= Planar face; False= Non planar face
	bool is_planar = true;
	//number of non-planar faces
	int plane_no = 0;
	//if condition is partialy implemented
	double tolerance = 0.00001; //0.00001 m= 0.01 mm - 13 non planar polygons for building.gml

	////option 2
	//vector<double> non_collinear_points = check_collinearity(linearRingCoords);
	////-todo- Linear least squares plane fitting 
	////first 3 non-collinear points that defines the plane
	//Point_3 non_collinear_point1(non_collinear_points[0], non_collinear_points[1], non_collinear_points[2]);
	//Point_3 non_collinear_point2(non_collinear_points[3], non_collinear_points[4], non_collinear_points[5]);
	//Point_3 non_collinear_point3(non_collinear_points[6], non_collinear_points[7], non_collinear_points[8]);

	////Calculate Plane equation //optional -> normalize the normal vector
	//Plane_3 plane(non_collinear_point1, non_collinear_point2, non_collinear_point3);
	
	//option 1
	//find least squares plane fitting 
	Plane_3 plane = plane_equation(polygon_mesh, fi); 	//Linear least squares plane fitting part
	//linear_least_squares_fitting_3(pts.begin(), pts.end(), plane, CGAL::Dimension_tag<0>());
	//std::cout << "least square plane fitting() = " << "(" << plane.a() << "," << plane.b() << "," << plane.c() << "," << plane.d() << ")" << endl;

	//Vector_3 normal = plane.orthogonal_vector();
	//std::cout << "(Normal) = " << normal << endl;
	////Plane parameters
	double A, B, C, D;
	A = CGAL::to_double(plane.a());
	B = CGAL::to_double(plane.b());
	C = CGAL::to_double(plane.c());
	D = CGAL::to_double(plane.d());

	//make it a new method
	BOOST_FOREACH(vertex_index vi, vertices_around_face(polygon_mesh.halfedge(fi), polygon_mesh)) {
		//Set newPoint and check if it is on the plane
		Point_3 point_of_interest = polygon_mesh.point(vi);
		
		//squared distance between the point and the fitted plane
		double dist2plane = sqrt(CGAL::to_double(squared_distance(point_of_interest, plane)));
		//cout << "the dist value is : " << dist << endl;
		//checkPlanar = AX+BY+CZ+D (Plane equation)
		//double checkPlanar = (A* CGAL::to_double(point_of_interest.x())) + (B*CGAL::to_double(point_of_interest.y())) + (C*CGAL::to_double(point_of_interest.z())) + D;
		//cout<<"the checkPlanar value is : "<<checkPlanar<<endl;

		//if checkPlanar = 0 (>D) then it's on the plane otherwise not and tolerance is a user defined value
		if (dist2plane > tolerance) {
			cout << std::setprecision(15)<< "the point (" << point_of_interest.x() << " " << point_of_interest.y() << " " << point_of_interest.z() << ") is NOT on the Plane" << endl;
			cout << "----------------------------------------" << endl;
			plane_no++;
			non_planar_vertices.push_back(vi);
			//mymap.insert(std::pair<char, int>('a', 100));
			vertex_map.insert(std::pair<vertex_index,Point_3> (vi, point_of_interest));
			//a plane can have more than 1 non planar points
			//number of non planar polygons
			if (plane_no == 1) { 
				no_of_non_planar_plane++;
				is_planar = false;
			}
		}
		else {
			//cout << "++++++++++++++++++++++++++++++++++++++++" << endl;
			//cout << std::setprecision(15)<< "the point (" << point_of_interest.x() << " " << point_of_interest.y() << " " << point_of_interest.z() << ") is on the Plane" << endl;
		}
	}

	return is_planar;
}

//--- old module -check_collinearity- NOT USED -- delete me later--
vector<double> check_collinearity(vector<double> &linearRingCoords) {
	vector<double> non_collinear_points;

	Point_3 X(linearRingCoords[0], linearRingCoords[1], linearRingCoords[2]);
	Point_3 Y(linearRingCoords[3], linearRingCoords[4], linearRingCoords[5]);
	Point_3 Z(linearRingCoords[6], linearRingCoords[7], linearRingCoords[8]);

	if (CGAL::collinear(X, Y, Z)) {
		cout << "---------- The points are colliear and can't define the plane -------------" << endl;
		for (unsigned int j = 9; j < linearRingCoords.size();j = j + 3) {
			Point_3 Z(linearRingCoords[j], linearRingCoords[j + 1], linearRingCoords[j + 2]);

			if (CGAL::collinear(X, Y, Z)) {
				//check if it is again co-linear
				cout << "choosing another point" << endl;
			}
			else {
				j = (unsigned int) linearRingCoords.size(); //(unsigned int) - due to convertion waring

				non_collinear_points.push_back(CGAL::to_double(X.x()));non_collinear_points.push_back(CGAL::to_double(X.y()));non_collinear_points.push_back(CGAL::to_double(X.z()));
				non_collinear_points.push_back(CGAL::to_double(Y.x()));non_collinear_points.push_back(CGAL::to_double(Y.y()));non_collinear_points.push_back(CGAL::to_double(Y.z()));
				non_collinear_points.push_back(CGAL::to_double(Z.x()));non_collinear_points.push_back(CGAL::to_double(Z.y()));non_collinear_points.push_back(CGAL::to_double(Z.z()));
			}
		}
	}

	else {
		non_collinear_points.push_back(CGAL::to_double(X.x()));non_collinear_points.push_back(CGAL::to_double(X.y()));non_collinear_points.push_back(CGAL::to_double(X.z()));
		non_collinear_points.push_back(CGAL::to_double(Y.x()));non_collinear_points.push_back(CGAL::to_double(Y.y()));non_collinear_points.push_back(CGAL::to_double(Y.z()));
		non_collinear_points.push_back(CGAL::to_double(Z.x()));non_collinear_points.push_back(CGAL::to_double(Z.y()));non_collinear_points.push_back(CGAL::to_double(Z.z()));
	}

	return non_collinear_points;
} //end of check_collinearity function

//floating point number error in input file
bool error_type1(double &tol, vector<Point_3> &pts) {
	
	bool Type1_error = false;
	int it1=0, it2=0;
	cout << "size of vertices: " << pts.size() << endl;
	for (unsigned int i = 0; i < pts.size(); i++) {
		it1++;

		Point_3 base_point(pts[i]);
		//Point_3 base_point(390840.99690062, 5819160.35802303, 33.7799987792969);
		for (unsigned int j = 1; j < pts.size(); j++) {
			it2++;
			if (base_point != pts[j]) {
				double dist = sqrt(CGAL::to_double(squared_distance(base_point, pts[j])));
				//cout << std::setprecision(15) << dist << endl;

				if (dist < tol) {
					Type1_error = true;
					cout << "Type1 error is present in the data file" << endl;
				}
			}

		}
	}
	cout << "it1: " << it1 << " it2: " << it2 << endl;
	return Type1_error;
}

void data_structure()
{
	//Nef_polyhedron nef(input);

	//std::string sm_file = "C:/dev/CGAL-4.9/build-example/3D_building_model/Inputfiles/mesh-2.off";
	//std::ifstream istrm(sm_file, std::ios::binary);
	//CGAL::OFF_to_nef_3(istrm, nef_1);

	//Polyhedron p;
	//nef_1.convert_to_polyhedron(p);
	//std::cout << p << std::endl;
	Vector_3 vec1(2, 1, -1);
	Vector_3 vec2(-3, 4, 1);
	Vector_3 add = vec1 *3;
	cout << add << endl;
	Vector_3 cross = CGAL::cross_product(vec1, vec2);

	cout<<cross<<endl; 

	cout << " --data_structure() method-- "<< endl;
	Surface_Mesh mesh;

	//SM_to_Nef SmToNef;
	//Nef_polyhedron n = SmToNef.get_Nef();

	vertex_index u1 = mesh.add_vertex(Kernel::Point_3(0, 0, 0));
	vertex_index v1 = mesh.add_vertex(Kernel::Point_3(1, 0, 0));
	vertex_index w1 = mesh.add_vertex(Kernel::Point_3(1, 1, 0));
	vertex_index x1 = mesh.add_vertex(Kernel::Point_3(0, 1, 0));
	vertex_index z1 = mesh.add_vertex(Kernel::Point_3(0.5, 0.5, 0));
	
	face_index face1 = mesh.add_face(v1, w1, z1);
	face_index face2 = mesh.add_face(x1, z1, w1);
	face_index face3 = mesh.add_face(x1, u1, z1);
	face_index face4 = mesh.add_face(u1, v1, z1);	

	{
		std::cout << "vertices around vertex " << v1 << std::endl;
		CGAL::Vertex_around_target_circulator<Surface_Mesh> vbegin(mesh.halfedge(v1), mesh), done(vbegin);
		do {
			std::cout << *vbegin++ << std::endl;
		} while (vbegin != done);
	}

	BOOST_FOREACH(vertex_index vd, vertices_around_target(mesh.halfedge(face2), mesh)) {
		std::cout << "Face: " << face1 << " Vertex: "<<vd <<" " << mesh.point(vd) << endl;
	}
	Surface_Mesh sm;
	std::string sm_file = "C:/dev/CGAL-4.9/build-example/3D_building_model/Inputfiles/box.off";
	std::ifstream istrm(sm_file, std::ios::binary);
	istrm >> sm;

	vector<vertex_index> sm_vertex;
	BOOST_FOREACH(vertex_index v, sm.vertices()) {
		sm_vertex.push_back(v);
	}

	vector<face_index> sm_faces;
	BOOST_FOREACH(face_index fi, sm.faces()) {
		sm_faces.push_back(fi);
	}

	for (unsigned int i = 0; i < sm_vertex.size();i++) {
	{
		std::cout << "vertices around vertex " << sm_vertex[i]<< sm.point(sm_vertex[i]) << std::endl;
		CGAL::Vertex_around_target_circulator<Surface_Mesh> vbegin(sm.halfedge(sm_vertex[i]), sm), done(vbegin);
		do {
			std::cout << *vbegin++ << sm.point(*vbegin++) << std::endl;
		} while (vbegin != done);
	}
}
	cout << sm.number_of_faces();


}	

void write_off_format(Surface_Mesh& m) {
	std::ofstream out("C:/dev/CGAL-4.9/build-example/3D_building_model/OutputFiles/mesh-1.off");
	out << std::setprecision(15) << m;
	out.close();
}

Plane_3 plane_equation(Surface_Mesh &mesh, face_index &fi) {
	//Linear least squares plane fitting part
	vector<Point_3> pts; //later replace this with face_x 

	BOOST_FOREACH(vertex_index vi, vertices_around_face(mesh.halfedge(fi), mesh)) {
		pts.push_back(mesh.point(vi));
	}

	//find least squares plane fitting 
	Plane_3 plane;
	double plane_quality = linear_least_squares_fitting_3(pts.begin(), pts.end(), plane, CGAL::Dimension_tag<0>());
	std::cout << "least square plane fitting() = " << "(" << plane.a() << "," << plane.b() << "," << plane.c() << "," << plane.d() << ")"<< "(1=Best fit,0=NOT best) : "<<plane_quality << endl;

	return plane;
}

void healing(Surface_Mesh &mesh) {
	
	//to test 2-manifold situation
	int no_of_incident_vertices;
	bool count=true;
	bool is_2manifold = false;
	for (unsigned int i = 0; i < non_planar_vertices.size(); i++) {
	
			std::cout << "vertices around vertex " << non_planar_vertices[i] <<" --  "<< mesh.point(non_planar_vertices[i]) << std::endl;
			CGAL::Vertex_around_target_circulator<Surface_Mesh> vbegin(mesh.halfedge(non_planar_vertices[i]), mesh), done(vbegin);
			no_of_incident_vertices = 0;
			do {
				std::cout << *vbegin++ << " ---- "<< mesh.point(*vbegin++) << std::endl;
				no_of_incident_vertices++;
			} while (vbegin != done);

			if (no_of_incident_vertices == 3) {
				if (count) {
					is_2manifold = true;
				}

			}
			else {
				is_2manifold = false;
				count = false;
			}
	}
	
	if (is_2manifold) {
		cout << "+++++ The building is a 2-manifold object +++++" << endl;

	}
	else
	{
		cout << "----- Non-manifold object ----" << endl;
	}

	vector<face_index> adj_faces;
	vector<Plane_3> adj_planes;
	//adjacent faces to a vertex 
	for (unsigned int k = 0; k < non_planar_vertices.size(); k++) {

		//std::cout << "faces around vertex " << non_planar_vertices[i] << " --  " << mesh.point(non_planar_vertices[i]) << std::endl;
		CGAL::Face_around_target_circulator<Surface_Mesh> vbegin(mesh.halfedge(non_planar_vertices[k]), mesh), done(vbegin);
		do {
			//std::cout << *vbegin++ << " ---- " << std::endl;
			adj_faces.push_back(*vbegin++);
			adj_planes.push_back(plane_equation(mesh, *vbegin++));

		} while (vbegin != done);
		
		// find intersection point of three planes
		cout << "size of adj_faces " << adj_faces.size() << endl;
		cout << "size of adj_planes " << adj_planes.size() << endl;
		for (unsigned int i = 0; i < adj_planes.size(); i = i + 3) {
			Vector_3 n1, n2, n3;
			n1 = adj_planes[i].orthogonal_vector();
			n2 = adj_planes[i + 1].orthogonal_vector();
			n3 = adj_planes[i + 2].orthogonal_vector();
			double d1, d2, d3;
			d1 = -(adj_planes[i].d()); //d = -D 
			d2 = -(adj_planes[i + 1].d());
			d3 = -(adj_planes[i + 2].d());
			Vector_3 cross_n23 = CGAL::cross_product(n2, n3);
			Vector_3 cross_n31 = CGAL::cross_product(n3, n1);
			Vector_3 cross_n12 = CGAL::cross_product(n1, n2);
			Vector_3 mul1 = (d1 * cross_n23);
			Vector_3 mul2 = (d2 * cross_n31);
			Vector_3 mul3 = (d3 * cross_n12);
			Vector_3 part1 = (mul1 + mul2 + mul3);
			double dot_n23 = CGAL::to_double(CGAL::scalar_product(n1, cross_n23));
			if (dot_n23 == 0) {
				cout << "Two planes are parallel!! Determinant is zero! => n1.(n2*n3)!=0)" << endl;
			}
			else
			{
				Vector_3 pt = (part1 / dot_n23);
				cout << "intersection point: " << pt << endl;
				intersection_points.insert(std::pair<vertex_index, Vector_3>(non_planar_vertices[k], pt));
			}

		}

	}
	// showing contents of a std::map
	std::cout << "mymap contains:\n";
	for (std::map<vertex_index, Vector_3>::iterator it = intersection_points.begin(); it != intersection_points.end(); ++it) {
		std::cout << it->first << " => " << it->second << '\n';
	}


	//for (unsigned int i = 0; i < faces.size(); i++) {
	//	BOOST_FOREACH(face_index fi, faces_around_face(polygon_mesh.halfedge(faces[i]), polygon_mesh)) {
	//		//std::cout << "Adjacent faces: " << fi << std::endl;
	//		//vertices around that adjacent face
	//		BOOST_FOREACH(vertex_index vd, vertices_around_target(polygon_mesh.halfedge(faces[i]), polygon_mesh)) {
	//			std::cout << "Face: " << faces[i] << " Vertex: " << polygon_mesh.point(vd) << endl;
	//		}
	//	}
	//	//check if there is any duplicate points 
	//	//decision pts or pts2? //pts - Andreas - adjacent faces 
	//	//bool error2 = error_type1(tolerence, pts2); //decision pts or pts2? // less iteratin
	//	//if (error1)												
	//	//	cout << "error type 1 is present? " << error1 << endl;
	//}

	//to find duplicate points (floating point errors in input files)
	//query also in neighbouring faces 
	//std::vector<Point_3> pts, pts2;
	//BOOST_FOREACH(vertex_index vi, polygon_mesh.vertices()) {
	//	pts2.push_back(polygon_mesh.point(vi));
	//}
	////BOOST_FOREACH(face_index fd, polygon_mesh.faces()) {
	////	BOOST_FOREACH(vertex_index vd, vertices_around_face(polygon_mesh.halfedge(fd), polygon_mesh)) {
	////		pt.push_back((polygon_mesh.point(vd)));
	////	}

	////}


	//for (unsigned int i = 0; i < faces.size(); i++) {
	//	BOOST_FOREACH(face_index fi, faces_around_face(polygon_mesh.halfedge(faces[i]), polygon_mesh)) {
	//		//std::cout << "Adjacent faces: " << fi << std::endl;
	//		//vertices around that adjacent face
	//		BOOST_FOREACH(vertex_index vd, vertices_around_face(polygon_mesh.halfedge(faces[i]), polygon_mesh)) {
	//			//std::cout << "Face: " << fi2  << " Vertex: " << polygon_mesh.point(vd) << endl;
	//			//to check type1error? computationally expensive
	//			pts.push_back(polygon_mesh.point(vd));
	//		}
	//	}
	//	//check if there is any duplicate points 
	//	//decision pts or pts2? //pts - Andreas - adjacent faces 
	//	//bool error2 = error_type1(tolerence, pts2); //decision pts or pts2? // less iteratin
	//	//if (error1)												
	//	//	cout << "error type 1 is present? " << error1 << endl;
	//}

	//double tolerence = 0.0010; //in meter
	//bool error1 = error_type1(tolerence, pts2);
	//-----todo------- heal type1 error 
}

void polyhedron_check() {
	//from .OFF surface mesh 
	Polyhedron P;
	std::ifstream in("C:/dev/CGAL-4.9/build-example/3D_building_model/OutputFiles/mesh-1.off");
	in >> P;

	//CGAL_precondition(P.is_valid());
	//cout << P << endl;

	if (P.is_closed()) {
		cout << "Polyhedron is closed" << endl;
	}
	if (P.is_valid()) {
		cout << "Polyhedron is valid" << endl;
	}
	if (P.is_pure_trivalent()) {
		cout << "all vertices have exactly three incident edges" << endl;
	}
	if (P.is_pure_bivalent()) {
		cout << "all vertices have exactly two incident edges" << endl;
	}
	if (P.is_pure_triangle()) {
		cout << "all facets are triangles" << endl;
	}

	//CGAL_postcondition(P.is_valid());
}

//write citygml file
void cgal2citygml(vector<string> &rings, vector<string> &gml_id) {

}