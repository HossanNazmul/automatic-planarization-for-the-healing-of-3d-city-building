	<cityObjectMember>
		<bldg:Building gml:id="GML_7b1a5a6f-ddad-4c3d-a507-3eb9ee0a8e68">
			<gml:name>Example Building LOD2 </gml:name>
			<bldg:boundedBy>
				<bldg:GroundSurface>
					<gml:name>Ground Slab</gml:name>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface>
							<gml:surfaceMember>
								<gml:Polygon gml:id="GML_d3981803-d4b0-4b5b-969c-53f657594757">
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">458875.0 5438350.0 112.0 458875.0 5438355.0 112.0001 458885.0 5438355.0 112.0 458885.0001 5438350.0 112.0 458875.0 5438350.0 112.0</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface>
					<gml:name>Roof Surfaces</gml:name>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface>
							<gml:surfaceMember>
								<gml:Polygon gml:id="GML_ec6a8966-58d9-4894-8edd-9aceb91b923f">
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">458885.0 5438355.0 115.0 458875.0 5438355.0 115.0 458875.0 5438352.5 117.0 458885.0 5438352.5 117.0 458885.0 5438355.0 115.0</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface>
							<gml:surfaceMember>
								<gml:Polygon gml:id="GML_b41dc792-5da6-4cd9-8f85-247583f305e3">
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">458875.0 5438350.0 115.0 458885.0 5438350.0 115.0001 458885.0 5438352.5 117.0 458875.0 5438352.5 117.0 458875.0 5438350.0 115.0</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
				</bldg:boundedBy>
  			   <bldg:boundedBy>
				<bldg:WallSurface>
					<gml:name>Wall Surfaces</gml:name>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface>
							<gml:surfaceMember>
								<gml:Polygon gml:id="GML_1d350a50-6acc-4d3c-8c28-326ca4305fd1">
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">458875.0 5438350.0 112.0 458885.0 5438350.0 112.0 458885.0 5438350.0 115.0 458875.0 5438350.0 115.0 458875.0 5438350.0 112.0</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface>
							<gml:surfaceMember>
								<gml:Polygon gml:id="GML_d3909000-2f18-4472-8886-1c127ea67df1">
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">458885.0 5438355.0 112.0 458875.0 5438355.0 112.0 458875.0 5438355.0 115.0 458885.0 5438355.0 115.0 458885.0 5438355.0 112.0</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface>
							<gml:surfaceMember>
								<gml:Polygon gml:id="GML_6286ffa9-3811-4796-a92f-3fd037c8e668">
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">458885.0 5438350.0 112.0 458885.0 5438355.0 112.0 458885.0 5438355.0 115.0 458885.0 5438352.5 117.0 458885.0 5438350.0 115.0 458885.0 5438350.0 112.0</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface>
							<gml:surfaceMember>
								<gml:Polygon gml:id="GML_5cc4fd92-d5de-4dd8-971e-892c91da2d9f">
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">458875.0 5438355.0 112.0 458875.0 5438350.0 112.0 458875.0 5438350.0 115.0 458875.0 5438352.5 117.0001 458875.0 5438355.0 115.0 458875.0 5438355.0 112.0</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
		</bldg:Building>
	</cityObjectMember>
</CityModel>
