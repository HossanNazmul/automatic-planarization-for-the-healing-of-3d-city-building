<?xml version="1.0" encoding="UTF-8"?>
<CityModel xmlns:bldg="http://www.opengis.net/citygml/building/1.0" xmlns:app="http://www.opengis.net/citygml/appearance/1.0" xmlns:smil20="http://www.w3.org/2001/SMIL20/" xmlns:grp="http://www.opengis.net/citygml/cityobjectgroup/1.0" xmlns:base="http://www.citygml.org/citygml/profiles/base/1.0" xmlns:frn="http://www.opengis.net/citygml/cityfurniture/1.0" xmlns:smil20lang="http://www.w3.org/2001/SMIL20/Language" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:wtr="http://www.opengis.net/citygml/waterbody/1.0" xmlns:tex="http://www.opengis.net/citygml/texturedsurface/1.0" xmlns:veg="http://www.opengis.net/citygml/vegetation/1.0" xmlns:sch="http://www.ascc.net/xml/schematron" xmlns:gen="http://www.opengis.net/citygml/generics/1.0" xmlns:dem="http://www.opengis.net/citygml/relief/1.0" xmlns:gml="http://www.opengis.net/gml" xmlns:tran="http://www.opengis.net/citygml/transportation/1.0" xmlns:xAL="urn:oasis:names:tc:ciq:xsdschema:xAL:2.0" xmlns:luse="http://www.opengis.net/citygml/landuse/1.0" xmlns="http://www.opengis.net/citygml/1.0">
	<gml:boundedBy>
		<gml:Envelope srsName="EPSG:25833" srsDimension="3">
			<gml:lowerCorner>389498.169383512 5818943.99656938 34.9999998856199</gml:lowerCorner>
			<gml:upperCorner>389763.136320258 5819123.96866847 137.000205520431</gml:upperCorner>
		</gml:Envelope>
	</gml:boundedBy>
	<cityObjectMember>
		<bldg:Building gml:id="BLDG_1190001">
			<gml:name>Sony Center</gml:name>
			<bldg:roofType/>
			<bldg:boundedBy>
				<bldg:GroundSurface gml:id="UUID_d4a42931-7629-4858-99df-c16ac28fc8c6">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389528.753907087 5819046.91449133 35 389505.300673001 5819061.01702812 35 389498.169383512 5819123.96866847 35 389505.056317231 5819122.51634296 35 389511.639407607 5819120.82905027 35 389517.92156749 5819118.93163814 35 389523.905709729 5819116.84895431 35 389529.594747173 5819114.60584654 35 389534.991592668 5819112.22716256 35 389540.09915906 5819109.73775011 35 389544.920359192 5819107.16245694 35 389549.458105908 5819104.52613077 35 389553.715312049 5819101.85361934 35 389557.694890453 5819099.1697704 35 389561.39975396 5819096.49943166 35 389564.832815407 5819093.86745086 35 389567.996987628 5819091.29867574 35 389570.895183456 5819088.81795401 35 389573.530315727 5819086.45013342 35 389575.905297269 5819084.22006169 35 389578.023040915 5819082.15258655 35 389579.886459493 5819080.27255574 35 389581.498465832 5819078.60481698 35 389564.887771436 5819069.22662666 35 389523.449044491 5819093.7432764 35 389528.753907087 5819046.91449133 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface gml:id="UUID_7eb9a059-f08a-4ac2-bb89-413ff98fa827">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389517.699795846 5818950.47923669 35 389507.353381952 5819041.81225663 35 389509.389848078 5819042.04295193 35 389531.137822062 5819027.9893862 35 389535.474896761 5818989.70382601 35 389527.895269781 5818988.84518835 35 389530.220231619 5818968.32158494 35 389543.531764969 5818969.82954695 35 389544.841741097 5818967.69436135 35 389546.243284902 5818965.61813161 35 389547.733747336 5818963.604782 35 389549.310311286 5818961.65811793 35 389550.969996898 5818959.78181879 35 389552.709667212 5818957.97943094 35 389554.52603409 5818956.25436107 35 389556.415664429 5818954.60986971 35 389558.374986651 5818953.04906512 35 389560.400297457 5818951.57489734 35 389562.48776882 5818950.19015269 35 389564.633455225 5818948.89744847 35 389521.370972857 5818943.99656938 35 389520.599390346 5818950.80770977 35 389517.699795846 5818950.47923669 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface gml:id="UUID_3a2332d9-fdc1-4090-a801-e10d7daaa73f">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389547.201260434 5818986.66290736 35 389547.457281619 5818984.6568458 35 389548.711039471 5818978.66133764 35 389544.146650882 5818976.21482251 35 389539.557499529 5818980.45492022 35 389538.037159752 5818981.78848574 35 389537.131728622 5818983.59680665 35 389536.974813782 5818985.61304269 35 389537.58956997 5818987.53967314 35 389538.885282253 5818989.09239969 35 389540.670752148 5818990.0420982 35 389542.682511342 5818990.24862885 35 389544.623699727 5818989.68151548 35 389546.207870806 5818988.42444274 35 389547.201260434 5818986.66290736 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface gml:id="UUID_8db24ac2-7a24-4501-bad8-2eed1c8e1962">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389561.001612858 5818982.28306285 35 389560.195114673 5818983.38681057 35 389559.105256072 5818982.59046104 35 389556.441197887 5818986.23640622 35 389553.502883722 5818991.10850873 35 389551.100841482 5818996.01127926 35 389549.203246215 5819000.90164155 35 389547.777935612 5819005.73688246 35 389546.792560916 5819010.47475055 35 389546.214753047 5819015.07351358 35 389546.196366784 5819015.47478221 35 389547.544753698 5819015.53656575 35 389547.363136871 5819019.50023935 35 389547.500527998 5819023.58961106 35 389547.720747059 5819025.50242012 34.9999998856199 389558.722057833 5819024.76505696 35.000003406187 389563.605065882 5819024.43777322 35 389586.75550641 5819024.01784527 35 389594.801599708 5819005.08146698 35 389575.879938891 5818989.78074407 35 389582.209917453 5818983.4849781 35 389585.224466633 5818987.30827701 35 389586.045714357 5818986.51791605 35 389586.894279352 5818985.75695895 35 389587.769109454 5818985.02634924 35 389588.669119933 5818984.32699283 35 389589.593194835 5818983.65975688 35 389590.540188367 5818983.0254687 35 389591.508926322 5818982.42491479 35 389592.498207529 5818981.85883978 35 389593.506805343 5818981.32794557 35 389594.533469173 5818980.83289043 35 389595.576926021 5818980.3742882 35 389596.635882072 5818979.95270752 35 389594.334988224 5818975.15858579 35 389587.668496849 5818961.26834343 35 389580.963443878 5818965.00020917 35 389574.976128217 5818969.0092217 35 389569.674650645 5818973.25377331 35 389565.027111937 5818977.69225627 35 389561.001612858 5818982.28306285 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface gml:id="UUID_766e4b51-f064-4627-a531-ee481289a866">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389698.835311784 5818975.64619299 35 389756.630335332 5818980.55465584 35 389757.454087066 5818973.28298673 35 389763.136320258 5818973.92668348 35 389759.658743696 5818968.14180581 35 389755.140940603 5818963.12704714 35 389749.749027977 5818959.06679729 35 389743.681263332 5818956.11034939 35 389737.160754892 5818954.36641039 35 389730.427258036 5818953.89910401 35 389723.728359628 5818954.72561291 35 389717.310374397 5818956.81554689 35 389711.409288076 5818960.09206031 35 389706.242080335 5818964.43467766 35 389701.998746553 5818969.68372338 35 389698.835311784 5818975.64619299 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface gml:id="UUID_b392c833-2e25-4396-b1f1-76a60093ced3">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389629.645542779 5819022.03156949 35 389662.919489386 5819037.14606766 35 389733.080241832 5818995.52014053 35 389727.666616355 5818987.47428098 35 389712.528566747 5818986.55021732 35 389706.588054941 5818990.21170472 35 389703.550803908 5818985.28397351 35 389702.938029037 5818985.66166276 35 389702.4921459 5818984.93824798 35 389696.44950477 5818988.66268366 35 389689.079318673 5818976.70506253 35 389688.389946907 5818977.12996294 35 389687.624077046 5818975.8873916 35 389687.01130214 5818976.26508085 35 389685.909708505 5818974.4778207 35 389672.760580078 5818982.58240271 35 389673.683820523 5818984.08029697 35 389673.139131705 5818984.41602077 35 389681.658123288 5818998.23749922 35 389679.258088286 5818999.71678228 35 389682.200917388 5819004.49131997 35 389677.571062678 5819007.34497249 35 389676.912316913 5819005.83262315 35 389680.427671732 5819004.30141272 35 389673.241354754 5818987.803056 35 389662.205786908 5818992.60990352 35 389667.104459876 5819003.8562837 35 389665.527950203 5819004.54297627 35 389667.839549055 5819009.84994775 35 389673.238178017 5819007.49842492 35 389674.245495711 5819009.19084383 35 389663.712979977 5819015.45973485 35 389659.114260156 5819013.40042673 35 389660.135797726 5819013.20671718 35 389665.196294824 5819012.24711806 35 389662.096388072 5818995.89959266 35 389651.615805337 5818997.8869776 35 389654.100502451 5819010.99016173 35 389638.232005167 5819003.5983743 35 389629.645542779 5819022.03156949 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-0e768c62-9530-4eed-84c1-4c456bd44c14">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389505.300673001 5819061.01702812 35 389528.753907087 5819046.91449133 35 389528.753907087 5819046.91449133 66.1592781138443 389505.300673001 5819061.01702812 66.1592781138443 389505.300673001 5819061.01702812 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-093fbfb2-91f4-469b-9426-80b45eb1025c">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389528.753907087 5819046.91449133 35 389523.449044491 5819093.7432764 35 389523.449044491 5819093.7432764 66.1592781138443 389528.753907087 5819046.91449133 66.1592781138443 389528.753907087 5819046.91449133 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-7f1ef063-ea74-4e54-aa47-683410a8a78f">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389581.498465832 5819078.60481698 35 389579.886459493 5819080.27255574 35 389579.886459493 5819080.27255574 73.4873121927641 389581.498465832 5819078.60481698 73.4873121927641 389581.498465832 5819078.60481698 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-dc0fa6ad-d762-43e8-99c6-4afd2cfa5b65">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389579.886459493 5819080.27255574 35 389578.023040915 5819082.15258655 35 389578.023040915 5819082.15258655 73.4873121927641 389579.886459493 5819080.27255574 73.4873121927641 389579.886459493 5819080.27255574 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-b8d72bdd-0186-455c-933a-f9528e5e40eb">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389578.023040915 5819082.15258655 35 389575.905297269 5819084.22006169 35 389575.905297269 5819084.22006169 73.4873121927641 389578.023040915 5819082.15258655 73.4873121927641 389578.023040915 5819082.15258655 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-e80b0542-0adc-4775-9e60-1164c2355598">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389575.905297269 5819084.22006169 35 389573.530315727 5819086.45013342 35 389573.530315727 5819086.45013342 73.4873121927641 389575.905297269 5819084.22006169 73.4873121927641 389575.905297269 5819084.22006169 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-4c3f26cd-2189-4ac5-8f47-13008c8adadf">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389573.530315727 5819086.45013342 35 389570.895183456 5819088.81795401 35 389570.895183456 5819088.81795401 73.4873121927641 389573.530315727 5819086.45013342 73.4873121927641 389573.530315727 5819086.45013342 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-f0ce800b-6eae-42f9-b02e-3a66a3fb0d66">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389570.895183456 5819088.81795401 35 389567.996987628 5819091.29867574 35 389567.996987628 5819091.29867574 73.4873121927641 389570.895183456 5819088.81795401 73.4873121927641 389570.895183456 5819088.81795401 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-31c28229-4832-4115-9ed0-1c64c0f3d7bc">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389567.996987628 5819091.29867574 35 389564.832815407 5819093.86745086 35 389564.832815407 5819093.86745086 73.4873121927641 389567.996987628 5819091.29867574 73.4873121927641 389567.996987628 5819091.29867574 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-8ebd24ef-f772-4509-a3d8-6c9a856132a5">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389564.832815407 5819093.86745086 35 389561.39975396 5819096.49943166 35 389561.39975396 5819096.49943166 73.4873121927641 389564.832815407 5819093.86745086 73.4873121927641 389564.832815407 5819093.86745086 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-6874ec44-b023-42b8-9188-a7080f144b79">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389561.39975396 5819096.49943166 35 389557.694890453 5819099.1697704 35 389557.694890453 5819099.1697704 73.4873121927641 389561.39975396 5819096.49943166 73.4873121927641 389561.39975396 5819096.49943166 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-57b7387f-6c13-4e32-a617-e9ec0c6de50c">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389557.694890453 5819099.1697704 35 389553.715312049 5819101.85361934 35 389553.715312049 5819101.85361934 73.4873121927641 389557.694890453 5819099.1697704 73.4873121927641 389557.694890453 5819099.1697704 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-5a4bc547-9f9e-4bac-a06c-b233f1da49a6">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389553.715312049 5819101.85361934 35 389549.458105908 5819104.52613077 35 389549.458105908 5819104.52613077 73.4873121927641 389553.715312049 5819101.85361934 73.4873121927641 389553.715312049 5819101.85361934 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-57527ded-ddd3-4e96-b34e-84a19884867e">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389549.458105908 5819104.52613077 35 389544.920359192 5819107.16245694 35 389544.920359192 5819107.16245694 73.4873121927641 389549.458105908 5819104.52613077 73.4873121927641 389549.458105908 5819104.52613077 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-79b6b559-4f0c-4660-8a28-49ff0ddf7acc">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389544.920359192 5819107.16245694 35 389540.09915906 5819109.73775011 35 389540.09915906 5819109.73775011 73.4873121927641 389544.920359192 5819107.16245694 73.4873121927641 389544.920359192 5819107.16245694 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-3fb08a05-925d-4be1-9202-ba6b5e9b6c49">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389540.09915906 5819109.73775011 35 389534.991592668 5819112.22716256 35 389534.991592668 5819112.22716256 73.4873121927641 389540.09915906 5819109.73775011 73.4873121927641 389540.09915906 5819109.73775011 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-3ba288a2-1265-4ed4-9fb7-68ec12473d1f">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389534.991592668 5819112.22716256 35 389529.594747173 5819114.60584654 35 389529.594747173 5819114.60584654 73.4873121927641 389534.991592668 5819112.22716256 73.4873121927641 389534.991592668 5819112.22716256 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-8e750c58-276c-43b3-8ffa-a5eabc1284cc">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389529.594747173 5819114.60584654 35 389523.905709729 5819116.84895431 35 389523.905709729 5819116.84895431 73.4873121927641 389529.594747173 5819114.60584654 73.4873121927641 389529.594747173 5819114.60584654 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-040226d4-c598-4853-a4b1-189d34e241f3">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389523.905709729 5819116.84895431 35 389517.92156749 5819118.93163814 35 389517.92156749 5819118.93163814 73.4873121927641 389523.905709729 5819116.84895431 73.4873121927641 389523.905709729 5819116.84895431 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-de220131-5a1f-44ba-ad46-82402e7e2a70">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389517.92156749 5819118.93163814 35 389511.639407607 5819120.82905027 35 389511.639407607 5819120.82905027 73.4873121927641 389517.92156749 5819118.93163814 73.4873121927641 389517.92156749 5819118.93163814 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-d4ef35fb-d8c2-4735-afc1-f8b3a981229d">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389511.639407607 5819120.82905027 35 389505.056317231 5819122.51634296 35 389505.056317231 5819122.51634296 73.4873121927641 389511.639407607 5819120.82905027 73.4873121927641 389511.639407607 5819120.82905027 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-3ff98f35-9545-4f6c-b30a-98b1c2bb880f">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389505.056317231 5819122.51634296 35 389498.169383512 5819123.96866847 35 389498.169383512 5819123.96866847 73.4873121927641 389505.056317231 5819122.51634296 73.4873121927641 389505.056317231 5819122.51634296 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-53fe73f9-1169-416e-9ca1-6ceded67be3b">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389498.169383512 5819123.96866847 35 389505.300673001 5819061.01702812 35 389505.300673001 5819061.01702812 66.1592781138443 389500.023344705 5819107.60276937 66.1592781138443 389500.023344705 5819107.60276937 73.4873121927641 389498.169383512 5819123.96866847 73.4873121927641 389498.169383512 5819123.96866847 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="fme-gen-ac9f0aa7-0a61-4218-8a48-b39e4c68b80f">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389498.169383512 5819123.96866847 73.4873121927641 389500.023344705 5819107.60276937 73.4873121927641 389564.887771436 5819069.22662666 73.4873121927641 389581.498465832 5819078.60481698 73.4873121927641 389579.886459493 5819080.27255574 73.4873121927641 389578.023040915 5819082.15258655 73.4873121927641 389575.905297269 5819084.22006169 73.4873121927641 389573.530315727 5819086.45013342 73.4873121927641 389570.895183456 5819088.81795401 73.4873121927641 389567.996987628 5819091.29867574 73.4873121927641 389564.832815407 5819093.86745086 73.4873121927641 389561.39975396 5819096.49943166 73.4873121927641 389557.694890453 5819099.1697704 73.4873121927641 389553.715312049 5819101.85361934 73.4873121927641 389549.458105908 5819104.52613077 73.4873121927641 389544.920359192 5819107.16245694 73.4873121927641 389540.09915906 5819109.73775011 73.4873121927641 389534.991592668 5819112.22716256 73.4873121927641 389529.594747173 5819114.60584654 73.4873121927641 389523.905709729 5819116.84895431 73.4873121927641 389517.92156749 5819118.93163814 73.4873121927641 389511.639407607 5819120.82905027 73.4873121927641 389505.056317231 5819122.51634296 73.4873121927641 389498.169383512 5819123.96866847 73.4873121927641</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="fme-gen-79be42d4-ebe0-4488-9f61-5b64c197fc04">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389528.753907087 5819046.91449133 66.1592781138443 389523.449044491 5819093.7432764 66.1592781138443 389500.023344705 5819107.60276937 66.1592781138443 389505.300673001 5819061.01702812 66.1592781138443 389528.753907087 5819046.91449133 66.1592781138443</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-599d37e6-b37a-4585-960d-5737977440fd">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389507.353381952 5819041.81225663 35 389517.699795846 5818950.47923669 35 389517.699795846 5818950.47923669 70.5000730001461 389515.862819106 5818966.69514321 70.5000730001461 389515.862819106 5818966.69514321 66.1592781138444 389507.353381952 5819041.81225663 66.1592781138444 389507.353381952 5819041.81225663 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-69d62ff3-48c2-464e-a1ee-4696a171f491">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389517.699795846 5818950.47923669 35 389520.599390346 5818950.80770977 35 389520.599390346 5818950.80770977 70.5000730001461 389517.699795846 5818950.47923669 70.5000730001461 389517.699795846 5818950.47923669 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-b37e6caa-296c-40a5-b343-d00f1f0ed399">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389520.599390346 5818950.80770977 35 389521.370972857 5818943.99656938 35 389521.370972857 5818943.99656938 70.5000730001461 389520.599390346 5818950.80770977 70.5000730001461 389520.599390346 5818950.80770977 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-a545bb7d-f1a3-4b53-8e9d-38843f5fa406">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389521.370972857 5818943.99656938 35 389564.633455225 5818948.89744847 35 389564.633455225 5818948.89744847 70.5000730001461 389521.370972857 5818943.99656938 70.5000730001461 389521.370972857 5818943.99656938 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-d3f0ee57-255f-4b7b-a94e-123ee1084a6f">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389564.633455225 5818948.89744847 35 389562.48776882 5818950.19015269 35 389562.48776882 5818950.19015269 70.5000730001461 389564.633455225 5818948.89744847 70.5000730001461 389564.633455225 5818948.89744847 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-67437657-152d-469b-a014-6d818c3c8e12">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389562.48776882 5818950.19015269 35 389560.400297457 5818951.57489734 35 389560.400297457 5818951.57489734 70.5000730001461 389562.48776882 5818950.19015269 70.5000730001461 389562.48776882 5818950.19015269 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-c2888e98-8628-4ef0-87b8-50fcdd64a11e">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389560.400297457 5818951.57489734 35 389558.374986651 5818953.04906512 35 389558.374986651 5818953.04906512 70.5000730001461 389560.400297457 5818951.57489734 70.5000730001461 389560.400297457 5818951.57489734 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-24b6b46a-be6a-49ca-9f5f-acfc451de5d9">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389558.374986651 5818953.04906512 35 389556.415664429 5818954.60986971 35 389556.415664429 5818954.60986971 70.5000730001461 389558.374986651 5818953.04906512 70.5000730001461 389558.374986651 5818953.04906512 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-7cb5f1f4-ea02-406f-8cae-8ef1a32fade5">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389556.415664429 5818954.60986971 35 389554.52603409 5818956.25436107 35 389554.52603409 5818956.25436107 70.5000730001461 389556.415664429 5818954.60986971 70.5000730001461 389556.415664429 5818954.60986971 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-7503ac38-e263-4b42-abe1-6a4d5252094e">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389554.52603409 5818956.25436107 35 389552.709667212 5818957.97943094 35 389552.709667212 5818957.97943094 70.5000730001461 389554.52603409 5818956.25436107 70.5000730001461 389554.52603409 5818956.25436107 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-ef6aaf17-1ad9-4e0f-8350-6e8a7c0f92c8">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389552.709667212 5818957.97943094 35 389550.969996898 5818959.78181879 35 389550.969996898 5818959.78181879 70.5000730001461 389552.709667212 5818957.97943094 70.5000730001461 389552.709667212 5818957.97943094 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-a8627e8b-48bf-4326-a6f3-874e67424236">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389550.969996898 5818959.78181879 35 389549.310311286 5818961.65811793 35 389549.310311286 5818961.65811793 70.5000730001461 389550.969996898 5818959.78181879 70.5000730001461 389550.969996898 5818959.78181879 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-61bcb08e-a883-4a69-9201-0d5c1474cec5">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389549.310311286 5818961.65811793 35 389547.733747336 5818963.604782 35 389547.733747336 5818963.604782 70.5000730001461 389549.310311286 5818961.65811793 70.5000730001461 389549.310311286 5818961.65811793 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-488ff226-ea05-4259-b5ed-0274814a4e9f">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389547.733747336 5818963.604782 35 389546.243284902 5818965.61813161 35 389546.243284902 5818965.61813161 70.5000730001461 389547.733747336 5818963.604782 70.5000730001461 389547.733747336 5818963.604782 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-952cd48c-7e8c-439e-885f-aff68a97a874">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389546.243284902 5818965.61813161 35 389544.841741097 5818967.69436135 35 389544.841741097 5818967.69436135 70.5000730001461 389546.243284902 5818965.61813161 70.5000730001461 389546.243284902 5818965.61813161 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-adba8064-8ed2-4817-86a8-d0bc1199bf4a">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389544.841741097 5818967.69436135 35 389543.531764969 5818969.82954695 35 389543.531764969 5818969.82954695 70.5000730001461 389544.841741097 5818967.69436135 70.5000730001461 389544.841741097 5818967.69436135 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-fbccbb47-5f49-4aff-b7e3-1af156c2303d">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389530.220231619 5818968.32158494 35 389530.220231619 5818968.32158494 66.1592781138444 389515.862819106 5818966.69514321 66.1592781138444 389515.862819106 5818966.69514321 70.5000730001461 389543.531764969 5818969.82954695 70.5000730001461 389543.531764969 5818969.82954695 35 389530.220231619 5818968.32158494 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-4f141840-79e7-4525-bfd5-cef298aaa1fb">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389530.220231619 5818968.32158494 35 389527.895269781 5818988.84518835 35 389527.895269781 5818988.84518835 66.1592781138444 389530.220231619 5818968.32158494 66.1592781138444 389530.220231619 5818968.32158494 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-e092832a-f9de-485c-991d-1fe869ffcd52">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389527.895269781 5818988.84518835 35 389535.474896761 5818989.70382601 35 389535.474896761 5818989.70382601 66.1592781138444 389527.895269781 5818988.84518835 66.1592781138444 389527.895269781 5818988.84518835 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-6901bac3-900c-4f73-bcbf-f6650c904648">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389535.474896761 5818989.70382601 35 389531.137822062 5819027.9893862 35 389531.137822062 5819027.9893862 66.1592781138444 389535.474896761 5818989.70382601 66.1592781138444 389535.474896761 5818989.70382601 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-f3933432-4c32-46a4-883e-c16e132cef7f">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389531.137822062 5819027.9893862 35 389509.389848078 5819042.04295193 35 389509.389848078 5819042.04295193 66.1592781138444 389531.137822062 5819027.9893862 66.1592781138444 389531.137822062 5819027.9893862 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-9a5e63d0-7895-4fa7-b6ec-90b8369519f7">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389509.389848078 5819042.04295193 35 389507.353381952 5819041.81225663 35 389507.353381952 5819041.81225663 66.1592781138444 389509.389848078 5819042.04295193 66.1592781138444 389509.389848078 5819042.04295193 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="fme-gen-0f8e8351-1a1b-4b2f-83f0-4f14a59ff9ff">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389757.454087066 5818973.28298673 128.565182922846 389757.273320181 5818974.87870645 128.565182922846 389756.327658879 5818974.77157971 128.565182922846 389711.172270779 5818970.86692347 128.565182922846 389757.266974099 5818974.93472649 128.565182922846 389756.630335332 5818980.55465584 128.565182922846 389698.835311784 5818975.64619299 128.565182922846 389701.998746553 5818969.68372338 128.565182922846 389706.242080335 5818964.43467766 128.565182922846 389711.409288076 5818960.09206031 128.565182922846 389717.310374397 5818956.81554689 128.565182922846 389723.728359628 5818954.72561291 128.565182922846 389730.427258036 5818953.89910401 128.565182922846 389737.160754892 5818954.36641039 128.565182922846 389743.681263332 5818956.11034939 128.565182922846 389749.749027977 5818959.06679729 128.565182922846 389755.140940603 5818963.12704714 128.565182922846 389759.658743696 5818968.14180581 128.565182922846 389763.136320258 5818973.92668348 128.565182922846 389758.420299536 5818973.39244155 128.565182922846 389757.454087066 5818973.28298673 128.565182922846</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
									<gml:interior>
										<gml:LinearRing>
											<gml:posList>389752.302797341 5818967.17456432 128.565182922846 389752.338408785 5818966.86020434 128.565182922846 389752.097407914 5818966.83290319 128.565182922846 389752.302797341 5818967.17456432 128.565182922846</gml:posList>
										</gml:LinearRing>
									</gml:interior>
									<gml:interior>
										<gml:LinearRing>
											<gml:posList>389752.255928134 5818967.58830218 128.565182922846 389752.154206026 5818968.48625399 128.565182922846 389754.970690042 5818971.61254944 128.565182922846 389752.774756536 5818967.95965883 128.565182922846 389752.302797341 5818967.17456432 128.565182922846 389752.255928134 5818967.58830218 128.565182922846</gml:posList>
										</gml:LinearRing>
									</gml:interior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-334d212b-3626-41d9-a50d-89a0744f5433">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389756.630335332 5818980.55465584 35 389698.835311784 5818975.64619299 35 389698.835311784 5818975.64619299 128.565182922846 389756.630335332 5818980.55465584 128.565182922846 389756.630335332 5818980.55465584 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-b442a92a-7384-45aa-ae4b-9b1b45685dec">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389698.835311784 5818975.64619299 35 389701.998746553 5818969.68372338 35 389701.998746553 5818969.68372338 128.565182922846 389698.835311784 5818975.64619299 128.565182922846 389698.835311784 5818975.64619299 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-ee3591ce-87bc-4b2e-9619-dda09b842df1">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389701.998746553 5818969.68372338 35 389706.242080335 5818964.43467766 35 389706.242080335 5818964.43467766 128.565182922846 389701.998746553 5818969.68372338 128.565182922846 389701.998746553 5818969.68372338 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-69c3cd8b-db7c-4007-b2b1-2cb213942de4">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389706.242080335 5818964.43467766 35 389711.409288076 5818960.09206031 35 389711.409288076 5818960.09206031 128.565182922846 389706.242080335 5818964.43467766 128.565182922846 389706.242080335 5818964.43467766 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-2ec96b40-f1a3-4385-a9bc-73d1845fc6ec">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389711.409288076 5818960.09206031 35 389717.310374397 5818956.81554689 35 389717.310374397 5818956.81554689 128.565182922846 389711.409288076 5818960.09206031 128.565182922846 389711.409288076 5818960.09206031 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-6d2dccd3-a5a0-41d2-a139-e62ea8124d6e">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389717.310374397 5818956.81554689 35 389723.728359628 5818954.72561291 35 389723.728359628 5818954.72561291 128.565182922846 389717.310374397 5818956.81554689 128.565182922846 389717.310374397 5818956.81554689 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-caecdc4e-0199-4bda-a5ad-28b038ec05d9">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389723.728359628 5818954.72561291 35 389730.427258036 5818953.89910401 35 389730.427258036 5818953.89910401 128.565182922846 389723.728359628 5818954.72561291 128.565182922846 389723.728359628 5818954.72561291 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-189038bf-77ca-4821-a065-3180a61ad905">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389730.427258036 5818953.89910401 35 389737.160754892 5818954.36641039 35 389737.160754892 5818954.36641039 128.565182922846 389730.427258036 5818953.89910401 128.565182922846 389730.427258036 5818953.89910401 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-d9a9f5c8-1f18-4057-a66e-14d674ae3c8a">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389737.160754892 5818954.36641039 35 389743.681263332 5818956.11034939 35 389743.681263332 5818956.11034939 128.565182922846 389737.160754892 5818954.36641039 128.565182922846 389737.160754892 5818954.36641039 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-03a82c78-ae4a-414a-9d10-f5aca9b835f1">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389743.681263332 5818956.11034939 35 389749.749027977 5818959.06679729 35 389749.749027977 5818959.06679729 128.565182922846 389743.681263332 5818956.11034939 128.565182922846 389743.681263332 5818956.11034939 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-9dff7261-5a67-424e-bd74-4ad2f9f340ed">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389749.749027977 5818959.06679729 35 389755.140940603 5818963.12704714 35 389755.140940603 5818963.12704714 128.565182922846 389749.749027977 5818959.06679729 128.565182922846 389749.749027977 5818959.06679729 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-d665c877-fa1a-4da0-a153-18193c7460be">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389755.140940603 5818963.12704714 35 389759.658743696 5818968.14180581 35 389759.658743696 5818968.14180581 128.565182922846 389755.140940603 5818963.12704714 128.565182922846 389755.140940603 5818963.12704714 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-7b36260d-9dd8-4cb3-9f46-36fe2f94538f">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389759.658743696 5818968.14180581 35 389763.136320258 5818973.92668348 35 389763.136320258 5818973.92668348 128.565182922846 389759.658743696 5818968.14180581 128.565182922846 389759.658743696 5818968.14180581 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-a2960637-886d-4b0c-a692-5c3ef56f1661">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389763.136320258 5818973.92668348 35 389757.454087066 5818973.28298673 35 389757.454087066 5818973.28298673 128.565182922846 389758.420299536 5818973.39244155 128.565182922846 389763.136320258 5818973.92668348 128.565182922846 389763.136320258 5818973.92668348 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-5f2fe4b1-8777-430c-9331-2df24e1c1761">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389757.454087066 5818973.28298673 35 389756.630335332 5818980.55465584 35 389756.630335332 5818980.55465584 128.565182922846 389757.266974099 5818974.93472649 128.565182922846 389757.273320181 5818974.87870645 128.565182922846 389757.454087066 5818973.28298673 128.565182922846 389757.454087066 5818973.28298673 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="fme-gen-8dc1ed57-7214-4c72-a308-536267b2e01f">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389507.353381952 5819041.81225663 66.1592781138444 389515.862819106 5818966.69514321 66.1592781138444 389530.220231619 5818968.32158494 66.1592781138444 389527.895269781 5818988.84518835 66.1592781138444 389535.474896761 5818989.70382601 66.1592781138444 389531.137822062 5819027.9893862 66.1592781138444 389509.389848078 5819042.04295193 66.1592781138444 389507.353381952 5819041.81225663 66.1592781138444</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="fme-gen-8b256efa-b3c4-48bf-8e70-cb575e62ce1b">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389543.531764969 5818969.82954695 70.5000730001461 389515.862819106 5818966.69514321 70.5000730001461 389517.699795846 5818950.47923669 70.5000730001461 389520.599390346 5818950.80770977 70.5000730001461 389521.370972857 5818943.99656938 70.5000730001461 389564.633455225 5818948.89744847 70.5000730001461 389562.48776882 5818950.19015269 70.5000730001461 389560.400297457 5818951.57489734 70.5000730001461 389558.374986651 5818953.04906512 70.5000730001461 389556.415664429 5818954.60986971 70.5000730001461 389554.52603409 5818956.25436107 70.5000730001461 389552.709667212 5818957.97943094 70.5000730001461 389550.969996898 5818959.78181879 70.5000730001461 389549.310311286 5818961.65811793 70.5000730001461 389547.733747336 5818963.604782 70.5000730001461 389546.243284902 5818965.61813161 70.5000730001461 389544.841741097 5818967.69436135 70.5000730001461 389543.531764969 5818969.82954695 70.5000730001461</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="fme-gen-361da926-212c-4846-9104-9232ee6d1360">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389638.232005167 5819003.5983743 73.8975467930565 389654.100502451 5819010.99016173 73.8975467930565 389659.114260156 5819013.40042673 73.8975467930565 389663.712979977 5819015.45973485 73.8975467930565 389674.245495711 5819009.19084383 73.8975467930565 389677.571062678 5819007.34497249 73.8975467930565 389682.200917388 5819004.49131997 73.8975467930565 389706.588054941 5818990.21170472 73.8975467930565 389712.528566747 5818986.55021732 73.8975467930565 389727.666616355 5818987.47428098 73.8975467930565 389733.080241832 5818995.52014053 73.8975467930565 389662.919489386 5819037.14606766 73.8975467930565 389629.645542779 5819022.03156949 73.8975467930565 389638.232005167 5819003.5983743 73.8975467930565</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-d8f5780e-e4df-4d8c-90e0-2c514f08a7b6">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389662.919489386 5819037.14606766 35 389629.645542779 5819022.03156949 35 389629.645542779 5819022.03156949 73.8975467930565 389662.919489386 5819037.14606766 73.8975467930565 389662.919489386 5819037.14606766 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-b1411292-cfbe-4198-b1c7-6564b481f5ec">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389629.645542779 5819022.03156949 35 389638.232005167 5819003.5983743 35 389638.232005167 5819003.5983743 73.8975467930565 389629.645542779 5819022.03156949 73.8975467930565 389629.645542779 5819022.03156949 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-94cf9b9a-7fb9-4ddc-9588-d1248d98813b">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389638.232005167 5819003.5983743 35 389654.100502451 5819010.99016173 35 389654.100502451 5819010.99016173 41.2000159074514 389654.100502451 5819010.99016173 73.8975467930565 389638.232005167 5819003.5983743 73.8975467930565 389638.232005167 5819003.5983743 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_7dfbaac2-59bb-4156-8b8f-13395cbe4672">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389654.100502451 5819010.99016173 35 389651.615805337 5818997.8869776 35 389651.615805337 5818997.8869776 41.2000159074514 389654.100502451 5819010.99016173 41.2000159074514 389654.100502451 5819010.99016173 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-3e538a4e-0961-4aeb-a0b1-704cbf0f6a95">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389651.615805337 5818997.8869776 35 389662.096388072 5818995.89959266 35 389662.096388072 5818995.89959266 41.2000159074514 389651.615805337 5818997.8869776 41.2000159074514 389651.615805337 5818997.8869776 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-eaca9e9d-bc09-45f2-a7f2-edf21b6db795">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389662.096388072 5818995.89959266 35 389665.196294824 5819012.24711806 35 389665.196294824 5819012.24711806 41.2000159074514 389662.096388072 5818995.89959266 41.2000159074514 389662.096388072 5818995.89959266 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_588de59a-8283-4de9-858e-a83e3873f020">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389665.196294824 5819012.24711806 35 389660.135797726 5819013.20671718 35 389660.135797726 5819013.20671718 41.2000159074514 389665.196294824 5819012.24711806 41.2000159074514 389665.196294824 5819012.24711806 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_d0b3a5fc-bec4-4b3d-87ee-a7e986ba928e">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389660.135797726 5819013.20671718 35 389659.114260156 5819013.40042673 35 389659.114260156 5819013.40042673 41.2000159074514 389660.135797726 5819013.20671718 41.2000159074514 389660.135797726 5819013.20671718 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-60fd02d9-d3a5-49aa-901b-a0681d25d9f7">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389659.114260156 5819013.40042673 35 389663.712979977 5819015.45973485 35 389663.712979977 5819015.45973485 73.8975467930565 389659.114260156 5819013.40042673 73.8975467930565 389659.114260156 5819013.40042673 41.2000159074514 389659.114260156 5819013.40042673 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-4612e66e-c85b-443d-9d93-49f97875ba66">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389663.712979977 5819015.45973485 35 389674.245495711 5819009.19084383 35 389674.245495711 5819009.19084383 39.2774775529181 389674.245495711 5819009.19084383 73.8975467930565 389663.712979977 5819015.45973485 73.8975467930565 389663.712979977 5819015.45973485 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_1f41e0fa-e488-4cdb-9428-e4d8870a2c99">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389674.245495711 5819009.19084383 35 389673.238178017 5819007.49842492 35 389673.238178017 5819007.49842492 39.2774775529181 389674.245495711 5819009.19084383 39.2774775529181 389674.245495711 5819009.19084383 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_44ecef6b-3d01-4648-aa61-e6b765f41e5b">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389673.238178017 5819007.49842492 35 389667.839549055 5819009.84994775 35 389667.839549055 5819009.84994775 39.2774775529181 389673.238178017 5819007.49842492 39.2774775529181 389673.238178017 5819007.49842492 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_708143e5-22da-4843-be50-a393850658dc">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389667.839549055 5819009.84994775 35 389665.527950203 5819004.54297627 35 389665.527950203 5819004.54297627 39.2774775529181 389667.839549055 5819009.84994775 39.2774775529181 389667.839549055 5819009.84994775 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_6f3a997c-d8c2-49b0-8bb3-e01fd22d0a65">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389665.527950203 5819004.54297627 35 389667.104459876 5819003.8562837 35 389667.104459876 5819003.8562837 39.2774775529181 389665.527950203 5819004.54297627 39.2774775529181 389665.527950203 5819004.54297627 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_68ac5268-1256-413f-86c3-48f67d4150aa">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389667.104459876 5819003.8562837 35 389662.205786908 5818992.60990352 35 389662.205786908 5818992.60990352 39.2774775529181 389667.104459876 5819003.8562837 39.2774775529181 389667.104459876 5819003.8562837 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-c50081d3-b76c-4d29-9683-0f4f0465f30b">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389662.205786908 5818992.60990352 35 389673.241354754 5818987.803056 35 389673.241354754 5818987.803056 39.2774775529181 389662.205786908 5818992.60990352 39.2774775529181 389662.205786908 5818992.60990352 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_e0d88c74-043b-434b-b96a-ede6941546fb">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389673.241354754 5818987.803056 35 389680.427671732 5819004.30141272 35 389680.427671732 5819004.30141272 39.2774775529181 389673.241354754 5818987.803056 39.2774775529181 389673.241354754 5818987.803056 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_bfc16510-7aaf-49c5-8175-5a371f489cc1">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389680.427671732 5819004.30141272 35 389676.912316913 5819005.83262315 35 389676.912316913 5819005.83262315 39.2774775529181 389680.427671732 5819004.30141272 39.2774775529181 389680.427671732 5819004.30141272 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_f2886761-eb5c-4b42-8b5d-a2081f234703">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389676.912316913 5819005.83262315 35 389677.571062678 5819007.34497249 35 389677.571062678 5819007.34497249 39.2774775529181 389676.912316913 5819005.83262315 39.2774775529181 389676.912316913 5819005.83262315 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-365b194b-bac7-4e25-98a7-540ed588a002">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389677.571062678 5819007.34497249 35 389682.200917388 5819004.49131997 35 389682.200917388 5819004.49131997 48.60083522169 389682.200917388 5819004.49131997 73.8975467930565 389677.571062678 5819007.34497249 73.8975467930565 389677.571062678 5819007.34497249 39.2774775529181 389677.571062678 5819007.34497249 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_7ec3c367-1264-46fe-82a7-e8e78af18c67">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389682.200917388 5819004.49131997 35 389679.258088286 5818999.71678228 35 389679.258088286 5818999.71678228 48.60083522169 389682.200917388 5819004.49131997 48.60083522169 389682.200917388 5819004.49131997 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-6d934b17-0f53-4eaf-89bc-c7cd168f6c0f">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389679.258088286 5818999.71678228 35 389681.658123288 5818998.23749922 35 389681.658123288 5818998.23749922 44.3998857764866 389681.658123288 5818998.23749922 48.60083522169 389679.258088286 5818999.71678228 48.60083522169 389679.258088286 5818999.71678228 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_ce0299f5-1865-4f06-94e2-34654cf5dce4">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389681.658123288 5818998.23749922 35 389673.139131705 5818984.41602077 35 389673.139131705 5818984.41602077 44.3998857764866 389681.658123288 5818998.23749922 44.3998857764866 389681.658123288 5818998.23749922 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_9c68ed09-fb45-49e1-803e-0772e49df430">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389673.139131705 5818984.41602077 35 389673.683820523 5818984.08029697 35 389673.683820523 5818984.08029697 44.3998857764866 389673.139131705 5818984.41602077 44.3998857764866 389673.139131705 5818984.41602077 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_e8755580-4d3d-484a-ba40-f650562abc6c">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389673.683820523 5818984.08029697 35 389672.760580078 5818982.58240271 35 389672.760580078 5818982.58240271 44.3998857764866 389673.683820523 5818984.08029697 44.3998857764866 389673.683820523 5818984.08029697 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-f613808b-75b8-4001-a782-ead2006e0228">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389672.760580078 5818982.58240271 35 389685.909708505 5818974.4778207 35 389685.909708505 5818974.4778207 44.3998857764866 389672.760580078 5818982.58240271 44.3998857764866 389672.760580078 5818982.58240271 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-0308cfd8-cfd2-433d-bd70-0e18bd00d89d">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389685.909708505 5818974.4778207 35 389687.01130214 5818976.26508085 35 389687.01130214 5818976.26508085 44.3998857764866 389685.909708505 5818974.4778207 44.3998857764866 389685.909708505 5818974.4778207 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-c19aec75-35f2-4aa4-acaf-7cf678be5b4a">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389687.01130214 5818976.26508085 35 389687.624077046 5818975.8873916 35 389687.624077046 5818975.8873916 44.3998857764866 389687.01130214 5818976.26508085 44.3998857764866 389687.01130214 5818976.26508085 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-53938a91-436e-4dd6-b052-19fe73d9708f">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389687.624077046 5818975.8873916 35 389688.389946907 5818977.12996294 35 389688.389946907 5818977.12996294 44.3998857764866 389687.624077046 5818975.8873916 44.3998857764866 389687.624077046 5818975.8873916 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-51f8badd-fd21-4761-b268-ea875862e1dc">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389688.389946907 5818977.12996294 35 389689.079318673 5818976.70506253 35 389689.079318673 5818976.70506253 44.3998857764866 389688.389946907 5818977.12996294 44.3998857764866 389688.389946907 5818977.12996294 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-ace31be7-8bed-4467-8cf5-ebe309ebcb04">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389689.079318673 5818976.70506253 35 389696.44950477 5818988.66268366 35 389696.44950477 5818988.66268366 44.3998857764866 389689.079318673 5818976.70506253 44.3998857764866 389689.079318673 5818976.70506253 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-d081a0fb-5885-4f59-aadd-de5d6a08567d">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389696.44950477 5818988.66268366 35 389702.4921459 5818984.93824798 35 389702.4921459 5818984.93824798 48.60083522169 389696.44950477 5818988.66268366 48.60083522169 389696.44950477 5818988.66268366 44.3998857764866 389696.44950477 5818988.66268366 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_6bdb82d6-ee64-4fb0-b7eb-8db153787b31">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389702.4921459 5818984.93824798 35 389702.938029037 5818985.66166276 35 389702.938029037 5818985.66166276 48.60083522169 389702.4921459 5818984.93824798 48.60083522169 389702.4921459 5818984.93824798 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_2e52dc5e-e631-4b46-88bf-69c8e7a21a13">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389702.938029037 5818985.66166276 35 389703.550803908 5818985.28397351 35 389703.550803908 5818985.28397351 48.60083522169 389702.938029037 5818985.66166276 48.60083522169 389702.938029037 5818985.66166276 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_0b49ac3a-ef39-49cb-bc63-7807cd0709be">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389703.550803908 5818985.28397351 35 389706.588054941 5818990.21170472 35 389706.588054941 5818990.21170472 48.60083522169 389703.550803908 5818985.28397351 48.60083522169 389703.550803908 5818985.28397351 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-d8c9e3f7-2b42-4d64-aa53-d74a9b82369f">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389706.588054941 5818990.21170472 35 389712.528566747 5818986.55021732 35 389712.528566747 5818986.55021732 73.8975467930565 389706.588054941 5818990.21170472 73.8975467930565 389706.588054941 5818990.21170472 48.60083522169 389706.588054941 5818990.21170472 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-d1383117-0ead-4cb2-a1ef-6211e4880baf">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389712.528566747 5818986.55021732 35 389727.666616355 5818987.47428098 35 389727.666616355 5818987.47428098 73.8975467930565 389712.528566747 5818986.55021732 73.8975467930565 389712.528566747 5818986.55021732 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-ce77f489-3cf2-44c1-bfc1-8d74f9478db5">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389727.666616355 5818987.47428098 35 389733.080241832 5818995.52014053 35 389733.080241832 5818995.52014053 73.8975467930565 389727.666616355 5818987.47428098 73.8975467930565 389727.666616355 5818987.47428098 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-969c1ef4-565a-4bc1-8eda-8678d88a0e51">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389733.080241832 5818995.52014053 35 389662.919489386 5819037.14606766 35 389662.919489386 5819037.14606766 73.8975467930565 389733.080241832 5818995.52014053 73.8975467930565 389733.080241832 5818995.52014053 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="fme-gen-d362d531-d16d-4cdc-83f7-b03e1f3451ff">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389631.029202839 5819036.14843452 74.1500784145367 389648.318748284 5819043.54130969 74.1500784145367 389641.91679563 5819048.8388685 74.1500784145367 389635.461644158 5819053.23615887 74.1500784145367 389628.990793463 5819056.78851764 74.1500784145367 389622.541743151 5819059.55128163 74.1500784145367 389616.151992836 5819061.57978769 74.1500784145367 389609.859042144 5819062.92937265 74.1500784145367 389603.700390704 5819063.65537334 74.1500784145367 389597.713538154 5819063.81312663 74.1500784145367 389591.935984138 5819063.45796935 74.1500784145367 389586.4052283 5819062.64523836 74.1500784145367 389581.158770291 5819061.43027053 74.1500784145367 389576.234109763 5819059.86840272 74.1500784145367 389571.668746368 5819058.01497181 74.1500784145367 389567.500179762 5819055.92531467 74.1500784145367 389563.765909597 5819053.65476818 74.1500784145367 389560.503435526 5819051.25866924 74.1500784145367 389557.7502572 5819048.79235474 74.1500784145367 389555.543874268 5819046.31116156 74.1500784145367 389553.921786375 5819043.87042661 74.1500784145367 389552.921493165 5819041.52548678 74.1500784145367 389553.572386952 5819041.13074625 74.1500784145367 389566.904492522 5819033.04536821 74.1500784145367 389567.684483834 5819032.57233532 74.1500784145367 389568.087472932 5819033.32756834 74.1500784145367 389568.525516702 5819034.06302381 74.1500784145367 389568.997646743 5819034.77707587 74.1500784145367 389569.502819297 5819035.46814591 74.1500784145367 389570.039917558 5819036.13470615 74.1500784145367 389570.607754141 5819036.77528302 74.1500784145367 389571.205073705 5819037.38846035 74.1500784145367 389571.830555732 5819037.97288257 74.1500784145367 389572.482817442 5819038.52725768 74.1500784145367 389573.160416853 5819039.05036009 74.1500784145367 389573.861855967 5819039.54103336 74.1500784145367 389574.585584086 5819039.99819274 74.1500784145367 389578.827647126 5819033.83465727 74.1500784145367 389579.659574271 5819034.41801317 74.1500784145367 389580.518468821 5819034.96088243 74.1500784145367 389581.402362078 5819035.46202071 74.1500784145367 389582.309228046 5819035.92027935 74.1500784145367 389583.23698807 5819036.33460797 74.1500784145367 389584.183515606 5819036.70405685 74.1500784145367 389585.14664109 5819037.02777919 74.1500784145367 389586.124156915 5819037.30503297 74.1500784145367 389587.113822489 5819037.53518268 74.1500784145367 389588.113369371 5819037.7177008 74.1500784145367 389589.120506472 5819037.85216897 74.1500784145367 389590.132925303 5819037.93827898 74.1500784145367 389589.445340923 5819044.00794009 74.1500784145367 389591.769891984 5819044.27127025 74.1500784145367 389590.959645389 5819051.42373505 74.1500784145367 389604.115099385 5819051.24041322 74.1500784145367 389602.991375563 5819044.40387253 74.1500784145367 389605.7033517 5819044.71109105 74.1500784145367 389604.422373698 5819037.87174664 74.1500784145367 389606.393531668 5819037.65795189 74.1500784145367 389608.352089184 5819037.34936745 74.1500784145367 389610.293490666 5819036.94671109 74.1500784145367 389612.213220438 5819036.45091938 74.1500784145367 389614.106813233 5819035.86314552 74.1500784145367 389615.96986458 5819035.18475669 74.1500784145367 389617.798041045 5819034.41733079 74.1500784145367 389619.587090314 5819033.56265285 74.1500784145367 389621.332851081 5819032.62271085 74.1500784145367 389623.031262731 5819031.59969108 74.1500784145367 389624.67837478 5819030.49597308 74.1500784145367 389626.270356068 5819029.31412406 74.1500784145367 389631.029202839 5819036.14843452 74.1500784145367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-09f7eacd-1be3-4b6d-8b19-c6eb9786a9de">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389631.029202839 5819036.14843452 74.1500784145367 389631.029202839 5819036.14843452 59.6500582003239 389648.318748284 5819043.54130969 59.6500582003239 389648.318748284 5819043.54130969 74.1500784145367 389631.029202839 5819036.14843452 74.1500784145367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-9d18795b-f223-459a-8f29-d602474c0460">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389648.318748284 5819043.54130969 74.1500784145367 389648.318748284 5819043.54130969 59.6500582003239 389641.91679563 5819048.8388685 59.6500582003239 389641.91679563 5819048.8388685 74.1500784145367 389648.318748284 5819043.54130969 74.1500784145367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-50f86a08-d7d3-4b53-b28c-d7025c29e944">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389641.91679563 5819048.8388685 74.1500784145367 389641.91679563 5819048.8388685 59.6500582003239 389640.525642277 5819049.78653103 59.6500582003239 389640.525642277 5819049.78653103 35 389635.461644158 5819053.23615887 35 389635.461644158 5819053.23615887 74.1500784145367 389641.91679563 5819048.8388685 74.1500784145367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-65e6f22a-4389-4100-aae5-a7c207980699">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389635.461644158 5819053.23615887 35 389628.990793463 5819056.78851764 35 389628.990793463 5819056.78851764 74.1500784145367 389635.461644158 5819053.23615887 74.1500784145367 389635.461644158 5819053.23615887 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-d66a94eb-7724-414c-ab07-ad5e0974cf4e">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389628.990793463 5819056.78851764 35 389622.541743151 5819059.55128163 35 389622.541743151 5819059.55128163 74.1500784145367 389628.990793463 5819056.78851764 74.1500784145367 389628.990793463 5819056.78851764 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-76b7687f-82bc-4c98-8fa6-3f3a480b9cec">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389622.541743151 5819059.55128163 35 389616.151992836 5819061.57978769 35 389616.151992836 5819061.57978769 74.1500784145367 389622.541743151 5819059.55128163 74.1500784145367 389622.541743151 5819059.55128163 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-47d070c1-c9d4-4d25-875f-1b7eece3962d">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389616.151992836 5819061.57978769 35 389609.859042144 5819062.92937265 35 389609.859042144 5819062.92937265 74.1500784145367 389616.151992836 5819061.57978769 74.1500784145367 389616.151992836 5819061.57978769 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-f01b9b66-45cd-4439-a5f0-511744b8efc5">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389609.859042144 5819062.92937265 35 389606.109077809 5819063.37142993 35 389606.109077809 5819063.37142993 56.8200436400873 389603.700390704 5819063.65537334 56.8200436400873 389603.700390704 5819063.65537334 74.1500784145367 389609.859042144 5819062.92937265 74.1500784145367 389609.859042144 5819062.92937265 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-9cd333e9-9ce3-467b-b223-6f070a61f16b">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389603.700390704 5819063.65537334 74.1500784145367 389603.700390704 5819063.65537334 56.8200436400873 389597.713538154 5819063.81312663 56.8200436400873 389597.713538154 5819063.81312663 74.1500784145367 389603.700390704 5819063.65537334 74.1500784145367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-ea16c6c1-bf74-4a01-89a7-7936bc20ece8">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389597.713538154 5819063.81312663 74.1500784145367 389597.713538154 5819063.81312663 56.8200436400873 389591.935984138 5819063.45796935 56.8200436400873 389591.935984138 5819063.45796935 74.1500784145367 389597.713538154 5819063.81312663 74.1500784145367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-047c2836-7302-48b7-b55d-d965ec673522">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389591.935984138 5819063.45796935 74.1500784145367 389591.935984138 5819063.45796935 56.8200436400873 389589.634690313 5819063.11979987 56.8200436400873 389589.634690313 5819063.11979987 35 389586.4052283 5819062.64523836 35 389586.4052283 5819062.64523836 74.1500784145367 389591.935984138 5819063.45796935 74.1500784145367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-9a2108ae-82c7-4c30-a151-f2a7ea76b1bc">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389586.4052283 5819062.64523836 35 389581.158770291 5819061.43027053 35 389581.158770291 5819061.43027053 74.1500784145367 389586.4052283 5819062.64523836 74.1500784145367 389586.4052283 5819062.64523836 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-b3b0c8d6-df8e-49f1-bb25-b37fa5b53006">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389581.158770291 5819061.43027053 35 389576.234109763 5819059.86840272 35 389576.234109763 5819059.86840272 74.1500784145367 389581.158770291 5819061.43027053 74.1500784145367 389581.158770291 5819061.43027053 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-78ba0bd1-7b37-4304-b266-8710e7b666be">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389576.234109763 5819059.86840272 35 389571.668746368 5819058.01497181 35 389571.668746368 5819058.01497181 74.1500784145367 389576.234109763 5819059.86840272 74.1500784145367 389576.234109763 5819059.86840272 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-858fe307-f067-4c7d-83b5-820bb1ab44ec">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389571.668746368 5819058.01497181 35 389567.500179762 5819055.92531467 35 389567.500179762 5819055.92531467 74.1500784145367 389571.668746368 5819058.01497181 74.1500784145367 389571.668746368 5819058.01497181 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-b359dbf3-e108-4c41-8a78-f6420b710fd9">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389567.500179762 5819055.92531467 35 389563.765909597 5819053.65476818 35 389563.765909597 5819053.65476818 74.1500784145367 389567.500179762 5819055.92531467 74.1500784145367 389567.500179762 5819055.92531467 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-08fcde34-900a-4818-bf59-052ca0c0da3c">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389563.765909597 5819053.65476818 35 389560.503435526 5819051.25866924 35 389560.503435526 5819051.25866924 74.1500784145367 389563.765909597 5819053.65476818 74.1500784145367 389563.765909597 5819053.65476818 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-7ed5eac7-ea4f-4757-b163-93a119a8fd6d">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389560.503435526 5819051.25866924 35 389557.7502572 5819048.79235474 35 389557.7502572 5819048.79235474 74.1500784145367 389560.503435526 5819051.25866924 74.1500784145367 389560.503435526 5819051.25866924 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-3c280dd5-4e06-422f-ac9d-9e1f03f173e9">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389557.7502572 5819048.79235474 35 389555.543874268 5819046.31116156 35 389555.543874268 5819046.31116156 74.1500784145367 389557.7502572 5819048.79235474 74.1500784145367 389557.7502572 5819048.79235474 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-dbb868a5-6176-4371-9932-4f451837f058">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389555.543874268 5819046.31116156 35 389553.921786375 5819043.87042661 35 389553.921786375 5819043.87042661 74.1500784145367 389555.543874268 5819046.31116156 74.1500784145367 389555.543874268 5819046.31116156 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-db72c43a-21df-4bf7-b247-5a663987cbdc">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389553.921786375 5819043.87042661 35 389552.921493165 5819041.52548678 35 389552.921493165 5819041.52548678 74.1500784145367 389553.921786375 5819043.87042661 74.1500784145367 389553.921786375 5819043.87042661 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-4230d7b2-eb21-4cdc-9ead-edec6906cc0b">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389552.921493165 5819041.52548678 35 389553.572386952 5819041.13074625 35 389553.572387207 5819041.13074738 59.3800487600975 389553.572386952 5819041.13074625 74.1500784145367 389552.921493165 5819041.52548678 74.1500784145367 389552.921493165 5819041.52548678 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-879fbb12-1e7a-4015-9f9e-25cf37edc5b1">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389566.904492522 5819033.04536821 35 389567.684483834 5819032.57233532 35 389567.684483834 5819032.57233532 74.1500784145367 389566.904492522 5819033.04536821 74.1500784145367 389566.904492777 5819033.04536935 59.3800487600975 389566.904492522 5819033.04536821 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_87793415-3309-4d63-a2ab-14d14e8362ca">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389567.684483834 5819032.57233532 35 389568.087472932 5819033.32756834 35 389568.087472932 5819033.32756834 74.1500784145367 389567.684483834 5819032.57233532 74.1500784145367 389567.684483834 5819032.57233532 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_a44f8ab7-fd05-4175-a911-f5b84c3e98dc">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389568.087472932 5819033.32756834 35 389568.525516702 5819034.06302381 35 389568.525516702 5819034.06302381 74.1500784145367 389568.087472932 5819033.32756834 74.1500784145367 389568.087472932 5819033.32756834 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_68a26885-e892-43ec-bd7b-bc3c182ed601">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389568.525516702 5819034.06302381 35 389568.997646743 5819034.77707587 35 389568.997646743 5819034.77707587 74.1500784145367 389568.525516702 5819034.06302381 74.1500784145367 389568.525516702 5819034.06302381 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_d437ac88-3ec2-41e3-ba40-117ed6b62783">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389568.997646743 5819034.77707587 35 389569.502819297 5819035.46814591 35 389569.502819297 5819035.46814591 74.1500784145367 389568.997646743 5819034.77707587 74.1500784145367 389568.997646743 5819034.77707587 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_5de88091-03ad-4840-ad20-fbaf3ad2e9d7">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389569.502819297 5819035.46814591 35 389570.039917558 5819036.13470615 35 389570.039917558 5819036.13470615 74.1500784145367 389569.502819297 5819035.46814591 74.1500784145367 389569.502819297 5819035.46814591 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_568a7839-a3b0-4719-956c-020466449b53">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389570.039917558 5819036.13470615 35 389570.607754141 5819036.77528302 35 389570.607754141 5819036.77528302 74.1500784145367 389570.039917558 5819036.13470615 74.1500784145367 389570.039917558 5819036.13470615 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_597a6f9f-ff6a-40b1-8512-8cb5605ed64b">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389570.607754141 5819036.77528302 35 389571.205073705 5819037.38846035 35 389571.205073705 5819037.38846035 74.1500784145367 389570.607754141 5819036.77528302 74.1500784145367 389570.607754141 5819036.77528302 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_5278ab40-33fd-481b-a6ee-b99254eb7a0f">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389571.205073705 5819037.38846035 35 389571.830555732 5819037.97288257 35 389571.830555732 5819037.97288257 74.1500784145367 389571.205073705 5819037.38846035 74.1500784145367 389571.205073705 5819037.38846035 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_64de02b2-4fc9-41d4-8e52-47e20d0eabca">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389571.830555732 5819037.97288257 35 389572.482817442 5819038.52725768 35 389572.482817442 5819038.52725768 74.1500784145367 389571.830555732 5819037.97288257 74.1500784145367 389571.830555732 5819037.97288257 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_89a34688-a185-43d1-8720-c0882d9a0965">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389572.482817442 5819038.52725768 35 389573.160416853 5819039.05036009 35 389573.160416853 5819039.05036009 74.1500784145367 389572.482817442 5819038.52725768 74.1500784145367 389572.482817442 5819038.52725768 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_05680c7f-fe03-47af-95c5-9ebf4a4b4deb">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389573.160416853 5819039.05036009 35 389573.861855967 5819039.54103336 35 389573.861855967 5819039.54103336 74.1500784145367 389573.160416853 5819039.05036009 74.1500784145367 389573.160416853 5819039.05036009 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_52c5beb6-0f96-4bac-8ecf-872111251b2f">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389573.861855967 5819039.54103336 35 389574.585584086 5819039.99819274 35 389574.585584086 5819039.99819274 74.1500784145367 389573.861855967 5819039.54103336 74.1500784145367 389573.861855967 5819039.54103336 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-75385dd8-d2ba-4951-b8bc-370ff9b9ce09">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389574.585584086 5819039.99819274 35 389578.827647126 5819033.83465727 35 389578.827647126 5819033.83465727 74.1500784145367 389574.585584086 5819039.99819274 74.1500784145367 389574.585584086 5819039.99819274 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_e182d9c3-4294-4a72-96eb-7d03dd5b20c8">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389578.827647126 5819033.83465727 35 389579.659574271 5819034.41801317 35 389579.659574271 5819034.41801317 74.1500784145367 389578.827647126 5819033.83465727 74.1500784145367 389578.827647126 5819033.83465727 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_aef6c526-308f-4d8a-af16-574072a1b641">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389579.659574271 5819034.41801317 35 389580.518468821 5819034.96088243 35 389580.518468821 5819034.96088243 74.1500784145367 389579.659574271 5819034.41801317 74.1500784145367 389579.659574271 5819034.41801317 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_de4d36c5-d378-4803-abb4-bbcd4d238342">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389580.518468821 5819034.96088243 35 389581.402362078 5819035.46202071 35 389581.402362078 5819035.46202071 74.1500784145367 389580.518468821 5819034.96088243 74.1500784145367 389580.518468821 5819034.96088243 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_5932c15e-b9cf-4f2d-a9ed-f5db8482585a">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389581.402362078 5819035.46202071 35 389582.309228046 5819035.92027935 35 389582.309228046 5819035.92027935 74.1500784145367 389581.402362078 5819035.46202071 74.1500784145367 389581.402362078 5819035.46202071 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_d7d23a10-0234-4767-bfc9-b3c2a926a995">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389582.309228046 5819035.92027935 35 389583.23698807 5819036.33460797 35 389583.23698807 5819036.33460797 74.1500784145367 389582.309228046 5819035.92027935 74.1500784145367 389582.309228046 5819035.92027935 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_d7323ed2-0ccd-40cc-8416-dc66cfe554fe">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389583.23698807 5819036.33460797 35 389584.183515606 5819036.70405685 35 389584.183515606 5819036.70405685 74.1500784145367 389583.23698807 5819036.33460797 74.1500784145367 389583.23698807 5819036.33460797 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_3e433a54-c0df-4868-93be-e7c40586fd23">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389584.183515606 5819036.70405685 35 389585.14664109 5819037.02777919 35 389585.14664109 5819037.02777919 74.1500784145367 389584.183515606 5819036.70405685 74.1500784145367 389584.183515606 5819036.70405685 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_64f40864-994f-4732-8fc2-710fa0610c46">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389585.14664109 5819037.02777919 35 389586.124156915 5819037.30503297 35 389586.124156915 5819037.30503297 74.1500784145367 389585.14664109 5819037.02777919 74.1500784145367 389585.14664109 5819037.02777919 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_13818dcf-89c5-49f9-8008-e486d2ff969f">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389586.124156915 5819037.30503297 35 389587.113822489 5819037.53518268 35 389587.113822489 5819037.53518268 74.1500784145367 389586.124156915 5819037.30503297 74.1500784145367 389586.124156915 5819037.30503297 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_04880f3a-d5da-4eba-9081-690a4115b0ce">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389587.113822489 5819037.53518268 35 389588.113369371 5819037.7177008 35 389588.113369371 5819037.7177008 74.1500784145367 389587.113822489 5819037.53518268 74.1500784145367 389587.113822489 5819037.53518268 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_91405f0b-089a-40b7-b01b-c55f4f9c84c6">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389588.113369371 5819037.7177008 35 389589.120506472 5819037.85216897 35 389589.120506472 5819037.85216897 74.1500784145367 389588.113369371 5819037.7177008 74.1500784145367 389588.113369371 5819037.7177008 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_515f6888-d413-4299-aa3f-e45a4e15380f">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389589.120506472 5819037.85216897 35 389590.132925303 5819037.93827898 35 389590.132925303 5819037.93827898 74.1500784145367 389589.120506472 5819037.85216897 74.1500784145367 389589.120506472 5819037.85216897 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_2c62985e-4381-4243-b344-a1f65200c8e8">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389590.132925303 5819037.93827898 35 389589.445340923 5819044.00794009 35 389589.445340923 5819044.00794009 74.1500784145367 389590.132925303 5819037.93827898 74.1500784145367 389590.132925303 5819037.93827898 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_fd02b130-62eb-4051-bac6-5df5c440f092">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389589.445340923 5819044.00794009 35 389591.769891984 5819044.27127025 35 389591.769891984 5819044.27127025 74.1500784145367 389589.445340923 5819044.00794009 74.1500784145367 389589.445340923 5819044.00794009 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-622ddb8d-17de-4563-922d-d0826898ac2d">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389591.769891984 5819044.27127025 35 389590.959645389 5819051.42373505 35 389590.959645389 5819051.42373505 56.8200436400873 389590.959645389 5819051.42373505 74.1500784145367 389591.769891984 5819044.27127025 74.1500784145367 389591.769891984 5819044.27127025 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_f2e88f24-2bf8-45d1-8824-6b0b0348746f">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389590.959645389 5819051.42373505 74.1500784145367 389590.959645389 5819051.42373505 56.8200436400873 389604.115099385 5819051.24041322 56.8200436400873 389604.115099385 5819051.24041322 74.1500784145367 389590.959645389 5819051.42373505 74.1500784145367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-4cba2008-4e0a-4a58-ac3a-cffb75458b54">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389604.115099385 5819051.24041322 35 389602.991375563 5819044.40387253 35 389602.991375563 5819044.40387253 74.1500784145367 389604.115099385 5819051.24041322 74.1500784145367 389604.115099385 5819051.24041322 56.8200436400873 389604.115099385 5819051.24041322 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-8794748b-7023-4209-8f7a-d90f5761f820">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389602.991375563 5819044.40387253 35 389605.7033517 5819044.71109105 35 389605.7033517 5819044.71109105 74.1500784145367 389602.991375563 5819044.40387253 74.1500784145367 389602.991375563 5819044.40387253 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-6a509d02-788f-463a-a325-2c890c841a35">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389605.7033517 5819044.71109105 35 389604.422373698 5819037.87174664 35 389604.422373698 5819037.87174664 74.1500784145367 389605.7033517 5819044.71109105 74.1500784145367 389605.7033517 5819044.71109105 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-6e9416fa-7508-40ce-8982-1885cabb64f6">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389604.422373698 5819037.87174664 35 389606.393531668 5819037.65795189 35 389606.393531668 5819037.65795189 74.1500784145367 389604.422373698 5819037.87174664 74.1500784145367 389604.422373698 5819037.87174664 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-a4a2e012-c4f9-44cc-88eb-3bcc964236a7">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389606.393531668 5819037.65795189 35 389608.352089184 5819037.34936745 35 389608.352089184 5819037.34936745 74.1500784145367 389606.393531668 5819037.65795189 74.1500784145367 389606.393531668 5819037.65795189 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-64e5976f-e832-42f1-aad7-1d868fe9c78e">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389608.352089184 5819037.34936745 35 389610.293490666 5819036.94671109 35 389610.293490666 5819036.94671109 74.1500784145367 389608.352089184 5819037.34936745 74.1500784145367 389608.352089184 5819037.34936745 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-ebda3e54-d12a-475e-b06d-d39741e5cfa8">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389610.293490666 5819036.94671109 35 389612.213220438 5819036.45091938 35 389612.213220438 5819036.45091938 74.1500784145367 389610.293490666 5819036.94671109 74.1500784145367 389610.293490666 5819036.94671109 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-1687e982-00f9-47be-a125-ac4f73d73c3c">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389612.213220438 5819036.45091938 35 389614.106813233 5819035.86314552 35 389614.106813233 5819035.86314552 74.1500784145367 389612.213220438 5819036.45091938 74.1500784145367 389612.213220438 5819036.45091938 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-bb4f93a4-06af-4f38-b4c4-4f7392a783ac">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389614.106813233 5819035.86314552 35 389615.96986458 5819035.18475669 35 389615.96986458 5819035.18475669 74.1500784145367 389614.106813233 5819035.86314552 74.1500784145367 389614.106813233 5819035.86314552 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-f68de211-f372-4e4f-94b7-ed15df940a01">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389615.96986458 5819035.18475669 35 389617.798041045 5819034.41733079 35 389617.798041045 5819034.41733079 74.1500784145367 389615.96986458 5819035.18475669 74.1500784145367 389615.96986458 5819035.18475669 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-ebd54494-509a-43c2-a748-8ea436fdc574">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389617.798041045 5819034.41733079 35 389619.587090314 5819033.56265285 35 389619.587090314 5819033.56265285 74.1500784145367 389617.798041045 5819034.41733079 74.1500784145367 389617.798041045 5819034.41733079 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-5086969b-0b9d-4437-bd08-4536a7c6a2a5">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389619.587090314 5819033.56265285 35 389621.332851081 5819032.62271085 35 389621.332851081 5819032.62271085 74.1500784145367 389619.587090314 5819033.56265285 74.1500784145367 389619.587090314 5819033.56265285 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-45065cfe-4e7d-4b7c-8164-f5e59ed1c634">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389621.332851081 5819032.62271085 35 389623.031262731 5819031.59969108 35 389623.031262731 5819031.59969108 74.1500784145367 389621.332851081 5819032.62271085 74.1500784145367 389621.332851081 5819032.62271085 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-fa065a4a-bd7b-4118-b862-aac226a6f9f3">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389623.031262731 5819031.59969108 35 389624.67837478 5819030.49597308 35 389624.67837478 5819030.49597308 74.1500784145367 389623.031262731 5819031.59969108 74.1500784145367 389623.031262731 5819031.59969108 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-b25e48fa-0763-4021-bdb6-63e88fb5b809">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389624.67837478 5819030.49597308 35 389626.270356068 5819029.31412406 35 389626.270356068 5819029.31412406 74.1500784145367 389624.67837478 5819030.49597308 74.1500784145367 389624.67837478 5819030.49597308 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-b6bfd025-6350-4a8c-8007-4374789c4706">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389626.270356068 5819029.31412406 35 389631.029202839 5819036.14843452 35 389631.029202839 5819036.14843452 59.6500582003239 389631.029202839 5819036.14843452 74.1500784145367 389626.270356068 5819029.31412406 74.1500784145367 389626.270356068 5819029.31412406 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-ccedde38-f61a-4f65-8527-b7c7bd0d79f8">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389560.195114673 5818983.38681057 35 389561.001612858 5818982.28306285 35 389561.001612858 5818982.28306285 74.1500783001567 389560.195114673 5818983.38681057 74.1500783001567 389560.195114673 5818983.38681057 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-73b0e034-3896-42e9-b6be-2390fb727784">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389561.001612858 5818982.28306285 35 389565.027111937 5818977.69225627 35 389565.027111937 5818977.69225627 74.1500783001567 389563.327576635 5818979.63046011 74.1500783001567 389561.001612858 5818982.28306285 74.1500783001567 389561.001612858 5818982.28306285 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-7e587f36-502e-4fb3-86e4-86f6d62c9314">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389565.027111937 5818977.69225627 35 389569.674650645 5818973.25377331 35 389569.674650645 5818973.25377331 74.1500783001567 389565.027111937 5818977.69225627 74.1500783001567 389565.027111937 5818977.69225627 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-bb6aabdf-4b9e-4849-a145-29d077eaca2d">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389569.674650645 5818973.25377331 35 389574.976128217 5818969.0092217 35 389574.976128217 5818969.0092217 74.1500783001567 389569.674650645 5818973.25377331 74.1500783001567 389569.674650645 5818973.25377331 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-a0f64bbe-96cd-4265-b4d5-7d45d8dcf0f8">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389574.976128217 5818969.0092217 35 389580.963443878 5818965.00020917 35 389580.963443878 5818965.00020917 74.1500783001567 389574.976128217 5818969.0092217 74.1500783001567 389574.976128217 5818969.0092217 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-52fc41e5-33f3-4d11-9cb6-d6ed46117cef">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389580.963443878 5818965.00020917 35 389587.668496849 5818961.26834343 35 389587.668496849 5818961.26834343 59.1400482800966 389587.668496849 5818961.26834343 74.1500783001567 389580.963443878 5818965.00020917 74.1500783001567 389580.963443878 5818965.00020917 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-13e4d83a-afdd-40dd-98bb-bf4a84a0ceee">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389594.334988224 5818975.15858579 35 389596.635882072 5818979.95270752 35 389596.635882072 5818979.95270752 74.1500783001567 389594.334988224 5818975.15858579 74.1500783001567 389594.334988224 5818975.15858579 59.1400482800966 389594.334988224 5818975.15858579 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_aeb9ce41-021d-44a6-8473-833884cade34">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389596.635882072 5818979.95270752 35 389595.576926021 5818980.3742882 35 389595.576926021 5818980.3742882 74.1500783001567 389596.635882072 5818979.95270752 74.1500783001567 389596.635882072 5818979.95270752 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_aca39feb-1604-4764-a479-4ccbd38940be">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389595.576926021 5818980.3742882 35 389594.533469173 5818980.83289043 35 389594.533469173 5818980.83289043 74.1500783001567 389595.576926021 5818980.3742882 74.1500783001567 389595.576926021 5818980.3742882 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_1ab521ef-1ee8-455d-8738-23634fbc7a6d">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389594.533469173 5818980.83289043 35 389593.506805343 5818981.32794557 35 389593.506805343 5818981.32794557 74.1500783001567 389594.533469173 5818980.83289043 74.1500783001567 389594.533469173 5818980.83289043 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_dbca375a-b25d-4499-993b-c83452390036">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389593.506805343 5818981.32794557 35 389592.498207529 5818981.85883978 35 389592.498207529 5818981.85883978 74.1500783001567 389593.506805343 5818981.32794557 74.1500783001567 389593.506805343 5818981.32794557 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_e0cb24ef-06ff-4b68-963e-7abf60ea0951">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389592.498207529 5818981.85883978 35 389591.508926322 5818982.42491479 35 389591.508926322 5818982.42491479 74.1500783001567 389592.498207529 5818981.85883978 74.1500783001567 389592.498207529 5818981.85883978 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_2c454689-de90-4f8d-a546-3c024f180151">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389591.508926322 5818982.42491479 35 389590.540188367 5818983.0254687 35 389590.540188367 5818983.0254687 74.1500783001567 389591.508926322 5818982.42491479 74.1500783001567 389591.508926322 5818982.42491479 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_f4b79b4e-6324-427b-96e9-4052fedfdf29">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389590.540188367 5818983.0254687 35 389589.593194835 5818983.65975688 35 389589.593194835 5818983.65975688 74.1500783001567 389590.540188367 5818983.0254687 74.1500783001567 389590.540188367 5818983.0254687 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_91291cf8-4346-4906-8841-83c530c462a8">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389589.593194835 5818983.65975688 35 389588.669119933 5818984.32699283 35 389588.669119933 5818984.32699283 74.1500783001567 389589.593194835 5818983.65975688 74.1500783001567 389589.593194835 5818983.65975688 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_270c526b-6ab0-4144-9c87-0d689913d65b">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389588.669119933 5818984.32699283 35 389587.769109454 5818985.02634924 35 389587.769109454 5818985.02634924 74.1500783001567 389588.669119933 5818984.32699283 74.1500783001567 389588.669119933 5818984.32699283 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_06edce20-92a9-468a-84be-50beb297255a">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389587.769109454 5818985.02634924 35 389586.894279352 5818985.75695895 35 389586.894279352 5818985.75695895 74.1500783001567 389587.769109454 5818985.02634924 74.1500783001567 389587.769109454 5818985.02634924 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_93ea680e-b434-4a94-9477-568d2498abc5">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389586.894279352 5818985.75695895 35 389586.045714357 5818986.51791605 35 389586.045714357 5818986.51791605 74.1500783001567 389586.894279352 5818985.75695895 74.1500783001567 389586.894279352 5818985.75695895 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_5e9bacf3-2741-4165-b085-80d9a2d0b403">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389586.045714357 5818986.51791605 35 389585.224466633 5818987.30827701 35 389585.224466633 5818987.30827701 74.1500783001567 389586.045714357 5818986.51791605 74.1500783001567 389586.045714357 5818986.51791605 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_4539e22d-b5b9-4414-a514-d0820577797a">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389585.224466633 5818987.30827701 35 389582.209917453 5818983.4849781 35 389582.209917453 5818983.4849781 74.1500783001567 389585.224466633 5818987.30827701 74.1500783001567 389585.224466633 5818987.30827701 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_dafdbc06-dc25-42e3-9c7a-77bdd40203ab">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389582.209917453 5818983.4849781 35 389575.879938891 5818989.78074407 35 389575.879938891 5818989.78074407 74.1500783001567 389581.023843906 5818984.66464108 74.1500783001567 389582.209917453 5818983.4849781 74.1500783001567 389582.209917453 5818983.4849781 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-becb9dae-6590-4bfb-9aff-b3e1ea6375b0">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389575.879938891 5818989.78074407 35 389594.801599708 5819005.08146698 35 389594.801599708 5819005.08146698 74.1500783001567 389575.879938891 5818989.78074407 74.1500783001567 389575.879938891 5818989.78074407 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-41e48bd4-a4c9-49da-be73-70f3f0b35243">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389594.801599708 5819005.08146698 35 389586.75550641 5819024.01784527 35 389586.75550641 5819024.01784527 74.1500783001567 389594.801599708 5819005.08146698 74.1500783001567 389594.801599708 5819005.08146698 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-7d4d577d-4c1d-455e-bdb2-c80219b0baf5">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389586.75550641 5819024.01784527 35 389563.605065882 5819024.43777322 35 389563.605066137 5819024.43777435 59.3800487600975 389563.605065882 5819024.43777322 74.1500783001567 389586.75550641 5819024.01784527 74.1500783001567 389586.75550641 5819024.01784527 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_dbec628f-a233-4a15-85e6-5091a00fc2ec">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389553.572386952 5819041.13074625 74.1500784145367 389553.572387207 5819041.13074738 59.3800487600975 389566.904492777 5819033.04536935 59.3800487600975 389566.904492522 5819033.04536821 74.1500784145367 389553.572386952 5819041.13074625 74.1500784145367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-ae772924-21bd-45e0-970b-41789742851f">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389547.720747059 5819025.50242012 34.9999998856199 389547.500527998 5819023.58961106 35 389547.500527998 5819023.58961106 74.1500783001567 389547.720747059 5819025.50242012 74.1500783001567 389547.940966129 5819027.41522918 74.1500783001567 389547.940966384 5819027.41523031 59.3800487600975 389547.720747314 5819025.50242125 59.3800486457174 389547.720747059 5819025.50242012 34.9999998856199</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-a7cb9f01-50e6-4b1e-a341-7643b4bbf1ae">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389547.500527998 5819023.58961106 35 389547.363136871 5819019.50023935 35 389547.363136871 5819019.50023935 74.1500783001567 389547.381262197 5819020.03972986 74.1500783001567 389547.500527998 5819023.58961106 74.1500783001567 389547.500527998 5819023.58961106 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-5fd7c8d4-f241-4e49-b9d2-441edb231005">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389547.363136871 5819019.50023935 35 389547.544753698 5819015.53656575 35 389547.544753698 5819015.53656575 74.1500783001567 389547.363136871 5819019.50023935 74.1500783001567 389547.363136871 5819019.50023935 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-174823bd-1c42-495a-9959-a810a2bfe85d">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389547.544753698 5819015.53656575 35 389546.196366784 5819015.47478221 35 389546.196366784 5819015.47478221 74.1500783001567 389547.544753698 5819015.53656575 74.1500783001567 389547.544753698 5819015.53656575 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-1f1e522d-3553-4521-b34e-d3f4faf09274">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389546.196366784 5819015.47478221 35 389546.214753047 5819015.07351358 35 389546.214753047 5819015.07351358 74.1500783001567 389546.196366784 5819015.47478221 74.1500783001567 389546.196366784 5819015.47478221 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-80db5fe1-3a0f-4e24-8b6a-f2e11fbb3dc3">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389546.214753047 5819015.07351358 35 389546.792560916 5819010.47475055 35 389546.792560916 5819010.47475055 74.1500783001567 389546.214753047 5819015.07351358 74.1500783001567 389546.214753047 5819015.07351358 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-d7370316-5f51-484b-aa79-9602883fb269">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389546.792560916 5819010.47475055 35 389547.777935612 5819005.73688246 35 389547.777935612 5819005.73688246 74.1500783001567 389546.792560916 5819010.47475055 74.1500783001567 389546.792560916 5819010.47475055 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-4351ce8a-51d9-4ed5-83a1-affbb500e630">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389547.777935612 5819005.73688246 35 389549.203246215 5819000.90164155 35 389549.203246215 5819000.90164155 74.1500783001567 389547.777935612 5819005.73688246 74.1500783001567 389547.777935612 5819005.73688246 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-617d14aa-8fa3-473e-b77a-80f78571ea91">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389549.203246215 5819000.90164155 35 389551.100841482 5818996.01127926 35 389551.100841482 5818996.01127926 74.1500783001567 389549.203246215 5819000.90164155 74.1500783001567 389549.203246215 5819000.90164155 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-47214be0-efde-4b34-bb4d-6dff6133ce51">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389551.100841482 5818996.01127926 35 389553.502883722 5818991.10850873 35 389553.502883722 5818991.10850873 74.1500783001567 389551.100841482 5818996.01127926 74.1500783001567 389551.100841482 5818996.01127926 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-66e29104-27c5-4a17-8555-1d129f81a2df">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389553.502883722 5818991.10850873 35 389556.441197887 5818986.23640622 35 389556.441197887 5818986.23640622 74.1500783001567 389553.502883722 5818991.10850873 74.1500783001567 389553.502883722 5818991.10850873 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-2133fd3b-fa51-4e0b-83fc-450ddec80cfe">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389556.441197887 5818986.23640622 35 389559.105256072 5818982.59046104 35 389559.105256072 5818982.59046104 74.1500783001567 389556.441197887 5818986.23640622 74.1500783001567 389556.441197887 5818986.23640622 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-b6d45d99-6e31-40f2-b430-ea4eb95b2e62">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389559.105256072 5818982.59046104 35 389560.195114673 5818983.38681057 35 389560.195114673 5818983.38681057 74.1500783001567 389559.105256072 5818982.59046104 74.1500783001567 389559.105256072 5818982.59046104 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="fme-gen-d2a0d242-820f-432c-854b-b42636963ad4">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389677.67838862 5818958.07756462 74.1500783001566 389681.876100906 5818964.7559149 74.1500783001566 389649.916108334 5818983.59805962 74.1500783001566 389649.618930765 5818983.05651013 74.1500783001566 389649.302881483 5818982.52575292 74.1500783001566 389648.968351927 5818982.00644537 74.1500783001566 389648.615756426 5818981.49923065 74.1500783001566 389648.245531683 5818981.00473696 74.1500783001566 389647.858136237 5818980.52357677 74.1500783001566 389647.454049893 5818980.056346 74.1500783001566 389647.033773126 5818979.60362334 74.1500783001566 389646.597826468 5818979.16596951 74.1500783001566 389646.146749855 5818978.74392655 74.1500783001566 389645.681101965 5818978.33801719 74.1500783001566 389645.201459519 5818977.94874416 74.1500783001566 389642.005749324 5818982.70195574 74.1500783001567 389641.214683326 5818981.42715985 74.1500783001567 389640.125912432 5818980.28363913 74.1500783001567 389638.770974865 5818979.26706436 74.1500783001567 389637.181408846 5818978.37310631 74.1500783001566 389635.388752598 5818977.59743576 74.1500783001566 389633.424544344 5818976.9357235 74.1500783001566 389631.320322307 5818976.38364029 74.1500783001566 389629.107624713 5818975.93685693 74.1500783001566 389626.817989785 5818975.5910442 74.1500783001566 389624.482955749 5818975.34187286 74.1500783001566 389622.134060833 5818975.18501372 74.1500783001566 389619.802843263 5818975.11613753 74.1500783001566 389617.520841268 5818975.1309151 74.1500783001566 389615.319593078 5818975.22501719 74.1500783001566 389613.230636921 5818975.39411459 74.1500783001566 389611.285511029 5818975.63387808 74.1500783001566 389609.515753632 5818975.93997844 74.1500783001566 389607.952902964 5818976.30808644 74.1500783001566 389606.628497254 5818976.73387287 74.1500783001566 389605.574074735 5818977.2130085 74.1500783001566 389605.449305789 5818977.29980677 74.1500783001566 389605.36453492 5818976.99132171 74.1500783001566 389603.899588369 5818971.66031425 74.1500783001566 389594.334988224 5818975.15858579 74.1500783001567 389587.668496849 5818961.26834343 74.1500783001567 389582.41697492 5818950.32631791 74.1500783001566 389677.67838862 5818958.07756462 74.1500783001566</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-13e10eb1-93d4-4a90-9a41-54edf851ba63">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389677.67838862 5818958.07756462 35 389681.876100906 5818964.7559149 35 389681.876100906 5818964.7559149 74.1500783001566 389677.67838862 5818958.07756462 74.1500783001566 389677.67838862 5818958.07756462 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-c6518b3b-3233-4438-b3f3-ffe0d7fa2883">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389681.876100906 5818964.7559149 35 389649.916108334 5818983.59805962 35 389649.916108334 5818983.59805962 74.1500783001566 389681.876100906 5818964.7559149 74.1500783001566 389681.876100906 5818964.7559149 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_1fb052fe-e2d9-49d6-88b8-ecb55aad02cc">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389649.916108334 5818983.59805962 35 389649.618930765 5818983.05651013 35 389649.618930765 5818983.05651013 74.1500783001566 389649.916108334 5818983.59805962 74.1500783001566 389649.916108334 5818983.59805962 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_caa418df-08e2-4536-8ab9-8d8864ff6f48">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389649.618930765 5818983.05651013 35 389649.302881483 5818982.52575292 35 389649.302881483 5818982.52575292 74.1500783001566 389649.618930765 5818983.05651013 74.1500783001566 389649.618930765 5818983.05651013 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_2cb3718a-3ff2-4c8a-bc1d-eff2425b9807">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389649.302881483 5818982.52575292 35 389648.968351927 5818982.00644537 35 389648.968351927 5818982.00644537 74.1500783001566 389649.302881483 5818982.52575292 74.1500783001566 389649.302881483 5818982.52575292 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_cb3104c3-edc0-4400-ba61-d9a62a725af6">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389648.968351927 5818982.00644537 35 389648.615756426 5818981.49923065 35 389648.615756426 5818981.49923065 74.1500783001566 389648.968351927 5818982.00644537 74.1500783001566 389648.968351927 5818982.00644537 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_1997996f-f03b-4df4-8db0-88ecdc2aba18">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389648.615756426 5818981.49923065 35 389648.245531683 5818981.00473696 35 389648.245531683 5818981.00473696 74.1500783001566 389648.615756426 5818981.49923065 74.1500783001566 389648.615756426 5818981.49923065 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_df134634-d25a-4b72-a31d-7642b14177c8">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389648.245531683 5818981.00473696 35 389647.858136237 5818980.52357677 35 389647.858136237 5818980.52357677 74.1500783001566 389648.245531683 5818981.00473696 74.1500783001566 389648.245531683 5818981.00473696 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_08a6dafa-a6da-4309-871e-850a0a2cc136">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389647.858136237 5818980.52357677 35 389647.454049893 5818980.056346 35 389647.454049893 5818980.056346 74.1500783001566 389647.858136237 5818980.52357677 74.1500783001566 389647.858136237 5818980.52357677 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_0c4e8f9a-a43e-4141-9ab1-0ea30f600f63">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389647.454049893 5818980.056346 35 389647.033773126 5818979.60362334 35 389647.033773126 5818979.60362334 74.1500783001566 389647.454049893 5818980.056346 74.1500783001566 389647.454049893 5818980.056346 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_c2a16252-5ce7-4ac8-a2eb-6e2849b75ae0">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389647.033773126 5818979.60362334 35 389646.597826468 5818979.16596951 35 389646.597826468 5818979.16596951 74.1500783001566 389647.033773126 5818979.60362334 74.1500783001566 389647.033773126 5818979.60362334 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_4f8f3acf-7321-43f2-9d21-3d520c653196">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389646.597826468 5818979.16596951 35 389646.146749855 5818978.74392655 35 389646.146749855 5818978.74392655 74.1500783001566 389646.597826468 5818979.16596951 74.1500783001566 389646.597826468 5818979.16596951 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_86e348a1-fc62-4e0a-9d9d-6e35b7f8a36f">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389646.146749855 5818978.74392655 35 389645.681101965 5818978.33801719 35 389645.681101965 5818978.33801719 74.1500783001566 389646.146749855 5818978.74392655 74.1500783001566 389646.146749855 5818978.74392655 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_eefeefec-9263-4548-94e0-03779e8f38a0">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389645.681101965 5818978.33801719 35 389645.201459519 5818977.94874416 35 389645.201459519 5818977.94874416 74.1500783001566 389645.681101965 5818978.33801719 74.1500783001566 389645.681101965 5818978.33801719 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-c898cede-7c9d-48d3-91d6-14f13de959b6">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389645.201459519 5818977.94874416 35 389642.005749324 5818982.70195574 35 389642.005749324 5818982.70195574 74.1500783001567 389645.201459519 5818977.94874416 74.1500783001566 389645.201459519 5818977.94874416 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_5ae7267b-4093-431d-aaec-3211c8362aa5">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389642.005749324 5818982.70195574 35 389641.214683326 5818981.42715985 35 389641.214683326 5818981.42715985 74.1500783001567 389642.005749324 5818982.70195574 74.1500783001567 389642.005749324 5818982.70195574 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_04826b2a-e337-47c7-a0c9-a45608802f0b">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389641.214683326 5818981.42715985 35 389640.125912432 5818980.28363913 35 389640.125912432 5818980.28363913 74.1500783001567 389641.214683326 5818981.42715985 74.1500783001567 389641.214683326 5818981.42715985 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_b0f63fdb-21d0-4606-811f-7ab529c93533">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389640.125912432 5818980.28363913 35 389638.770974865 5818979.26706436 35 389638.770974865 5818979.26706436 74.1500783001567 389640.125912432 5818980.28363913 74.1500783001567 389640.125912432 5818980.28363913 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_2c3ac993-694e-4ab1-b1db-f1356a2249d9">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389638.770974865 5818979.26706436 35 389637.181408846 5818978.37310631 35 389637.181408846 5818978.37310631 74.1500783001566 389638.770974865 5818979.26706436 74.1500783001567 389638.770974865 5818979.26706436 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_6452864e-1957-41ea-b1e8-bd2f9a4eb2e6">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389637.181408846 5818978.37310631 35 389635.388752598 5818977.59743576 35 389635.388752598 5818977.59743576 74.1500783001566 389637.181408846 5818978.37310631 74.1500783001566 389637.181408846 5818978.37310631 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_0c501940-16b4-4e14-ab39-56a2f0f5a2fb">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389635.388752598 5818977.59743576 35 389633.424544344 5818976.9357235 35 389633.424544344 5818976.9357235 74.1500783001566 389635.388752598 5818977.59743576 74.1500783001566 389635.388752598 5818977.59743576 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_e520cbec-f4f8-4ed0-ab04-65853a21a27f">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389633.424544344 5818976.9357235 35 389631.320322307 5818976.38364029 35 389631.320322307 5818976.38364029 74.1500783001566 389633.424544344 5818976.9357235 74.1500783001566 389633.424544344 5818976.9357235 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_f9493d6f-45d5-4162-a5cf-696b0fa41aae">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389631.320322307 5818976.38364029 35 389629.107624713 5818975.93685693 35 389629.107624713 5818975.93685693 74.1500783001566 389631.320322307 5818976.38364029 74.1500783001566 389631.320322307 5818976.38364029 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_85a516c2-7fef-4c6d-8cd3-3dd9bba4c502">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389629.107624713 5818975.93685693 35 389626.817989785 5818975.5910442 35 389626.817989785 5818975.5910442 74.1500783001566 389629.107624713 5818975.93685693 74.1500783001566 389629.107624713 5818975.93685693 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_19aeee10-dcb5-4e55-9996-20ae4d125101">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389626.817989785 5818975.5910442 35 389624.482955749 5818975.34187286 35 389624.482955749 5818975.34187286 74.1500783001566 389626.817989785 5818975.5910442 74.1500783001566 389626.817989785 5818975.5910442 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_05938562-2a07-4059-827e-1d0adc7c5423">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389624.482955749 5818975.34187286 35 389622.134060833 5818975.18501372 35 389622.134060833 5818975.18501372 74.1500783001566 389624.482955749 5818975.34187286 74.1500783001566 389624.482955749 5818975.34187286 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_596f8fa2-665e-4404-a228-64794667bbcd">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389622.134060833 5818975.18501372 35 389619.802843263 5818975.11613753 35 389619.802843263 5818975.11613753 74.1500783001566 389622.134060833 5818975.18501372 74.1500783001566 389622.134060833 5818975.18501372 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_616bafbf-9d3f-45df-a3d2-d39b4c9df007">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389619.802843263 5818975.11613753 35 389617.520841268 5818975.1309151 35 389617.520841268 5818975.1309151 74.1500783001566 389619.802843263 5818975.11613753 74.1500783001566 389619.802843263 5818975.11613753 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_a2fd0467-720b-4b3b-8a79-ec81cbf33771">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389617.520841268 5818975.1309151 35 389615.319593078 5818975.22501719 35 389615.319593078 5818975.22501719 74.1500783001566 389617.520841268 5818975.1309151 74.1500783001566 389617.520841268 5818975.1309151 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_8fe08721-7b3f-4710-8df8-647716cd85a1">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389615.319593078 5818975.22501719 35 389613.230636921 5818975.39411459 35 389613.230636921 5818975.39411459 74.1500783001566 389615.319593078 5818975.22501719 74.1500783001566 389615.319593078 5818975.22501719 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_e6faa189-f967-4bf4-891e-0f16fe91e5b4">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389613.230636921 5818975.39411459 35 389611.285511029 5818975.63387808 35 389611.285511029 5818975.63387808 74.1500783001566 389613.230636921 5818975.39411459 74.1500783001566 389613.230636921 5818975.39411459 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_f98e2f42-9948-4e2a-9321-81b728bf53f4">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389611.285511029 5818975.63387808 35 389609.515753632 5818975.93997844 35 389609.515753632 5818975.93997844 74.1500783001566 389611.285511029 5818975.63387808 74.1500783001566 389611.285511029 5818975.63387808 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_2eb95982-7ae2-4d01-b145-4b92abbdcaca">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389609.515753632 5818975.93997844 35 389607.952902964 5818976.30808644 35 389607.952902964 5818976.30808644 74.1500783001566 389609.515753632 5818975.93997844 74.1500783001566 389609.515753632 5818975.93997844 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_0f32531b-687d-48b6-a28d-a5d187028989">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389607.952902964 5818976.30808644 35 389606.628497254 5818976.73387287 35 389606.628497254 5818976.73387287 74.1500783001566 389607.952902964 5818976.30808644 74.1500783001566 389607.952902964 5818976.30808644 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_760516a2-f981-4978-86a3-8d0d05a3e145">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389606.628497254 5818976.73387287 35 389605.574074735 5818977.2130085 35 389605.574074735 5818977.2130085 74.1500783001566 389606.628497254 5818976.73387287 74.1500783001566 389606.628497254 5818976.73387287 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_acf04be4-0964-4be0-a092-26f6387e9ed2">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389605.574074735 5818977.2130085 35 389605.449305789 5818977.29980677 35 389605.449305789 5818977.29980677 74.1500783001566 389605.574074735 5818977.2130085 74.1500783001566 389605.574074735 5818977.2130085 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_c166ff6e-35bd-4d4f-b018-8bcebfab7e5a">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389605.449305789 5818977.29980677 35 389605.36453492 5818976.99132171 35 389605.36453492 5818976.99132171 74.1500783001566 389605.449305789 5818977.29980677 74.1500783001566 389605.449305789 5818977.29980677 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-858f6e7d-c85e-410b-a923-9b8424739a9f">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389605.36453492 5818976.99132171 35 389603.899588369 5818971.66031425 35 389603.899588369 5818971.66031425 59.1400482800966 389603.899588369 5818971.66031425 74.1500783001566 389605.36453492 5818976.99132171 74.1500783001566 389605.36453492 5818976.99132171 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_b33bfc59-5d62-4077-9660-4b6dccb7602f">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389603.899588369 5818971.66031425 74.1500783001566 389603.899588369 5818971.66031425 59.1400482800966 389594.334988224 5818975.15858579 59.1400482800966 389594.334988224 5818975.15858579 74.1500783001567 389603.899588369 5818971.66031425 74.1500783001566</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_2b2ef1be-af34-4a3a-8c32-6bb80c9595b0">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389587.668496849 5818961.26834343 74.1500783001567 389587.668496849 5818961.26834343 59.1400482800966 389594.334988224 5818975.15858579 59.1400482800966 389594.334988224 5818975.15858579 74.1500783001567 389587.668496849 5818961.26834343 74.1500783001567</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-658fdcaf-46b2-47e3-85c6-1252993ac3f1">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389587.668496849 5818961.26834343 74.1500783001567 389587.668496849 5818961.26834343 59.1400482800966 389582.41697492 5818950.32631791 59.1400482800966 389582.41697492 5818950.32631791 74.1500783001566 389587.668496849 5818961.26834343 74.1500783001567</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-7c1b6a97-64af-433a-83af-d4260d4c9657">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389582.41697492 5818950.32631791 74.1500783001566 389582.41697492 5818950.32631791 59.1400482800966 389594.11746388 5818951.27836537 59.1400482800966 389594.11746388 5818951.27836537 35 389677.67838862 5818958.07756462 35 389677.67838862 5818958.07756462 74.1500783001566 389582.41697492 5818950.32631791 74.1500783001566</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-672b2f7c-09c3-47c9-a561-c960ce6f9129">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389575.879938891 5818989.78074407 74.1500783001567 389572.903276192 5818987.37370941 74.150079665474 389578.268613784 5818981.89443837 74.1500759876467 389581.023843906 5818984.66464108 74.1500783001567 389575.879938891 5818989.78074407 74.1500783001567</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="fme-gen-2aeb6d09-348b-4e9e-877a-1f7ebbb408da">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389589.600769974 5818972.87797293 78.9663358379556 389602.915815937 5818967.24534723 78.9663358379556 389623.446982122 5818995.6810992 101.45572387826 389622.14210775 5818996.23309657 101.45572387826 389589.600769974 5818972.87797293 78.9663358379556</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="fme-gen-2c52aee1-5095-4040-b525-ec8aa3c98e63">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389628.657091759 5819000.69227098 101.45572387826 389629.0415257 5818999.51027908 101.45572387826 389660.002993353 5819006.3186124 78.9663358379556 389656.080198751 5819018.37975342 78.9663358379556 389628.657091759 5819000.69227098 101.45572387826</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="fme-gen-e8d876df-9488-433c-9479-22cd6c058ccb">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389613.586575369 5819053.96271909 78.9663358379556 389624.492716256 5819004.17940131 101.45572387826 389625.797590607 5819003.62740392 101.45572387826 389626.901619228 5819048.33009083 78.9663358379556 389613.586575369 5819053.96271909 78.9663358379556</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="fme-gen-937229ee-ea3b-4a38-b077-ad0e3ee17027">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389556.499385724 5819014.88945637 78.9663358379556 389560.422181498 5819002.82831214 78.9663358379556 389619.282606512 5818999.16822953 101.45572387826 389618.89817256 5819000.35022146 101.45572387826 389556.499385724 5819014.88945637 78.9663358379556</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="fme-gen-926cc642-a30b-472b-94e5-add0dd3c7ff3">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389627.953229987 5819001.82233243 101.45572387826 389628.657091759 5819000.69227098 101.45572387826 389656.080198751 5819018.37975342 78.9663358379556 389648.897936951 5819029.91099242 78.9663358379556 389627.953229987 5819001.82233243 101.45572387826</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="fme-gen-91806e6d-6193-4fa7-a5c9-9a84ca44f821">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389616.594458427 5818964.56754471 78.9663358379556 389629.70452051 5818965.02705285 78.9663358379556 389626.072275093 5818995.46370615 101.45572387826 389624.787489012 5818995.41867445 101.45572387826 389616.594458427 5818964.56754471 78.9663358379556</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="fme-gen-828c3d8a-0cd8-4147-903b-91dfe7f2f269">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389567.604445867 5818991.29707091 78.9663358379556 389572.077364624 5818986.70584694 78.9663363146587 389577.508928078 5818981.13062301 78.9663346368352 389577.556719127 5818981.08156793 78.9663358379556 389620.961790931 5818997.03704886 101.45572387826 389619.986468309 5818998.03816805 101.45572387826 389567.604445867 5818991.29707091 78.9663358379556</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="fme-gen-a479dc77-515a-447f-9f96-a8d015403628">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389626.072275093 5818995.46370615 101.45572387826 389629.70452051 5818965.02705285 78.9663358379556 389641.352573237 5818968.59255662 78.9663358379556 389627.213784315 5818995.81312544 101.45572387826 389626.072275093 5818995.46370615 101.45572387826</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="fme-gen-768fb369-42c6-44c9-ac73-d56e1118e020">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389556.103390712 5819026.65855624 78.9663358379556 389556.499385724 5819014.88945637 78.9663358379556 389618.89817256 5819000.35022146 101.45572387826 389618.859364994 5819001.50359305 101.45572387826 389556.103390712 5819026.65855624 78.9663358379556</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="fme-gen-0a9b0f4e-4ffb-41dd-b357-b7dce666844e">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389627.213784315 5818995.81312544 101.45572387826 389641.352573237 5818968.59255662 78.9663358379556 389650.744821323 5818975.02107259 78.9663358379556 389628.134224714 5818996.44311997 101.45572387826 389627.213784315 5818995.81312544 101.45572387826</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="fme-gen-b05b315b-f364-40ec-9c8e-b5b9b6613166">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389577.556719127 5818981.08156793 78.9663358379556 389589.600769974 5818972.87797293 78.9663358379556 389622.14210775 5818996.23309657 101.45572387826 389620.961790931 5818997.03704886 101.45572387826 389577.556719127 5818981.08156793 78.9663358379556</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="fme-gen-ee817e38-85b7-4b8f-9109-0c73c2b9c330">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389625.797590607 5819003.62740392 101.45572387826 389626.977907396 5819002.82345161 101.45572387826 389638.945666968 5819040.12649473 78.9663358379556 389626.901619228 5819048.33009083 78.9663358379556 389625.797590607 5819003.62740392 101.45572387826</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="fme-gen-6a2b2715-195b-4825-9362-db2cf78cbd80">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389556.103390712 5819026.65855624 78.9663358379556 389618.859364994 5819001.50359305 101.45572387826 389619.168828483 5819002.54974392 101.45572387826 389559.261182812 5819037.3335667 78.9663358379556 389556.103390712 5819026.65855624 78.9663358379556</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="fme-gen-80cf80f1-7f6c-40c9-907a-326d0869b326">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389602.915815937 5818967.24534723 78.9663358379556 389616.594458427 5818964.56754471 78.9663358379556 389624.787489012 5818995.41867445 101.45572387826 389623.446982122 5818995.6810992 101.45572387826 389602.915815937 5818967.24534723 78.9663358379556</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="fme-gen-8e421dbf-9d1d-4418-80a6-1785cb97b67b">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389586.797870161 5819056.18102002 78.9663358379556 389621.867423279 5819004.39679443 101.45572387826 389623.15220937 5819004.44182608 101.45572387826 389599.907933417 5819056.64052495 78.9663358379556 389586.797870161 5819056.18102002 78.9663358379556</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="fme-gen-63d6ac2e-de8b-46f3-b4a9-1bb048d6ff04">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389628.134224714 5818996.44311997 101.45572387826 389650.744821323 5818975.02107259 78.9663358379556 389657.241198907 5818983.87450797 78.9663358379556 389628.770869803 5818997.31075667 101.45572387826 389628.134224714 5818996.44311997 101.45572387826</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="fme-gen-63bfe916-9b34-4d6f-b773-271d83d48a63">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389629.0415257 5818999.51027908 101.45572387826 389629.080333272 5818998.35690752 101.45572387826 389660.398988903 5818994.54951587 78.9663358379556 389660.002993353 5819006.3186124 78.9663358379556 389629.0415257 5818999.51027908 101.45572387826</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="fme-gen-442541b8-9eba-4b94-94bd-65f26d5f9769">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389599.907933417 5819056.64052495 78.9663358379556 389623.15220937 5819004.44182608 101.45572387826 389624.492716256 5819004.17940131 101.45572387826 389613.586575369 5819053.96271909 78.9663358379556 389599.907933417 5819056.64052495 78.9663358379556</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="fme-gen-499c9bf3-37a4-4c9f-ab58-af5454bc760d">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389575.149814864 5819052.6155185 78.9663358379556 389620.725914032 5819004.04737516 101.45572387826 389621.867423279 5819004.39679443 101.45572387826 389586.797870161 5819056.18102002 78.9663358379556 389575.149814864 5819052.6155185 78.9663358379556</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="fme-gen-479abe48-dc7e-490a-8fe9-f7662f08d995">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389560.422181498 5819002.82831214 78.9663358379556 389567.604445867 5818991.29707091 78.9663358379556 389619.986468309 5818998.03816805 101.45572387826 389619.282606512 5818999.16822953 101.45572387826 389560.422181498 5819002.82831214 78.9663358379556</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="fme-gen-70446ce8-310c-4a71-b503-118c291d354f">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389565.757563503 5819046.18700318 78.9663358379556 389619.805473601 5819003.41738063 101.45572387826 389620.725914032 5819004.04737516 101.45572387826 389575.149814864 5819052.6155185 78.9663358379556 389565.757563503 5819046.18700318 78.9663358379556</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="fme-gen-f18befad-1f78-428c-928f-b5ac5810b9fc">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389626.977907396 5819002.82345161 101.45572387826 389627.953229987 5819001.82233243 101.45572387826 389648.897936951 5819029.91099242 78.9663358379556 389638.945666968 5819040.12649473 78.9663358379556 389626.977907396 5819002.82345161 101.45572387826</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="fme-gen-0c51efc2-4adb-48a9-b99b-25e8931ce551">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389628.770869803 5818997.31075667 101.45572387826 389657.241198907 5818983.87450797 78.9663358379556 389660.398988903 5818994.54951587 78.9663358379556 389629.080333272 5818998.35690752 101.45572387826 389628.770869803 5818997.31075667 101.45572387826</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="fme-gen-d4b2caa7-4b6b-4ace-abdf-80f6bd1075c6">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389621.867423279 5819004.39679443 101.45572387826 389620.725914032 5819004.04737516 101.45572387826 389619.805473601 5819003.41738063 101.45572387826 389619.168828483 5819002.54974392 101.45572387826 389618.859364994 5819001.50359305 101.45572387826 389618.89817256 5819000.35022146 101.45572387826 389619.282606512 5818999.16822953 101.45572387826 389619.986468309 5818998.03816805 101.45572387826 389620.961790931 5818997.03704886 101.45572387826 389622.14210775 5818996.23309657 101.45572387826 389623.446982122 5818995.6810992 101.45572387826 389624.787489012 5818995.41867445 101.45572387826 389626.072275093 5818995.46370615 101.45572387826 389627.213784315 5818995.81312544 101.45572387826 389628.134224714 5818996.44311997 101.45572387826 389628.770869803 5818997.31075667 101.45572387826 389629.080333272 5818998.35690752 101.45572387826 389629.0415257 5818999.51027908 101.45572387826 389628.657091759 5819000.69227098 101.45572387826 389627.953229987 5819001.82233243 101.45572387826 389626.977907396 5819002.82345161 101.45572387826 389625.797590607 5819003.62740392 101.45572387826 389624.492716256 5819004.17940131 101.45572387826 389623.15220937 5819004.44182608 101.45572387826 389621.867423279 5819004.39679443 101.45572387826</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-1d0e02f6-c914-4590-a8e7-a1838650d630">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389706.588054941 5818990.21170472 73.8975467930565 389682.200917388 5819004.49131997 73.8975467930565 389682.200917388 5819004.49131997 48.60083522169 389706.588054941 5818990.21170472 48.60083522169 389706.588054941 5818990.21170472 73.8975467930565</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="fme-gen-8de7485f-e364-4873-8c1d-0119997ea3d9">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389676.912316913 5819005.83262315 39.2774775529181 389677.571062678 5819007.34497249 39.2774775529181 389674.245495711 5819009.19084383 39.2774775529181 389673.238178017 5819007.49842492 39.2774775529181 389667.839549055 5819009.84994775 39.2774775529181 389665.527950203 5819004.54297627 39.2774775529181 389667.104459876 5819003.8562837 39.2774775529181 389662.205786908 5818992.60990352 39.2774775529181 389673.241354754 5818987.803056 39.2774775529181 389680.427671732 5819004.30141272 39.2774775529181 389676.912316913 5819005.83262315 39.2774775529181</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-95990a2a-b7b0-4e76-816d-540f0af7eaf2">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389677.571062678 5819007.34497249 73.8975467930565 389674.245495711 5819009.19084383 73.8975467930565 389674.245495711 5819009.19084383 39.2774775529181 389677.571062678 5819007.34497249 39.2774775529181 389677.571062678 5819007.34497249 73.8975467930565</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="fme-gen-01b84bf8-361a-4a93-84e5-e820ede4b1ae">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389660.135797726 5819013.20671718 41.2000159074514 389659.114260156 5819013.40042673 41.2000159074514 389654.100502451 5819010.99016173 41.2000159074514 389651.615805337 5818997.8869776 41.2000159074514 389662.096388072 5818995.89959266 41.2000159074514 389665.196294824 5819012.24711806 41.2000159074514 389660.135797726 5819013.20671718 41.2000159074514</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-ddd5fc7c-f298-441d-8ee7-b4f152c2dfdb">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389659.114260156 5819013.40042673 73.8975467930565 389654.100502451 5819010.99016173 73.8975467930565 389654.100502451 5819010.99016173 41.2000159074514 389659.114260156 5819013.40042673 41.2000159074514 389659.114260156 5819013.40042673 73.8975467930565</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="fme-gen-436376fb-7d6f-4dbe-83f6-29456a1ed7d4">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389679.258088286 5818999.71678228 48.60083522169 389681.658123288 5818998.23749922 48.60083522169 389696.44950477 5818988.66268366 48.60083522169 389702.4921459 5818984.93824798 48.60083522169 389702.938029037 5818985.66166276 48.60083522169 389703.550803908 5818985.28397351 48.60083522169 389706.588054941 5818990.21170472 48.60083522169 389682.200917388 5819004.49131997 48.60083522169 389679.258088286 5818999.71678228 48.60083522169</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="fme-gen-705c3c7a-4db3-46ad-acf9-b3981141d34a">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389689.079318673 5818976.70506253 44.3998857764866 389696.44950477 5818988.66268366 44.3998857764866 389681.658123288 5818998.23749922 44.3998857764866 389673.139131705 5818984.41602077 44.3998857764866 389673.683820523 5818984.08029697 44.3998857764866 389672.760580078 5818982.58240271 44.3998857764866 389685.909708505 5818974.4778207 44.3998857764866 389687.01130214 5818976.26508085 44.3998857764866 389687.624077046 5818975.8873916 44.3998857764866 389688.389946907 5818977.12996294 44.3998857764866 389689.079318673 5818976.70506253 44.3998857764866</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-af4a92e1-7b7b-42eb-9fdf-650b98c48587">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389696.44950477 5818988.66268366 48.60083522169 389681.658123288 5818998.23749922 48.60083522169 389681.658123288 5818998.23749922 44.3998857764866 389696.44950477 5818988.66268366 44.3998857764866 389696.44950477 5818988.66268366 48.60083522169</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_6115045b-745c-438a-a435-d637c8ca00d4">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389752.154206026 5818968.48625399 128.565182922846 389752.255928134 5818967.58830218 128.565182922846 389752.774756536 5818967.95965883 128.565182922846 389754.970690042 5818971.61254944 128.565182922846 389752.154206026 5818968.48625399 128.565182922846</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_56d99cbd-7502-487d-b132-5b869ee270c4">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389752.338408785 5818966.86020434 128.565182922846 389752.302797341 5818967.17456432 128.565182922846 389752.097407914 5818966.83290319 128.565182922846 389752.338408785 5818966.86020434 128.565182922846</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_fa3948ff-9916-4a7a-b81c-5234e4232f55">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389711.172270779 5818970.86692347 137.000205520431 389735.354638219 5818972.95801024 137.000205520431 389735.352881674 5818973.00083383 137.000205520431 389711.172270779 5818970.86692347 137.000205520431</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface gml:id="UUID_4cf4b4b9-df04-4331-b77c-6fcdb2f58540">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389677.67838862 5818958.07756462 35 389594.11746388 5818951.27836537 35 389603.899588369 5818971.66031425 35 389605.36453492 5818976.99132171 35 389605.449305789 5818977.29980677 35 389605.574074735 5818977.2130085 35 389606.628497254 5818976.73387287 35 389607.952902964 5818976.30808644 35 389609.515753632 5818975.93997844 35 389611.285511029 5818975.63387808 35 389613.230636921 5818975.39411459 35 389615.319593078 5818975.22501719 35 389617.520841268 5818975.1309151 35 389619.802843263 5818975.11613753 35 389622.134060833 5818975.18501372 35 389624.482955749 5818975.34187286 35 389626.817989785 5818975.5910442 35 389629.107624713 5818975.93685693 35 389631.320322307 5818976.38364029 35 389633.424544344 5818976.9357235 35 389635.388752598 5818977.59743576 35 389637.181408846 5818978.37310631 35 389638.770974865 5818979.26706436 35 389640.125912432 5818980.28363913 35 389641.214683326 5818981.42715985 35 389642.005749324 5818982.70195574 35 389645.201459519 5818977.94874416 35 389645.681101965 5818978.33801719 35 389646.146749855 5818978.74392655 35 389646.597826468 5818979.16596951 35 389647.033773126 5818979.60362334 35 389647.454049893 5818980.056346 35 389647.858136237 5818980.52357677 35 389648.245531683 5818981.00473696 35 389648.615756426 5818981.49923065 35 389648.968351927 5818982.00644537 35 389649.302881483 5818982.52575292 35 389649.618930765 5818983.05651013 35 389649.916108334 5818983.59805962 35 389681.876100906 5818964.7559149 35 389677.67838862 5818958.07756462 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_d3247944-44e7-4dc4-ab15-67965e43b227">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389594.334988224 5818975.15858579 59.1400482800966 389603.899588369 5818971.66031425 59.1400482800966 389594.11746388 5818951.27836537 59.1400482800966 389582.41697492 5818950.32631791 59.1400482800966 389587.668496849 5818961.26834343 59.1400482800966 389594.334988224 5818975.15858579 59.1400482800966</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-fa64d291-5254-497f-9dc5-02a9b73ab64b">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389603.899588369 5818971.66031425 35 389594.11746388 5818951.27836537 35 389594.11746388 5818951.27836537 59.1400482800966 389603.899588369 5818971.66031425 59.1400482800966 389603.899588369 5818971.66031425 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-23d89999-3651-4cb9-8fb8-5b47307aa77e">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389587.668496849 5818961.26834343 35 389594.334988224 5818975.15858579 35 389594.334988224 5818975.15858579 59.1400482800966 389587.668496849 5818961.26834343 59.1400482800966 389587.668496849 5818961.26834343 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="fme-gen-6eb3aca7-e764-4bb4-850c-3a16cdf9b0ea">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389582.209917453 5818983.4849781 74.1500783001567 389581.023843906 5818984.66464108 74.1500783001567 389578.268613784 5818981.89443837 74.1500759876467 389569.674650645 5818973.25377331 74.1500783001567 389574.976128217 5818969.0092217 74.1500783001567 389580.963443878 5818965.00020917 74.1500783001567 389587.668496849 5818961.26834343 74.1500783001567 389594.334988224 5818975.15858579 74.1500783001567 389596.635882072 5818979.95270752 74.1500783001567 389595.576926021 5818980.3742882 74.1500783001567 389594.533469173 5818980.83289043 74.1500783001567 389593.506805343 5818981.32794557 74.1500783001567 389592.498207529 5818981.85883978 74.1500783001567 389591.508926322 5818982.42491479 74.1500783001567 389590.540188367 5818983.0254687 74.1500783001567 389589.593194835 5818983.65975688 74.1500783001567 389588.669119933 5818984.32699283 74.1500783001567 389587.769109454 5818985.02634924 74.1500783001567 389586.894279352 5818985.75695895 74.1500783001567 389586.045714357 5818986.51791605 74.1500783001567 389585.224466633 5818987.30827701 74.1500783001567 389582.209917453 5818983.4849781 74.1500783001567</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="fme-gen-c646cf0c-23ad-4709-866e-a384e306b4f8">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389563.605065882 5819024.43777322 74.1500783001567 389558.722057833 5819024.76505696 74.1500818207238 389558.38716597 5819019.7685368 74.1500853201902 389547.381262197 5819020.03972986 74.1500783001567 389547.363136871 5819019.50023935 74.1500783001567 389547.544753698 5819015.53656575 74.1500783001567 389546.196366784 5819015.47478221 74.1500783001567 389546.214753047 5819015.07351358 74.1500783001567 389546.792560916 5819010.47475055 74.1500783001567 389547.777935612 5819005.73688246 74.1500783001567 389549.203246215 5819000.90164155 74.1500783001567 389551.100841482 5818996.01127926 74.1500783001567 389553.502883722 5818991.10850873 74.1500783001567 389556.441197887 5818986.23640622 74.1500783001567 389559.105256072 5818982.59046104 74.1500783001567 389560.195114673 5818983.38681057 74.1500783001567 389561.001612858 5818982.28306285 74.1500783001567 389563.327576635 5818979.63046011 74.1500783001567 389572.903276192 5818987.37370941 74.150079665474 389575.879938891 5818989.78074407 74.1500783001567 389594.801599708 5819005.08146698 74.1500783001567 389586.75550641 5819024.01784527 74.1500783001567 389563.605065882 5819024.43777322 74.1500783001567</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_0aa079ef-bb75-43d0-a1a7-e1acf6cdbd22">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389578.268613784 5818981.89443837 74.1500759876467 389572.903276192 5818987.37370941 74.150079665474 389563.327576635 5818979.63046011 74.1500783001567 389565.027111937 5818977.69225627 74.1500783001567 389569.674650645 5818973.25377331 74.1500783001567 389578.268613784 5818981.89443837 74.1500759876467</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_3ab41b90-2476-43bf-87ee-5a51d643d58b">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389547.500527998 5819023.58961106 74.1500783001567 389547.381262197 5819020.03972986 74.1500783001567 389558.38716597 5819019.7685368 74.1500853201902 389558.722057833 5819024.76505696 74.1500818207238 389547.720747059 5819025.50242012 74.1500783001567 389547.500527998 5819023.58961106 74.1500783001567</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="fme-gen-8c21c99d-3dc8-4371-9a7f-e06cfe1dd0b7">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389558.387164518 5819019.76853789 77.3500943900055 389558.722056382 5819024.76505806 77.3500908905391 389547.720745608 5819025.50242122 77.350087369972 389547.500526547 5819023.58961215 77.350087369972 389547.381260746 5819020.03973095 77.350087369972 389558.387164518 5819019.76853789 77.3500943900055</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_bbbf6e26-0dc5-44fe-a758-e63ea4c53646">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389558.38716597 5819019.7685368 74.1500853201902 389558.722057833 5819024.76505696 74.1500818207238 389558.722056382 5819024.76505806 77.3500908905391 389558.387164518 5819019.76853789 77.3500943900055 389558.38716597 5819019.7685368 74.1500853201902</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-b03687d7-cc94-46f9-8c00-6fc051512789">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389558.722057833 5819024.76505696 74.1500818207238 389547.720747059 5819025.50242012 74.1500783001567 389547.720745608 5819025.50242122 77.350087369972 389558.722056382 5819024.76505806 77.3500908905391 389558.722057833 5819024.76505696 74.1500818207238</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-f9f11d3d-39ff-4073-bf60-a0ba50f15595">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389547.720747059 5819025.50242012 74.1500783001567 389547.500527998 5819023.58961106 74.1500783001567 389547.500526547 5819023.58961215 77.350087369972 389547.720745608 5819025.50242122 77.350087369972 389547.720747059 5819025.50242012 74.1500783001567</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-396359a9-39fa-463b-a8ca-49151dfe4fdd">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389547.500527998 5819023.58961106 74.1500783001567 389547.381262197 5819020.03972986 74.1500783001567 389547.381260746 5819020.03973095 77.350087369972 389547.500526547 5819023.58961215 77.350087369972 389547.500527998 5819023.58961106 74.1500783001567</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-eb8a8d6d-6cd7-4dfd-99ce-5540249148f0">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389547.381262197 5819020.03972986 74.1500783001567 389558.38716597 5819019.7685368 74.1500853201902 389558.387164518 5819019.76853789 77.3500943900055 389547.381260746 5819020.03973095 77.350087369972 389547.381262197 5819020.03972986 74.1500783001567</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_9965bd7c-98e0-40c0-8154-3e61c7add65d">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389572.903276192 5818987.37370941 74.150079665474 389572.903276896 5818987.37370883 78.7963360474207 389578.26861449 5818981.89443779 78.7963342088862 389578.268613784 5818981.89443837 74.1500759876467 389572.903276192 5818987.37370941 74.150079665474</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-ca3cb49d-fc26-4190-94c4-05592c8166e2">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389563.327577349 5818979.63045952 78.7963352038035 389572.077364613 5818986.70584695 78.796335974658 389572.903276896 5818987.37370883 78.7963360474207 389572.903276192 5818987.37370941 74.150079665474 389563.327576635 5818979.63046011 74.1500783001567 389563.327577349 5818979.63045952 78.7963352038035</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-dcdeeef8-5d45-411f-8947-41fd1c091622">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389563.327576635 5818979.63046011 74.1500783001567 389565.027111937 5818977.69225627 74.1500783001567 389565.027112651 5818977.69225567 78.7963352038035 389563.327577349 5818979.63045952 78.7963352038035 389563.327576635 5818979.63046011 74.1500783001567</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-1d8ad044-c3a1-4c6c-85a5-df64e2d16713">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389565.027111937 5818977.69225627 74.1500783001567 389569.674650645 5818973.25377331 74.1500783001567 389569.67465136 5818973.25377272 78.7963352038035 389565.027112651 5818977.69225567 78.7963352038035 389565.027111937 5818977.69225627 74.1500783001567</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-c52341df-8070-44ce-94f2-0c09f79ca94f">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389548.652551982 5819030.935486 74.1500783001567 389548.652552237 5819030.93548713 59.3800487600975 389547.940966384 5819027.41523031 59.3800487600975 389547.940966129 5819027.41522918 74.1500783001567 389548.652551982 5819030.935486 74.1500783001567</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-4fd8fcf6-0d90-47cd-9e04-ca0438c5e42e">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389553.572386952 5819041.13074625 74.1500784145367 389553.572387207 5819041.13074738 59.3800487600975 389552.095203297 5819039.24801278 59.3800487600975 389552.095203042 5819039.24801165 74.1500783001567 389553.572386952 5819041.13074625 74.1500784145367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-0b451687-3c2a-464c-8861-0748ee676701">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389552.095203042 5819039.24801165 74.1500783001567 389552.095203297 5819039.24801278 59.3800487600975 389550.761569977 5819036.89348608 59.3800487600975 389550.761569722 5819036.89348494 74.1500783001567 389552.095203042 5819039.24801165 74.1500783001567</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-f4f714e5-e54b-4848-8a84-3b9a02b242d8">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389550.761569722 5819036.89348494 74.1500783001567 389550.761569977 5819036.89348608 59.3800487600975 389549.603386529 5819034.10877496 59.3800487600975 389549.603386274 5819034.10877382 74.1500783001567 389550.761569722 5819036.89348494 74.1500783001567</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-db92f415-0216-4324-9aa5-e9fd4d7d1af4">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389549.603386274 5819034.10877382 74.1500783001567 389549.603386529 5819034.10877496 59.3800487600975 389548.652552237 5819030.93548713 59.3800487600975 389548.652551982 5819030.935486 74.1500783001567 389549.603386274 5819034.10877382 74.1500783001567</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_f133e61e-d2d7-4078-847a-13a311839531">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389564.049042082 5819027.53062797 74.1500783001567 389564.049042337 5819027.5306291 59.3800487600975 389564.267021748 5819028.28232737 59.3800487600975 389564.267021493 5819028.28232623 74.1500783001567 389564.049042082 5819027.53062797 74.1500783001567</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_4b08ecb5-ec60-4afd-b431-5e9f294ff439">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389563.873375689 5819026.76793102 74.1500783001567 389563.873375944 5819026.76793216 59.3800487600975 389564.049042337 5819027.5306291 59.3800487600975 389564.049042082 5819027.53062797 74.1500783001567 389563.873375689 5819026.76793102 74.1500783001567</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_38fa4b21-c6bf-4d25-be71-064b48114b3f">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389564.267021493 5819028.28232623 74.1500783001567 389564.267021748 5819028.28232737 59.3800487600975 389564.526634024 5819029.02068146 59.3800487600975 389564.526633769 5819029.02068033 74.1500783001567 389564.267021493 5819028.28232623 74.1500783001567</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_e41481ba-8cdb-4065-bf43-821f1a772627">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389564.526633769 5819029.02068033 74.1500783001567 389564.526634024 5819029.02068146 59.3800487600975 389564.827069108 5819029.74338753 59.3800487600975 389564.827068853 5819029.7433864 74.1500783001567 389564.526633769 5819029.02068033 74.1500783001567</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_777a47b5-6756-4d68-b6f5-4ef58411204f">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389566.416442027 5819032.43350874 74.1500783001567 389566.416442282 5819032.43350988 59.3800487600975 389566.904492777 5819033.04536935 59.3800487600975 389566.904492522 5819033.04536821 74.1500784145367 389566.416442027 5819032.43350874 74.1500783001567</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_a1287a36-db56-4e84-bf49-26f6e6b7a721">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389565.963317648 5819031.79535234 74.1500783001567 389565.963317902 5819031.79535347 59.3800487600975 389566.416442282 5819032.43350988 59.3800487600975 389566.416442027 5819032.43350874 74.1500783001567 389565.963317648 5819031.79535234 74.1500783001567</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_3963f477-8d1a-4edd-9b7c-b69eec256a7a">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389564.827068853 5819029.7433864 74.1500783001567 389564.827069108 5819029.74338753 59.3800487600975 389565.167389564 5819030.44819055 59.3800487600975 389565.167389309 5819030.44818942 74.1500783001567 389564.827068853 5819029.7433864 74.1500783001567</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_6631b05b-0cd0-4a19-b4ef-8a5e1a9324f6">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389563.605065882 5819024.43777322 74.1500783001567 389563.605066137 5819024.43777435 59.3800487600975 389563.651040973 5819025.21908838 59.3800487600975 389563.651040718 5819025.21908726 74.1500783001567 389563.605065882 5819024.43777322 74.1500783001567</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_be14568a-f1dd-4690-a7ca-45de29340128">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389563.651040718 5819025.21908726 74.1500783001567 389563.651040973 5819025.21908838 59.3800487600975 389563.740570694 5819025.99661635 59.3800487600975 389563.740570439 5819025.99661522 74.1500783001567 389563.651040718 5819025.21908726 74.1500783001567</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_7bef3504-bc65-42fb-b809-79c48841e19a">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389563.740570439 5819025.99661522 74.1500783001567 389563.740570694 5819025.99661635 59.3800487600975 389563.873375944 5819026.76793216 59.3800487600975 389563.873375689 5819026.76793102 74.1500783001567 389563.740570439 5819025.99661522 74.1500783001567</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_69107413-7393-46ed-9900-ef2e61ed044d">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389565.167389309 5819030.44818942 74.1500783001567 389565.167389564 5819030.44819055 59.3800487600975 389565.546533505 5819031.13289134 59.3800487600975 389565.54653325 5819031.13289021 74.1500783001567 389565.167389309 5819030.44818942 74.1500783001567</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_9ba094f9-7dc6-41b4-a533-973434a7c201">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389565.54653325 5819031.13289021 74.1500783001567 389565.546533505 5819031.13289134 59.3800487600975 389565.963317902 5819031.79535347 59.3800487600975 389565.963317648 5819031.79535234 74.1500783001567 389565.54653325 5819031.13289021 74.1500783001567</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="fme-gen-383d2fcc-bab8-4e0e-802b-52bac7b9ec95">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389547.940966129 5819027.41522918 74.1500783001567 389547.720747059 5819025.50242012 74.1500783001567 389558.722057833 5819024.76505696 74.1500818207238 389563.605065882 5819024.43777322 74.1500783001567 389563.651040718 5819025.21908726 74.1500783001567 389563.740570439 5819025.99661522 74.1500783001567 389563.873375689 5819026.76793102 74.1500783001567 389564.049042082 5819027.53062797 74.1500783001567 389564.267021493 5819028.28232623 74.1500783001567 389564.526633769 5819029.02068033 74.1500783001567 389564.827068853 5819029.7433864 74.1500783001567 389565.167389309 5819030.44818942 74.1500783001567 389565.54653325 5819031.13289021 74.1500783001567 389565.963317648 5819031.79535234 74.1500783001567 389566.416442027 5819032.43350874 74.1500783001567 389566.904492522 5819033.04536821 74.1500784145367 389553.572386952 5819041.13074625 74.1500784145367 389552.095203042 5819039.24801165 74.1500783001567 389550.761569722 5819036.89348494 74.1500783001567 389549.603386274 5819034.10877382 74.1500783001567 389548.652551982 5819030.935486 74.1500783001567 389547.940966129 5819027.41522918 74.1500783001567</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_335b5652-ad99-4767-a2b5-1264444307a0">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389547.720747314 5819025.50242125 59.3800486457174 389547.940966384 5819027.41523031 59.3800487600975 389548.652552237 5819030.93548713 59.3800487600975 389549.603386529 5819034.10877496 59.3800487600975 389550.761569977 5819036.89348608 59.3800487600975 389552.095203297 5819039.24801278 59.3800487600975 389553.572387207 5819041.13074738 59.3800487600975 389566.904492777 5819033.04536935 59.3800487600975 389566.416442282 5819032.43350988 59.3800487600975 389565.963317902 5819031.79535347 59.3800487600975 389565.546533505 5819031.13289134 59.3800487600975 389565.167389564 5819030.44819055 59.3800487600975 389564.827069108 5819029.74338753 59.3800487600975 389564.526634024 5819029.02068146 59.3800487600975 389564.267021748 5819028.28232737 59.3800487600975 389564.049042337 5819027.5306291 59.3800487600975 389563.873375944 5819026.76793216 59.3800487600975 389563.740570694 5819025.99661635 59.3800487600975 389563.651040973 5819025.21908838 59.3800487600975 389563.605066137 5819024.43777435 59.3800487600975 389558.722058088 5819024.76505809 59.3800521662846 389547.720747314 5819025.50242125 59.3800486457174</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-c2757442-7a2b-4592-8bea-d2a06d9a717c">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389553.572386952 5819041.13074625 35 389566.904492522 5819033.04536821 35 389566.904492777 5819033.04536935 59.3800487600975 389553.572387207 5819041.13074738 59.3800487600975 389553.572386952 5819041.13074625 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-0cae6efa-3996-4398-ac7e-16614ae60d29">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389563.605065882 5819024.43777322 35 389558.722057833 5819024.76505696 35.000003406187 389558.722058088 5819024.76505809 59.3800521662846 389563.605066137 5819024.43777435 59.3800487600975 389563.605065882 5819024.43777322 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-5bd8eb7f-3882-4b73-bcfa-3a01b294a650">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389558.722057833 5819024.76505696 35.000003406187 389547.720747059 5819025.50242012 34.9999998856199 389547.720747314 5819025.50242125 59.3800486457174 389558.722058088 5819024.76505809 59.3800521662846 389558.722057833 5819024.76505696 35.000003406187</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_309fed38-871a-4cc3-809d-b8e20132973e">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389759.533490998 5818975.13474391 128.565182922846 389757.273320181 5818974.87870645 128.565182922846 389757.454087066 5818973.28298673 128.565182922846 389758.420299536 5818973.39244155 128.565182922846 389759.533490998 5818975.13474391 128.565182922846</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-3d275c96-58c7-419e-a512-95a7d8fbffd8">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389711.971073997 5818969.92918546 137.000205520431 389711.172270779 5818970.86692347 137.000205520431 389711.172270779 5818970.86692347 128.565182922846 389711.971073997 5818969.92918546 128.565182922846 389711.971073997 5818969.92918546 137.000205520431</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-7d8add33-39f8-41da-8a03-2e3960fb4030">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389716.431027463 5818966.38953833 137.000205520431 389711.971073997 5818969.92918546 137.000205520431 389711.971073997 5818969.92918546 128.565182922846 389716.431027463 5818966.38953833 128.565182922846 389716.431027463 5818966.38953833 137.000205520431</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-e47cc9f3-086e-4f15-866a-4cf21181592c">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389721.533328956 5818963.72947889 137.000205520431 389716.431027463 5818966.38953833 137.000205520431 389716.431027463 5818966.38953833 128.565182922846 389721.533328956 5818963.72947889 128.565182922846 389721.533328956 5818963.72947889 137.000205520431</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-ca27e410-495b-4c0f-91e4-9d1bda40603e">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389727.090369691 5818962.04681602 137.000205520431 389721.533328956 5818963.72947889 137.000205520431 389721.533328956 5818963.72947889 128.565182922846 389727.090369691 5818962.04681602 128.565182922846 389727.090369691 5818962.04681602 137.000205520431</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-04afba4c-4fbe-4a90-9851-f4c57dd157b5">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389732.897820388 5818961.40342022 137.000205520431 389727.090369691 5818962.04681602 137.000205520431 389727.090369691 5818962.04681602 128.565182922846 389732.897820388 5818961.40342022 128.565182922846 389732.897820388 5818961.40342022 137.000205520431</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-81eed5b4-6b57-4473-9e87-7be651dc2f27">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389735.819982383 5818961.61318449 137.000205520431 389732.897820388 5818961.40342022 137.000205520431 389732.897820388 5818961.40342022 128.565182922846 389738.742144355 5818961.82294875 128.565182922846 389738.742144355 5818961.82294875 137.000205520431 389735.819982383 5818961.61318449 137.000205520431</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-a052c04c-f440-4d79-8b55-c0f935ec066c">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389744.408449105 5818963.28997574 137.000205520431 389738.742144355 5818961.82294875 137.000205520431 389738.742144355 5818961.82294875 128.565182922846 389744.408449105 5818963.28997574 128.565182922846 389744.408449105 5818963.28997574 137.000205520431</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-d0b4278f-879d-461a-9627-ed84a2d0dc6f">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389749.688387841 5818965.75055939 137.000205520431 389744.408449105 5818963.28997574 137.000205520431 389744.408449105 5818963.28997574 128.565182922846 389747.811068986 5818964.8756818 128.565182922846 389749.688387841 5818965.75055939 128.565182922846 389749.688387841 5818965.75055939 137.000205520431</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-80b8e3c1-2adb-47f7-8ac6-6a053af28591">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389754.387820234 5818969.11422539 137.000205520431 389749.688387841 5818965.75055939 137.000205520431 389749.688387841 5818965.75055939 128.565182922846 389752.255928134 5818967.58830218 128.565182922846 389752.774756536 5818967.95965883 128.565182922846 389754.387820234 5818969.11422539 128.565182922846 389754.387820234 5818969.11422539 137.000205520431</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-b1c31a33-d138-48f4-b3e7-91dd7426ec3d">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389754.387820234 5818969.11422539 128.565182922846 389756.694223226 5818971.53573292 128.565182922846 389758.333950859 5818973.25729363 128.565182922846 389758.333950859 5818973.25729363 137.000205520431 389756.694223226 5818971.53573292 137.000205520431 389754.387820234 5818969.11422539 137.000205520431 389754.387820234 5818969.11422539 128.565182922846</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-5f3d25b7-6ac0-4c32-b9ad-1bd83a0498d7">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389759.533490998 5818975.13474391 137.000205520431 389758.333950859 5818973.25729363 137.000205520431 389758.333950859 5818973.25729363 128.565182922846 389758.420299536 5818973.39244155 128.565182922846 389759.533490998 5818975.13474391 128.565182922846 389759.533490998 5818975.13474391 137.000205520431</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_5aeb19b9-f0f0-4ed8-be74-3b313d329f59">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389752.302797341 5818967.17456432 128.565182922846 389752.774756536 5818967.95965883 128.565182922846 389752.255928134 5818967.58830218 128.565182922846 389752.302797341 5818967.17456432 128.565182922846</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_3672dafd-02ef-47eb-951f-b4dd15bdce3a">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389759.533490998 5818975.13474391 137.000205520431 389735.352881674 5818973.00083383 137.000205520431 389735.354638219 5818972.95801024 137.000205520431 389756.327658879 5818974.77157971 137.000205520431 389759.533490998 5818975.13474391 137.000205520431</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="fme-gen-c13f88b6-542e-4644-9af3-3683529fd7fc">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389756.327658879 5818974.77157971 137.000205520431 389735.354638219 5818972.95801024 137.000205520431 389711.172270779 5818970.86692347 137.000205520431 389711.971073997 5818969.92918546 137.000205520431 389716.431027463 5818966.38953833 137.000205520431 389721.533328956 5818963.72947889 137.000205520431 389727.090369691 5818962.04681602 137.000205520431 389732.897820388 5818961.40342022 137.000205520431 389735.819982383 5818961.61318449 137.000205520431 389738.742144355 5818961.82294875 137.000205520431 389744.408449105 5818963.28997574 137.000205520431 389749.688387841 5818965.75055939 137.000205520431 389754.387820234 5818969.11422539 137.000205520431 389756.694223226 5818971.53573292 137.000205520431 389758.333950859 5818973.25729363 137.000205520431 389759.533490998 5818975.13474391 137.000205520431 389756.327658879 5818974.77157971 137.000205520431</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-2d60a669-e115-4554-bc91-cfcd5e399934">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389711.172270779 5818970.86692347 137.000205520431 389735.352881674 5818973.00083383 137.000205520431 389759.533490998 5818975.13474391 137.000205520431 389759.533490998 5818975.13474391 128.565182922846 389757.266974099 5818974.93472649 128.565182922846 389711.172270779 5818970.86692347 128.565182922846 389711.172270779 5818970.86692347 137.000205520431</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_89418395-18e1-4960-8734-1f69f8ed5a20">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389759.533490998 5818975.13474391 128.565182922846 389757.266974099 5818974.93472649 128.565182922846 389757.273320181 5818974.87870645 128.565182922846 389759.533490998 5818975.13474391 128.565182922846</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_5afc3385-a312-40d2-babf-0465f80c9956">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389757.273320181 5818974.87870645 128.565182922846 389757.266974099 5818974.93472649 128.565182922846 389711.172270779 5818970.86692347 128.565182922846 389756.327658879 5818974.77157971 128.565182922846 389757.273320181 5818974.87870645 128.565182922846</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="fme-gen-568405dd-5392-419f-a4f8-07ab9dafa45e">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389547.457281619 5818984.6568458 43.5400170800342 389547.201260434 5818986.66290736 43.5400170800342 389546.207870806 5818988.42444274 43.5400170800342 389544.623699727 5818989.68151548 43.5400170800342 389542.682511342 5818990.24862885 43.5400170800342 389540.670752148 5818990.0420982 43.5400170800342 389538.885282253 5818989.09239969 43.5400170800342 389537.58956997 5818987.53967314 43.5400170800342 389536.974813782 5818985.61304269 43.5400170800342 389537.131728622 5818983.59680665 43.5400170800342 389538.037159752 5818981.78848574 43.5400170800342 389539.557499529 5818980.45492022 43.5400170800342 389544.146650882 5818976.21482251 43.5400170800342 389548.711039471 5818978.66133764 43.5400170800342 389547.457281619 5818984.6568458 43.5400170800342</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-bc9b8063-fa6c-47e6-ba09-c8628f1eeeb7">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389547.457281619 5818984.6568458 35 389547.201260434 5818986.66290736 35 389547.201260434 5818986.66290736 43.5400170800342 389547.457281619 5818984.6568458 43.5400170800342 389547.457281619 5818984.6568458 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-9dad3f8d-486e-49ef-9ffe-5188ac56b7b0">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389547.201260434 5818986.66290736 35 389546.207870806 5818988.42444274 35 389546.207870806 5818988.42444274 43.5400170800342 389547.201260434 5818986.66290736 43.5400170800342 389547.201260434 5818986.66290736 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-7717a139-dc8a-4576-9d3e-a60babda29ad">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389546.207870806 5818988.42444274 35 389544.623699727 5818989.68151548 35 389544.623699727 5818989.68151548 43.5400170800342 389546.207870806 5818988.42444274 43.5400170800342 389546.207870806 5818988.42444274 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-fc34abbe-35fc-4e6b-b70c-25e1f26688f8">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389544.623699727 5818989.68151548 35 389542.682511342 5818990.24862885 35 389542.682511342 5818990.24862885 43.5400170800342 389544.623699727 5818989.68151548 43.5400170800342 389544.623699727 5818989.68151548 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-0e2512c6-b50e-4958-a2c5-33ea65a72668">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389542.682511342 5818990.24862885 35 389540.670752148 5818990.0420982 35 389540.670752148 5818990.0420982 43.5400170800342 389542.682511342 5818990.24862885 43.5400170800342 389542.682511342 5818990.24862885 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-c5e9c435-160c-40d9-bf56-205360326aa7">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389540.670752148 5818990.0420982 35 389538.885282253 5818989.09239969 35 389538.885282253 5818989.09239969 43.5400170800342 389540.670752148 5818990.0420982 43.5400170800342 389540.670752148 5818990.0420982 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-deabe8dd-78d7-481f-9baf-9dbf126bd757">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389538.885282253 5818989.09239969 35 389537.58956997 5818987.53967314 35 389537.58956997 5818987.53967314 43.5400170800342 389538.885282253 5818989.09239969 43.5400170800342 389538.885282253 5818989.09239969 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-820937e1-35f0-4c10-a23a-4aa84afd8503">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389537.58956997 5818987.53967314 35 389536.974813782 5818985.61304269 35 389536.974813782 5818985.61304269 43.5400170800342 389537.58956997 5818987.53967314 43.5400170800342 389537.58956997 5818987.53967314 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-c2fbb895-bfbb-4d03-a2a1-91dd42ea030b">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389536.974813782 5818985.61304269 35 389537.131728622 5818983.59680665 35 389537.131728622 5818983.59680665 43.5400170800342 389536.974813782 5818985.61304269 43.5400170800342 389536.974813782 5818985.61304269 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-7bdea79c-de3a-4b03-a70c-c2d0cd429ce9">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389537.131728622 5818983.59680665 35 389538.037159752 5818981.78848574 35 389538.037159752 5818981.78848574 43.5400170800342 389537.131728622 5818983.59680665 43.5400170800342 389537.131728622 5818983.59680665 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-64c0e67d-bc19-4da4-9fe9-7fa8ec8780db">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389538.037159752 5818981.78848574 35 389539.557499529 5818980.45492022 35 389539.557499529 5818980.45492022 43.5400170800342 389538.037159752 5818981.78848574 43.5400170800342 389538.037159752 5818981.78848574 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-6f64952b-83ea-44fb-9d4f-5399a50258b6">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389539.557499529 5818980.45492022 35 389544.146650882 5818976.21482251 35 389544.146650882 5818976.21482251 43.5400170800342 389539.557499529 5818980.45492022 43.5400170800342 389539.557499529 5818980.45492022 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-c80a0455-6e58-4579-9422-61aa526be610">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389544.146650882 5818976.21482251 35 389548.711039471 5818978.66133764 35 389548.711039471 5818978.66133764 43.5400170800342 389544.146650882 5818976.21482251 43.5400170800342 389544.146650882 5818976.21482251 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-e70e5b07-eab8-4e5c-8c92-c7383ec02f67">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389548.711039471 5818978.66133764 35 389547.457281619 5818984.6568458 35 389547.457281619 5818984.6568458 43.5400170800342 389548.711039471 5818978.66133764 43.5400170800342 389548.711039471 5818978.66133764 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="fme-gen-304eb547-15e7-4e8a-b354-c6b51543b48c">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389569.67465136 5818973.25377272 78.7963352038035 389577.508928068 5818981.13062303 78.7963342968345 389572.077364613 5818986.70584695 78.796335974658 389563.327577349 5818979.63045952 78.7963352038035 389565.027112651 5818977.69225567 78.7963352038035 389569.67465136 5818973.25377272 78.7963352038035</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-88e3c272-0d40-42e5-8184-4a6ff215e101">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389578.268613784 5818981.89443837 74.1500759876467 389578.26861449 5818981.89443779 78.7963342088862 389577.508928068 5818981.13062303 78.7963342968345 389569.67465136 5818973.25377272 78.7963352038035 389569.674650645 5818973.25377331 74.1500783001567 389578.268613784 5818981.89443837 74.1500759876467</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_b359b22b-2cb5-490d-850e-5230790ed165">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389578.26861449 5818981.89443779 78.7963342088862 389572.903276896 5818987.37370883 78.7963360474207 389572.077364613 5818986.70584695 78.796335974658 389577.508928068 5818981.13062303 78.7963342968345 389578.26861449 5818981.89443779 78.7963342088862</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="fme-gen-0985efe7-d24e-4cfb-9246-098adcf44de1">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389559.261182812 5819037.3335667 78.9663358379556 389619.168828483 5819002.54974392 101.45572387826 389619.805473601 5819003.41738063 101.45572387826 389565.757563503 5819046.18700318 78.9663358379556 389559.261182812 5819037.3335667 78.9663358379556</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface gml:id="UUID_52e704c4-6858-45f0-9dfa-bcd0327a0c61">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389602.991375563 5819044.40387253 35 389604.115099385 5819051.24041322 35 389606.109077809 5819063.37142993 35 389609.859042144 5819062.92937265 35 389616.151992836 5819061.57978769 35 389622.541743151 5819059.55128163 35 389628.990793463 5819056.78851764 35 389635.461644158 5819053.23615887 35 389640.525642277 5819049.78653103 35 389631.029202839 5819036.14843452 35 389626.270356068 5819029.31412406 35 389624.67837478 5819030.49597308 35 389623.031262731 5819031.59969108 35 389621.332851081 5819032.62271085 35 389619.587090314 5819033.56265285 35 389617.798041045 5819034.41733079 35 389615.96986458 5819035.18475669 35 389614.106813233 5819035.86314552 35 389612.213220438 5819036.45091938 35 389610.293490666 5819036.94671109 35 389608.352089184 5819037.34936745 35 389606.393531668 5819037.65795189 35 389604.422373698 5819037.87174664 35 389605.7033517 5819044.71109105 35 389602.991375563 5819044.40387253 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_853ff1c6-84dc-4f3a-87f5-2d485499d296">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389648.318748284 5819043.54130969 59.6500582003239 389631.029202839 5819036.14843452 59.6500582003239 389640.525642277 5819049.78653103 59.6500582003239 389641.91679563 5819048.8388685 59.6500582003239 389648.318748284 5819043.54130969 59.6500582003239</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-4777e711-4537-45c1-922c-8bb38aca1603">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389631.029202839 5819036.14843452 35 389640.525642277 5819049.78653103 35 389640.525642277 5819049.78653103 59.6500582003239 389631.029202839 5819036.14843452 59.6500582003239 389631.029202839 5819036.14843452 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-a3749487-c5bd-4036-924a-00829b4afc5a">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389564.887771436 5819069.22662666 35 389581.498465832 5819078.60481698 35 389581.498465832 5819078.60481698 73.4873121927641 389564.887771436 5819069.22662666 73.4873121927641 389564.887771436 5819069.22662666 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-2333be53-7955-4fbc-8e55-6265cc0e1be2">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389500.023344705 5819107.60276937 66.1592781138443 389523.449044491 5819093.7432764 66.1592781138443 389523.449044491 5819093.7432764 35 389564.887771436 5819069.22662666 35 389564.887771436 5819069.22662666 73.4873121927641 389500.023344705 5819107.60276937 73.4873121927641 389500.023344705 5819107.60276937 66.1592781138443</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface gml:id="UUID_974d8a6c-0023-4ea0-a83e-5273de58a81f">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389586.4052283 5819062.64523836 35 389589.634690313 5819063.11979987 35 389590.959645389 5819051.42373505 35 389591.769891984 5819044.27127025 35 389589.445340923 5819044.00794009 35 389590.132925303 5819037.93827898 35 389589.120506472 5819037.85216897 35 389588.113369371 5819037.7177008 35 389587.113822489 5819037.53518268 35 389586.124156915 5819037.30503297 35 389585.14664109 5819037.02777919 35 389584.183515606 5819036.70405685 35 389583.23698807 5819036.33460797 35 389582.309228046 5819035.92027935 35 389581.402362078 5819035.46202071 35 389580.518468821 5819034.96088243 35 389579.659574271 5819034.41801317 35 389578.827647126 5819033.83465727 35 389574.585584086 5819039.99819274 35 389573.861855967 5819039.54103336 35 389573.160416853 5819039.05036009 35 389572.482817442 5819038.52725768 35 389571.830555732 5819037.97288257 35 389571.205073705 5819037.38846035 35 389570.607754141 5819036.77528302 35 389570.039917558 5819036.13470615 35 389569.502819297 5819035.46814591 35 389568.997646743 5819034.77707587 35 389568.525516702 5819034.06302381 35 389568.087472932 5819033.32756834 35 389567.684483834 5819032.57233532 35 389566.904492522 5819033.04536821 35 389553.572386952 5819041.13074625 35 389552.921493165 5819041.52548678 35 389553.921786375 5819043.87042661 35 389555.543874268 5819046.31116156 35 389557.7502572 5819048.79235474 35 389560.503435526 5819051.25866924 35 389563.765909597 5819053.65476818 35 389567.500179762 5819055.92531467 35 389571.668746368 5819058.01497181 35 389576.234109763 5819059.86840272 35 389581.158770291 5819061.43027053 35 389586.4052283 5819062.64523836 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_d821dd28-29b8-4ea3-a5a2-ffb9f99f210c">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389604.115099385 5819051.24041322 56.8200436400873 389590.959645389 5819051.42373505 56.8200436400873 389589.634690313 5819063.11979987 56.8200436400873 389591.935984138 5819063.45796935 56.8200436400873 389597.713538154 5819063.81312663 56.8200436400873 389603.700390704 5819063.65537334 56.8200436400873 389606.109077809 5819063.37142993 56.8200436400873 389604.115099385 5819051.24041322 56.8200436400873</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-e03a0f2f-8708-4b01-99b7-debe34e00156">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389590.959645389 5819051.42373505 35 389589.634690313 5819063.11979987 35 389589.634690313 5819063.11979987 56.8200436400873 389590.959645389 5819051.42373505 56.8200436400873 389590.959645389 5819051.42373505 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-286ea3a5-900d-4039-b129-285a6b893c82">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389606.109077809 5819063.37142993 35 389604.115099385 5819051.24041322 35 389604.115099385 5819051.24041322 56.8200436400873 389606.109077809 5819063.37142993 56.8200436400873 389606.109077809 5819063.37142993 35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
		</bldg:Building>
	</cityObjectMember>
</CityModel>