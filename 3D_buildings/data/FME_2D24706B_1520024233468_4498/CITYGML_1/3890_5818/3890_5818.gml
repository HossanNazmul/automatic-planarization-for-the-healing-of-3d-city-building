<?xml version="1.0" encoding="UTF-8"?>
<CityModel xmlns:bldg="http://www.opengis.net/citygml/building/1.0" xmlns:app="http://www.opengis.net/citygml/appearance/1.0" xmlns:smil20="http://www.w3.org/2001/SMIL20/" xmlns:grp="http://www.opengis.net/citygml/cityobjectgroup/1.0" xmlns:base="http://www.citygml.org/citygml/profiles/base/1.0" xmlns:frn="http://www.opengis.net/citygml/cityfurniture/1.0" xmlns:smil20lang="http://www.w3.org/2001/SMIL20/Language" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:wtr="http://www.opengis.net/citygml/waterbody/1.0" xmlns:tex="http://www.opengis.net/citygml/texturedsurface/1.0" xmlns:veg="http://www.opengis.net/citygml/vegetation/1.0" xmlns:sch="http://www.ascc.net/xml/schematron" xmlns:gen="http://www.opengis.net/citygml/generics/1.0" xmlns:dem="http://www.opengis.net/citygml/relief/1.0" xmlns:gml="http://www.opengis.net/gml" xmlns:tran="http://www.opengis.net/citygml/transportation/1.0" xmlns:xAL="urn:oasis:names:tc:ciq:xsdschema:xAL:2.0" xmlns:luse="http://www.opengis.net/citygml/landuse/1.0" xmlns="http://www.opengis.net/citygml/1.0">
	<gml:boundedBy>
		<gml:Envelope srsName="EPSG:25833" srsDimension="3">
			<gml:lowerCorner>389564.908728853 5818621.98417586 32.9300003051758</gml:lowerCorner>
			<gml:upperCorner>389632.519410789 5818700.31543508 76.3318475278365</gml:upperCorner>
		</gml:Envelope>
	</gml:boundedBy>
	<cityObjectMember>
		<bldg:Building gml:id="BLDG_0003000b001e8ef0">
			<gml:name>B7</gml:name>
			<bldg:roofType>1000</bldg:roofType>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360892">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389583.547202109 5818679.60137902 32.9300003051758 389583.547953699 5818679.60249252 68.173418200798 389589.854633319 5818698.14626062 68.173418200798 389589.853881725 5818698.1451471 32.9300003051758 389583.547202109 5818679.60137902 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360893">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389589.853881725 5818698.1451471 32.9300003051758 389589.854633319 5818698.14626062 68.173418200798 389593.606830259 5818696.7977544 68.173418200798 389593.606078661 5818696.79664088 32.9300003051758 389589.853881725 5818698.1451471 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360894">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389593.606078661 5818696.79664088 32.9300003051758 389593.606830259 5818696.7977544 68.173418200798 389593.261060193 5818695.81747176 68.173418200798 389593.260308596 5818695.81635825 32.9300003051758 389593.606078661 5818696.79664088 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360769">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389593.334459765 5818666.71187592 34.7599983215332 389593.335306714 5818666.71313068 74.4746314249052 389593.219284838 5818665.62563693 74.4746314249052 389593.21843789 5818665.62438218 34.7599983215332 389593.334459765 5818666.71187592 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360773">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389593.999212819 5818662.47186339 34.7599983215332 389594.000059768 5818662.47311814 74.4746314249052 389594.610211901 5818661.56547235 74.4746314249052 389594.609364951 5818661.5642176 34.7599983215332 389593.999212819 5818662.47186339 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360772">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389593.555941861 5818663.4716714 34.7599983215332 389593.55678881 5818663.47292615 74.4746314249052 389594.000059768 5818662.47311814 74.4746314249052 389593.999212819 5818662.47186339 34.7599983215332 389593.555941861 5818663.4716714 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360768">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389593.637561001 5818667.76270125 34.7599983215332 389593.63840795 5818667.76395601 74.4746314249052 389593.335306714 5818666.71313068 74.4746314249052 389593.334459765 5818666.71187592 34.7599983215332 389593.637561001 5818667.76270125 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360494">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389604.488676 5818651.20129843 32.9300003051758 389604.489329012 5818651.20226584 63.5502536068305 389606.943953609 5818652.29053053 63.5502536068305 389606.943300596 5818652.28956311 32.9300003051758 389604.488676 5818651.20129843 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360771">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389593.293020642 5818664.53326297 34.7599983215332 389593.29386759 5818664.53451772 74.4746314249052 389593.55678881 5818663.47292615 74.4746314249052 389593.555941861 5818663.4716714 34.7599983215332 389593.293020642 5818664.53326297 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360846">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389581.97510165 5818686.20443093 34.5400009155273 389581.975728168 5818686.20535914 63.9185715336953 389582.326280855 5818687.42069427 63.9185715336953 389582.325654338 5818687.41976606 34.5400009155273 389581.97510165 5818686.20443093 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360767">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389594.11853202 5818668.74492937 34.7599983215332 389594.119378969 5818668.74618412 74.4746314249052 389593.63840795 5818667.76395601 74.4746314249052 389593.637561001 5818667.76270125 34.7599983215332 389594.11853202 5818668.74492937 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360497">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389611.106040706 5818655.64969653 32.9300003051758 389611.106693722 5818655.65066394 63.5502536068305 389612.688326503 5818657.82043671 63.5502536068305 389612.687673487 5818657.81946929 32.9300003051758 389611.106040706 5818655.64969653 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360770">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389593.21843789 5818665.62438218 34.7599983215332 389593.219284838 5818665.62563693 74.4746314249052 389593.29386759 5818664.53451772 74.4746314249052 389593.293020642 5818664.53326297 34.7599983215332 389593.21843789 5818665.62438218 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360496">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389609.171657999 5818653.78753418 32.9300003051758 389609.172311013 5818653.7885016 63.5502536068305 389611.106693722 5818655.65066394 63.5502536068305 389611.106040706 5818655.64969653 32.9300003051758 389609.171657999 5818653.78753418 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360495">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389606.943300596 5818652.28956311 32.9300003051758 389606.943953609 5818652.29053053 63.5502536068305 389609.172311013 5818653.7885016 63.5502536068305 389609.171657999 5818653.78753418 32.9300003051758 389606.943300596 5818652.28956311 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360506">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389611.643444178 5818651.64876838 32.9300003051758 389611.644097194 5818651.6497358 63.5502536068305 389607.575120468 5818649.71108743 63.5502536068305 389607.574467455 5818649.71012002 32.9300003051758 389611.643444178 5818651.64876838 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360627">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389599.789574523 5818681.17477809 34.5699996948242 389599.790342365 5818681.17591565 70.5749528632974 389602.460259404 5818680.89107075 70.5749528632974 389602.45949156 5818680.88993319 34.5699996948242 389599.789574523 5818681.17477809 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360500">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389614.612639173 5818662.81079289 32.9300003051758 389614.613292191 5818662.81176031 63.5502536068305 389614.851717171 5818664.54437138 63.5502536068305 389614.851064153 5818664.54340396 32.9300003051758 389614.612639173 5818662.81079289 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360624">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389592.049793496 5818679.25790474 34.5699996948242 389592.050561332 5818679.2590423 70.5749528632974 389594.50520014 5818680.3473131 70.5749528632974 389594.504432302 5818680.34617554 34.5699996948242 389592.049793496 5818679.25790474 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360775">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389595.367859093 5818660.77631241 34.7599983215332 389595.368706043 5818660.77756715 74.4746314249052 389596.252495731 5818660.13334264 74.4746314249052 389596.25164878 5818660.13208789 34.7599983215332 389595.367859093 5818660.77631241 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360623">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389589.82142314 5818677.75992517 34.5699996948242 389589.822190975 5818677.76106273 70.5749528632974 389592.050561332 5818679.2590423 70.5749528632974 389592.049793496 5818679.25790474 34.5699996948242 389589.82142314 5818677.75992517 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360777">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389597.23388051 5818659.6511185 34.7599983215332 389597.234727462 5818659.65237325 74.4746314249052 389598.285556619 5818659.34927298 74.4746314249052 389598.284709667 5818659.34801823 34.7599983215332 389597.23388051 5818659.6511185 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360778">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389598.284709667 5818659.34801823 34.7599983215332 389598.285556619 5818659.34927298 74.4746314249052 389599.373054292 5818659.23325139 74.4746314249052 389599.372207339 5818659.23199664 34.7599983215332 389598.284709667 5818659.34801823 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360776">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389596.25164878 5818660.13208789 34.7599983215332 389596.252495731 5818660.13334264 74.4746314249052 389597.234727462 5818659.65237325 74.4746314249052 389597.23388051 5818659.6511185 34.7599983215332 389596.25164878 5818660.13208789 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360499">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389613.868499238 5818660.23092503 32.9300003051758 389613.869152255 5818660.23189245 63.5502536068305 389614.613292191 5818662.81176031 63.5502536068305 389614.612639173 5818662.81079289 32.9300003051758 389613.868499238 5818660.23092503 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360629">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389605.865668213 5818679.79919477 34.5699996948242 389605.866436059 5818679.80033233 70.5749528632974 389608.929256373 5818677.95347941 70.5749528632974 389608.928488525 5818677.95234185 34.5699996948242 389605.865668213 5818679.79919477 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360625">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389594.504432302 5818680.34617554 34.5699996948242 389594.50520014 5818680.3473131 70.5749528632974 389597.111524434 5818680.99280856 70.5749528632974 389597.110756594 5818680.991671 34.5699996948242 389594.504432302 5818680.34617554 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360628">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389602.45949156 5818680.88993319 34.5699996948242 389602.460259404 5818680.89107075 70.5749528632974 389605.866436059 5818679.80033233 70.5749528632974 389605.865668213 5818679.79919477 34.5699996948242 389602.45949156 5818680.88993319 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360626">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389597.110756594 5818680.991671 34.5699996948242 389597.111524434 5818680.99280856 70.5749528632974 389599.790342365 5818681.17591565 70.5749528632974 389599.789574523 5818681.17477809 34.5699996948242 389597.110756594 5818680.991671 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360498">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389612.687673487 5818657.81946929 32.9300003051758 389612.688326503 5818657.82043671 63.5502536068305 389613.869152255 5818660.23189245 63.5502536068305 389613.868499238 5818660.23092503 32.9300003051758 389612.687673487 5818657.81946929 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360504">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389615.986785747 5818657.4126811 32.9300003051758 389615.987438766 5818657.41364852 63.5502536068305 389614.321515089 5818654.62287462 63.5502536068305 389614.320862072 5818654.6219072 32.9300003051758 389615.986785747 5818657.4126811 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360782">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389583.705219068 5818693.20672093 34.5400009155273 389583.705845587 5818693.20764914 63.9185715336953 389584.278817323 5818691.5335145 63.9185715336953 389584.278190804 5818691.53258629 34.5400009155273 389583.705219068 5818693.20672093 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360780">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389582.277211408 5818695.84436305 34.5400009155273 389582.277837926 5818695.84529126 63.9185715336953 389583.127123878 5818694.57948657 63.9185715336953 389583.12649736 5818694.57855836 34.5400009155273 389582.277211408 5818695.84436305 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360781">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389583.12649736 5818694.57855836 34.5400009155273 389583.127123878 5818694.57948657 63.9185715336953 389583.705845587 5818693.20764914 63.9185715336953 389583.705219068 5818693.20672093 34.5400009155273 389583.12649736 5818694.57855836 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360779">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389599.372207339 5818659.23199664 34.7599983215332 389599.373054292 5818659.23325139 74.4746314249052 389599.532006945 5818659.23224188 74.4746314249052 389599.531159991 5818659.23098714 34.7599983215332 389599.372207339 5818659.23199664 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360507">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389607.574467455 5818649.71012002 32.9300003051758 389607.575120468 5818649.71108743 63.5502536068305 389604.331438952 5818648.7647631 63.5502536068305 389604.33078594 5818648.76379568 32.9300003051758 389607.574467455 5818649.71012002 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360805">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389565.355905275 5818685.57776979 34.5400009155273 389565.356531783 5818685.578698 63.9185715336953 389565.140819169 5818686.39847123 63.9185715336953 389565.140192661 5818686.39754302 34.5400009155273 389565.355905275 5818685.57776979 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360535">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389573.319966069 5818671.7923693 32.9300003051758 389573.320619062 5818671.79333673 63.5502536068305 389572.056695477 5818674.52163072 63.5502536068305 389572.056042486 5818674.52066329 32.9300003051758 389573.319966069 5818671.7923693 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360860">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389572.852946875 5818695.68030387 34.5400009155273 389572.853573388 5818695.68123208 63.9185715336953 389571.697238298 5818695.16856783 63.9185715336953 389571.696611786 5818695.16763961 34.5400009155273 389572.852946875 5818695.68030387 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360528">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389582.857044974 5818655.16329942 32.9300003051758 389582.857697973 5818655.16426685 63.5502536068305 389581.214441922 5818657.65046678 63.5502536068305 389581.213788925 5818657.64949936 32.9300003051758 389582.857044974 5818655.16329942 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360540">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389573.601616178 5818678.73213004 32.9300003051758 389573.60226917 5818678.73309748 63.5502536068305 389575.418934011 5818678.84974616 63.5502536068305 389575.418281017 5818678.84877873 32.9300003051758 389573.601616178 5818678.73213004 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360837">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389581.638275466 5818696.42470903 34.5400009155273 389581.638901983 5818696.42563725 63.9185715336953 389582.277837926 5818695.84529126 63.9185715336953 389582.277211408 5818695.84436305 34.5400009155273 389581.638275466 5818696.42470903 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360914">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389599.789576655 5818681.17478125 34.6699981689453 389599.790362088 5818681.17594487 71.499800088065 389598.894373982 5818681.16692188 71.499800088065 389598.89358855 5818681.16575825 34.6699981689453 389599.789576655 5818681.17478125 34.6699981689453</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360564">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389568.339862129 5818686.46378577 34.5400009155273 389568.340564371 5818686.46482618 67.4698521577859 389568.036482108 5818687.69261311 67.4698521577859 389568.035779867 5818687.6915727 34.5400009155273 389568.339862129 5818686.46378577 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360912">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389605.039272715 5818680.14580182 34.6699981689453 389605.040058151 5818680.14696544 71.499800088065 389602.460180855 5818680.89110318 71.499800088065 389602.459395421 5818680.88993956 34.6699981689453 389605.039272715 5818680.14580182 34.6699981689453</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360674">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389594.609942244 5818661.56507286 61.8300018310547 389594.610251508 5818661.56553103 76.3318475278365 389594.000099375 5818662.47317682 76.3318475278365 389593.999790112 5818662.47271865 61.8300018310547 389594.609942244 5818661.56507286 61.8300018310547</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360902">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389626.6040739 5818684.98976283 32.9300003051758 389626.60482552 5818684.99087633 68.173418200798 389627.169147868 5818684.78728676 68.173418200798 389627.168396247 5818684.78617326 32.9300003051758 389626.6040739 5818684.98976283 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360749">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389604.864520908 5818662.2651958 34.7599983215332 389604.865367866 5818662.26645055 74.4746314249052 389605.346338858 5818663.24867864 74.4746314249052 389605.3454919 5818663.24742389 34.7599983215332 389604.864520908 5818662.2651958 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360660">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389587.98536719 5818661.5633194 34.5699996948242 389587.986135023 5818661.56445695 70.5749528632974 389588.845417619 5818659.62632501 70.5749528632974 389588.844649785 5818659.62518746 34.5699996948242 389587.98536719 5818661.5633194 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360651">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389595.549769728 5818677.01117076 34.5699996948242 389595.550537567 5818677.01230832 70.5749528632974 389593.612398737 5818676.15302918 70.5749528632974 389593.6116309 5818676.15189162 34.5699996948242 389595.549769728 5818677.01117076 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360582">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389582.06949877 5818691.16723421 34.5400009155273 389582.070201021 5818691.16827462 67.4698521577859 389582.374283263 5818689.94048772 67.4698521577859 389582.373581011 5818689.93944731 34.5400009155273 389582.06949877 5818691.16723421 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360820">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389568.442179149 5818696.32448119 34.5400009155273 389568.442805658 5818696.3254094 63.9185715336953 389569.087273702 5818696.83801229 63.9185715336953 389569.086647192 5818696.83708408 34.5400009155273 389568.442179149 5818696.32448119 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360747">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389603.432386282 5818660.62291813 34.7599983215332 389603.433233239 5818660.62417288 74.4746314249052 389604.221141159 5818661.38266416 74.4746314249052 389604.220294202 5818661.38140941 34.7599983215332 389603.432386282 5818660.62291813 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360809">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389564.908728853 5818688.92402324 34.5400009155273 389564.909355361 5818688.92495144 63.9185715336953 389564.949371538 5818689.55280234 63.9185715336953 389564.948745031 5818689.55187413 34.5400009155273 389564.908728853 5818688.92402324 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360703">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389601.525503098 5818659.57035437 61.8300018310547 389601.525812363 5818659.57081254 76.3318475278365 389600.464217019 5818659.30789241 76.3318475278365 389600.463907754 5818659.30743424 61.8300018310547 389601.525503098 5818659.57035437 61.8300018310547</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360489">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389593.953974266 5818651.40167653 32.9300003051758 389593.95462727 5818651.40264395 63.5502536068305 389596.534500069 5818650.6585077 63.5502536068305 389596.533847063 5818650.65754028 32.9300003051758 389593.953974266 5818651.40167653 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360676">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389593.556519153 5818663.47252665 61.8300018310547 389593.556828416 5818663.47298482 76.3318475278365 389593.293907197 5818664.5345764 76.3318475278365 389593.293597934 5818664.53411823 61.8300018310547 389593.556519153 5818663.47252665 61.8300018310547</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360680">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389593.638138294 5818667.76355651 61.8300018310547 389593.638447557 5818667.76401468 76.3318475278365 389594.119418576 5818668.7462428 76.3318475278365 389594.119109313 5818668.74578463 61.8300018310547 389593.638138294 5818667.76355651 61.8300018310547</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360856">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389576.600428614 5818695.93645794 34.5400009155273 389576.601055129 5818695.93738615 63.9185715336953 389575.471327491 5818696.06800592 63.9185715336953 389575.470700977 5818696.06707771 34.5400009155273 389576.600428614 5818695.93645794 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360549">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389584.278156469 5818691.53253542 32.9300003051758 389584.278809468 5818691.53350286 63.5502536068305 389583.705837732 5818693.2076375 63.5502536068305 389583.705184733 5818693.20667006 32.9300003051758 389584.278156469 5818691.53253542 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360752">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389605.764614986 5818665.38574291 34.7599983215332 389605.765461945 5818665.38699766 74.4746314249052 389605.690879198 5818666.47811685 74.4746314249052 389605.690032239 5818666.47686209 34.7599983215332 389605.764614986 5818665.38574291 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360677">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389593.293597934 5818664.53411823 61.8300018310547 389593.293907197 5818664.5345764 76.3318475278365 389593.219324445 5818665.62569561 76.3318475278365 389593.219015182 5818665.62523744 61.8300018310547 389593.293597934 5818664.53411823 61.8300018310547</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360482">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389584.095729186 5818666.06653458 32.9300003051758 389584.096382185 5818666.06750201 63.5502536068305 389584.279489036 5818663.38871377 63.5502536068305 389584.278836037 5818663.38774634 32.9300003051758 389584.095729186 5818666.06653458 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360520">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389597.930113469 5818636.86106198 32.9300003051758 389597.930766476 5818636.86202939 63.5502536068305 389595.929734014 5818638.92385567 63.5502536068305 389595.929081008 5818638.92288826 32.9300003051758 389597.930113469 5818636.86106198 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360654">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389590.324786758 5818673.49877532 34.5699996948242 389590.325554593 5818673.49991287 70.5749528632974 389589.076718385 5818671.7866893 70.5749528632974 389589.075950552 5818671.78555175 34.5699996948242 389590.324786758 5818673.49877532 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360886">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389614.851064153 5818664.54340396 32.9300003051758 389614.851815766 5818664.54451745 68.173418200798 389619.192318461 5818677.5132738 68.173418200798 389619.191566845 5818677.5121603 32.9300003051758 389614.851064153 5818664.54340396 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360598">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389614.714593536 5818668.15963154 34.5699996948242 389614.715361389 5818668.16076909 70.5749528632974 389614.898469586 5818665.48196091 70.5749528632974 389614.897701734 5818665.48082336 34.5699996948242 389614.714593536 5818668.15963154 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360725">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389588.844653837 5818659.62519346 34.7599983215332 389588.845500782 5818659.62644821 74.4746314249052 389587.986218186 5818661.56458016 74.4746314249052 389587.985371242 5818661.56332541 34.7599983215332 389588.844653837 5818659.62519346 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360885">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389618.733203101 5818664.01338607 32.9300003051758 389618.733954716 5818664.01449956 68.173418200798 389614.851815766 5818664.54451745 68.173418200798 389614.851064153 5818664.54340396 32.9300003051758 389618.733203101 5818664.01338607 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360813">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389565.351085348 5818691.58110737 34.5400009155273 389565.351711856 5818691.58203558 63.9185715336953 389565.620830813 5818692.35078118 63.9185715336953 389565.620204305 5818692.34985297 34.5400009155273 389565.351085348 5818691.58110737 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360492">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389599.514647059 5818650.36992521 32.9300003051758 389599.515300067 5818650.37089263 63.5502536068305 389601.88301974 5818650.55677391 63.5502536068305 389601.88236673 5818650.55580649 32.9300003051758 389599.514647059 5818650.36992521 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360509">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389600.922272737 5818639.15161836 32.9300003051758 389600.922925746 5818639.15258577 63.5502536068305 389612.72899249 5818634.49272858 63.5502536068305 389612.728339474 5818634.49176117 32.9300003051758 389600.922272737 5818639.15161836 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360682">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389594.763336052 5818669.62957103 61.8300018310547 389594.763645315 5818669.6300292 76.3318475278365 389595.551553269 5818670.38852047 76.3318475278365 389595.551244006 5818670.3880623 61.8300018310547 389594.763336052 5818669.62957103 61.8300018310547</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360533">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389576.040005065 5818666.429165 32.9300003051758 389576.040658059 5818666.43013242 63.5502536068305 389574.649205058 5818669.0955493 63.5502536068305 389574.648552065 5818669.09458187 32.9300003051758 389576.040005065 5818666.429165 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360534">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389574.648552065 5818669.09458187 32.9300003051758 389574.649205058 5818669.0955493 63.5502536068305 389573.320619062 5818671.79333673 63.5502536068305 389573.319966069 5818671.7923693 32.9300003051758 389574.648552065 5818669.09458187 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360595">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389611.482835078 5818675.44893874 34.5699996948242 389611.483602928 5818675.4500763 70.5749528632974 389612.981588154 5818673.2217139 70.5749528632974 389612.980820303 5818673.22057635 34.5699996948242 389611.482835078 5818675.44893874 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360890">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389589.335769595 5818687.15224176 32.9300003051758 389589.33652119 5818687.15335526 68.173418200798 389584.026051081 5818672.94045396 68.173418200798 389584.025299491 5818672.93934047 32.9300003051758 389589.335769595 5818687.15224176 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360828">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389574.406285706 5818698.77370153 34.5400009155273 389574.406912219 5818698.77462974 63.9185715336953 389575.289209629 5818698.79599157 63.9185715336953 389575.288583115 5818698.79506336 34.5400009155273 389574.406285706 5818698.77370153 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360887">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389619.191566845 5818677.5121603 32.9300003051758 389619.192318461 5818677.5132738 68.173418200798 389612.83528805 5818682.47269519 68.173418200798 389612.834536439 5818682.47158169 32.9300003051758 389619.191566845 5818677.5121603 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360898">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389620.638795249 5818687.09922403 32.9300003051758 389620.639546866 5818687.10033754 68.173418200798 389620.302923023 5818686.11957501 68.173418200798 389620.302171407 5818686.11846151 32.9300003051758 389620.638795249 5818687.09922403 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360599">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389614.897701734 5818665.48082336 34.5699996948242 389614.898469586 5818665.48196091 70.5749528632974 389614.613624145 5818662.81205361 70.5749528632974 389614.612856292 5818662.81091606 34.5699996948242 389614.897701734 5818665.48082336 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360755">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389604.983840103 5818668.53826162 34.7599983215332 389604.984687061 5818668.53951638 74.4746314249052 389604.37453496 5818669.44716215 74.4746314249052 389604.373688002 5818669.44590739 34.7599983215332 389604.983840103 5818668.53826162 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360873">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389571.457589875 5818682.6013803 34.5400009155273 389571.458216386 5818682.60230851 63.9185715336953 389572.594219419 5818682.04604185 63.9185715336953 389572.593592907 5818682.04511364 34.5400009155273 389571.457589875 5818682.6013803 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360697">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389605.649170417 5818664.29910445 61.8300018310547 389605.649479684 5818664.29956263 76.3318475278365 389605.346378465 5818663.24873732 76.3318475278365 389605.346069199 5818663.24827915 61.8300018310547 389605.649170417 5818664.29910445 61.8300018310547</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360666">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389597.152103147 5818653.56960083 34.5699996948242 389597.152870987 5818653.57073837 70.5749528632974 389599.260989695 5818653.34583008 70.5749528632974 389599.260221854 5818653.34469253 34.5699996948242 389597.152103147 5818653.56960083 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360600">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389614.612856292 5818662.81091606 34.5699996948242 389614.613624145 5818662.81205361 70.5749528632974 389613.869479947 5818660.23217098 70.5749528632974 389613.868712095 5818660.23103343 34.5699996948242 389614.612856292 5818662.81091606 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360882">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389614.320862072 5818654.6219072 32.9300003051758 389614.321613684 5818654.62302069 68.173418200798 389615.98753736 5818657.41379459 68.173418200798 389615.986785747 5818657.4126811 32.9300003051758 389614.320862072 5818654.6219072 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360859">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389574.080738197 5818695.98438483 34.5400009155273 389574.08136471 5818695.98531305 63.9185715336953 389572.853573388 5818695.68123208 63.9185715336953 389572.852946875 5818695.68030387 34.5400009155273 389574.080738197 5818695.98438483 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360543">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389579.824606334 5818679.97459275 32.9300003051758 389579.82525933 5818679.97556018 63.5502536068305 389581.510899875 5818681.15312409 63.5502536068305 389581.510246878 5818681.15215665 32.9300003051758 389579.824606334 5818679.97459275 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360753">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389605.690032239 5818666.47686209 34.7599983215332 389605.690879198 5818666.47811685 74.4746314249052 389605.427957994 5818667.53970839 74.4746314249052 389605.427111036 5818667.53845363 34.7599983215332 389605.690032239 5818666.47686209 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360702">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389602.525314628 5818660.01362359 61.8300018310547 389602.525623894 5818660.01408176 76.3318475278365 389601.525812363 5818659.57081254 76.3318475278365 389601.525503098 5818659.57035437 61.8300018310547 389602.525314628 5818660.01362359 61.8300018310547</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360745">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389601.524925801 5818659.56949912 34.7599983215332 389601.525772756 5818659.57075386 74.4746314249052 389602.525584287 5818660.01402308 74.4746314249052 389602.524737331 5818660.01276833 34.7599983215332 389601.524925801 5818659.56949912 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360795">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389570.274332572 5818679.57648336 34.5400009155273 389570.274959083 5818679.57741156 63.9185715336953 389569.524270732 5818680.02592082 63.9185715336953 389569.523644222 5818680.02499262 34.5400009155273 389570.274332572 5818679.57648336 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360806">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389565.140192661 5818686.39754302 34.5400009155273 389565.140819169 5818686.39847123 63.9185715336953 389564.993748958 5818687.2334165 63.9185715336953 389564.993122451 5818687.2324883 34.5400009155273 389565.140192661 5818686.39754302 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360834">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389579.460922752 5818697.81580903 34.5400009155273 389579.461549268 5818697.81673725 63.9185715336953 389580.225943506 5818697.41496053 63.9185715336953 389580.22531699 5818697.41403232 34.5400009155273 389579.460922752 5818697.81580903 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360927">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389619.191603953 5818677.51221528 34.6699981689453 389619.1923894 5818677.5133789 71.499800088065 389614.851886705 5818664.54462254 71.499800088065 389614.851101261 5818664.54345893 34.6699981689453 389619.191603953 5818677.51221528 34.6699981689453</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360909">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389611.482687264 5818675.44897627 34.6699981689453 389611.483472704 5818675.45013988 71.499800088065 389609.621303478 5818677.38451597 71.499800088065 389609.620518038 5818677.38335236 34.6699981689453 389611.482687264 5818675.44897627 34.6699981689453</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360701">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389603.43296358 5818660.62377339 61.8300018310547 389603.433272846 5818660.62423156 76.3318475278365 389602.525623894 5818660.01408176 76.3318475278365 389602.525314628 5818660.01362359 61.8300018310547 389603.43296358 5818660.62377339 61.8300018310547</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360643">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389610.138394866 5818671.38492533 34.5699996948242 389610.139162715 5818671.38606289 70.5749528632974 389608.956380391 5818673.14553793 70.5749528632974 389608.955612543 5818673.14440038 34.5699996948242 389610.138394866 5818671.38492533 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360857">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389575.470700977 5818696.06707771 34.5400009155273 389575.471327491 5818696.06800592 63.9185715336953 389575.343306444 5818696.07157137 63.9185715336953 389575.34267993 5818696.07064316 34.5400009155273 389575.470700977 5818696.06707771 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360708">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389611.507355236 5818667.38889993 34.7599983215332 389611.508202199 5818667.39015468 74.4746314249052 389611.652781156 5818665.2750157 74.4746314249052 389611.651934193 5818665.27376095 34.7599983215332 389611.507355236 5818667.38889993 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360610">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389596.533775963 5818650.65759692 34.5699996948242 389596.534543803 5818650.65873446 70.5749528632974 389593.954651623 5818651.40287631 70.5749528632974 389593.953883786 5818651.40173877 34.5699996948242 389596.533775963 5818650.65759692 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360609">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389599.203693044 5818650.37275221 34.5699996948242 389599.204460885 5818650.37388975 70.5749528632974 389596.534543803 5818650.65873446 70.5749528632974 389596.533775963 5818650.65759692 34.5699996948242 389599.203693044 5818650.37275221 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360833">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389578.66536973 5818698.15165571 34.5400009155273 389578.665996246 5818698.15258392 63.9185715336953 389579.461549268 5818697.81673725 63.9185715336953 389579.460922752 5818697.81580903 34.5400009155273 389578.66536973 5818698.15165571 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360734">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389593.611634952 5818676.15189763 34.7599983215332 389593.612481901 5818676.15315239 74.4746314249052 389595.55062073 5818677.01243153 74.4746314249052 389595.54977378 5818677.01117677 34.7599983215332 389593.611634952 5818676.15189763 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360867">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389567.949521027 5818688.9535099 34.5400009155273 389567.950147537 5818688.95443811 63.9185715336953 389568.036406376 5818687.69250091 63.9185715336953 389568.035779867 5818687.6915727 34.5400009155273 389567.949521027 5818688.9535099 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360858">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389575.34267993 5818696.07064316 34.5400009155273 389575.343306444 5818696.07157137 63.9185715336953 389574.08136471 5818695.98531305 63.9185715336953 389574.080738197 5818695.98438483 34.5400009155273 389575.34267993 5818696.07064316 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360505">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389614.320862072 5818654.6219072 32.9300003051758 389614.321515089 5818654.62287462 63.5502536068305 389611.644097194 5818651.6497358 63.5502536068305 389611.643444178 5818651.64876838 32.9300003051758 389614.320862072 5818654.6219072 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360850">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389582.06949877 5818691.16723421 34.5400009155273 389582.070125288 5818691.16816242 63.9185715336953 389581.557459022 5818692.32449336 63.9185715336953 389581.556832504 5818692.32356515 34.5400009155273 389582.06949877 5818691.16723421 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360619">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389584.380410133 5818668.73661446 34.5699996948242 389584.381177964 5818668.73775201 70.5749528632974 389585.125322271 5818671.31763481 70.5749528632974 389585.12455444 5818671.31649725 34.5699996948242 389584.380410133 5818668.73661446 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360567">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389568.083706544 5818690.21125404 34.5400009155273 389568.084408785 5818690.21229445 67.4698521577859 389568.434961498 5818691.42762961 67.4698521577859 389568.434259256 5818691.4265892 34.5400009155273 389568.083706544 5818690.21125404 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360614">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389587.510431772 5818656.09859104 34.5699996948242 389587.511199605 5818656.09972859 70.5749528632974 389586.013214189 5818658.32809106 70.5749528632974 389586.012446357 5818658.32695351 34.5699996948242 389587.510431772 5818656.09859104 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360880">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389607.574467455 5818649.71012002 32.9300003051758 389607.575219062 5818649.71123349 68.173418200798 389611.644195788 5818651.64988187 68.173418200798 389611.643444178 5818651.64876838 32.9300003051758 389607.574467455 5818649.71012002 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360865">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389568.434259256 5818691.4265892 34.5400009155273 389568.434885765 5818691.42751741 63.9185715336953 389568.084333053 5818690.21218225 63.9185715336953 389568.083706544 5818690.21125404 34.5400009155273 389568.434259256 5818691.4265892 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360574">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389574.080738197 5818695.98438483 34.5400009155273 389574.081440442 5818695.98542525 67.4698521577859 389575.343382176 5818696.07168357 67.4698521577859 389575.34267993 5818696.07064316 34.5400009155273 389574.080738197 5818695.98438483 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360690">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389602.731981538 5818670.87889237 61.8300018310547 389602.732290804 5818670.87935055 76.3318475278365 389603.616080459 5818670.23512602 76.3318475278365 389603.615771193 5818670.23466785 61.8300018310547 389602.731981538 5818670.87889237 61.8300018310547</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360916">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389595.360193784 5818680.61179399 34.6699981689453 389595.360979214 5818680.61295761 71.499800088065 389592.050581055 5818679.25907152 71.499800088065 389592.049795629 5818679.2579079 34.6699981689453 389595.360193784 5818680.61179399 34.6699981689453</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360896">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389594.985330105 5818695.1903319 32.9300003051758 389594.986081704 5818695.19144541 68.173418200798 389595.333343948 5818696.18604252 68.173418200798 389595.332592349 5818696.18492901 32.9300003051758 389594.985330105 5818695.1903319 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360716">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389603.433279463 5818653.99894853 34.7599983215332 389603.43412642 5818654.00020327 74.4746314249052 389601.376219502 5818653.49053149 74.4746314249052 389601.375372547 5818653.48927675 34.7599983215332 389603.433279463 5818653.99894853 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360687">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389599.61142303 5818671.7789837 61.8300018310547 389599.611732294 5818671.77944187 76.3318475278365 389600.699229959 5818671.66342026 76.3318475278365 389600.698920694 5818671.66296208 61.8300018310547 389599.61142303 5818671.7789837 61.8300018310547</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360849">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389582.373581011 5818689.93944731 34.5400009155273 389582.374207529 5818689.94037552 63.9185715336953 389582.070125288 5818691.16816242 63.9185715336953 389582.06949877 5818691.16723421 34.5400009155273 389582.373581011 5818689.93944731 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360925">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389599.621615291 5818687.3598599 34.6699981689453 389599.622400724 5818687.36102353 71.499800088065 389612.835358989 5818682.47280028 71.499800088065 389612.834573546 5818682.47163666 34.6699981689453 389599.621615291 5818687.3598599 34.6699981689453</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360918">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389590.711739521 5818678.42695792 34.6699981689453 389590.712524947 5818678.42812154 71.499800088065 389589.141934875 5818677.17845513 71.499800088065 389589.14114945 5818677.17729151 34.6699981689453 389590.711739521 5818678.42695792 34.6699981689453</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360508">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389604.33078594 5818648.76379568 32.9300003051758 389604.331438952 5818648.7647631 63.5502536068305 389600.922925746 5818639.15258577 63.5502536068305 389600.922272737 5818639.15161836 32.9300003051758 389604.33078594 5818648.76379568 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360743">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389599.531159991 5818659.23098714 34.7599983215332 389599.532006945 5818659.23224188 74.4746314249052 389600.464177412 5818659.30783374 74.4746314249052 389600.463330458 5818659.30657899 34.7599983215332 389599.531159991 5818659.23098714 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360731">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389589.075954603 5818671.78555775 34.7599983215332 389589.076801548 5818671.78681251 74.4746314249052 389590.325637756 5818673.50003608 74.4746314249052 389590.32479081 5818673.49878132 34.7599983215332 389589.075954603 5818671.78555775 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360515">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389615.376140732 5818634.83136156 32.9300003051758 389615.37679375 5818634.83232897 63.5502536068305 389610.756541621 5818621.98514326 63.5502536068305 389610.755888606 5818621.98417586 32.9300003051758 389615.376140732 5818634.83136156 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360620">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389585.12455444 5818671.31649725 34.5699996948242 389585.125322271 5818671.31763481 70.5749528632974 389586.306154947 5818673.72910447 70.5749528632974 389586.305387115 5818673.72796692 34.5699996948242 389585.12455444 5818671.31649725 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360844">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389580.673750761 5818684.04628636 34.5400009155273 389580.674377277 5818684.04721457 63.9185715336953 389581.419459663 5818685.06936032 63.9185715336953 389581.418833146 5818685.06843212 34.5400009155273 389580.673750761 5818684.04628636 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360803">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389565.986972246 5818684.00613727 34.5400009155273 389565.987598755 5818684.00706548 63.9185715336953 389565.639777587 5818684.77991646 63.9185715336953 389565.639151079 5818684.77898825 34.5400009155273 389565.986972246 5818684.00613727 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360652">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389593.6116309 5818676.15189162 34.5699996948242 389593.612398737 5818676.15302918 70.5749528632974 389591.852917428 5818674.97025133 70.5749528632974 389591.852149592 5818674.96911377 34.5699996948242 389593.6116309 5818676.15189162 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360589">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389579.762493444 5818683.16905105 34.5400009155273 389579.763195694 5818683.17009146 67.4698521577859 389578.713451535 5818682.46442094 67.4698521577859 389578.712749287 5818682.46338053 34.5400009155273 389579.762493444 5818683.16905105 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360511">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389629.100052922 5818684.08764175 32.9300003051758 389629.100705948 5818684.08860919 63.5502536068305 389632.519410789 5818682.85232686 63.5502536068305 389632.518757761 5818682.85135942 32.9300003051758 389629.100052922 5818684.08764175 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360713">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389608.65826209 5818657.5113441 34.7599983215332 389608.659109051 5818657.51259885 74.4746314249052 389607.131746345 5818656.04226039 74.4746314249052 389607.130899385 5818656.04100564 34.7599983215332 389608.65826209 5818657.5113441 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360926">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389612.834573546 5818682.47163666 34.6699981689453 389612.835358989 5818682.47280028 71.499800088065 389619.1923894 5818677.5133789 71.499800088065 389619.191603953 5818677.51221528 34.6699981689453 389612.834573546 5818682.47163666 34.6699981689453</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360537">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389570.856752282 5818677.27793751 32.9300003051758 389570.857405273 5818677.27890494 63.5502536068305 389571.429443068 5818677.51306079 63.5502536068305 389571.428790078 5818677.51209335 32.9300003051758 389570.856752282 5818677.27793751 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360636">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389608.658258038 5818657.51133809 34.5699996948242 389608.659025887 5818657.51247564 70.5749528632974 389609.907861973 5818659.22569918 70.5749528632974 389609.907094123 5818659.22456163 34.5699996948242 389608.658258038 5818657.51133809 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360672">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389596.252226074 5818660.13294314 61.8300018310547 389596.252535337 5818660.13340132 76.3318475278365 389595.368745649 5818660.77762583 76.3318475278365 389595.368436386 5818660.77716766 61.8300018310547 389596.252226074 5818660.13294314 61.8300018310547</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360588">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389580.673750761 5818684.04628636 34.5400009155273 389580.674453011 5818684.04732677 67.4698521577859 389579.763195694 5818683.17009146 67.4698521577859 389579.762493444 5818683.16905105 34.5400009155273 389580.673750761 5818684.04628636 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360750">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389605.3454919 5818663.24742389 34.7599983215332 389605.346338858 5818663.24867864 74.4746314249052 389605.649440077 5818664.29950395 74.4746314249052 389605.648593118 5818664.2982492 34.7599983215332 389605.3454919 5818663.24742389 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360617">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389584.278672874 5818663.38789861 34.5699996948242 389584.279440705 5818663.38903616 70.5749528632974 389584.096332479 5818666.06784453 70.5749528632974 389584.095564649 5818666.06670698 34.5699996948242 389584.278672874 5818663.38789861 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360562">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389569.558201659 5818684.25771431 34.5400009155273 389569.558903901 5818684.25875472 67.4698521577859 389568.853230671 5818685.3084952 67.4698521577859 389568.852528429 5818685.3074548 34.5400009155273 389569.558201659 5818684.25771431 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360608">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389599.514682034 5818650.36997703 34.5699996948242 389599.515449875 5818650.37111457 70.5749528632974 389599.204460885 5818650.37388975 70.5749528632974 389599.203693044 5818650.37275221 34.5699996948242 389599.514682034 5818650.36997703 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360591">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389577.556414231 5818681.95071625 34.5400009155273 389577.557116479 5818681.95175666 67.4698521577859 389576.329325179 5818681.64767566 67.4698521577859 389576.328622932 5818681.64663525 34.5400009155273 389577.556414231 5818681.95071625 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360812">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389565.143857635 5818690.79348594 34.5400009155273 389565.144484143 5818690.79441415 63.9185715336953 389565.351711856 5818691.58203558 63.9185715336953 389565.351085348 5818691.58110737 34.5400009155273 389565.143857635 5818690.79348594 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360908">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389612.980663909 5818673.22062664 34.6699981689453 389612.981449352 5818673.22179026 71.499800088065 389611.483472704 5818675.45013988 71.499800088065 389611.482687264 5818675.44897627 34.6699981689453 389612.980663909 5818673.22062664 34.6699981689453</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360810">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389564.948745031 5818689.55187413 34.5400009155273 389564.949371538 5818689.55280234 63.9185715336953 389565.02775907 5818690.17656665 63.9185715336953 389565.027132562 5818690.17563844 34.5400009155273 389564.948745031 5818689.55187413 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360863">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389569.735610224 5818693.58473381 34.5400009155273 389569.736236734 5818693.58566201 63.9185715336953 389568.991154306 5818692.56351625 63.9185715336953 389568.990527796 5818692.56258804 34.5400009155273 389569.735610224 5818693.58473381 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360688">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389600.698920694 5818671.66296208 61.8300018310547 389600.699229959 5818671.66342026 76.3318475278365 389601.7500591 5818671.36031996 76.3318475278365 389601.749749834 5818671.35986179 61.8300018310547 389600.698920694 5818671.66296208 61.8300018310547</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360861">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389571.696611786 5818695.16763961 34.5400009155273 389571.697238298 5818695.16856783 63.9185715336953 389570.647494097 5818694.46289733 63.9185715336953 389570.646867586 5818694.46196911 34.5400009155273 389571.696611786 5818695.16763961 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360488">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389591.542513828 5818652.58249627 32.9300003051758 389591.543166832 5818652.58346369 63.5502536068305 389593.95462727 5818651.40264395 63.5502536068305 389593.953974266 5818651.40167653 32.9300003051758 389591.542513828 5818652.58249627 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360728">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389587.331118389 5818665.73636415 34.7599983215332 389587.331965333 5818665.7376189 74.4746314249052 389587.556874222 5818667.84573002 74.4746314249052 389587.556027277 5818667.84447526 34.7599983215332 389587.331118389 5818665.73636415 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360759">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389601.749172538 5818671.35900653 34.7599983215332 389601.750019493 5818671.36026129 74.4746314249052 389600.699190353 5818671.66336158 74.4746314249052 389600.698343398 5818671.66210682 34.7599983215332 389601.749172538 5818671.35900653 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360679">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389593.335037057 5818666.71273118 61.8300018310547 389593.33534632 5818666.71318935 76.3318475278365 389593.638447557 5818667.76401468 76.3318475278365 389593.638138294 5818667.76355651 61.8300018310547 389593.335037057 5818666.71273118 61.8300018310547</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360835">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389580.22531699 5818697.41403232 34.5400009155273 389580.225943506 5818697.41496053 63.9185715336953 389580.95312984 5818696.95011462 63.9185715336953 389580.952503323 5818696.9491864 34.5400009155273 389580.22531699 5818697.41403232 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360659">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389587.475693312 5818663.62121905 34.5699996948242 389587.476461145 5818663.6223566 70.5749528632974 389587.986135023 5818661.56445695 70.5749528632974 389587.98536719 5818661.5633194 34.5699996948242 389587.475693312 5818663.62121905 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360862">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389570.646867586 5818694.46196911 34.5400009155273 389570.647494097 5818694.46289733 63.9185715336953 389569.736236734 5818693.58566201 63.9185715336953 389569.735610224 5818693.58473381 34.5400009155273 389570.646867586 5818694.46196911 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360915">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389598.89358855 5818681.16575825 34.6699981689453 389598.894373982 5818681.16692188 71.499800088065 389595.360979214 5818680.61295761 71.499800088065 389595.360193784 5818680.61179399 34.6699981689453 389598.89358855 5818681.16575825 34.6699981689453</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360658">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389587.331114337 5818665.73635815 34.5699996948242 389587.33188217 5818665.73749569 70.5749528632974 389587.476461145 5818663.6223566 70.5749528632974 389587.475693312 5818663.62121905 34.5699996948242 389587.331114337 5818665.73635815 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360756">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389604.373688002 5818669.44590739 34.7599983215332 389604.37453496 5818669.44716215 74.4746314249052 389603.616040852 5818670.23506734 74.4746314249052 389603.615193895 5818670.23381259 34.7599983215332 389604.373688002 5818669.44590739 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360538">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389571.428790078 5818677.51209335 32.9300003051758 389571.429443068 5818677.51306079 63.5502536068305 389571.877179292 5818678.88077804 63.5502536068305 389571.876526301 5818678.87981061 32.9300003051758 389571.428790078 5818677.51209335 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360818">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389567.288385127 5818695.15267532 34.5400009155273 389567.289011636 5818695.15360353 63.9185715336953 389567.84255073 5818695.76283744 63.9185715336953 389567.84192422 5818695.76190923 34.5400009155273 389567.288385127 5818695.15267532 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360606">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389604.48883514 5818651.20135512 34.5699996948242 389604.489602985 5818651.20249266 70.5749528632974 389601.883278787 5818650.55699702 70.5749528632974 389601.882510944 5818650.55585948 34.5699996948242 389604.48883514 5818651.20135512 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360727">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389587.475697364 5818663.62122505 34.7599983215332 389587.476544308 5818663.6224798 74.4746314249052 389587.331965333 5818665.7376189 74.4746314249052 389587.331118389 5818665.73636415 34.7599983215332 389587.475697364 5818663.62122505 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360649">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389599.722823363 5818677.66542053 34.5699996948242 389599.723591204 5818677.66655809 70.5749528632974 389597.608444544 5818677.52198 70.5749528632974 389597.607676704 5818677.52084244 34.5699996948242 389599.722823363 5818677.66542053 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360553">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389583.817782198 5818700.31446763 32.9300003051758 389583.818435196 5818700.31543508 63.5502536068305 389589.854534727 5818698.14611455 63.5502536068305 389589.853881725 5818698.1451471 32.9300003051758 389583.817782198 5818700.31446763 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360899">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389620.302171407 5818686.11846151 32.9300003051758 389620.302923023 5818686.11957501 68.173418200798 389622.081427861 5818685.48185228 68.173418200798 389622.080676244 5818685.48073878 32.9300003051758 389620.302171407 5818686.11846151 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360877">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389612.728339474 5818634.49176117 32.9300003051758 389612.729091085 5818634.49287464 68.173418200798 389600.923024339 5818639.15273183 68.173418200798 389600.922272737 5818639.15161836 32.9300003051758 389612.728339474 5818634.49176117 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360852">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389580.851159317 5818693.37330562 34.5400009155273 389580.851785834 5818693.37423383 63.9185715336953 389579.974547246 5818694.285488 63.9185715336953 389579.973920729 5818694.28455978 34.5400009155273 389580.851159317 5818693.37330562 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360512">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389632.518757761 5818682.85135942 32.9300003051758 389632.519410789 5818682.85232686 63.5502536068305 389616.922062041 5818639.13059527 63.5502536068305 389616.921409023 5818639.12962786 32.9300003051758 389632.518757761 5818682.85135942 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360798">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389568.150873091 5818681.10570715 34.5400009155273 389568.1514996 5818681.10663535 63.9185715336953 389567.539644234 5818681.7310177 63.9185715336953 389567.539017725 5818681.7300895 34.5400009155273 389568.150873091 5818681.10570715 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360530">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389579.854748691 5818659.80500214 32.9300003051758 389579.855401687 5818659.80596956 63.5502536068305 389578.539594741 5818661.98841782 63.5502536068305 389578.538941746 5818661.9874504 32.9300003051758 389579.854748691 5818659.80500214 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360481">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389584.380572531 5818668.736422 32.9300003051758 389584.38122553 5818668.73738943 63.5502536068305 389584.096382185 5818666.06750201 63.5502536068305 389584.095729186 5818666.06653458 32.9300003051758 389584.380572531 5818668.736422 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360632">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389601.375368495 5818653.48927074 34.5699996948242 389601.376136338 5818653.49040829 70.5749528632974 389603.434043255 5818654.00008007 70.5749528632974 389603.433275411 5818653.99894252 34.5699996948242 389601.375368495 5818653.48927074 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360699">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389604.865098207 5818662.26605105 61.8300018310547 389604.865407473 5818662.26650923 76.3318475278365 389604.221180766 5818661.38272284 76.3318475278365 389604.2208715 5818661.38226466 61.8300018310547 389604.865098207 5818662.26605105 61.8300018310547</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360573">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389572.852946875 5818695.68030387 34.5400009155273 389572.85364912 5818695.68134428 67.4698521577859 389574.081440442 5818695.98542525 67.4698521577859 389574.080738197 5818695.98438483 34.5400009155273 389572.852946875 5818695.68030387 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360700">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389604.2208715 5818661.38226466 61.8300018310547 389604.221180766 5818661.38272284 76.3318475278365 389603.433272846 5818660.62423156 76.3318475278365 389603.43296358 5818660.62377339 61.8300018310547 389604.2208715 5818661.38226466 61.8300018310547</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360748">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389604.220294202 5818661.38140941 34.7599983215332 389604.221141159 5818661.38266416 74.4746314249052 389604.865367866 5818662.26645055 74.4746314249052 389604.864520908 5818662.2651958 34.7599983215332 389604.220294202 5818661.38140941 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360486">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389587.510570657 5818656.09849353 32.9300003051758 389587.511223658 5818656.09946095 63.5502536068305 389589.373389765 5818654.16508831 63.5502536068305 389589.372736763 5818654.16412089 32.9300003051758 389587.510570657 5818656.09849353 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360480">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389584.815478129 5818670.43592008 32.9300003051758 389584.816131129 5818670.43688751 63.5502536068305 389584.38122553 5818668.73738943 63.5502536068305 389584.380572531 5818668.736422 32.9300003051758 389584.815478129 5818670.43592008 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360647">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389603.867978626 5818676.85295093 34.5699996948242 389603.868746471 5818676.85408849 70.5749528632974 389601.831709886 5818677.44164968 70.5749528632974 389601.830942043 5818677.44051212 34.5699996948242 389603.867978626 5818676.85295093 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360811">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389565.027132562 5818690.17563844 34.5400009155273 389565.02775907 5818690.17656665 63.9185715336953 389565.144484143 5818690.79441415 63.9185715336953 389565.143857635 5818690.79348594 34.5400009155273 389565.027132562 5818690.17563844 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360638">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389610.839458341 5818661.12861481 34.5699996948242 389610.84022619 5818661.12975235 70.5749528632974 389611.42778913 5818663.16678149 70.5749528632974 389611.42702128 5818663.16564395 34.5699996948242 389610.839458341 5818661.12861481 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360879">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389604.33078594 5818648.76379568 32.9300003051758 389604.331537546 5818648.76490916 68.173418200798 389607.575219062 5818649.71123349 68.173418200798 389607.574467455 5818649.71012002 32.9300003051758 389604.33078594 5818648.76379568 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360907">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389614.068932723 5818670.76601074 34.6699981689453 389614.069718166 5818670.76717435 71.499800088065 389612.981449352 5818673.22179026 71.499800088065 389612.980663909 5818673.22062664 34.6699981689453 389614.068932723 5818670.76601074 34.6699981689453</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360646">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389605.77203879 5818675.92058973 34.5699996948242 389605.772806636 5818675.92172728 70.5749528632974 389603.868746471 5818676.85408849 70.5749528632974 389603.867978626 5818676.85295093 34.5699996948242 389605.77203879 5818675.92058973 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360903">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389627.168396247 5818684.78617326 32.9300003051758 389627.169147868 5818684.78728676 68.173418200798 389629.100804544 5818684.08875525 68.173418200798 389629.100052922 5818684.08764175 32.9300003051758 389627.168396247 5818684.78617326 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360572">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389571.696611786 5818695.16763961 34.5400009155273 389571.69731403 5818695.16868003 67.4698521577859 389572.85364912 5818695.68134428 67.4698521577859 389572.852946875 5818695.68030387 34.5400009155273 389571.696611786 5818695.16763961 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360854">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389578.95177119 5818695.02963964 34.5400009155273 389578.952397706 5818695.03056785 63.9185715336953 389577.816394709 5818695.58683453 63.9185715336953 389577.815768194 5818695.58590632 34.5400009155273 389578.95177119 5818695.02963964 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360594">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389609.620655189 5818677.3833259 34.5699996948242 389609.621423038 5818677.38446346 70.5749528632974 389611.483602928 5818675.4500763 70.5749528632974 389611.482835078 5818675.44893874 34.5699996948242 389609.620655189 5818677.3833259 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360584">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389582.459839844 5818688.67751016 34.5400009155273 389582.460542095 5818688.67855057 67.4698521577859 389582.326356588 5818687.42080647 67.4698521577859 389582.325654338 5818687.41976606 34.5400009155273 389582.459839844 5818688.67751016 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360684">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389596.458892989 5818670.99821208 61.8300018310547 389596.459202253 5818670.99867026 76.3318475278365 389597.45901381 5818671.44193946 76.3318475278365 389597.458704546 5818671.44148128 61.8300018310547 389596.458892989 5818670.99821208 61.8300018310547</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360662">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389590.027432227 5818657.86571237 34.5699996948242 389590.028200062 5818657.86684992 70.5749528632974 389591.498544099 5818656.33949245 70.5749528632974 389591.497776264 5818656.33835491 34.5699996948242 389590.027432227 5818657.86571237 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360683">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389595.551244006 5818670.3880623 61.8300018310547 389595.551553269 5818670.38852047 76.3318475278365 389596.459202253 5818670.99867026 76.3318475278365 389596.458892989 5818670.99821208 61.8300018310547 389595.551244006 5818670.3880623 61.8300018310547</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360555">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389583.547202109 5818679.60137902 32.9300003051758 389583.547855108 5818679.60234645 63.5502536068305 389584.02595249 5818672.94030789 63.5502536068305 389584.025299491 5818672.93934047 32.9300003051758 389583.547202109 5818679.60137902 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360565">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389568.035779867 5818687.6915727 34.5400009155273 389568.036482108 5818687.69261311 67.4698521577859 389567.950223269 5818688.95455031 67.4698521577859 389567.949521027 5818688.9535099 34.5400009155273 389568.035779867 5818687.6915727 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360645">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389607.485268635 5818674.67175783 34.5699996948242 389607.486036482 5818674.67289539 70.5749528632974 389605.772806636 5818675.92172728 70.5749528632974 389605.77203879 5818675.92058973 34.5699996948242 389607.485268635 5818674.67175783 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360650">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389597.607676704 5818677.52084244 34.5699996948242 389597.608444544 5818677.52198 70.5749528632974 389595.550537567 5818677.01230832 70.5749528632974 389595.549769728 5818677.01117076 34.5699996948242 389597.607676704 5818677.52084244 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360698">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389605.346069199 5818663.24827915 61.8300018310547 389605.346378465 5818663.24873732 76.3318475278365 389604.865407473 5818662.26650923 76.3318475278365 389604.865098207 5818662.26605105 61.8300018310547 389605.346069199 5818663.24827915 61.8300018310547</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360751">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389605.648593118 5818664.2982492 34.7599983215332 389605.649440077 5818664.29950395 74.4746314249052 389605.765461945 5818665.38699766 74.4746314249052 389605.764614986 5818665.38574291 34.7599983215332 389605.648593118 5818664.2982492 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360585">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389582.325654338 5818687.41976606 34.5400009155273 389582.326356588 5818687.42080647 67.4698521577859 389581.9758039 5818686.20547134 67.4698521577859 389581.97510165 5818686.20443093 34.5400009155273 389582.325654338 5818687.41976606 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360550">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389583.705184733 5818693.20667006 32.9300003051758 389583.705837732 5818693.2076375 63.5502536068305 389583.127116024 5818694.57947494 63.5502536068305 389583.126463025 5818694.57850749 32.9300003051758 389583.705184733 5818693.20667006 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360539">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389571.876526301 5818678.87981061 32.9300003051758 389571.877179292 5818678.88077804 63.5502536068305 389573.60226917 5818678.73309748 63.5502536068305 389573.601616178 5818678.73213004 32.9300003051758 389571.876526301 5818678.87981061 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360706">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389610.138398918 5818671.38493133 34.7599983215332 389610.13924588 5818671.38618609 74.4746314249052 389610.998528381 5818669.44805423 74.4746314249052 389610.997681418 5818669.44679947 34.7599983215332 389610.138398918 5818671.38493133 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360722">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389593.211010282 5818655.08952905 34.7599983215332 389593.211857231 5818655.0907838 74.4746314249052 389591.498627263 5818656.33961566 74.4746314249052 389591.497780316 5818656.33836091 34.7599983215332 389593.211010282 5818655.08952905 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360563">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389568.852528429 5818685.3074548 34.5400009155273 389568.853230671 5818685.3084952 67.4698521577859 389568.340564371 5818686.46482618 67.4698521577859 389568.339862129 5818686.46378577 34.5400009155273 389568.852528429 5818685.3074548 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360744">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389600.463330458 5818659.30657899 34.7599983215332 389600.464177412 5818659.30783374 74.4746314249052 389601.525772756 5818659.57075386 74.4746314249052 389601.524925801 5818659.56949912 34.7599983215332 389600.463330458 5818659.30657899 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360514">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389616.425238196 5818637.75032096 32.9300003051758 389616.425891214 5818637.75128837 63.5502536068305 389615.37679375 5818634.83232897 63.5502536068305 389615.376140732 5818634.83136156 32.9300003051758 389616.425238196 5818637.75032096 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360485">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389586.012596495 5818658.32683926 32.9300003051758 389586.013249495 5818658.32780668 63.5502536068305 389587.511223658 5818656.09946095 63.5502536068305 389587.510570657 5818656.09849353 32.9300003051758 389586.012596495 5818658.32683926 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360490">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389596.533847063 5818650.65754028 32.9300003051758 389596.534500069 5818650.6585077 63.5502536068305 389599.017766888 5818650.3783465 63.5502536068305 389599.01711388 5818650.37737908 32.9300003051758 389596.533847063 5818650.65754028 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360794">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389571.060456632 5818679.19412777 34.5400009155273 389571.061083144 5818679.19505597 63.9185715336953 389570.274959083 5818679.57741156 63.9185715336953 389570.274332572 5818679.57648336 34.5400009155273 389571.060456632 5818679.19412777 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360889">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389599.621578184 5818687.35980493 32.9300003051758 389599.622329785 5818687.36091843 68.173418200798 389589.33652119 5818687.15335526 68.173418200798 389589.335769595 5818687.15224176 32.9300003051758 389599.621578184 5818687.35980493 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360692">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389604.3742653 5818669.44676265 61.8300018310547 389604.374574567 5818669.44722083 76.3318475278365 389604.984726668 5818668.53957505 76.3318475278365 389604.984417401 5818668.53911688 61.8300018310547 389604.3742653 5818669.44676265 61.8300018310547</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360675">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389593.999790112 5818662.47271865 61.8300018310547 389594.000099375 5818662.47317682 76.3318475278365 389593.556828416 5818663.47298482 76.3318475278365 389593.556519153 5818663.47252665 61.8300018310547 389593.999790112 5818662.47271865 61.8300018310547</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360523">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389591.924101882 5818643.34259275 32.9300003051758 389591.924754886 5818643.34356016 63.5502536068305 389590.001086688 5818645.62240258 63.5502536068305 389590.000433685 5818645.62143517 32.9300003051758 389591.924101882 5818643.34259275 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360787">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389583.053397554 5818682.78609222 34.5400009155273 389583.054024073 5818682.78702043 63.9185715336953 389581.510907729 5818681.15313573 63.9185715336953 389581.510281212 5818681.15220752 34.5400009155273 389583.053397554 5818682.78609222 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360644">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389608.955612543 5818673.14440038 34.5699996948242 389608.956380391 5818673.14553793 70.5749528632974 389607.486036482 5818674.67289539 70.5749528632974 389607.485268635 5818674.67175783 34.5699996948242 389608.955612543 5818673.14440038 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360729">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389587.556027277 5818667.84447526 34.7599983215332 389587.556874222 5818667.84573002 74.4746314249052 389588.144437229 5818669.88275926 74.4746314249052 389588.143590285 5818669.8815045 34.7599983215332 389587.556027277 5818667.84447526 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360917">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389592.049795629 5818679.2579079 34.6699981689453 389592.050581055 5818679.25907152 71.499800088065 389590.712524947 5818678.42812154 71.499800088065 389590.711739521 5818678.42695792 34.6699981689453 389592.049795629 5818679.2579079 34.6699981689453</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360590">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389578.712749287 5818682.46338053 34.5400009155273 389578.713451535 5818682.46442094 67.4698521577859 389577.557116479 5818681.95175666 67.4698521577859 389577.556414231 5818681.95071625 34.5400009155273 389578.712749287 5818682.46338053 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360593">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389609.43710328 5818677.54086973 34.5699996948242 389609.437871129 5818677.54200729 70.5749528632974 389609.621423038 5818677.38446346 70.5749528632974 389609.620655189 5818677.3833259 34.5699996948242 389609.43710328 5818677.54086973 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360657">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389587.556023226 5818667.84446926 34.5699996948242 389587.556791058 5818667.84560681 70.5749528632974 389587.33188217 5818665.73749569 70.5749528632974 389587.331114337 5818665.73635815 34.5699996948242 389587.556023226 5818667.84446926 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360804">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389565.639151079 5818684.77898825 34.5400009155273 389565.639777587 5818684.77991646 63.9185715336953 389565.356531783 5818685.578698 63.9185715336953 389565.355905275 5818685.57776979 34.5400009155273 389565.639151079 5818684.77898825 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360556">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389575.194745238 5818681.55907136 34.5400009155273 389575.195447484 5818681.56011177 67.4698521577859 389575.067383451 5818681.56141729 67.4698521577859 389575.066681205 5818681.56037688 34.5400009155273 389575.194745238 5818681.55907136 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360783">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389584.278190804 5818691.53258629 34.5400009155273 389584.278817323 5818691.5335145 63.9185715336953 389584.540865803 5818689.41168405 63.9185715336953 389584.540239284 5818689.41075584 34.5400009155273 389584.278190804 5818691.53258629 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360786">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389584.00054225 5818684.88492271 34.5400009155273 389584.001168768 5818684.88585092 63.9185715336953 389583.054024073 5818682.78702043 63.9185715336953 389583.053397554 5818682.78609222 34.5400009155273 389584.00054225 5818684.88492271 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360639">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389611.42702128 5818663.16564395 34.5699996948242 389611.42778913 5818663.16678149 70.5749528632974 389611.652697991 5818665.27489249 70.5749528632974 389611.651930141 5818665.27375495 34.5699996948242 389611.42702128 5818663.16564395 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360819">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389567.84192422 5818695.76190923 34.5400009155273 389567.84255073 5818695.76283744 63.9185715336953 389568.442805658 5818696.3254094 63.9185715336953 389568.442179149 5818696.32448119 34.5400009155273 389567.84192422 5818695.76190923 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360881">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389611.643444178 5818651.64876838 32.9300003051758 389611.644195788 5818651.64988187 68.173418200798 389614.321613684 5818654.62302069 68.173418200798 389614.320862072 5818654.6219072 32.9300003051758 389611.643444178 5818651.64876838 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360758">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389602.731404241 5818670.87803711 34.7599983215332 389602.732251197 5818670.87929187 74.4746314249052 389601.750019493 5818671.36026129 74.4746314249052 389601.749172538 5818671.35900653 34.7599983215332 389602.731404241 5818670.87803711 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360715">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389605.371418196 5818654.85822774 34.7599983215332 389605.372265154 5818654.85948249 74.4746314249052 389603.43412642 5818654.00020327 74.4746314249052 389603.433279463 5818653.99894853 34.7599983215332 389605.371418196 5818654.85822774 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360712">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389609.907098175 5818659.22456763 34.7599983215332 389609.907945137 5818659.22582238 74.4746314249052 389608.659109051 5818657.51259885 74.4746314249052 389608.65826209 5818657.5113441 34.7599983215332 389609.907098175 5818659.22456763 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360527">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389584.558414807 5818652.71627393 32.9300003051758 389584.559067806 5818652.71724135 63.5502536068305 389582.857697973 5818655.16426685 63.5502536068305 389582.857044974 5818655.16329942 32.9300003051758 389584.558414807 5818652.71627393 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360872">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389570.435440292 5818683.34646015 34.5400009155273 389570.436066803 5818683.34738835 63.9185715336953 389571.458216386 5818682.60230851 63.9185715336953 389571.457589875 5818682.6013803 34.5400009155273 389570.435440292 5818683.34646015 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360761">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389599.610845734 5818671.77812844 34.7599983215332 389599.611692688 5818671.7793832 74.4746314249052 389598.520569563 5818671.70480088 74.4746314249052 389598.51972261 5818671.70354613 34.7599983215332 389599.610845734 5818671.77812844 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360575">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389575.34267993 5818696.07064316 34.5400009155273 389575.343382176 5818696.07168357 67.4698521577859 389576.601130862 5818695.93749835 67.4698521577859 389576.600428614 5818695.93645794 34.5400009155273 389575.34267993 5818696.07064316 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360760">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389600.698343398 5818671.66210682 34.7599983215332 389600.699190353 5818671.66336158 74.4746314249052 389599.611692688 5818671.7793832 74.4746314249052 389599.610845734 5818671.77812844 34.7599983215332 389600.698343398 5818671.66210682 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360724">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389590.027436279 5818657.86571838 34.7599983215332 389590.028283225 5818657.86697312 74.4746314249052 389588.845500782 5818659.62644821 74.4746314249052 389588.844653837 5818659.62519346 34.7599983215332 389590.027436279 5818657.86571838 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360637">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389609.907094123 5818659.22456163 34.5699996948242 389609.907861973 5818659.22569918 70.5749528632974 389610.84022619 5818661.12975235 70.5749528632974 389610.839458341 5818661.12861481 34.5699996948242 389609.907094123 5818659.22456163 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360704">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389600.463907754 5818659.30743424 61.8300018310547 389600.464217019 5818659.30789241 76.3318475278365 389599.532046551 5818659.23230056 76.3318475278365 389599.531737287 5818659.23184239 61.8300018310547 389600.463907754 5818659.30743424 61.8300018310547</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360797">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389568.813811553 5818680.53589075 34.5400009155273 389568.814438063 5818680.53681895 63.9185715336953 389568.1514996 5818681.10663535 63.9185715336953 389568.150873091 5818681.10570715 34.5400009155273 389568.813811553 5818680.53589075 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360827">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389573.52926041 5818698.67656143 34.5400009155273 389573.529886923 5818698.67748964 63.9185715336953 389574.406912219 5818698.77462974 63.9185715336953 389574.406285706 5818698.77370153 34.5400009155273 389573.52926041 5818698.67656143 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360801">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389566.485899426 5818683.12391358 34.5400009155273 389566.486525935 5818683.12484179 63.9185715336953 389566.226263657 5818683.55975004 63.9185715336953 389566.225637149 5818683.55882184 34.5400009155273 389566.485899426 5818683.12391358 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360560">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389571.457589875 5818682.6013803 34.5400009155273 389571.458292118 5818682.60242071 67.4698521577859 389570.436142535 5818683.34750055 67.4698521577859 389570.435440292 5818683.34646015 34.5400009155273 389571.457589875 5818682.6013803 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360603">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389611.106237744 5818655.64977869 34.5699996948242 389611.107005593 5818655.65091623 70.5749528632974 389609.172611807 5818653.78874322 70.5749528632974 389609.171843958 5818653.78760568 34.5699996948242 389611.106237744 5818655.64977869 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360710">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389611.427025331 5818663.16564995 34.7599983215332 389611.427872295 5818663.1669047 74.4746314249052 389610.840309355 5818661.12987556 74.4746314249052 389610.839462392 5818661.12862081 34.7599983215332 389611.427025331 5818663.16564995 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360615">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389586.012446357 5818658.32695351 34.5699996948242 389586.013214189 5818658.32809106 70.5749528632974 389584.924938989 5818660.78272115 70.5749528632974 389584.924171158 5818660.7815836 34.5699996948242 389586.012446357 5818658.32695351 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360653">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389591.852149592 5818674.96911377 34.5699996948242 389591.852917428 5818674.97025133 70.5749528632974 389590.325554593 5818673.49991287 70.5749528632974 389590.324786758 5818673.49877532 34.5699996948242 389591.852149592 5818674.96911377 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360836">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389580.952503323 5818696.9491864 34.5400009155273 389580.95312984 5818696.95011462 63.9185715336953 389581.638901983 5818696.42563725 63.9185715336953 389581.638275466 5818696.42470903 34.5400009155273 389580.952503323 5818696.9491864 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360826">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389572.663619176 5818698.50413776 34.5400009155273 389572.664245688 5818698.50506597 63.9185715336953 389573.529886923 5818698.67748964 63.9185715336953 389573.52926041 5818698.67656143 34.5400009155273 389572.663619176 5818698.50413776 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360709">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389611.651934193 5818665.27376095 34.7599983215332 389611.652781156 5818665.2750157 74.4746314249052 389611.427872295 5818663.1669047 74.4746314249052 389611.427025331 5818663.16564995 34.7599983215332 389611.651934193 5818665.27376095 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360605">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389606.943473793 5818652.28962603 34.5699996948242 389606.94424164 5818652.29076358 70.5749528632974 389604.489602985 5818651.20249266 70.5749528632974 389604.48883514 5818651.20135512 34.5699996948242 389606.943473793 5818652.28962603 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360607">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389601.882510944 5818650.55585948 34.5699996948242 389601.883278787 5818650.55699702 70.5749528632974 389599.515449875 5818650.37111457 70.5749528632974 389599.514682034 5818650.36997703 34.5699996948242 389601.882510944 5818650.55585948 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360853">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389579.973920729 5818694.28455978 34.5400009155273 389579.974547246 5818694.285488 63.9185715336953 389578.952397706 5818695.03056785 63.9185715336953 389578.95177119 5818695.02963964 34.5400009155273 389579.973920729 5818694.28455978 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360814">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389565.620204305 5818692.34985297 34.5400009155273 389565.620830813 5818692.35078118 63.9185715336953 389565.949914157 5818693.09549902 63.9185715336953 389565.949287649 5818693.09457081 34.5400009155273 389565.620204305 5818692.34985297 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360911">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389607.450737296 5818678.96497985 34.6699981689453 389607.451522734 5818678.96614347 71.499800088065 389605.040058151 5818680.14696544 71.499800088065 389605.039272715 5818680.14580182 34.6699981689453 389607.450737296 5818678.96497985 34.6699981689453</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360487">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389589.372736763 5818654.16412089 32.9300003051758 389589.373389765 5818654.16508831 63.5502536068305 389591.543166832 5818652.58346369 63.5502536068305 389591.542513828 5818652.58249627 32.9300003051758 389589.372736763 5818654.16412089 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360816">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389566.337025914 5818693.81071016 34.5400009155273 389566.337652423 5818693.81163837 63.9185715336953 389566.786539749 5818694.50189758 63.9185715336953 389566.78591324 5818694.50096937 34.5400009155273 389566.337025914 5818693.81071016 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360596">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389612.980820303 5818673.22057635 34.5699996948242 389612.981588154 5818673.2217139 70.5749528632974 389614.069863201 5818670.76708394 70.5749528632974 389614.069095349 5818670.76594639 34.5699996948242 389612.980820303 5818673.22057635 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360576">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389576.600428614 5818695.93645794 34.5400009155273 389576.601130862 5818695.93749835 67.4698521577859 389577.816470442 5818695.58694674 67.4698521577859 389577.815768194 5818695.58590632 34.5400009155273 389576.600428614 5818695.93645794 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360901">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389622.435753915 5818686.46908366 32.9300003051758 389622.436505533 5818686.47019717 68.173418200798 389626.60482552 5818684.99087633 68.173418200798 389626.6040739 5818684.98976283 32.9300003051758 389622.435753915 5818686.46908366 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360855">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389577.815768194 5818695.58590632 34.5400009155273 389577.816394709 5818695.58683453 63.9185715336953 389576.601055129 5818695.93738615 63.9185715336953 389576.600428614 5818695.93645794 34.5400009155273 389577.815768194 5818695.58590632 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360910">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389609.620518038 5818677.38335236 34.6699981689453 389609.621303478 5818677.38451597 71.499800088065 389607.451522734 5818678.96614347 71.499800088065 389607.450737296 5818678.96497985 34.6699981689453 389609.620518038 5818677.38335236 34.6699981689453</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360689">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389601.749749834 5818671.35986179 61.8300018310547 389601.7500591 5818671.36031996 76.3318475278365 389602.732290804 5818670.87935055 76.3318475278365 389602.731981538 5818670.87889237 61.8300018310547 389601.749749834 5818671.35986179 61.8300018310547</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360519">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389599.977779028 5818634.84442706 32.9300003051758 389599.978432036 5818634.84539447 63.5502536068305 389597.930766476 5818636.86202939 63.5502536068305 389597.930113469 5818636.86106198 32.9300003051758 389599.977779028 5818634.84442706 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360479">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389584.025299491 5818672.93934047 32.9300003051758 389584.02595249 5818672.94030789 63.5502536068305 389584.816131129 5818670.43688751 63.5502536068305 389584.815478129 5818670.43592008 32.9300003051758 389584.025299491 5818672.93934047 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360732">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389590.32479081 5818673.49878132 34.7599983215332 389590.325637756 5818673.50003608 74.4746314249052 389591.853000592 5818674.97037454 74.4746314249052 389591.852153644 5818674.96911978 34.7599983215332 389590.32479081 5818673.49878132 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360524">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389590.000433685 5818645.62143517 32.9300003051758 389590.001086688 5818645.62240258 63.5502536068305 389588.130751058 5818647.94569898 63.5502536068305 389588.130098056 5818647.94473157 32.9300003051758 389590.000433685 5818645.62143517 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360841">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389577.556414231 5818681.95071625 34.5400009155273 389577.557040746 5818681.95164446 63.9185715336953 389578.713375803 5818682.46430874 63.9185715336953 389578.712749287 5818682.46338053 34.5400009155273 389577.556414231 5818681.95071625 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360842">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389578.712749287 5818682.46338053 34.5400009155273 389578.713375803 5818682.46430874 63.9185715336953 389579.763119961 5818683.16997926 63.9185715336953 389579.762493444 5818683.16905105 34.5400009155273 389578.712749287 5818682.46338053 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360796">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389569.523644222 5818680.02499262 34.5400009155273 389569.524270732 5818680.02592082 63.9185715336953 389568.814438063 5818680.53681895 63.9185715336953 389568.813811553 5818680.53589075 34.5400009155273 389569.523644222 5818680.02499262 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360774">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389594.609364951 5818661.5642176 34.7599983215332 389594.610211901 5818661.56547235 74.4746314249052 389595.368706043 5818660.77756715 74.4746314249052 389595.367859093 5818660.77631241 34.7599983215332 389594.609364951 5818661.5642176 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360561">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389570.435440292 5818683.34646015 34.5400009155273 389570.436142535 5818683.34750055 67.4698521577859 389569.558903901 5818684.25875472 67.4698521577859 389569.558201659 5818684.25771431 34.5400009155273 389570.435440292 5818683.34646015 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360714">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389607.130899385 5818656.04100564 34.7599983215332 389607.131746345 5818656.04226039 74.4746314249052 389605.372265154 5818654.85948249 74.4746314249052 389605.371418196 5818654.85822774 34.7599983215332 389607.130899385 5818656.04100564 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360883">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389615.986785747 5818657.4126811 32.9300003051758 389615.98753736 5818657.41379459 68.173418200798 389618.064192018 5818660.59996475 68.173418200798 389618.063440403 5818660.59885126 32.9300003051758 389615.986785747 5818657.4126811 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360784">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389584.540239284 5818689.41075584 34.5400009155273 389584.540865803 5818689.41168405 63.9185715336953 389584.651765143 5818687.29272859 63.9185715336953 389584.651138624 5818687.29180039 34.5400009155273 389584.540239284 5818689.41075584 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360552">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389582.277177073 5818695.84431218 32.9300003051758 389582.277830071 5818695.84527962 63.5502536068305 389583.818435196 5818700.31543508 63.5502536068305 389583.817782198 5818700.31446763 32.9300003051758 389582.277177073 5818695.84431218 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360878">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389600.922272737 5818639.15161836 32.9300003051758 389600.923024339 5818639.15273183 68.173418200798 389604.331537546 5818648.76490916 68.173418200798 389604.33078594 5818648.76379568 32.9300003051758 389600.922272737 5818639.15161836 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360808">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389564.915799095 5818688.0764831 34.5400009155273 389564.916425603 5818688.07741131 63.9185715336953 389564.909355361 5818688.92495144 63.9185715336953 389564.908728853 5818688.92402324 34.5400009155273 389564.915799095 5818688.0764831 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360839">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389575.194745238 5818681.55907136 34.5400009155273 389575.195371751 5818681.55999956 63.9185715336953 389576.329249446 5818681.64756346 63.9185715336953 389576.328622932 5818681.64663525 34.5400009155273 389575.194745238 5818681.55907136 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360557">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389575.066681205 5818681.56037688 34.5400009155273 389575.067383451 5818681.56141729 67.4698521577859 389573.809634756 5818681.69560247 67.4698521577859 389573.80893251 5818681.69456206 34.5400009155273 389575.066681205 5818681.56037688 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360621">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389586.305387115 5818673.72796692 34.5699996948242 389586.306154947 5818673.72910447 70.5749528632974 389587.887796982 5818675.89888973 70.5749528632974 389587.887029149 5818675.89775217 34.5699996948242 389586.305387115 5818673.72796692 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360545">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389583.05336322 5818682.78604136 32.9300003051758 389583.054016218 5818682.78700879 63.5502536068305 389584.001160914 5818684.88583928 63.5502536068305 389584.000507915 5818684.88487184 32.9300003051758 389583.05336322 5818682.78604136 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360924">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389589.335806702 5818687.15229673 34.6699981689453 389589.336592127 5818687.15346036 71.499800088065 389599.622400724 5818687.36102353 71.499800088065 389599.621615291 5818687.3598599 34.6699981689453 389589.335806702 5818687.15229673 34.6699981689453</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360691">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389603.615771193 5818670.23466785 61.8300018310547 389603.616080459 5818670.23512602 76.3318475278365 389604.374574567 5818669.44722083 76.3318475278365 389604.3742653 5818669.44676265 61.8300018310547 389603.615771193 5818670.23466785 61.8300018310547</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360631">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389599.370643305 5818653.34309366 34.5699996948242 389599.371411147 5818653.34423121 70.5749528632974 389601.376136338 5818653.49040829 70.5749528632974 389601.375368495 5818653.48927074 34.5699996948242 389599.370643305 5818653.34309366 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360888">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389612.834536439 5818682.47158169 32.9300003051758 389612.83528805 5818682.47269519 68.173418200798 389599.622329785 5818687.36091843 68.173418200798 389599.621578184 5818687.35980493 32.9300003051758 389612.834536439 5818682.47158169 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360838">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389574.853356367 5818681.56757429 34.5400009155273 389574.85398288 5818681.5685025 63.9185715336953 389575.195371751 5818681.55999956 63.9185715336953 389575.194745238 5818681.55907136 34.5400009155273 389574.853356367 5818681.56757429 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360891">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389584.025299491 5818672.93934047 32.9300003051758 389584.026051081 5818672.94045396 68.173418200798 389583.547953699 5818679.60249252 68.173418200798 389583.547202109 5818679.60137902 32.9300003051758 389584.025299491 5818672.93934047 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360612">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389591.542405233 5818652.58256738 34.5699996948242 389591.543173068 5818652.58370492 70.5749528632974 389589.373379702 5818654.16534142 70.5749528632974 389589.372611867 5818654.16420388 34.5699996948242 389591.542405233 5818652.58256738 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360829">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389575.288583115 5818698.79506336 34.5400009155273 389575.289209629 5818698.79599157 63.9185715336953 389576.151172765 5818698.74297712 63.9185715336953 389576.15054625 5818698.74204891 34.5400009155273 389575.288583115 5818698.79506336 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360559">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389572.593592907 5818682.04511364 34.5400009155273 389572.594295152 5818682.04615405 67.4698521577859 389571.458292118 5818682.60242071 67.4698521577859 389571.457589875 5818682.6013803 34.5400009155273 389572.593592907 5818682.04511364 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360757">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389603.615193895 5818670.23381259 34.7599983215332 389603.616040852 5818670.23506734 74.4746314249052 389602.732251197 5818670.87929187 74.4746314249052 389602.731404241 5818670.87803711 34.7599983215332 389603.615193895 5818670.23381259 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360592">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389576.328622932 5818681.64663525 34.5400009155273 389576.329325179 5818681.64767566 67.4698521577859 389575.195447484 5818681.56011177 67.4698521577859 389575.194745238 5818681.55907136 34.5400009155273 389576.328622932 5818681.64663525 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360919">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389589.14114945 5818677.17729151 34.6699981689453 389589.141934875 5818677.17845513 71.499800088065 389586.791298042 5818674.48289982 71.499800088065 389586.790512619 5818674.4817362 34.6699981689453 389589.14114945 5818677.17729151 34.6699981689453</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360656">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389588.143586233 5818669.8814985 34.5699996948242 389588.144354066 5818669.88263605 70.5749528632974 389587.556791058 5818667.84560681 70.5749528632974 389587.556023226 5818667.84446926 34.5699996948242 389588.143586233 5818669.8814985 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360823">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389570.370707913 5818697.63809442 34.5400009155273 389570.371334424 5818697.63902263 63.9185715336953 389570.994070885 5818697.93965276 63.9185715336953 389570.993444374 5818697.93872454 34.5400009155273 389570.370707913 5818697.63809442 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360681">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389594.119109313 5818668.74578463 61.8300018310547 389594.119418576 5818668.7462428 76.3318475278365 389594.763645315 5818669.6300292 76.3318475278365 389594.763336052 5818669.62957103 61.8300018310547 389594.119109313 5818668.74578463 61.8300018310547</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360521">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389595.929081008 5818638.92288826 32.9300003051758 389595.929734014 5818638.92385567 63.5502536068305 389593.901778827 5818641.11039017 63.5502536068305 389593.901125822 5818641.10942276 32.9300003051758 389595.929081008 5818638.92288826 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360766">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389594.762758759 5818669.62871577 34.7599983215332 389594.763605709 5818669.62997052 74.4746314249052 389594.119378969 5818668.74618412 74.4746314249052 389594.11853202 5818668.74492937 34.7599983215332 389594.762758759 5818669.62871577 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360604">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389609.171843958 5818653.78760568 34.5699996948242 389609.172611807 5818653.78874322 70.5749528632974 389606.94424164 5818652.29076358 70.5749528632974 389606.943473793 5818652.28962603 34.5699996948242 389609.171843958 5818653.78760568 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360848">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389582.459839844 5818688.67751016 34.5400009155273 389582.460466362 5818688.67843837 63.9185715336953 389582.374207529 5818689.94037552 63.9185715336953 389582.373581011 5818689.93944731 34.5400009155273 389582.459839844 5818688.67751016 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360904">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389614.851101261 5818664.54345893 34.6699981689453 389614.851886705 5818664.54462254 71.499800088065 389614.898319806 5818665.48208158 71.499800088065 389614.897534363 5818665.48091797 34.6699981689453 389614.851101261 5818664.54345893 34.6699981689453</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360742">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389608.955616595 5818673.14440638 34.7599983215332 389608.956463556 5818673.14566114 74.4746314249052 389610.01229403 5818671.61041435 74.4746314249052 389610.011447068 5818671.60915959 34.7599983215332 389608.955616595 5818673.14440638 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360536">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389572.056042486 5818674.52066329 32.9300003051758 389572.056695477 5818674.52163072 63.5502536068305 389570.857405273 5818677.27890494 63.5502536068305 389570.856752282 5818677.27793751 32.9300003051758 389572.056042486 5818674.52066329 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360711">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389610.839462392 5818661.12862081 34.7599983215332 389610.840309355 5818661.12987556 74.4746314249052 389609.907945137 5818659.22582238 74.4746314249052 389609.907098175 5818659.22456763 34.7599983215332 389610.839462392 5818661.12862081 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360673">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389595.368436386 5818660.77716766 61.8300018310547 389595.368745649 5818660.77762583 76.3318475278365 389594.610251508 5818661.56553103 76.3318475278365 389594.609942244 5818661.56507286 61.8300018310547 389595.368436386 5818660.77716766 61.8300018310547</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360501">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389614.851064153 5818664.54340396 32.9300003051758 389614.851717171 5818664.54437138 63.5502536068305 389618.733856121 5818664.01435349 63.5502536068305 389618.733203101 5818664.01338607 32.9300003051758 389614.851064153 5818664.54340396 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360871">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389569.558201659 5818684.25771431 34.5400009155273 389569.558828169 5818684.25864252 63.9185715336953 389570.436066803 5818683.34738835 63.9185715336953 389570.435440292 5818683.34646015 34.5400009155273 389569.558201659 5818684.25771431 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360548">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389584.540204949 5818689.41070497 32.9300003051758 389584.540857948 5818689.41167241 63.5502536068305 389584.278809468 5818691.53350286 63.5502536068305 389584.278156469 5818691.53253542 32.9300003051758 389584.540204949 5818689.41070497 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360707">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389610.997681418 5818669.44679947 34.7599983215332 389610.998528381 5818669.44805423 74.4746314249052 389611.508202199 5818667.39015468 74.4746314249052 389611.507355236 5818667.38889993 34.7599983215332 389610.997681418 5818669.44679947 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360616">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389584.924171158 5818660.7815836 34.5699996948242 389584.924938989 5818660.78272115 70.5749528632974 389584.279440705 5818663.38903616 70.5749528632974 389584.278672874 5818663.38789861 34.5699996948242 389584.924171158 5818660.7815836 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360633">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389603.433275411 5818653.99894252 34.5699996948242 389603.434043255 5818654.00008007 70.5749528632974 389605.372181989 5818654.85935928 70.5749528632974 389605.371414143 5818654.85822174 34.5699996948242 389603.433275411 5818653.99894252 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360705">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389610.011447068 5818671.60915959 34.7599983215332 389610.01229403 5818671.61041435 74.4746314249052 389610.13924588 5818671.38618609 74.4746314249052 389610.138398918 5818671.38493133 34.7599983215332 389610.011447068 5818671.60915959 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360661">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389588.844649785 5818659.62518746 34.5699996948242 389588.845417619 5818659.62632501 70.5749528632974 389590.028200062 5818657.86684992 70.5749528632974 389590.027432227 5818657.86571237 34.5699996948242 389588.844649785 5818659.62518746 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360821">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389569.086647192 5818696.83708408 34.5400009155273 389569.087273702 5818696.83801229 63.9185715336953 389569.770386784 5818697.29678234 63.9185715336953 389569.769760273 5818697.29585413 34.5400009155273 389569.086647192 5818696.83708408 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360738">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389601.830946095 5818677.44051812 34.7599983215332 389601.83179305 5818677.44177288 74.4746314249052 389603.868829635 5818676.8542117 74.4746314249052 389603.867982678 5818676.85295694 34.7599983215332 389601.830946095 5818677.44051812 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360791">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389575.418315352 5818678.84882959 34.5400009155273 389575.418941865 5818678.8497578 63.9185715336953 389573.602277025 5818678.73310912 63.9185715336953 389573.601650512 5818678.73218091 34.5400009155273 389575.418315352 5818678.84882959 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360762">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389598.51972261 5818671.70354613 34.7599983215332 389598.520569563 5818671.70480088 74.4746314249052 389597.458974203 5818671.44188078 74.4746314249052 389597.458127251 5818671.44062603 34.7599983215332 389598.51972261 5818671.70354613 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360866">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389568.083706544 5818690.21125404 34.5400009155273 389568.084333053 5818690.21218225 63.9185715336953 389567.950147537 5818688.95443811 63.9185715336953 389567.949521027 5818688.9535099 34.5400009155273 389568.083706544 5818690.21125404 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360765">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389595.550666712 5818670.38720704 34.7599983215332 389595.551513663 5818670.3884618 74.4746314249052 389594.763605709 5818669.62997052 74.4746314249052 389594.762758759 5818669.62871577 34.7599983215332 389595.550666712 5818670.38720704 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360601">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389613.868712095 5818660.23103343 34.5699996948242 389613.869479947 5818660.23217098 70.5749528632974 389612.688647433 5818657.82070143 70.5749528632974 389612.687879582 5818657.81956388 34.5699996948242 389613.868712095 5818660.23103343 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360763">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389597.458127251 5818671.44062603 34.7599983215332 389597.458974203 5818671.44188078 74.4746314249052 389596.459162647 5818670.99861158 74.4746314249052 389596.458315695 5818670.99735683 34.7599983215332 389597.458127251 5818671.44062603 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360832">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389577.844712825 5818698.41901537 34.5400009155273 389577.845339341 5818698.41994358 63.9185715336953 389578.665996246 5818698.15258392 63.9185715336953 389578.66536973 5818698.15165571 34.5400009155273 389577.844712825 5818698.41901537 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360847">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389582.325654338 5818687.41976606 34.5400009155273 389582.326280855 5818687.42069427 63.9185715336953 389582.460466362 5818688.67843837 63.9185715336953 389582.459839844 5818688.67751016 34.5400009155273 389582.325654338 5818687.41976606 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360790">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389577.391879302 5818679.26490049 34.5400009155273 389577.392505817 5818679.2658287 63.9185715336953 389575.418941865 5818678.8497578 63.9185715336953 389575.418315352 5818678.84882959 34.5400009155273 389577.391879302 5818679.26490049 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360635">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389607.130895333 5818656.04099963 34.5699996948242 389607.13166318 5818656.04213718 70.5749528632974 389608.659025887 5818657.51247564 70.5749528632974 389608.658258038 5818657.51133809 34.5699996948242 389607.130895333 5818656.04099963 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360923">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389584.025336597 5818672.93939544 34.6699981689453 389584.026122018 5818672.94055906 71.499800088065 389589.336592127 5818687.15346036 71.499800088065 389589.335806702 5818687.15229673 34.6699981689453 389584.025336597 5818672.93939544 34.6699981689453</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360793">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389571.876560635 5818678.87986148 34.5400009155273 389571.877187146 5818678.88078968 63.9185715336953 389571.061083144 5818679.19505597 63.9185715336953 389571.060456632 5818679.19412777 34.5400009155273 389571.876560635 5818678.87986148 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360532">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389577.266961466 5818664.19591991 32.9300003051758 389577.267614461 5818664.19688733 63.5502536068305 389576.040658059 5818666.43013242 63.5502536068305 389576.040005065 5818666.429165 32.9300003051758 389577.266961466 5818664.19591991 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360817">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389566.78591324 5818694.50096937 34.5400009155273 389566.786539749 5818694.50189758 63.9185715336953 389567.289011636 5818695.15360353 63.9185715336953 389567.288385127 5818695.15267532 34.5400009155273 389566.78591324 5818694.50096937 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360671">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389597.234457804 5818659.65197375 61.8300018310547 389597.234767068 5818659.65243192 76.3318475278365 389596.252535337 5818660.13340132 76.3318475278365 389596.252226074 5818660.13294314 61.8300018310547 389597.234457804 5818659.65197375 61.8300018310547</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360693">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389604.984417401 5818668.53911688 61.8300018310547 389604.984726668 5818668.53957505 76.3318475278365 389605.427997601 5818667.53976707 76.3318475278365 389605.427688334 5818667.53930889 61.8300018310547 389604.984417401 5818668.53911688 61.8300018310547</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360913">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389602.459395421 5818680.88993956 34.6699981689453 389602.460180855 5818680.89110318 71.499800088065 389599.790362088 5818681.17594487 71.499800088065 389599.789576655 5818681.17478125 34.6699981689453 389602.459395421 5818680.88993956 34.6699981689453</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360735">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389595.54977378 5818677.01117677 34.7599983215332 389595.55062073 5818677.01243153 74.4746314249052 389597.608527708 5818677.5221032 74.4746314249052 389597.607680756 5818677.52084844 34.7599983215332 389595.54977378 5818677.01117677 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360554">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389589.853881725 5818698.1451471 32.9300003051758 389589.854534727 5818698.14611455 63.5502536068305 389583.547855108 5818679.60234645 63.5502536068305 389583.547202109 5818679.60137902 32.9300003051758 389589.853881725 5818698.1451471 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360642">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389610.997677366 5818669.44679347 34.5699996948242 389610.998445216 5818669.44793102 70.5749528632974 389610.139162715 5818671.38606289 70.5749528632974 389610.138394866 5818671.38492533 34.5699996948242 389610.997677366 5818669.44679347 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360544">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389581.510246878 5818681.15215665 32.9300003051758 389581.510899875 5818681.15312409 63.5502536068305 389583.054016218 5818682.78700879 63.5502536068305 389583.05336322 5818682.78604136 32.9300003051758 389581.510246878 5818681.15215665 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360518">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389602.069664747 5818632.87455427 32.9300003051758 389602.070317756 5818632.87552168 63.5502536068305 389599.978432036 5818634.84539447 63.5502536068305 389599.977779028 5818634.84442706 32.9300003051758 389602.069664747 5818632.87455427 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360764">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389596.458315695 5818670.99735683 34.7599983215332 389596.459162647 5818670.99861158 74.4746314249052 389595.551513663 5818670.3884618 74.4746314249052 389595.550666712 5818670.38720704 34.7599983215332 389596.458315695 5818670.99735683 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360736">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389597.607680756 5818677.52084844 34.7599983215332 389597.608527708 5818677.5221032 74.4746314249052 389599.723674368 5818677.66668129 74.4746314249052 389599.722827415 5818677.66542653 34.7599983215332 389597.607680756 5818677.52084844 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360542">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389577.391844968 5818679.26484963 32.9300003051758 389577.392497963 5818679.26581706 63.5502536068305 389579.82525933 5818679.97556018 63.5502536068305 389579.824606334 5818679.97459275 32.9300003051758 389577.391844968 5818679.26484963 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360733">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389591.852153644 5818674.96911978 34.7599983215332 389591.853000592 5818674.97037454 74.4746314249052 389593.612481901 5818676.15315239 74.4746314249052 389593.611634952 5818676.15189763 34.7599983215332 389591.852153644 5818674.96911978 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360618">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389584.095564649 5818666.06670698 34.5699996948242 389584.096332479 5818666.06784453 70.5749528632974 389584.381177964 5818668.73775201 70.5749528632974 389584.380410133 5818668.73661446 34.5699996948242 389584.095564649 5818666.06670698 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360613">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389589.372611867 5818654.16420388 34.5699996948242 389589.373379702 5818654.16534142 70.5749528632974 389587.511199605 5818656.09972859 70.5749528632974 389587.510431772 5818656.09859104 34.5699996948242 389589.372611867 5818654.16420388 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360491">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389599.01711388 5818650.37737908 32.9300003051758 389599.017766888 5818650.3783465 63.5502536068305 389599.515300067 5818650.37089263 63.5502536068305 389599.514647059 5818650.36992521 32.9300003051758 389599.01711388 5818650.37737908 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360568">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389568.434259256 5818691.4265892 34.5400009155273 389568.434961498 5818691.42762961 67.4698521577859 389568.991230038 5818692.56362845 67.4698521577859 389568.990527796 5818692.56258804 34.5400009155273 389568.434259256 5818691.4265892 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360685">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389597.458704546 5818671.44148128 61.8300018310547 389597.45901381 5818671.44193946 76.3318475278365 389598.52060917 5818671.70485956 76.3318475278365 389598.520299905 5818671.70440139 61.8300018310547 389597.458704546 5818671.44148128 61.8300018310547</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360569">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389568.990527796 5818692.56258804 34.5400009155273 389568.991230038 5818692.56362845 67.4698521577859 389569.736312467 5818693.58577422 67.4698521577859 389569.735610224 5818693.58473381 34.5400009155273 389568.990527796 5818692.56258804 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360503">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389618.063440403 5818660.59885126 32.9300003051758 389618.064093423 5818660.59981868 63.5502536068305 389615.987438766 5818657.41364852 63.5502536068305 389615.986785747 5818657.4126811 32.9300003051758 389618.063440403 5818660.59885126 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360721">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389595.115070547 5818654.15716792 34.7599983215332 389595.115917498 5818654.15842267 74.4746314249052 389593.211857231 5818655.0907838 74.4746314249052 389593.211010282 5818655.08952905 34.7599983215332 389595.115070547 5818654.15716792 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360718">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389599.370647357 5818653.34309967 34.7599983215332 389599.371494311 5818653.34435441 74.4746314249052 389599.261072859 5818653.34595328 74.4746314249052 389599.260225906 5818653.34469854 34.7599983215332 389599.370647357 5818653.34309967 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360884">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389618.063440403 5818660.59885126 32.9300003051758 389618.064192018 5818660.59996475 68.173418200798 389618.733954716 5818664.01449956 68.173418200798 389618.733203101 5818664.01338607 32.9300003051758 389618.063440403 5818660.59885126 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360922">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389584.815515236 5818670.43597505 34.6699981689453 389584.816300658 5818670.43713866 71.499800088065 389584.026122018 5818672.94055906 71.499800088065 389584.025336597 5818672.93939544 34.6699981689453 389584.815515236 5818670.43597505 34.6699981689453</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360587">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389581.418833146 5818685.06843212 34.5400009155273 389581.419535396 5818685.06947252 67.4698521577859 389580.674453011 5818684.04732677 67.4698521577859 389580.673750761 5818684.04628636 34.5400009155273 389581.418833146 5818685.06843212 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360529">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389581.213788925 5818657.64949936 32.9300003051758 389581.214441922 5818657.65046678 63.5502536068305 389579.855401687 5818659.80596956 63.5502536068305 389579.854748691 5818659.80500214 32.9300003051758 389581.213788925 5818657.64949936 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360741">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389607.485272687 5818674.67176384 34.7599983215332 389607.486119647 5818674.6730186 74.4746314249052 389608.956463556 5818673.14566114 74.4746314249052 389608.955616595 5818673.14440638 34.7599983215332 389607.485272687 5818674.67176384 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360695">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389605.690609538 5818666.47771735 61.8300018310547 389605.690918805 5818666.47817552 76.3318475278365 389605.765501552 5818665.38705634 76.3318475278365 389605.765192285 5818665.38659816 61.8300018310547 389605.690609538 5818666.47771735 61.8300018310547</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360546">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389584.000507915 5818684.88487184 32.9300003051758 389584.001160914 5818684.88583928 63.5502536068305 389584.651757288 5818687.29271696 63.5502536068305 389584.651104289 5818687.29174952 32.9300003051758 389584.000507915 5818684.88487184 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360547">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389584.651104289 5818687.29174952 32.9300003051758 389584.651757288 5818687.29271696 63.5502536068305 389584.540857948 5818689.41167241 63.5502536068305 389584.540204949 5818689.41070497 32.9300003051758 389584.651104289 5818687.29174952 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360789">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389579.824640668 5818679.97464362 34.5400009155273 389579.825267184 5818679.97557182 63.9185715336953 389577.392505817 5818679.2658287 63.9185715336953 389577.391879302 5818679.26490049 34.5400009155273 389579.824640668 5818679.97464362 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360686">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389598.520299905 5818671.70440139 61.8300018310547 389598.52060917 5818671.70485956 76.3318475278365 389599.611732294 5818671.77944187 76.3318475278365 389599.61142303 5818671.7789837 61.8300018310547 389598.520299905 5818671.70440139 61.8300018310547</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360634">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389605.371414143 5818654.85822174 34.5699996948242 389605.372181989 5818654.85935928 70.5749528632974 389607.13166318 5818656.04213718 70.5749528632974 389607.130895333 5818656.04099963 34.5699996948242 389605.371414143 5818654.85822174 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360571">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389570.646867586 5818694.46196911 34.5400009155273 389570.647569829 5818694.46300953 67.4698521577859 389571.69731403 5818695.16868003 67.4698521577859 389571.696611786 5818695.16763961 34.5400009155273 389570.646867586 5818694.46196911 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360730">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389588.143590285 5818669.8815045 34.7599983215332 389588.144437229 5818669.88275926 74.4746314249052 389589.076801548 5818671.78681251 74.4746314249052 389589.075954603 5818671.78555775 34.7599983215332 389588.143590285 5818669.8815045 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360824">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389570.993444374 5818697.93872454 34.5400009155273 389570.994070885 5818697.93965276 63.9185715336953 389571.817333523 5818698.25843993 63.9185715336953 389571.816707012 5818698.25751172 34.5400009155273 389570.993444374 5818697.93872454 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360670">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389598.285286962 5818659.34887348 61.8300018310547 389598.285596226 5818659.34933165 76.3318475278365 389597.234767068 5818659.65243192 76.3318475278365 389597.234457804 5818659.65197375 61.8300018310547 389598.285286962 5818659.34887348 61.8300018310547</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360579">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389579.973920729 5818694.28455978 34.5400009155273 389579.974622979 5818694.2856002 67.4698521577859 389580.851861567 5818693.37434603 67.4698521577859 389580.851159317 5818693.37330562 34.5400009155273 389579.973920729 5818694.28455978 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360531">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389578.538941746 5818661.9874504 32.9300003051758 389578.539594741 5818661.98841782 63.5502536068305 389577.267614461 5818664.19688733 63.5502536068305 389577.266961466 5818664.19591991 32.9300003051758 389578.538941746 5818661.9874504 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360551">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389583.126463025 5818694.57850749 32.9300003051758 389583.127116024 5818694.57947494 63.5502536068305 389582.277830071 5818695.84527962 63.5502536068305 389582.277177073 5818695.84431218 32.9300003051758 389583.126463025 5818694.57850749 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360792">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389573.601650512 5818678.73218091 34.5400009155273 389573.602277025 5818678.73310912 63.9185715336953 389571.877187146 5818678.88078968 63.9185715336953 389571.876560635 5818678.87986148 34.5400009155273 389573.601650512 5818678.73218091 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360678">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389593.219015182 5818665.62523744 61.8300018310547 389593.219324445 5818665.62569561 76.3318475278365 389593.33534632 5818666.71318935 76.3318475278365 389593.335037057 5818666.71273118 61.8300018310547 389593.219015182 5818665.62523744 61.8300018310547</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360622">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389587.887029149 5818675.89775217 34.5699996948242 389587.887796982 5818675.89888973 70.5749528632974 389589.822190975 5818677.76106273 70.5749528632974 389589.82142314 5818677.75992517 34.5699996948242 389587.887029149 5818675.89775217 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360655">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389589.075950552 5818671.78555175 34.5699996948242 389589.076718385 5818671.7866893 70.5749528632974 389588.144354066 5818669.88263605 70.5749528632974 389588.143586233 5818669.8814985 34.5699996948242 389589.075950552 5818671.78555175 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360851">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389581.556832504 5818692.32356515 34.5400009155273 389581.557459022 5818692.32449336 63.9185715336953 389580.851785834 5818693.37423383 63.9185715336953 389580.851159317 5818693.37330562 34.5400009155273 389581.556832504 5818692.32356515 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360597">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389614.069095349 5818670.76594639 34.5699996948242 389614.069863201 5818670.76708394 70.5749528632974 389614.715361389 5818668.16076909 70.5749528632974 389614.714593536 5818668.15963154 34.5699996948242 389614.069095349 5818670.76594639 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360864">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389568.990527796 5818692.56258804 34.5400009155273 389568.991154306 5818692.56351625 63.9185715336953 389568.434885765 5818691.42751741 63.9185715336953 389568.434259256 5818691.4265892 34.5400009155273 389568.990527796 5818692.56258804 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360570">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389569.735610224 5818693.58473381 34.5400009155273 389569.736312467 5818693.58577422 67.4698521577859 389570.647569829 5818694.46300953 67.4698521577859 389570.646867586 5818694.46196911 34.5400009155273 389569.735610224 5818693.58473381 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360785">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389584.651138624 5818687.29180039 34.5400009155273 389584.651765143 5818687.29272859 63.9185715336953 389584.001168768 5818684.88585092 63.9185715336953 389584.00054225 5818684.88492271 34.5400009155273 389584.651138624 5818687.29180039 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360694">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389605.427688334 5818667.53930889 61.8300018310547 389605.427997601 5818667.53976707 76.3318475278365 389605.690918805 5818666.47817552 76.3318475278365 389605.690609538 5818666.47771735 61.8300018310547 389605.427688334 5818667.53930889 61.8300018310547</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360897">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389595.332592349 5818696.18492901 32.9300003051758 389595.333343948 5818696.18604252 68.173418200798 389620.639546866 5818687.10033754 68.173418200798 389620.638795249 5818687.09922403 32.9300003051758 389595.332592349 5818696.18492901 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360611">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389593.953883786 5818651.40173877 34.5699996948242 389593.954651623 5818651.40287631 70.5749528632974 389591.543173068 5818652.58370492 70.5749528632974 389591.542405233 5818652.58256738 34.5699996948242 389593.953883786 5818651.40173877 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360895">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389593.260308596 5818695.81635825 32.9300003051758 389593.261060193 5818695.81747176 68.173418200798 389594.986081704 5818695.19144541 68.173418200798 389594.985330105 5818695.1903319 32.9300003051758 389593.260308596 5818695.81635825 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360522">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389593.901125822 5818641.10942276 32.9300003051758 389593.901778827 5818641.11039017 63.5502536068305 389591.924754886 5818643.34356016 63.5502536068305 389591.924101882 5818643.34259275 32.9300003051758 389593.901125822 5818641.10942276 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360870">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389568.852528429 5818685.3074548 34.5400009155273 389568.853154939 5818685.308383 63.9185715336953 389569.558828169 5818684.25864252 63.9185715336953 389569.558201659 5818684.25771431 34.5400009155273 389568.852528429 5818685.3074548 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360720">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389597.152107199 5818653.56960684 34.7599983215332 389597.152954151 5818653.57086158 74.4746314249052 389595.115917498 5818654.15842267 74.4746314249052 389595.115070547 5818654.15716792 34.7599983215332 389597.152107199 5818653.56960684 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360580">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389580.851159317 5818693.37330562 34.5400009155273 389580.851861567 5818693.37434603 67.4698521577859 389581.557534754 5818692.32460556 67.4698521577859 389581.556832504 5818692.32356515 34.5400009155273 389580.851159317 5818693.37330562 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360502">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389618.733203101 5818664.01338607 32.9300003051758 389618.733856121 5818664.01435349 63.5502536068305 389618.064093423 5818660.59981868 63.5502536068305 389618.063440403 5818660.59885126 32.9300003051758 389618.733203101 5818664.01338607 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360825">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389571.816707012 5818698.25751172 34.5400009155273 389571.817333523 5818698.25843993 63.9185715336953 389572.664245688 5818698.50506597 63.9185715336953 389572.663619176 5818698.50413776 34.5400009155273 389571.816707012 5818698.25751172 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360831">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389577.004411621 5818698.61625829 34.5400009155273 389577.005038136 5818698.6171865 63.9185715336953 389577.845339341 5818698.41994358 63.9185715336953 389577.844712825 5818698.41901537 34.5400009155273 389577.004411621 5818698.61625829 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360723">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389591.497780316 5818656.33836091 34.7599983215332 389591.498627263 5818656.33961566 74.4746314249052 389590.028283225 5818657.86697312 74.4746314249052 389590.027436279 5818657.86571838 34.7599983215332 389591.497780316 5818656.33836091 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360726">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389587.985371242 5818661.56332541 34.7599983215332 389587.986218186 5818661.56458016 74.4746314249052 389587.476544308 5818663.6224798 74.4746314249052 389587.475697364 5818663.62122505 34.7599983215332 389587.985371242 5818661.56332541 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360648">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389601.830942043 5818677.44051212 34.5699996948242 389601.831709886 5818677.44164968 70.5749528632974 389599.723591204 5818677.66655809 70.5749528632974 389599.722823363 5818677.66542053 34.5699996948242 389601.830942043 5818677.44051212 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360840">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389576.328622932 5818681.64663525 34.5400009155273 389576.329249446 5818681.64756346 63.9185715336953 389577.557040746 5818681.95164446 63.9185715336953 389577.556414231 5818681.95071625 34.5400009155273 389576.328622932 5818681.64663525 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360906">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389614.714427213 5818668.15971081 34.6699981689453 389614.715212657 5818668.16087443 71.499800088065 389614.069718166 5818670.76717435 71.499800088065 389614.068932723 5818670.76601074 34.6699981689453 389614.714427213 5818668.15971081 34.6699981689453</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360566">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389567.949521027 5818688.9535099 34.5400009155273 389567.950223269 5818688.95455031 67.4698521577859 389568.084408785 5818690.21229445 67.4698521577859 389568.083706544 5818690.21125404 34.5400009155273 389567.949521027 5818688.9535099 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360581">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389581.556832504 5818692.32356515 34.5400009155273 389581.557534754 5818692.32460556 67.4698521577859 389582.070201021 5818691.16827462 67.4698521577859 389582.06949877 5818691.16723421 34.5400009155273 389581.556832504 5818692.32356515 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360558">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389573.80893251 5818681.69456206 34.5400009155273 389573.809634756 5818681.69560247 67.4698521577859 389572.594295152 5818682.04615405 67.4698521577859 389572.593592907 5818682.04511364 34.5400009155273 389573.80893251 5818681.69456206 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360921">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389585.124877675 5818671.31674763 34.6699981689453 389585.125663097 5818671.31791124 71.499800088065 389584.816300658 5818670.43713866 71.499800088065 389584.815515236 5818670.43597505 34.6699981689453 389585.124877675 5818671.31674763 34.6699981689453</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360822">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389569.769760273 5818697.29585413 34.5400009155273 389569.770386784 5818697.29678234 63.9185715336953 389570.371334424 5818697.63902263 63.9185715336953 389570.370707913 5818697.63809442 34.5400009155273 389569.769760273 5818697.29585413 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360513">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389616.921409023 5818639.12962786 32.9300003051758 389616.922062041 5818639.13059527 63.5502536068305 389616.425891214 5818637.75128837 63.5502536068305 389616.425238196 5818637.75032096 32.9300003051758 389616.921409023 5818639.12962786 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360905">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389614.897534363 5818665.48091797 34.6699981689453 389614.898319806 5818665.48208158 71.499800088065 389614.715212657 5818668.16087443 71.499800088065 389614.714427213 5818668.15971081 34.6699981689453 389614.897534363 5818665.48091797 34.6699981689453</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360830">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389576.15054625 5818698.74204891 34.5400009155273 389576.151172765 5818698.74297712 63.9185715336953 389577.005038136 5818698.6171865 63.9185715336953 389577.004411621 5818698.61625829 34.5400009155273 389576.15054625 5818698.74204891 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360641">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389611.507351184 5818667.38889393 34.5699996948242 389611.508119034 5818667.39003148 70.5749528632974 389610.998445216 5818669.44793102 70.5749528632974 389610.997677366 5818669.44679347 34.5699996948242 389611.507351184 5818667.38889393 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360800">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389566.982428569 5818682.40438158 34.5400009155273 389566.983055078 5818682.40530978 63.9185715336953 389566.486525935 5818683.12484179 63.9185715336953 389566.485899426 5818683.12391358 34.5400009155273 389566.982428569 5818682.40438158 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360740">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389605.772042842 5818675.92059573 34.7599983215332 389605.772889801 5818675.9218505 74.4746314249052 389607.486119647 5818674.6730186 74.4746314249052 389607.485272687 5818674.67176384 34.7599983215332 389605.772042842 5818675.92059573 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360510">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389612.728339474 5818634.49176117 32.9300003051758 389612.72899249 5818634.49272858 63.5502536068305 389629.100705948 5818684.08860919 63.5502536068305 389629.100052922 5818684.08764175 32.9300003051758 389612.728339474 5818634.49176117 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360541">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389575.418281017 5818678.84877873 32.9300003051758 389575.418934011 5818678.84974616 63.5502536068305 389577.392497963 5818679.26581706 63.5502536068305 389577.391844968 5818679.26484963 32.9300003051758 389575.418281017 5818678.84877873 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360483">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389584.278836037 5818663.38774634 32.9300003051758 389584.279489036 5818663.38871377 63.5502536068305 389584.924982471 5818660.78241833 63.5502536068305 389584.924329471 5818660.7814509 32.9300003051758 389584.278836037 5818663.38774634 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360484">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389584.924329471 5818660.7814509 32.9300003051758 389584.924982471 5818660.78241833 63.5502536068305 389586.013249495 5818658.32780668 63.5502536068305 389586.012596495 5818658.32683926 32.9300003051758 389584.924329471 5818660.7814509 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360669">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389599.372784634 5818659.2328519 61.8300018310547 389599.373093899 5818659.23331007 76.3318475278365 389598.285596226 5818659.34933165 76.3318475278365 389598.285286962 5818659.34887348 61.8300018310547 389599.372784634 5818659.2328519 61.8300018310547</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360586">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389581.97510165 5818686.20443093 34.5400009155273 389581.9758039 5818686.20547134 67.4698521577859 389581.419535396 5818685.06947252 67.4698521577859 389581.418833146 5818685.06843212 34.5400009155273 389581.97510165 5818686.20443093 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360583">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389582.373581011 5818689.93944731 34.5400009155273 389582.374283263 5818689.94048772 67.4698521577859 389582.460542095 5818688.67855057 67.4698521577859 389582.459839844 5818688.67751016 34.5400009155273 389582.373581011 5818689.93944731 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360788">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389581.510281212 5818681.15220752 34.5400009155273 389581.510907729 5818681.15313573 63.9185715336953 389579.825267184 5818679.97557182 63.9185715336953 389579.824640668 5818679.97464362 34.5400009155273 389581.510281212 5818681.15220752 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360664">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389593.21100623 5818655.08952305 34.5699996948242 389593.211774067 5818655.09066059 70.5749528632974 389595.115834334 5818654.15829946 70.5749528632974 389595.115066495 5818654.15716192 34.5699996948242 389593.21100623 5818655.08952305 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360665">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389595.115066495 5818654.15716192 34.5699996948242 389595.115834334 5818654.15829946 70.5749528632974 389597.152870987 5818653.57073837 70.5749528632974 389597.152103147 5818653.56960083 34.5699996948242 389595.115066495 5818654.15716192 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360493">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389601.88236673 5818650.55580649 32.9300003051758 389601.88301974 5818650.55677391 63.5502536068305 389604.489329012 5818651.20226584 63.5502536068305 389604.488676 5818651.20129843 32.9300003051758 389601.88236673 5818650.55580649 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360900">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389622.080676244 5818685.48073878 32.9300003051758 389622.081427861 5818685.48185228 68.173418200798 389622.436505533 5818686.47019717 68.173418200798 389622.435753915 5818686.46908366 32.9300003051758 389622.080676244 5818685.48073878 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360868">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389568.035779867 5818687.6915727 34.5400009155273 389568.036406376 5818687.69250091 63.9185715336953 389568.340488639 5818686.46471398 63.9185715336953 389568.339862129 5818686.46378577 34.5400009155273 389568.035779867 5818687.6915727 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360874">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389572.593592907 5818682.04511364 34.5400009155273 389572.594219419 5818682.04604185 63.9185715336953 389573.809559023 5818681.69549027 63.9185715336953 389573.80893251 5818681.69456206 34.5400009155273 389572.593592907 5818682.04511364 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360876">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389629.100052922 5818684.08764175 32.9300003051758 389629.100804544 5818684.08875525 68.173418200798 389612.729091085 5818634.49287464 68.173418200798 389612.728339474 5818634.49176117 32.9300003051758 389629.100052922 5818684.08764175 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360869">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389568.339862129 5818686.46378577 34.5400009155273 389568.340488639 5818686.46471398 63.9185715336953 389568.853154939 5818685.308383 63.9185715336953 389568.852528429 5818685.3074548 34.5400009155273 389568.339862129 5818686.46378577 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360845">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389581.418833146 5818685.06843212 34.5400009155273 389581.419459663 5818685.06936032 63.9185715336953 389581.975728168 5818686.20535914 63.9185715336953 389581.97510165 5818686.20443093 34.5400009155273 389581.418833146 5818685.06843212 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360802">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389566.225637149 5818683.55882184 34.5400009155273 389566.226263657 5818683.55975004 63.9185715336953 389565.987598755 5818684.00706548 63.9185715336953 389565.986972246 5818684.00613727 34.5400009155273 389566.225637149 5818683.55882184 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360843">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389579.762493444 5818683.16905105 34.5400009155273 389579.763119961 5818683.16997926 63.9185715336953 389580.674377277 5818684.04721457 63.9185715336953 389580.673750761 5818684.04628636 34.5400009155273 389579.762493444 5818683.16905105 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360526">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389586.316695352 5818650.3093607 32.9300003051758 389586.317348352 5818650.31032812 63.5502536068305 389584.559067806 5818652.71724135 63.5502536068305 389584.558414807 5818652.71627393 32.9300003051758 389586.316695352 5818650.3093607 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360807">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389564.993122451 5818687.2324883 34.5400009155273 389564.993748958 5818687.2334165 63.9185715336953 389564.916425603 5818688.07741131 63.9185715336953 389564.915799095 5818688.0764831 34.5400009155273 389564.993122451 5818687.2324883 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360663">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389591.497776264 5818656.33835491 34.5699996948242 389591.498544099 5818656.33949245 70.5749528632974 389593.211774067 5818655.09066059 70.5749528632974 389593.21100623 5818655.08952305 34.5699996948242 389591.497776264 5818656.33835491 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360875">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389573.80893251 5818681.69456206 34.5400009155273 389573.809559023 5818681.69549027 63.9185715336953 389574.85398288 5818681.5685025 63.9185715336953 389574.853356367 5818681.56757429 34.5400009155273 389573.80893251 5818681.69456206 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360578">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389578.95177119 5818695.02963964 34.5400009155273 389578.952473439 5818695.03068006 67.4698521577859 389579.974622979 5818694.2856002 67.4698521577859 389579.973920729 5818694.28455978 34.5400009155273 389578.95177119 5818695.02963964 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360719">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389599.260225906 5818653.34469854 34.7599983215332 389599.261072859 5818653.34595328 74.4746314249052 389597.152954151 5818653.57086158 74.4746314249052 389597.152107199 5818653.56960684 34.7599983215332 389599.260225906 5818653.34469854 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360815">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389565.949287649 5818693.09457081 34.5400009155273 389565.949914157 5818693.09549902 63.9185715336953 389566.337652423 5818693.81163837 63.9185715336953 389566.337025914 5818693.81071016 34.5400009155273 389565.949287649 5818693.09457081 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360630">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389608.928488525 5818677.95234185 34.5699996948242 389608.929256373 5818677.95347941 70.5749528632974 389609.437871129 5818677.54200729 70.5749528632974 389609.43710328 5818677.54086973 34.5699996948242 389608.928488525 5818677.95234185 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360640">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389611.651930141 5818665.27375495 34.5699996948242 389611.652697991 5818665.27489249 70.5749528632974 389611.508119034 5818667.39003148 70.5749528632974 389611.507351184 5818667.38889393 34.5699996948242 389611.651930141 5818665.27375495 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360525">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389588.130098056 5818647.94473157 32.9300003051758 389588.130751058 5818647.94569898 63.5502536068305 389586.317348352 5818650.31032812 63.5502536068305 389586.316695352 5818650.3093607 32.9300003051758 389588.130098056 5818647.94473157 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360754">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389605.427111036 5818667.53845363 34.7599983215332 389605.427957994 5818667.53970839 74.4746314249052 389604.984687061 5818668.53951638 74.4746314249052 389604.983840103 5818668.53826162 34.7599983215332 389605.427111036 5818667.53845363 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360577">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389577.815768194 5818695.58590632 34.5400009155273 389577.816470442 5818695.58694674 67.4698521577859 389578.952473439 5818695.03068006 67.4698521577859 389578.95177119 5818695.02963964 34.5400009155273 389577.815768194 5818695.58590632 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360920">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389586.790512619 5818674.4817362 34.6699981689453 389586.791298042 5818674.48289982 71.499800088065 389585.125663097 5818671.31791124 71.499800088065 389585.124877675 5818671.31674763 34.6699981689453 389586.790512619 5818674.4817362 34.6699981689453</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360717">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389601.375372547 5818653.48927675 34.7599983215332 389601.376219502 5818653.49053149 74.4746314249052 389599.371494311 5818653.34435441 74.4746314249052 389599.370647357 5818653.34309967 34.7599983215332 389601.375372547 5818653.48927675 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360737">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389599.722827415 5818677.66542653 34.7599983215332 389599.723674368 5818677.66668129 74.4746314249052 389601.83179305 5818677.44177288 74.4746314249052 389601.830946095 5818677.44051812 34.7599983215332 389599.722827415 5818677.66542653 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360516">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389610.755888606 5818621.98417586 32.9300003051758 389610.756541621 5818621.98514326 63.5502536068305 389602.924904247 5818633.81439077 63.5502536068305 389602.924251236 5818633.81342336 32.9300003051758 389610.755888606 5818621.98417586 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360739">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389603.867982678 5818676.85295694 34.7599983215332 389603.868829635 5818676.8542117 74.4746314249052 389605.772889801 5818675.9218505 74.4746314249052 389605.772042842 5818675.92059573 34.7599983215332 389603.867982678 5818676.85295694 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360799">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389567.539017725 5818681.7300895 34.5400009155273 389567.539644234 5818681.7310177 63.9185715336953 389566.983055078 5818682.40530978 63.9185715336953 389566.982428569 5818682.40438158 34.5400009155273 389567.539017725 5818681.7300895 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360746">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389602.524737331 5818660.01276833 34.7599983215332 389602.525584287 5818660.01402308 74.4746314249052 389603.433233239 5818660.62417288 74.4746314249052 389603.432386282 5818660.62291813 34.7599983215332 389602.524737331 5818660.01276833 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360668">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389599.531737287 5818659.23184239 61.8300018310547 389599.532046551 5818659.23230056 76.3318475278365 389599.373093899 5818659.23331007 76.3318475278365 389599.372784634 5818659.2328519 61.8300018310547 389599.531737287 5818659.23184239 61.8300018310547</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360696">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389605.765192285 5818665.38659816 61.8300018310547 389605.765501552 5818665.38705634 76.3318475278365 389605.649479684 5818664.29956263 76.3318475278365 389605.649170417 5818664.29910445 61.8300018310547 389605.765192285 5818665.38659816 61.8300018310547</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360517">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389602.924251236 5818633.81342336 32.9300003051758 389602.924904247 5818633.81439077 63.5502536068305 389602.070317756 5818632.87552168 63.5502536068305 389602.069664747 5818632.87455427 32.9300003051758 389602.924251236 5818633.81342336 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360667">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389599.260221854 5818653.34469253 34.5699996948242 389599.260989695 5818653.34583008 70.5749528632974 389599.371411147 5818653.34423121 70.5749528632974 389599.370643305 5818653.34309366 34.5699996948242 389599.260221854 5818653.34469253 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_360602">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389612.687879582 5818657.81956388 34.5699996948242 389612.688647433 5818657.82070143 70.5749528632974 389611.107005593 5818655.65091623 70.5749528632974 389611.106237744 5818655.64977869 34.5699996948242 389612.687879582 5818657.81956388 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface gml:id="GEOM_360472">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389608.928488525 5818677.95234185 34.5699996948242 389609.43710328 5818677.54086973 34.5699996948242 389609.620655189 5818677.3833259 34.5699996948242 389611.482835078 5818675.44893874 34.5699996948242 389612.980820303 5818673.22057635 34.5699996948242 389614.069095349 5818670.76594639 34.5699996948242 389614.714593536 5818668.15963154 34.5699996948242 389614.897701734 5818665.48082336 34.5699996948242 389614.612856292 5818662.81091606 34.5699996948242 389613.868712095 5818660.23103343 34.5699996948242 389612.687879582 5818657.81956388 34.5699996948242 389611.106237744 5818655.64977869 34.5699996948242 389609.171843958 5818653.78760568 34.5699996948242 389606.943473793 5818652.28962603 34.5699996948242 389604.48883514 5818651.20135512 34.5699996948242 389601.882510944 5818650.55585948 34.5699996948242 389599.514682034 5818650.36997703 34.5699996948242 389599.203693044 5818650.37275221 34.5699996948242 389596.533775963 5818650.65759692 34.5699996948242 389593.953883786 5818651.40173877 34.5699996948242 389591.542405233 5818652.58256738 34.5699996948242 389589.372611867 5818654.16420388 34.5699996948242 389587.510431772 5818656.09859104 34.5699996948242 389586.012446357 5818658.32695351 34.5699996948242 389584.924171158 5818660.7815836 34.5699996948242 389584.278672874 5818663.38789861 34.5699996948242 389584.095564649 5818666.06670698 34.5699996948242 389584.380410133 5818668.73661446 34.5699996948242 389585.12455444 5818671.31649725 34.5699996948242 389586.305387115 5818673.72796692 34.5699996948242 389587.887029149 5818675.89775217 34.5699996948242 389589.82142314 5818677.75992517 34.5699996948242 389592.049793496 5818679.25790474 34.5699996948242 389594.504432302 5818680.34617554 34.5699996948242 389597.110756594 5818680.991671 34.5699996948242 389599.789574523 5818681.17477809 34.5699996948242 389602.45949156 5818680.88993319 34.5699996948242 389605.865668213 5818679.79919477 34.5699996948242 389608.928488525 5818677.95234185 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
									<gml:interior>
										<gml:LinearRing>
											<gml:posList>389599.260221854 5818653.34469253 34.5699996948242 389599.370643305 5818653.34309366 34.5699996948242 389601.375368495 5818653.48927074 34.5699996948242 389603.433275411 5818653.99894252 34.5699996948242 389605.371414143 5818654.85822174 34.5699996948242 389607.130895333 5818656.04099963 34.5699996948242 389608.658258038 5818657.51133809 34.5699996948242 389609.907094123 5818659.22456163 34.5699996948242 389610.839458341 5818661.12861481 34.5699996948242 389611.42702128 5818663.16564395 34.5699996948242 389611.651930141 5818665.27375495 34.5699996948242 389611.507351184 5818667.38889393 34.5699996948242 389610.997677366 5818669.44679347 34.5699996948242 389610.138394866 5818671.38492533 34.5699996948242 389608.955612543 5818673.14440038 34.5699996948242 389607.485268635 5818674.67175783 34.5699996948242 389605.77203879 5818675.92058973 34.5699996948242 389603.867978626 5818676.85295093 34.5699996948242 389601.830942043 5818677.44051212 34.5699996948242 389599.722823363 5818677.66542053 34.5699996948242 389597.607676704 5818677.52084244 34.5699996948242 389595.549769728 5818677.01117076 34.5699996948242 389593.6116309 5818676.15189162 34.5699996948242 389591.852149592 5818674.96911377 34.5699996948242 389590.324786758 5818673.49877532 34.5699996948242 389589.075950552 5818671.78555175 34.5699996948242 389588.143586233 5818669.8814985 34.5699996948242 389587.556023226 5818667.84446926 34.5699996948242 389587.331114337 5818665.73635815 34.5699996948242 389587.475693312 5818663.62121905 34.5699996948242 389587.98536719 5818661.5633194 34.5699996948242 389588.844649785 5818659.62518746 34.5699996948242 389590.027432227 5818657.86571237 34.5699996948242 389591.497776264 5818656.33835491 34.5699996948242 389593.21100623 5818655.08952305 34.5699996948242 389595.115066495 5818654.15716192 34.5699996948242 389597.152103147 5818653.56960083 34.5699996948242 389599.260221854 5818653.34469253 34.5699996948242</gml:posList>
										</gml:LinearRing>
									</gml:interior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface gml:id="GEOM_360471">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389576.328622932 5818681.64663525 34.5400009155273 389575.194745238 5818681.55907136 34.5400009155273 389575.066681205 5818681.56037688 34.5400009155273 389573.80893251 5818681.69456206 34.5400009155273 389572.593592907 5818682.04511364 34.5400009155273 389571.457589875 5818682.6013803 34.5400009155273 389570.435440292 5818683.34646015 34.5400009155273 389569.558201659 5818684.25771431 34.5400009155273 389568.852528429 5818685.3074548 34.5400009155273 389568.339862129 5818686.46378577 34.5400009155273 389568.035779867 5818687.6915727 34.5400009155273 389567.949521027 5818688.9535099 34.5400009155273 389568.083706544 5818690.21125404 34.5400009155273 389568.434259256 5818691.4265892 34.5400009155273 389568.990527796 5818692.56258804 34.5400009155273 389569.735610224 5818693.58473381 34.5400009155273 389570.646867586 5818694.46196911 34.5400009155273 389571.696611786 5818695.16763961 34.5400009155273 389572.852946875 5818695.68030387 34.5400009155273 389574.080738197 5818695.98438483 34.5400009155273 389575.34267993 5818696.07064316 34.5400009155273 389576.600428614 5818695.93645794 34.5400009155273 389577.815768194 5818695.58590632 34.5400009155273 389578.95177119 5818695.02963964 34.5400009155273 389579.973920729 5818694.28455978 34.5400009155273 389580.851159317 5818693.37330562 34.5400009155273 389581.556832504 5818692.32356515 34.5400009155273 389582.06949877 5818691.16723421 34.5400009155273 389582.373581011 5818689.93944731 34.5400009155273 389582.459839844 5818688.67751016 34.5400009155273 389582.325654338 5818687.41976606 34.5400009155273 389581.97510165 5818686.20443093 34.5400009155273 389581.418833146 5818685.06843212 34.5400009155273 389580.673750761 5818684.04628636 34.5400009155273 389579.762493444 5818683.16905105 34.5400009155273 389578.712749287 5818682.46338053 34.5400009155273 389577.556414231 5818681.95071625 34.5400009155273 389576.328622932 5818681.64663525 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface gml:id="GEOM_360470">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389583.547202109 5818679.60137902 32.9300003051758 389584.025299491 5818672.93934047 32.9300003051758 389584.815478129 5818670.43592008 32.9300003051758 389584.380572531 5818668.736422 32.9300003051758 389584.095729186 5818666.06653458 32.9300003051758 389584.278836037 5818663.38774634 32.9300003051758 389584.924329471 5818660.7814509 32.9300003051758 389586.012596495 5818658.32683926 32.9300003051758 389587.510570657 5818656.09849353 32.9300003051758 389589.372736763 5818654.16412089 32.9300003051758 389591.542513828 5818652.58249627 32.9300003051758 389593.953974266 5818651.40167653 32.9300003051758 389596.533847063 5818650.65754028 32.9300003051758 389599.01711388 5818650.37737908 32.9300003051758 389599.514647059 5818650.36992521 32.9300003051758 389601.88236673 5818650.55580649 32.9300003051758 389604.488676 5818651.20129843 32.9300003051758 389606.943300596 5818652.28956311 32.9300003051758 389609.171657999 5818653.78753418 32.9300003051758 389611.106040706 5818655.64969653 32.9300003051758 389612.687673487 5818657.81946929 32.9300003051758 389613.868499238 5818660.23092503 32.9300003051758 389614.612639173 5818662.81079289 32.9300003051758 389614.851064153 5818664.54340396 32.9300003051758 389618.733203101 5818664.01338607 32.9300003051758 389618.063440403 5818660.59885126 32.9300003051758 389615.986785747 5818657.4126811 32.9300003051758 389614.320862072 5818654.6219072 32.9300003051758 389611.643444178 5818651.64876838 32.9300003051758 389607.574467455 5818649.71012002 32.9300003051758 389604.33078594 5818648.76379568 32.9300003051758 389600.922272737 5818639.15161836 32.9300003051758 389612.728339474 5818634.49176117 32.9300003051758 389629.100052922 5818684.08764175 32.9300003051758 389632.518757761 5818682.85135942 32.9300003051758 389616.921409023 5818639.12962786 32.9300003051758 389616.425238196 5818637.75032096 32.9300003051758 389615.376140732 5818634.83136156 32.9300003051758 389610.755888606 5818621.98417586 32.9300003051758 389602.924251236 5818633.81342336 32.9300003051758 389602.069664747 5818632.87455427 32.9300003051758 389599.977779028 5818634.84442706 32.9300003051758 389597.930113469 5818636.86106198 32.9300003051758 389595.929081008 5818638.92288826 32.9300003051758 389593.901125822 5818641.10942276 32.9300003051758 389591.924101882 5818643.34259275 32.9300003051758 389590.000433685 5818645.62143517 32.9300003051758 389588.130098056 5818647.94473157 32.9300003051758 389586.316695352 5818650.3093607 32.9300003051758 389584.558414807 5818652.71627393 32.9300003051758 389582.857044974 5818655.16329942 32.9300003051758 389581.213788925 5818657.64949936 32.9300003051758 389579.854748691 5818659.80500214 32.9300003051758 389578.538941746 5818661.9874504 32.9300003051758 389577.266961466 5818664.19591991 32.9300003051758 389576.040005065 5818666.429165 32.9300003051758 389574.648552065 5818669.09458187 32.9300003051758 389573.319966069 5818671.7923693 32.9300003051758 389572.056042486 5818674.52066329 32.9300003051758 389570.856752282 5818677.27793751 32.9300003051758 389571.428790078 5818677.51209335 32.9300003051758 389571.876526301 5818678.87981061 32.9300003051758 389573.601616178 5818678.73213004 32.9300003051758 389575.418281017 5818678.84877873 32.9300003051758 389577.391844968 5818679.26484963 32.9300003051758 389579.824606334 5818679.97459275 32.9300003051758 389581.510246878 5818681.15215665 32.9300003051758 389583.05336322 5818682.78604136 32.9300003051758 389584.000507915 5818684.88487184 32.9300003051758 389584.651104289 5818687.29174952 32.9300003051758 389584.540204949 5818689.41070497 32.9300003051758 389584.278156469 5818691.53253542 32.9300003051758 389583.705184733 5818693.20667006 32.9300003051758 389583.126463025 5818694.57850749 32.9300003051758 389582.277177073 5818695.84431218 32.9300003051758 389583.817782198 5818700.31446763 32.9300003051758 389589.853881725 5818698.1451471 32.9300003051758 389583.547202109 5818679.60137902 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface gml:id="GEOM_360474">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389608.955616595 5818673.14440638 34.7599983215332 389610.011447068 5818671.60915959 34.7599983215332 389610.138398918 5818671.38493133 34.7599983215332 389610.997681418 5818669.44679947 34.7599983215332 389611.507355236 5818667.38889993 34.7599983215332 389611.651934193 5818665.27376095 34.7599983215332 389611.427025331 5818663.16564995 34.7599983215332 389610.839462392 5818661.12862081 34.7599983215332 389609.907098175 5818659.22456763 34.7599983215332 389608.65826209 5818657.5113441 34.7599983215332 389607.130899385 5818656.04100564 34.7599983215332 389605.371418196 5818654.85822774 34.7599983215332 389603.433279463 5818653.99894853 34.7599983215332 389601.375372547 5818653.48927675 34.7599983215332 389599.370647357 5818653.34309967 34.7599983215332 389599.260225906 5818653.34469854 34.7599983215332 389597.152107199 5818653.56960684 34.7599983215332 389595.115070547 5818654.15716792 34.7599983215332 389593.211010282 5818655.08952905 34.7599983215332 389591.497780316 5818656.33836091 34.7599983215332 389590.027436279 5818657.86571838 34.7599983215332 389588.844653837 5818659.62519346 34.7599983215332 389587.985371242 5818661.56332541 34.7599983215332 389587.475697364 5818663.62122505 34.7599983215332 389587.331118389 5818665.73636415 34.7599983215332 389587.556027277 5818667.84447526 34.7599983215332 389588.143590285 5818669.8815045 34.7599983215332 389589.075954603 5818671.78555775 34.7599983215332 389590.32479081 5818673.49878132 34.7599983215332 389591.852153644 5818674.96911978 34.7599983215332 389593.611634952 5818676.15189763 34.7599983215332 389595.54977378 5818677.01117677 34.7599983215332 389597.607680756 5818677.52084844 34.7599983215332 389599.722827415 5818677.66542653 34.7599983215332 389601.830946095 5818677.44051812 34.7599983215332 389603.867982678 5818676.85295694 34.7599983215332 389605.772042842 5818675.92059573 34.7599983215332 389607.485272687 5818674.67176384 34.7599983215332 389608.955616595 5818673.14440638 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
									<gml:interior>
										<gml:LinearRing>
											<gml:posList>389599.372207339 5818659.23199664 34.7599983215332 389599.531159991 5818659.23098714 34.7599983215332 389600.463330458 5818659.30657899 34.7599983215332 389601.524925801 5818659.56949912 34.7599983215332 389602.524737331 5818660.01276833 34.7599983215332 389603.432386282 5818660.62291813 34.7599983215332 389604.220294202 5818661.38140941 34.7599983215332 389604.864520908 5818662.2651958 34.7599983215332 389605.3454919 5818663.24742389 34.7599983215332 389605.648593118 5818664.2982492 34.7599983215332 389605.764614986 5818665.38574291 34.7599983215332 389605.690032239 5818666.47686209 34.7599983215332 389605.427111036 5818667.53845363 34.7599983215332 389604.983840103 5818668.53826162 34.7599983215332 389604.373688002 5818669.44590739 34.7599983215332 389603.615193895 5818670.23381259 34.7599983215332 389602.731404241 5818670.87803711 34.7599983215332 389601.749172538 5818671.35900653 34.7599983215332 389600.698343398 5818671.66210682 34.7599983215332 389599.610845734 5818671.77812844 34.7599983215332 389598.51972261 5818671.70354613 34.7599983215332 389597.458127251 5818671.44062603 34.7599983215332 389596.458315695 5818670.99735683 34.7599983215332 389595.550666712 5818670.38720704 34.7599983215332 389594.762758759 5818669.62871577 34.7599983215332 389594.11853202 5818668.74492937 34.7599983215332 389593.637561001 5818667.76270125 34.7599983215332 389593.334459765 5818666.71187592 34.7599983215332 389593.21843789 5818665.62438218 34.7599983215332 389593.293020642 5818664.53326297 34.7599983215332 389593.555941861 5818663.4716714 34.7599983215332 389593.999212819 5818662.47186339 34.7599983215332 389594.609364951 5818661.5642176 34.7599983215332 389595.367859093 5818660.77631241 34.7599983215332 389596.25164878 5818660.13208789 34.7599983215332 389597.23388051 5818659.6511185 34.7599983215332 389598.284709667 5818659.34801823 34.7599983215332 389599.372207339 5818659.23199664 34.7599983215332</gml:posList>
										</gml:LinearRing>
									</gml:interior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface gml:id="GEOM_360475">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389581.638275466 5818696.42470903 34.5400009155273 389582.277211408 5818695.84436305 34.5400009155273 389583.12649736 5818694.57855836 34.5400009155273 389583.705219068 5818693.20672093 34.5400009155273 389584.278190804 5818691.53258629 34.5400009155273 389584.540239284 5818689.41075584 34.5400009155273 389584.651138624 5818687.29180039 34.5400009155273 389584.00054225 5818684.88492271 34.5400009155273 389583.053397554 5818682.78609222 34.5400009155273 389581.510281212 5818681.15220752 34.5400009155273 389579.824640668 5818679.97464362 34.5400009155273 389577.391879302 5818679.26490049 34.5400009155273 389575.418315352 5818678.84882959 34.5400009155273 389573.601650512 5818678.73218091 34.5400009155273 389571.876560635 5818678.87986148 34.5400009155273 389571.060456632 5818679.19412777 34.5400009155273 389570.274332572 5818679.57648336 34.5400009155273 389569.523644222 5818680.02499262 34.5400009155273 389568.813811553 5818680.53589075 34.5400009155273 389568.150873091 5818681.10570715 34.5400009155273 389567.539017725 5818681.7300895 34.5400009155273 389566.982428569 5818682.40438158 34.5400009155273 389566.485899426 5818683.12391358 34.5400009155273 389566.225637149 5818683.55882184 34.5400009155273 389565.986972246 5818684.00613727 34.5400009155273 389565.639151079 5818684.77898825 34.5400009155273 389565.355905275 5818685.57776979 34.5400009155273 389565.140192661 5818686.39754302 34.5400009155273 389564.993122451 5818687.2324883 34.5400009155273 389564.915799095 5818688.0764831 34.5400009155273 389564.908728853 5818688.92402324 34.5400009155273 389564.948745031 5818689.55187413 34.5400009155273 389565.027132562 5818690.17563844 34.5400009155273 389565.143857635 5818690.79348594 34.5400009155273 389565.351085348 5818691.58110737 34.5400009155273 389565.620204305 5818692.34985297 34.5400009155273 389565.949287649 5818693.09457081 34.5400009155273 389566.337025914 5818693.81071016 34.5400009155273 389566.78591324 5818694.50096937 34.5400009155273 389567.288385127 5818695.15267532 34.5400009155273 389567.84192422 5818695.76190923 34.5400009155273 389568.442179149 5818696.32448119 34.5400009155273 389569.086647192 5818696.83708408 34.5400009155273 389569.769760273 5818697.29585413 34.5400009155273 389570.370707913 5818697.63809442 34.5400009155273 389570.993444374 5818697.93872454 34.5400009155273 389571.816707012 5818698.25751172 34.5400009155273 389572.663619176 5818698.50413776 34.5400009155273 389573.52926041 5818698.67656143 34.5400009155273 389574.406285706 5818698.77370153 34.5400009155273 389575.288583115 5818698.79506336 34.5400009155273 389576.15054625 5818698.74204891 34.5400009155273 389577.004411621 5818698.61625829 34.5400009155273 389577.844712825 5818698.41901537 34.5400009155273 389578.66536973 5818698.15165571 34.5400009155273 389579.460922752 5818697.81580903 34.5400009155273 389580.22531699 5818697.41403232 34.5400009155273 389580.952503323 5818696.9491864 34.5400009155273 389581.638275466 5818696.42470903 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
									<gml:interior>
										<gml:LinearRing>
											<gml:posList>389573.80893251 5818681.69456206 34.5400009155273 389574.853356367 5818681.56757429 34.5400009155273 389575.194745238 5818681.55907136 34.5400009155273 389576.328622932 5818681.64663525 34.5400009155273 389577.556414231 5818681.95071625 34.5400009155273 389578.712749287 5818682.46338053 34.5400009155273 389579.762493444 5818683.16905105 34.5400009155273 389580.673750761 5818684.04628636 34.5400009155273 389581.418833146 5818685.06843212 34.5400009155273 389581.97510165 5818686.20443093 34.5400009155273 389582.325654338 5818687.41976606 34.5400009155273 389582.459839844 5818688.67751016 34.5400009155273 389582.373581011 5818689.93944731 34.5400009155273 389582.06949877 5818691.16723421 34.5400009155273 389581.556832504 5818692.32356515 34.5400009155273 389580.851159317 5818693.37330562 34.5400009155273 389579.973920729 5818694.28455978 34.5400009155273 389578.95177119 5818695.02963964 34.5400009155273 389577.815768194 5818695.58590632 34.5400009155273 389576.600428614 5818695.93645794 34.5400009155273 389575.470700977 5818696.06707771 34.5400009155273 389575.34267993 5818696.07064316 34.5400009155273 389574.080738197 5818695.98438483 34.5400009155273 389572.852946875 5818695.68030387 34.5400009155273 389571.696611786 5818695.16763961 34.5400009155273 389570.646867586 5818694.46196911 34.5400009155273 389569.735610224 5818693.58473381 34.5400009155273 389568.990527796 5818692.56258804 34.5400009155273 389568.434259256 5818691.4265892 34.5400009155273 389568.083706544 5818690.21125404 34.5400009155273 389567.949521027 5818688.9535099 34.5400009155273 389568.035779867 5818687.6915727 34.5400009155273 389568.339862129 5818686.46378577 34.5400009155273 389568.852528429 5818685.3074548 34.5400009155273 389569.558201659 5818684.25771431 34.5400009155273 389570.435440292 5818683.34646015 34.5400009155273 389571.457589875 5818682.6013803 34.5400009155273 389572.593592907 5818682.04511364 34.5400009155273 389573.80893251 5818681.69456206 34.5400009155273</gml:posList>
										</gml:LinearRing>
									</gml:interior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface gml:id="GEOM_360477">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389619.191603953 5818677.51221528 34.6699981689453 389614.851101261 5818664.54345893 34.6699981689453 389614.897534363 5818665.48091797 34.6699981689453 389614.714427213 5818668.15971081 34.6699981689453 389614.068932723 5818670.76601074 34.6699981689453 389612.980663909 5818673.22062664 34.6699981689453 389611.482687264 5818675.44897627 34.6699981689453 389609.620518038 5818677.38335236 34.6699981689453 389607.450737296 5818678.96497985 34.6699981689453 389605.039272715 5818680.14580182 34.6699981689453 389602.459395421 5818680.88993956 34.6699981689453 389599.789576655 5818681.17478125 34.6699981689453 389598.89358855 5818681.16575825 34.6699981689453 389595.360193784 5818680.61179399 34.6699981689453 389592.049795629 5818679.2579079 34.6699981689453 389590.711739521 5818678.42695792 34.6699981689453 389589.14114945 5818677.17729151 34.6699981689453 389586.790512619 5818674.4817362 34.6699981689453 389585.124877675 5818671.31674763 34.6699981689453 389584.815515236 5818670.43597505 34.6699981689453 389584.025336597 5818672.93939544 34.6699981689453 389589.335806702 5818687.15229673 34.6699981689453 389599.621615291 5818687.3598599 34.6699981689453 389612.834573546 5818682.47163666 34.6699981689453 389619.191603953 5818677.51221528 34.6699981689453</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface gml:id="GEOM_360473">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389600.463907754 5818659.30743424 61.8300018310547 389599.531737287 5818659.23184239 61.8300018310547 389599.372784634 5818659.2328519 61.8300018310547 389598.285286962 5818659.34887348 61.8300018310547 389597.234457804 5818659.65197375 61.8300018310547 389596.252226074 5818660.13294314 61.8300018310547 389595.368436386 5818660.77716766 61.8300018310547 389594.609942244 5818661.56507286 61.8300018310547 389593.999790112 5818662.47271865 61.8300018310547 389593.556519153 5818663.47252665 61.8300018310547 389593.293597934 5818664.53411823 61.8300018310547 389593.219015182 5818665.62523744 61.8300018310547 389593.335037057 5818666.71273118 61.8300018310547 389593.638138294 5818667.76355651 61.8300018310547 389594.119109313 5818668.74578463 61.8300018310547 389594.763336052 5818669.62957103 61.8300018310547 389595.551244006 5818670.3880623 61.8300018310547 389596.458892989 5818670.99821208 61.8300018310547 389597.458704546 5818671.44148128 61.8300018310547 389598.520299905 5818671.70440139 61.8300018310547 389599.61142303 5818671.7789837 61.8300018310547 389600.698920694 5818671.66296208 61.8300018310547 389601.749749834 5818671.35986179 61.8300018310547 389602.731981538 5818670.87889237 61.8300018310547 389603.615771193 5818670.23466785 61.8300018310547 389604.3742653 5818669.44676265 61.8300018310547 389604.984417401 5818668.53911688 61.8300018310547 389605.427688334 5818667.53930889 61.8300018310547 389605.690609538 5818666.47771735 61.8300018310547 389605.765192285 5818665.38659816 61.8300018310547 389605.649170417 5818664.29910445 61.8300018310547 389605.346069199 5818663.24827915 61.8300018310547 389604.865098207 5818662.26605105 61.8300018310547 389604.2208715 5818661.38226466 61.8300018310547 389603.43296358 5818660.62377339 61.8300018310547 389602.525314628 5818660.01362359 61.8300018310547 389601.525503098 5818659.57035437 61.8300018310547 389600.463907754 5818659.30743424 61.8300018310547</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface gml:id="GEOM_360476">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389627.168396247 5818684.78617326 32.9300003051758 389629.100052922 5818684.08764175 32.9300003051758 389612.728339474 5818634.49176117 32.9300003051758 389600.922272737 5818639.15161836 32.9300003051758 389604.33078594 5818648.76379568 32.9300003051758 389607.574467455 5818649.71012002 32.9300003051758 389611.643444178 5818651.64876838 32.9300003051758 389614.320862072 5818654.6219072 32.9300003051758 389615.986785747 5818657.4126811 32.9300003051758 389618.063440403 5818660.59885126 32.9300003051758 389618.733203101 5818664.01338607 32.9300003051758 389614.851064153 5818664.54340396 32.9300003051758 389619.191566845 5818677.5121603 32.9300003051758 389612.834536439 5818682.47158169 32.9300003051758 389599.621578184 5818687.35980493 32.9300003051758 389589.335769595 5818687.15224176 32.9300003051758 389584.025299491 5818672.93934047 32.9300003051758 389583.547202109 5818679.60137902 32.9300003051758 389589.853881725 5818698.1451471 32.9300003051758 389593.606078661 5818696.79664088 32.9300003051758 389593.260308596 5818695.81635825 32.9300003051758 389594.985330105 5818695.1903319 32.9300003051758 389595.332592349 5818696.18492901 32.9300003051758 389620.638795249 5818687.09922403 32.9300003051758 389620.302171407 5818686.11846151 32.9300003051758 389622.080676244 5818685.48073878 32.9300003051758 389622.435753915 5818686.46908366 32.9300003051758 389626.6040739 5818684.98976283 32.9300003051758 389627.168396247 5818684.78617326 32.9300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="GEOM_360935">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389629.100804544 5818684.08875525 68.173418200798 389627.169147868 5818684.78728676 68.173418200798 389626.60482552 5818684.99087633 68.173418200798 389622.436505533 5818686.47019717 68.173418200798 389622.081427861 5818685.48185228 68.173418200798 389620.302923023 5818686.11957501 68.173418200798 389620.639546866 5818687.10033754 68.173418200798 389595.333343948 5818696.18604252 68.173418200798 389594.986081704 5818695.19144541 68.173418200798 389593.261060193 5818695.81747176 68.173418200798 389593.606830259 5818696.7977544 68.173418200798 389589.854633319 5818698.14626062 68.173418200798 389583.547953699 5818679.60249252 68.173418200798 389584.026051081 5818672.94045396 68.173418200798 389589.33652119 5818687.15335526 68.173418200798 389599.622329785 5818687.36091843 68.173418200798 389612.83528805 5818682.47269519 68.173418200798 389619.192318461 5818677.5132738 68.173418200798 389614.851815766 5818664.54451745 68.173418200798 389618.733954716 5818664.01449956 68.173418200798 389618.064192018 5818660.59996475 68.173418200798 389615.98753736 5818657.41379459 68.173418200798 389614.321613684 5818654.62302069 68.173418200798 389611.644195788 5818651.64988187 68.173418200798 389607.575219062 5818649.71123349 68.173418200798 389604.331537546 5818648.76490916 68.173418200798 389600.923024339 5818639.15273183 68.173418200798 389612.729091085 5818634.49287464 68.173418200798 389629.100804544 5818684.08875525 68.173418200798</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="GEOM_360936">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389614.851886705 5818664.54462254 71.499800088065 389619.1923894 5818677.5133789 71.499800088065 389612.835358989 5818682.47280028 71.499800088065 389599.622400724 5818687.36102353 71.499800088065 389589.336592127 5818687.15346036 71.499800088065 389584.026122018 5818672.94055906 71.499800088065 389584.816300658 5818670.43713866 71.499800088065 389585.125663097 5818671.31791124 71.499800088065 389586.791298042 5818674.48289982 71.499800088065 389589.141934875 5818677.17845513 71.499800088065 389590.712524947 5818678.42812154 71.499800088065 389592.050581055 5818679.25907152 71.499800088065 389595.360979214 5818680.61295761 71.499800088065 389598.894373982 5818681.16692188 71.499800088065 389599.790362088 5818681.17594487 71.499800088065 389602.460180855 5818680.89110318 71.499800088065 389605.040058151 5818680.14696544 71.499800088065 389607.451522734 5818678.96614347 71.499800088065 389609.621303478 5818677.38451597 71.499800088065 389611.483472704 5818675.45013988 71.499800088065 389612.981449352 5818673.22179026 71.499800088065 389614.069718166 5818670.76717435 71.499800088065 389614.715212657 5818668.16087443 71.499800088065 389614.898319806 5818665.48208158 71.499800088065 389614.851886705 5818664.54462254 71.499800088065</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="GEOM_360930">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389575.195447484 5818681.56011177 67.4698521577859 389576.329325179 5818681.64767566 67.4698521577859 389577.557116479 5818681.95175666 67.4698521577859 389578.713451535 5818682.46442094 67.4698521577859 389579.763195694 5818683.17009146 67.4698521577859 389580.674453011 5818684.04732677 67.4698521577859 389581.419535396 5818685.06947252 67.4698521577859 389581.9758039 5818686.20547134 67.4698521577859 389582.326356588 5818687.42080647 67.4698521577859 389582.460542095 5818688.67855057 67.4698521577859 389582.374283263 5818689.94048772 67.4698521577859 389582.070201021 5818691.16827462 67.4698521577859 389581.557534754 5818692.32460556 67.4698521577859 389580.851861567 5818693.37434603 67.4698521577859 389579.974622979 5818694.2856002 67.4698521577859 389578.952473439 5818695.03068006 67.4698521577859 389577.816470442 5818695.58694674 67.4698521577859 389576.601130862 5818695.93749835 67.4698521577859 389575.343382176 5818696.07168357 67.4698521577859 389574.081440442 5818695.98542525 67.4698521577859 389572.85364912 5818695.68134428 67.4698521577859 389571.69731403 5818695.16868003 67.4698521577859 389570.647569829 5818694.46300953 67.4698521577859 389569.736312467 5818693.58577422 67.4698521577859 389568.991230038 5818692.56362845 67.4698521577859 389568.434961498 5818691.42762961 67.4698521577859 389568.084408785 5818690.21229445 67.4698521577859 389567.950223269 5818688.95455031 67.4698521577859 389568.036482108 5818687.69261311 67.4698521577859 389568.340564371 5818686.46482618 67.4698521577859 389568.853230671 5818685.3084952 67.4698521577859 389569.558903901 5818684.25875472 67.4698521577859 389570.436142535 5818683.34750055 67.4698521577859 389571.458292118 5818682.60242071 67.4698521577859 389572.594295152 5818682.04615405 67.4698521577859 389573.809634756 5818681.69560247 67.4698521577859 389575.067383451 5818681.56141729 67.4698521577859 389575.195447484 5818681.56011177 67.4698521577859</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="GEOM_360933">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389610.01229403 5818671.61041435 74.4746314249052 389608.956463556 5818673.14566114 74.4746314249052 389607.486119647 5818674.6730186 74.4746314249052 389605.772889801 5818675.9218505 74.4746314249052 389603.868829635 5818676.8542117 74.4746314249052 389601.83179305 5818677.44177288 74.4746314249052 389599.723674368 5818677.66668129 74.4746314249052 389597.608527708 5818677.5221032 74.4746314249052 389595.55062073 5818677.01243153 74.4746314249052 389593.612481901 5818676.15315239 74.4746314249052 389591.853000592 5818674.97037454 74.4746314249052 389590.325637756 5818673.50003608 74.4746314249052 389589.076801548 5818671.78681251 74.4746314249052 389588.144437229 5818669.88275926 74.4746314249052 389587.556874222 5818667.84573002 74.4746314249052 389587.331965333 5818665.7376189 74.4746314249052 389587.476544308 5818663.6224798 74.4746314249052 389587.986218186 5818661.56458016 74.4746314249052 389588.845500782 5818659.62644821 74.4746314249052 389590.028283225 5818657.86697312 74.4746314249052 389591.498627263 5818656.33961566 74.4746314249052 389593.211857231 5818655.0907838 74.4746314249052 389595.115917498 5818654.15842267 74.4746314249052 389597.152954151 5818653.57086158 74.4746314249052 389599.261072859 5818653.34595328 74.4746314249052 389599.371494311 5818653.34435441 74.4746314249052 389601.376219502 5818653.49053149 74.4746314249052 389603.43412642 5818654.00020327 74.4746314249052 389605.372265154 5818654.85948249 74.4746314249052 389607.131746345 5818656.04226039 74.4746314249052 389608.659109051 5818657.51259885 74.4746314249052 389609.907945137 5818659.22582238 74.4746314249052 389610.840309355 5818661.12987556 74.4746314249052 389611.427872295 5818663.1669047 74.4746314249052 389611.652781156 5818665.2750157 74.4746314249052 389611.508202199 5818667.39015468 74.4746314249052 389610.998528381 5818669.44805423 74.4746314249052 389610.13924588 5818671.38618609 74.4746314249052 389610.01229403 5818671.61041435 74.4746314249052</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
									<gml:interior>
										<gml:LinearRing>
											<gml:posList>389599.532006945 5818659.23224188 74.4746314249052 389599.373054292 5818659.23325139 74.4746314249052 389598.285556619 5818659.34927298 74.4746314249052 389597.234727462 5818659.65237325 74.4746314249052 389596.252495731 5818660.13334264 74.4746314249052 389595.368706043 5818660.77756715 74.4746314249052 389594.610211901 5818661.56547235 74.4746314249052 389594.000059768 5818662.47311814 74.4746314249052 389593.55678881 5818663.47292615 74.4746314249052 389593.29386759 5818664.53451772 74.4746314249052 389593.219284838 5818665.62563693 74.4746314249052 389593.335306714 5818666.71313068 74.4746314249052 389593.63840795 5818667.76395601 74.4746314249052 389594.119378969 5818668.74618412 74.4746314249052 389594.763605709 5818669.62997052 74.4746314249052 389595.551513663 5818670.3884618 74.4746314249052 389596.459162647 5818670.99861158 74.4746314249052 389597.458974203 5818671.44188078 74.4746314249052 389598.520569563 5818671.70480088 74.4746314249052 389599.611692688 5818671.7793832 74.4746314249052 389600.699190353 5818671.66336158 74.4746314249052 389601.750019493 5818671.36026129 74.4746314249052 389602.732251197 5818670.87929187 74.4746314249052 389603.616040852 5818670.23506734 74.4746314249052 389604.37453496 5818669.44716215 74.4746314249052 389604.984687061 5818668.53951638 74.4746314249052 389605.427957994 5818667.53970839 74.4746314249052 389605.690879198 5818666.47811685 74.4746314249052 389605.765461945 5818665.38699766 74.4746314249052 389605.649440077 5818664.29950395 74.4746314249052 389605.346338858 5818663.24867864 74.4746314249052 389604.865367866 5818662.26645055 74.4746314249052 389604.221141159 5818661.38266416 74.4746314249052 389603.433233239 5818660.62417288 74.4746314249052 389602.525584287 5818660.01402308 74.4746314249052 389601.525772756 5818659.57075386 74.4746314249052 389600.464177412 5818659.30783374 74.4746314249052 389599.532006945 5818659.23224188 74.4746314249052</gml:posList>
										</gml:LinearRing>
									</gml:interior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="GEOM_360934">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389582.277837926 5818695.84529126 63.9185715336953 389581.638901983 5818696.42563725 63.9185715336953 389580.95312984 5818696.95011462 63.9185715336953 389580.225943506 5818697.41496053 63.9185715336953 389579.461549268 5818697.81673725 63.9185715336953 389578.665996246 5818698.15258392 63.9185715336953 389577.845339341 5818698.41994358 63.9185715336953 389577.005038136 5818698.6171865 63.9185715336953 389576.151172765 5818698.74297712 63.9185715336953 389575.289209629 5818698.79599157 63.9185715336953 389574.406912219 5818698.77462974 63.9185715336953 389573.529886923 5818698.67748964 63.9185715336953 389572.664245688 5818698.50506597 63.9185715336953 389571.817333523 5818698.25843993 63.9185715336953 389570.994070885 5818697.93965276 63.9185715336953 389570.371334424 5818697.63902263 63.9185715336953 389569.770386784 5818697.29678234 63.9185715336953 389569.087273702 5818696.83801229 63.9185715336953 389568.442805658 5818696.3254094 63.9185715336953 389567.84255073 5818695.76283744 63.9185715336953 389567.289011636 5818695.15360353 63.9185715336953 389566.786539749 5818694.50189758 63.9185715336953 389566.337652423 5818693.81163837 63.9185715336953 389565.949914157 5818693.09549902 63.9185715336953 389565.620830813 5818692.35078118 63.9185715336953 389565.351711856 5818691.58203558 63.9185715336953 389565.144484143 5818690.79441415 63.9185715336953 389565.02775907 5818690.17656665 63.9185715336953 389564.949371538 5818689.55280234 63.9185715336953 389564.909355361 5818688.92495144 63.9185715336953 389564.916425603 5818688.07741131 63.9185715336953 389564.993748958 5818687.2334165 63.9185715336953 389565.140819169 5818686.39847123 63.9185715336953 389565.356531783 5818685.578698 63.9185715336953 389565.639777587 5818684.77991646 63.9185715336953 389565.987598755 5818684.00706548 63.9185715336953 389566.226263657 5818683.55975004 63.9185715336953 389566.486525935 5818683.12484179 63.9185715336953 389566.983055078 5818682.40530978 63.9185715336953 389567.539644234 5818681.7310177 63.9185715336953 389568.1514996 5818681.10663535 63.9185715336953 389568.814438063 5818680.53681895 63.9185715336953 389569.524270732 5818680.02592082 63.9185715336953 389570.274959083 5818679.57741156 63.9185715336953 389571.061083144 5818679.19505597 63.9185715336953 389571.877187146 5818678.88078968 63.9185715336953 389573.602277025 5818678.73310912 63.9185715336953 389575.418941865 5818678.8497578 63.9185715336953 389577.392505817 5818679.2658287 63.9185715336953 389579.825267184 5818679.97557182 63.9185715336953 389581.510907729 5818681.15313573 63.9185715336953 389583.054024073 5818682.78702043 63.9185715336953 389584.001168768 5818684.88585092 63.9185715336953 389584.651765143 5818687.29272859 63.9185715336953 389584.540865803 5818689.41168405 63.9185715336953 389584.278817323 5818691.5335145 63.9185715336953 389583.705845587 5818693.20764914 63.9185715336953 389583.127123878 5818694.57948657 63.9185715336953 389582.277837926 5818695.84529126 63.9185715336953</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
									<gml:interior>
										<gml:LinearRing>
											<gml:posList>389574.85398288 5818681.5685025 63.9185715336953 389573.809559023 5818681.69549027 63.9185715336953 389572.594219419 5818682.04604185 63.9185715336953 389571.458216386 5818682.60230851 63.9185715336953 389570.436066803 5818683.34738835 63.9185715336953 389569.558828169 5818684.25864252 63.9185715336953 389568.853154939 5818685.308383 63.9185715336953 389568.340488639 5818686.46471398 63.9185715336953 389568.036406376 5818687.69250091 63.9185715336953 389567.950147537 5818688.95443811 63.9185715336953 389568.084333053 5818690.21218225 63.9185715336953 389568.434885765 5818691.42751741 63.9185715336953 389568.991154306 5818692.56351625 63.9185715336953 389569.736236734 5818693.58566201 63.9185715336953 389570.647494097 5818694.46289733 63.9185715336953 389571.697238298 5818695.16856783 63.9185715336953 389572.853573388 5818695.68123208 63.9185715336953 389574.08136471 5818695.98531305 63.9185715336953 389575.343306444 5818696.07157137 63.9185715336953 389575.471327491 5818696.06800592 63.9185715336953 389576.601055129 5818695.93738615 63.9185715336953 389577.816394709 5818695.58683453 63.9185715336953 389578.952397706 5818695.03056785 63.9185715336953 389579.974547246 5818694.285488 63.9185715336953 389580.851785834 5818693.37423383 63.9185715336953 389581.557459022 5818692.32449336 63.9185715336953 389582.070125288 5818691.16816242 63.9185715336953 389582.374207529 5818689.94037552 63.9185715336953 389582.460466362 5818688.67843837 63.9185715336953 389582.326280855 5818687.42069427 63.9185715336953 389581.975728168 5818686.20535914 63.9185715336953 389581.419459663 5818685.06936032 63.9185715336953 389580.674377277 5818684.04721457 63.9185715336953 389579.763119961 5818683.16997926 63.9185715336953 389578.713375803 5818682.46430874 63.9185715336953 389577.557040746 5818681.95164446 63.9185715336953 389576.329249446 5818681.64756346 63.9185715336953 389575.195371751 5818681.55999956 63.9185715336953 389574.85398288 5818681.5685025 63.9185715336953</gml:posList>
										</gml:LinearRing>
									</gml:interior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="GEOM_360929">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389584.02595249 5818672.94030789 63.5502536068305 389583.547855108 5818679.60234645 63.5502536068305 389589.854534727 5818698.14611455 63.5502536068305 389583.818435196 5818700.31543508 63.5502536068305 389582.277830071 5818695.84527962 63.5502536068305 389583.127116024 5818694.57947494 63.5502536068305 389583.705837732 5818693.2076375 63.5502536068305 389584.278809468 5818691.53350286 63.5502536068305 389584.540857948 5818689.41167241 63.5502536068305 389584.651757288 5818687.29271696 63.5502536068305 389584.001160914 5818684.88583928 63.5502536068305 389583.054016218 5818682.78700879 63.5502536068305 389581.510899875 5818681.15312409 63.5502536068305 389579.82525933 5818679.97556018 63.5502536068305 389577.392497963 5818679.26581706 63.5502536068305 389575.418934011 5818678.84974616 63.5502536068305 389573.60226917 5818678.73309748 63.5502536068305 389571.877179292 5818678.88077804 63.5502536068305 389571.429443068 5818677.51306079 63.5502536068305 389570.857405273 5818677.27890494 63.5502536068305 389572.056695477 5818674.52163072 63.5502536068305 389573.320619062 5818671.79333673 63.5502536068305 389574.649205058 5818669.0955493 63.5502536068305 389576.040658059 5818666.43013242 63.5502536068305 389577.267614461 5818664.19688733 63.5502536068305 389578.539594741 5818661.98841782 63.5502536068305 389579.855401687 5818659.80596956 63.5502536068305 389581.214441922 5818657.65046678 63.5502536068305 389582.857697973 5818655.16426685 63.5502536068305 389584.559067806 5818652.71724135 63.5502536068305 389586.317348352 5818650.31032812 63.5502536068305 389588.130751058 5818647.94569898 63.5502536068305 389590.001086688 5818645.62240258 63.5502536068305 389591.924754886 5818643.34356016 63.5502536068305 389593.901778827 5818641.11039017 63.5502536068305 389595.929734014 5818638.92385567 63.5502536068305 389597.930766476 5818636.86202939 63.5502536068305 389599.978432036 5818634.84539447 63.5502536068305 389602.070317756 5818632.87552168 63.5502536068305 389602.924904247 5818633.81439077 63.5502536068305 389610.756541621 5818621.98514326 63.5502536068305 389615.37679375 5818634.83232897 63.5502536068305 389616.425891214 5818637.75128837 63.5502536068305 389616.922062041 5818639.13059527 63.5502536068305 389632.519410789 5818682.85232686 63.5502536068305 389629.100705948 5818684.08860919 63.5502536068305 389612.72899249 5818634.49272858 63.5502536068305 389600.922925746 5818639.15258577 63.5502536068305 389604.331438952 5818648.7647631 63.5502536068305 389607.575120468 5818649.71108743 63.5502536068305 389611.644097194 5818651.6497358 63.5502536068305 389614.321515089 5818654.62287462 63.5502536068305 389615.987438766 5818657.41364852 63.5502536068305 389618.064093423 5818660.59981868 63.5502536068305 389618.733856121 5818664.01435349 63.5502536068305 389614.851717171 5818664.54437138 63.5502536068305 389614.613292191 5818662.81176031 63.5502536068305 389613.869152255 5818660.23189245 63.5502536068305 389612.688326503 5818657.82043671 63.5502536068305 389611.106693722 5818655.65066394 63.5502536068305 389609.172311013 5818653.7885016 63.5502536068305 389606.943953609 5818652.29053053 63.5502536068305 389604.489329012 5818651.20226584 63.5502536068305 389601.88301974 5818650.55677391 63.5502536068305 389599.515300067 5818650.37089263 63.5502536068305 389599.017766888 5818650.3783465 63.5502536068305 389596.534500069 5818650.6585077 63.5502536068305 389593.95462727 5818651.40264395 63.5502536068305 389591.543166832 5818652.58346369 63.5502536068305 389589.373389765 5818654.16508831 63.5502536068305 389587.511223658 5818656.09946095 63.5502536068305 389586.013249495 5818658.32780668 63.5502536068305 389584.924982471 5818660.78241833 63.5502536068305 389584.279489036 5818663.38871377 63.5502536068305 389584.096382185 5818666.06750201 63.5502536068305 389584.38122553 5818668.73738943 63.5502536068305 389584.816131129 5818670.43688751 63.5502536068305 389584.02595249 5818672.94030789 63.5502536068305</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="GEOM_360931">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389609.437871129 5818677.54200729 70.5749528632974 389608.929256373 5818677.95347941 70.5749528632974 389605.866436059 5818679.80033233 70.5749528632974 389602.460259404 5818680.89107075 70.5749528632974 389599.790342365 5818681.17591565 70.5749528632974 389597.111524434 5818680.99280856 70.5749528632974 389594.50520014 5818680.3473131 70.5749528632974 389592.050561332 5818679.2590423 70.5749528632974 389589.822190975 5818677.76106273 70.5749528632974 389587.887796982 5818675.89888973 70.5749528632974 389586.306154947 5818673.72910447 70.5749528632974 389585.125322271 5818671.31763481 70.5749528632974 389584.381177964 5818668.73775201 70.5749528632974 389584.096332479 5818666.06784453 70.5749528632974 389584.279440705 5818663.38903616 70.5749528632974 389584.924938989 5818660.78272115 70.5749528632974 389586.013214189 5818658.32809106 70.5749528632974 389587.511199605 5818656.09972859 70.5749528632974 389589.373379702 5818654.16534142 70.5749528632974 389591.543173068 5818652.58370492 70.5749528632974 389593.954651623 5818651.40287631 70.5749528632974 389596.534543803 5818650.65873446 70.5749528632974 389599.204460885 5818650.37388975 70.5749528632974 389599.515449875 5818650.37111457 70.5749528632974 389601.883278787 5818650.55699702 70.5749528632974 389604.489602985 5818651.20249266 70.5749528632974 389606.94424164 5818652.29076358 70.5749528632974 389609.172611807 5818653.78874322 70.5749528632974 389611.107005593 5818655.65091623 70.5749528632974 389612.688647433 5818657.82070143 70.5749528632974 389613.869479947 5818660.23217098 70.5749528632974 389614.613624145 5818662.81205361 70.5749528632974 389614.898469586 5818665.48196091 70.5749528632974 389614.715361389 5818668.16076909 70.5749528632974 389614.069863201 5818670.76708394 70.5749528632974 389612.981588154 5818673.2217139 70.5749528632974 389611.483602928 5818675.4500763 70.5749528632974 389609.621423038 5818677.38446346 70.5749528632974 389609.437871129 5818677.54200729 70.5749528632974</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
									<gml:interior>
										<gml:LinearRing>
											<gml:posList>389599.371411147 5818653.34423121 70.5749528632974 389599.260989695 5818653.34583008 70.5749528632974 389597.152870987 5818653.57073837 70.5749528632974 389595.115834334 5818654.15829946 70.5749528632974 389593.211774067 5818655.09066059 70.5749528632974 389591.498544099 5818656.33949245 70.5749528632974 389590.028200062 5818657.86684992 70.5749528632974 389588.845417619 5818659.62632501 70.5749528632974 389587.986135023 5818661.56445695 70.5749528632974 389587.476461145 5818663.6223566 70.5749528632974 389587.33188217 5818665.73749569 70.5749528632974 389587.556791058 5818667.84560681 70.5749528632974 389588.144354066 5818669.88263605 70.5749528632974 389589.076718385 5818671.7866893 70.5749528632974 389590.325554593 5818673.49991287 70.5749528632974 389591.852917428 5818674.97025133 70.5749528632974 389593.612398737 5818676.15302918 70.5749528632974 389595.550537567 5818677.01230832 70.5749528632974 389597.608444544 5818677.52198 70.5749528632974 389599.723591204 5818677.66655809 70.5749528632974 389601.831709886 5818677.44164968 70.5749528632974 389603.868746471 5818676.85408849 70.5749528632974 389605.772806636 5818675.92172728 70.5749528632974 389607.486036482 5818674.67289539 70.5749528632974 389608.956380391 5818673.14553793 70.5749528632974 389610.139162715 5818671.38606289 70.5749528632974 389610.998445216 5818669.44793102 70.5749528632974 389611.508119034 5818667.39003148 70.5749528632974 389611.652697991 5818665.27489249 70.5749528632974 389611.42778913 5818663.16678149 70.5749528632974 389610.84022619 5818661.12975235 70.5749528632974 389609.907861973 5818659.22569918 70.5749528632974 389608.659025887 5818657.51247564 70.5749528632974 389607.13166318 5818656.04213718 70.5749528632974 389605.372181989 5818654.85935928 70.5749528632974 389603.434043255 5818654.00008007 70.5749528632974 389601.376136338 5818653.49040829 70.5749528632974 389599.371411147 5818653.34423121 70.5749528632974</gml:posList>
										</gml:LinearRing>
									</gml:interior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="GEOM_360932">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389599.532046551 5818659.23230056 76.3318475278365 389600.464217019 5818659.30789241 76.3318475278365 389601.525812363 5818659.57081254 76.3318475278365 389602.525623894 5818660.01408176 76.3318475278365 389603.433272846 5818660.62423156 76.3318475278365 389604.221180766 5818661.38272284 76.3318475278365 389604.865407473 5818662.26650923 76.3318475278365 389605.346378465 5818663.24873732 76.3318475278365 389605.649479684 5818664.29956263 76.3318475278365 389605.765501552 5818665.38705634 76.3318475278365 389605.690918805 5818666.47817552 76.3318475278365 389605.427997601 5818667.53976707 76.3318475278365 389604.984726668 5818668.53957505 76.3318475278365 389604.374574567 5818669.44722083 76.3318475278365 389603.616080459 5818670.23512602 76.3318475278365 389602.732290804 5818670.87935055 76.3318475278365 389601.7500591 5818671.36031996 76.3318475278365 389600.699229959 5818671.66342026 76.3318475278365 389599.611732294 5818671.77944187 76.3318475278365 389598.52060917 5818671.70485956 76.3318475278365 389597.45901381 5818671.44193946 76.3318475278365 389596.459202253 5818670.99867026 76.3318475278365 389595.551553269 5818670.38852047 76.3318475278365 389594.763645315 5818669.6300292 76.3318475278365 389594.119418576 5818668.7462428 76.3318475278365 389593.638447557 5818667.76401468 76.3318475278365 389593.33534632 5818666.71318935 76.3318475278365 389593.219324445 5818665.62569561 76.3318475278365 389593.293907197 5818664.5345764 76.3318475278365 389593.556828416 5818663.47298482 76.3318475278365 389594.000099375 5818662.47317682 76.3318475278365 389594.610251508 5818661.56553103 76.3318475278365 389595.368745649 5818660.77762583 76.3318475278365 389596.252535337 5818660.13340132 76.3318475278365 389597.234767068 5818659.65243192 76.3318475278365 389598.285596226 5818659.34933165 76.3318475278365 389599.373093899 5818659.23331007 76.3318475278365 389599.532046551 5818659.23230056 76.3318475278365</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
		</bldg:Building>
	</cityObjectMember>
</CityModel>