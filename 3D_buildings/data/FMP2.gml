<?xml version="1.0" encoding="UTF-8"?>
<CityModel xmlns:bldg="http://www.opengis.net/citygml/building/1.0" xmlns:app="http://www.opengis.net/citygml/appearance/1.0" xmlns:smil20="http://www.w3.org/2001/SMIL20/" xmlns:grp="http://www.opengis.net/citygml/cityobjectgroup/1.0" xmlns:base="http://www.citygml.org/citygml/profiles/base/1.0" xmlns:frn="http://www.opengis.net/citygml/cityfurniture/1.0" xmlns:smil20lang="http://www.w3.org/2001/SMIL20/Language" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:wtr="http://www.opengis.net/citygml/waterbody/1.0" xmlns:tex="http://www.opengis.net/citygml/texturedsurface/1.0" xmlns:veg="http://www.opengis.net/citygml/vegetation/1.0" xmlns:sch="http://www.ascc.net/xml/schematron" xmlns:gen="http://www.opengis.net/citygml/generics/1.0" xmlns:dem="http://www.opengis.net/citygml/relief/1.0" xmlns:gml="http://www.opengis.net/gml" xmlns:tran="http://www.opengis.net/citygml/transportation/1.0" xmlns:xAL="urn:oasis:names:tc:ciq:xsdschema:xAL:2.0" xmlns:luse="http://www.opengis.net/citygml/landuse/1.0" xmlns="http://www.opengis.net/citygml/1.0">
	<gml:boundedBy>
		<gml:Envelope srsName="EPSG:25833" srsDimension="3">
			<gml:lowerCorner>393977.244315236 5819223.40375021 35.0699996948242</gml:lowerCorner>
			<gml:upperCorner>394022.344116057 5819305.54358065 102.051980539221</gml:upperCorner>
		</gml:Envelope>
	</gml:boundedBy>
	<cityObjectMember>
		<bldg:Building gml:id="BLDG_0003000f00445bf0">
			<gml:name>BLDG_0003000f00445bf0</gml:name>
			<bldg:roofType>1000</bldg:roofType>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="GEOM_1337076">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">393996.506521125 5819239.94748965 100.144068365582 393989.520092561 5819243.33281489 100.144068365582 393984.502551981 5819233.81166121 100.144068365582 393991.59255732 5819230.13088562 100.144068365582 393996.506521125 5819239.94748965 100.144068365582</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="GEOM_1337073">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">394001.863640073 5819256.99007557 96.439374897578 394002.249832741 5819257.78557539 96.439374897578 394003.527496695 5819260.4193725 96.439374897578 393996.015812365 5819264.19333674 96.439374897578 393988.215799701 5819248.10042225 96.439374897578 393988.661475117 5819247.88413814 96.439374897578 393989.369902605 5819247.54049187 96.439374897578 393988.133449104 5819244.99039044 96.439374897578 393986.979346192 5819245.55032082 96.439374897578 393984.962897726 5819246.527619 96.439374897578 393984.500296654 5819246.75246434 96.439374897578 393979.650508299 5819236.75559443 96.439374897578 393977.456846415 5819232.2347432 96.439374897578 393977.245629387 5819231.79873641 96.439374897578 393984.654624659 5819228.20413162 96.439374897578 393984.86584266 5819228.64013739 96.439374897578 393987.806272873 5819227.21317188 96.439374897578 393987.580495155 5819226.74967455 96.439374897578 393994.4712324 5819223.40568974 96.439374897578 393994.69610134 5819223.86951032 96.439374897578 393996.864976511 5819228.33865259 96.439374897578 394001.721022386 5819238.34364242 96.439374897578 394001.27807433 5819238.55895872 96.439374897578 394000.575707501 5819238.90035387 96.439374897578 394001.813076516 5819241.45043783 96.439374897578 394002.95840881 5819240.89464195 96.439374897578 394005.122082639 5819239.84466356 96.439374897578 394005.578945324 5819239.62297912 96.439374897578 394012.077996232 5819253.01712351 96.439374897578 394011.621438405 5819253.23880116 96.439374897578 394004.975307652 5819256.46400858 96.439374897578 394004.58911399 5819255.66850878 96.439374897578 394001.863640073 5819256.99007557 96.439374897578</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
									<gml:interior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">393996.670486695 5819256.90124826 96.439374897578 394003.359275523 5819253.63724767 96.439374897578 393991.592477988 5819230.13076854 96.439374897578 393984.50247265 5819233.81154412 96.439374897578 393996.670486695 5819256.90124826 96.439374897578</gml:posList>
										</gml:LinearRing>
									</gml:interior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="GEOM_1337072">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">394012.101377634 5819298.14542408 88.1262548943376 394014.011170538 5819302.0845701 88.1262548943376 394007.217523965 5819305.54358065 88.1262548943376 394005.296007218 5819301.5655991 88.1262548943376 393998.750880305 5819288.0112131 88.1262548943376 393999.257046856 5819287.7666207 88.1262548943376 394000.022961244 5819287.39716405 88.1262548943376 393998.718714712 5819284.70004966 88.1262548943376 393997.448773624 5819285.31436397 88.1262548943376 393995.795116576 5819286.11357353 88.1262548943376 393995.346091494 5819286.33022637 88.1262548943376 393988.720176809 5819272.63548231 88.1262548943376 393989.168879645 5819272.41792069 88.1262548943376 393995.261526971 5819269.46841565 88.1262548943376 393995.647456418 5819270.2660563 88.1262548943376 393998.328484064 5819268.96608502 88.1262548943376 393997.943510824 5819268.170562 88.1262548943376 393996.216961778 5819264.6084509 88.1262548943376 393996.015634348 5819264.19307401 88.1262548943376 394003.527318677 5819260.41910977 88.1262548943376 394003.780502584 5819260.94102716 88.1262548943376 394012.124002412 5819278.14373524 88.1262548943376 394011.636596498 5819278.38003646 88.1262548943376 394010.965669148 5819278.70588196 88.1262548943376 394012.270709331 5819281.39657277 88.1262548943376 394013.429962976 5819280.83471351 88.1262548943376 394015.344202129 5819279.90635127 88.1262548943376 394015.79984535 5819279.684691 88.1262548943376 394022.344116057 5819293.17806234 88.1262548943376 394021.888168008 5819293.39972943 88.1262548943376 394012.101377634 5819298.14542408 88.1262548943376</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
									<gml:interior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">394006.83317682 5819296.46275161 88.1262548943376 394013.79073921 5819292.98906617 88.1262548943376 394002.438122635 5819269.74747214 88.1262548943376 393995.41239384 5819273.22245397 88.1262548943376 394006.83317682 5819296.46275161 88.1262548943376</gml:posList>
										</gml:LinearRing>
									</gml:interior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="GEOM_1337077">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">393996.506561981 5819239.94754994 102.051980539221 394003.359395712 5819253.63742506 102.051980539221 393996.670606883 5819256.90142564 102.051980539221 393989.520133417 5819243.33287519 102.051980539221 393996.506561981 5819239.94754994 102.051980539221</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="GEOM_1337075">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">394007.27574299 5819279.65120174 93.6537681800732 394013.790857579 5819292.98924087 93.6537681800732 394006.833295188 5819296.46292631 93.6537681800732 394000.301958192 5819283.17222533 93.6537681800732 394007.27574299 5819279.65120174 93.6537681800732</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="GEOM_1337074">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">394007.275703374 5819279.65114327 91.8037659285905 394000.301918576 5819283.17216686 91.8037659285905 393995.41247259 5819273.2225702 91.8037659285905 394002.438201386 5819269.74758837 91.8037659285905 394007.275703374 5819279.65114327 91.8037659285905</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1337064">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">393991.591169601 5819230.12883754 35.3400001525879 393991.59255732 5819230.13088562 100.144068365582 393984.502551981 5819233.81166121 100.144068365582 393984.501164272 5819233.80961312 35.3400001525879 393991.591169601 5819230.12883754 35.3400001525879</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1337019">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">394001.862325892 5819256.988136 35.0699996948242 394001.863640073 5819256.99007557 96.439374897578 394004.58911399 5819255.66850878 96.439374897578 394004.587799806 5819255.66656922 35.0699996948242 394001.862325892 5819256.988136 35.0699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1337042">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">393984.961583566 5819246.52567945 35.0699996948242 393984.962897726 5819246.527619 96.439374897578 393986.979346192 5819245.55032082 96.439374897578 393986.97803203 5819245.54838127 35.0699996948242 393984.961583566 5819246.52567945 35.0699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1337046">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">393988.660160952 5819247.88219858 35.0699996948242 393988.661475117 5819247.88413814 96.439374897578 393988.215799701 5819248.10042225 96.439374897578 393988.214485537 5819248.0984827 35.0699996948242 393988.660160952 5819247.88219858 35.0699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1337025">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">394005.120768454 5819239.84272401 35.0699996948242 394005.122082639 5819239.84466356 96.439374897578 394002.95840881 5819240.89464195 96.439374897578 394002.957094628 5819240.89270241 35.0699996948242 394005.120768454 5819239.84272401 35.0699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1337003">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">393989.167744994 5819272.41624606 35.1399993896484 393989.168879645 5819272.41792069 88.1262548943376 393988.720176809 5819272.63548231 88.1262548943376 393988.719042159 5819272.63380767 35.1399993896484 393989.167744994 5819272.41624606 35.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1337008">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">393998.717580051 5819284.69837501 35.1399993896484 393998.718714712 5819284.70004966 88.1262548943376 394000.022961244 5819287.39716405 88.1262548943376 394000.021826582 5819287.3954894 35.1399993896484 393998.717580051 5819284.69837501 35.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1337048">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">393996.014498191 5819264.19139717 35.0699996948242 393996.015812365 5819264.19333674 96.439374897578 394003.527496695 5819260.4193725 96.439374897578 394003.526182512 5819260.41743293 35.0699996948242 393996.014498191 5819264.19139717 35.0699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1337036">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">393984.8645285 5819228.63819786 35.0699996948242 393984.86584266 5819228.64013739 96.439374897578 393984.654624659 5819228.20413162 96.439374897578 393984.653310499 5819228.20219209 35.0699996948242 393984.8645285 5819228.63819786 35.0699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1337033">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">393994.469918228 5819223.40375021 35.0699996948242 393994.4712324 5819223.40568974 96.439374897578 393987.580495155 5819226.74967455 96.439374897578 393987.579180992 5819226.74773502 35.0699996948242 393994.469918228 5819223.40375021 35.0699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1337049">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">394003.526182512 5819260.41743293 35.0699996948242 394003.527496695 5819260.4193725 96.439374897578 394002.249832741 5819257.78557539 96.439374897578 394002.248518559 5819257.78363582 35.0699996948242 394003.526182512 5819260.41743293 35.0699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1337052">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">393984.50115849 5819233.80960459 35.0699996948242 393984.50247265 5819233.81154412 96.439374897578 393991.592477988 5819230.13076854 96.439374897578 393991.591163819 5819230.128829 35.0699996948242 393984.50115849 5819233.80960459 35.0699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1337021">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">394004.973993467 5819256.46206902 35.0699996948242 394004.975307652 5819256.46400858 96.439374897578 394011.621438405 5819253.23880116 96.439374897578 394011.620124212 5819253.2368616 35.0699996948242 394004.973993467 5819256.46206902 35.0699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1336989">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">394015.34306745 5819279.90467663 35.1399993896484 394015.344202129 5819279.90635127 88.1262548943376 394013.429962976 5819280.83471351 88.1262548943376 394013.4288283 5819280.83303886 35.1399993896484 394015.34306745 5819279.90467663 35.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1337018">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">394013.789604533 5819292.98739151 35.1399993896484 394013.79073921 5819292.98906617 88.1262548943376 394006.83317682 5819296.46275161 88.1262548943376 394006.832042151 5819296.46107695 35.1399993896484 394013.789604533 5819292.98739151 35.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1337069">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">393996.669176161 5819256.89931407 35.2400016784668 393996.670606883 5819256.90142564 102.051980539221 394003.359395712 5819253.63742506 102.051980539221 394003.357964981 5819253.63531348 35.2400016784668 393996.669176161 5819256.89931407 35.2400016784668</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1337061">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">394006.83204365 5819296.46107916 35.2099990844727 394006.833295188 5819296.46292631 93.6537681800732 394013.790857579 5819292.98924087 93.6537681800732 394013.789606033 5819292.98739372 35.2099990844727 394006.83204365 5819296.46107916 35.2099990844727</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1337054">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">394003.35796134 5819253.63530811 35.0699996948242 394003.359275523 5819253.63724767 96.439374897578 393996.670486695 5819256.90124826 96.439374897578 393996.66917252 5819256.89930869 35.0699996948242 394003.35796134 5819253.63530811 35.0699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1337001">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">393995.64632176 5819270.26438166 35.1399993896484 393995.647456418 5819270.2660563 88.1262548943376 393995.261526971 5819269.46841565 88.1262548943376 393995.260392314 5819269.46674102 35.1399993896484 393995.64632176 5819270.26438166 35.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1337027">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">394001.811762335 5819241.44849827 35.0699996948242 394001.813076516 5819241.45043783 96.439374897578 394000.575707501 5819238.90035387 96.439374897578 394000.574393322 5819238.89841433 35.0699996948242 394001.811762335 5819241.44849827 35.0699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1337031">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">393996.863662336 5819228.33671305 35.0699996948242 393996.864976511 5819228.33865259 96.439374897578 393994.69610134 5819223.86951032 96.439374897578 393994.694787168 5819223.86757079 35.0699996948242 393996.863662336 5819228.33671305 35.0699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1337059">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">394007.274491452 5819279.64935461 35.2099990844727 394007.27574299 5819279.65120174 93.6537681800732 394000.301958192 5819283.17222533 93.6537681800732 394000.300706662 5819283.1703782 35.2099990844727 394007.274491452 5819279.64935461 35.2099990844727</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1337004">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">393988.719042159 5819272.63380767 35.1399993896484 393988.720176809 5819272.63548231 88.1262548943376 393995.346091494 5819286.33022637 88.1262548943376 393995.344956837 5819286.32855171 35.1399993896484 393988.719042159 5819272.63380767 35.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1337034">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">393987.579180992 5819226.74773502 35.0699996948242 393987.580495155 5819226.74967455 96.439374897578 393987.806272873 5819227.21317188 96.439374897578 393987.804958709 5819227.21123234 35.0699996948242 393987.579180992 5819226.74773502 35.0699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1336995">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">394003.779367918 5819260.93935254 35.1399993896484 394003.780502584 5819260.94102716 88.1262548943376 394003.527318677 5819260.41910977 88.1262548943376 394003.526184011 5819260.41743514 35.1399993896484 394003.779367918 5819260.93935254 35.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1337066">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">393989.518704845 5819243.33076679 35.3400001525879 393989.520092561 5819243.33281489 100.144068365582 393996.506521125 5819239.94748965 100.144068365582 393996.5051334 5819239.94544155 35.3400001525879 393989.518704845 5819243.33076679 35.3400001525879</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1337051">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">393996.66917252 5819256.89930869 35.0699996948242 393996.670486695 5819256.90124826 96.439374897578 393984.50247265 5819233.81154412 96.439374897578 393984.50115849 5819233.80960459 35.0699996948242 393996.66917252 5819256.89930869 35.0699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1337014">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">394014.01003586 5819302.08289543 35.1399993896484 394014.011170538 5819302.0845701 88.1262548943376 394012.101377634 5819298.14542408 88.1262548943376 394012.100242959 5819298.14374942 35.1399993896484 394014.01003586 5819302.08289543 35.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1336988">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">394015.798710671 5819279.68301635 35.1399993896484 394015.79984535 5819279.684691 88.1262548943376 394015.344202129 5819279.90635127 88.1262548943376 394015.34306745 5819279.90467663 35.1399993896484 394015.798710671 5819279.68301635 35.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1337056">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">394002.43699054 5819269.7458013 35.2599983215332 394002.438201386 5819269.74758837 91.8037659285905 393995.41247259 5819273.2225702 91.8037659285905 393995.411261753 5819273.22078313 35.2599983215332 394002.43699054 5819269.7458013 35.2599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1336997">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">393996.01449969 5819264.19139938 35.1399993896484 393996.015634348 5819264.19307401 88.1262548943376 393996.216961778 5819264.6084509 88.1262548943376 393996.21582712 5819264.60677627 35.1399993896484 393996.01449969 5819264.19139938 35.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1336991">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">394012.269574656 5819281.39489812 35.1399993896484 394012.270709331 5819281.39657277 88.1262548943376 394010.965669148 5819278.70588196 88.1262548943376 394010.964534474 5819278.70420732 35.1399993896484 394012.269574656 5819281.39489812 35.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1336999">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">393997.942376164 5819268.16888737 35.1399993896484 393997.943510824 5819268.170562 88.1262548943376 393998.328484064 5819268.96608502 88.1262548943376 393998.327349404 5819268.96441038 35.1399993896484 393997.942376164 5819268.16888737 35.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1337017">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">394002.43698797 5819269.74579751 35.1399993896484 394002.438122635 5819269.74747214 88.1262548943376 394013.79073921 5819292.98906617 88.1262548943376 394013.789604533 5819292.98739151 35.1399993896484 394002.43698797 5819269.74579751 35.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1337065">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">393984.501164272 5819233.80961312 35.3400001525879 393984.502551981 5819233.81166121 100.144068365582 393989.520092561 5819243.33281489 100.144068365582 393989.518704845 5819243.33076679 35.3400001525879 393984.501164272 5819233.80961312 35.3400001525879</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1337007">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">393997.447638964 5819285.31268932 35.1399993896484 393997.448773624 5819285.31436397 88.1262548943376 393998.718714712 5819284.70004966 88.1262548943376 393998.717580051 5819284.69837501 35.1399993896484 393997.447638964 5819285.31268932 35.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1337058">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">394000.300707733 5819283.17037978 35.2599983215332 394000.301918576 5819283.17216686 91.8037659285905 394007.275703374 5819279.65114327 91.8037659285905 394007.274492523 5819279.64935619 35.2599983215332 394000.300707733 5819283.17037978 35.2599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1337029">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">394001.27676015 5819238.55701917 35.0699996948242 394001.27807433 5819238.55895872 96.439374897578 394001.721022386 5819238.34364242 96.439374897578 394001.719708205 5819238.34170288 35.0699996948242 394001.27676015 5819238.55701917 35.0699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1337032">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">393994.694787168 5819223.86757079 35.0699996948242 393994.69610134 5819223.86951032 96.439374897578 393994.4712324 5819223.40568974 96.439374897578 393994.469918228 5819223.40375021 35.0699996948242 393994.694787168 5819223.86757079 35.0699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1337057">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">393995.411261753 5819273.22078313 35.2599983215332 393995.41247259 5819273.2225702 91.8037659285905 394000.301918576 5819283.17216686 91.8037659285905 394000.300707733 5819283.17037978 35.2599983215332 393995.411261753 5819273.22078313 35.2599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1337063">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">393996.5051334 5819239.94544155 35.3400001525879 393996.506521125 5819239.94748965 100.144068365582 393991.59255732 5819230.13088562 100.144068365582 393991.591169601 5819230.12883754 35.3400001525879 393996.5051334 5819239.94544155 35.3400001525879</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1337070">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">394003.357964981 5819253.63531348 35.2400016784668 394003.359395712 5819253.63742506 102.051980539221 393996.506561981 5819239.94754994 102.051980539221 393996.505131259 5819239.94543839 35.2400016784668 394003.357964981 5819253.63531348 35.2400016784668</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1336996">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">394003.526184011 5819260.41743514 35.1399993896484 394003.527318677 5819260.41910977 88.1262548943376 393996.015634348 5819264.19307401 88.1262548943376 393996.01449969 5819264.19139938 35.1399993896484 394003.526184011 5819260.41743514 35.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1337000">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">393998.327349404 5819268.96441038 35.1399993896484 393998.328484064 5819268.96608502 88.1262548943376 393995.647456418 5819270.2660563 88.1262548943376 393995.64632176 5819270.26438166 35.1399993896484 393998.327349404 5819268.96441038 35.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1337006">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">393995.793981918 5819286.11189887 35.1399993896484 393995.795116576 5819286.11357353 88.1262548943376 393997.448773624 5819285.31436397 88.1262548943376 393997.447638964 5819285.31268932 35.1399993896484 393995.793981918 5819286.11189887 35.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1337009">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">394000.021826582 5819287.3954894 35.1399993896484 394000.022961244 5819287.39716405 88.1262548943376 393999.257046856 5819287.7666207 88.1262548943376 393999.255912195 5819287.76494605 35.1399993896484 394000.021826582 5819287.3954894 35.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1337037">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">393984.653310499 5819228.20219209 35.0699996948242 393984.654624659 5819228.20413162 96.439374897578 393977.245629387 5819231.79873641 96.439374897578 393977.244315236 5819231.79679687 35.0699996948242 393984.653310499 5819228.20219209 35.0699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1337002">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">393995.260392314 5819269.46674102 35.1399993896484 393995.261526971 5819269.46841565 88.1262548943376 393989.168879645 5819272.41792069 88.1262548943376 393989.167744994 5819272.41624606 35.1399993896484 393995.260392314 5819269.46674102 35.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1337055">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">394007.274492523 5819279.64935619 35.2599983215332 394007.275703374 5819279.65114327 91.8037659285905 394002.438201386 5819269.74758837 91.8037659285905 394002.43699054 5819269.7458013 35.2599983215332 394007.274492523 5819279.64935619 35.2599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1337062">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">394013.789606033 5819292.98739372 35.2099990844727 394013.790857579 5819292.98924087 93.6537681800732 394007.27574299 5819279.65120174 93.6537681800732 394007.274491452 5819279.64935461 35.2099990844727 394013.789606033 5819292.98739372 35.2099990844727</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1337045">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">393989.36858844 5819247.53855231 35.0699996948242 393989.369902605 5819247.54049187 96.439374897578 393988.661475117 5819247.88413814 96.439374897578 393988.660160952 5819247.88219858 35.0699996948242 393989.36858844 5819247.53855231 35.0699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1337060">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">394000.300706662 5819283.1703782 35.2099990844727 394000.301958192 5819283.17222533 93.6537681800732 394006.833295188 5819296.46292631 93.6537681800732 394006.83204365 5819296.46107916 35.2099990844727 394000.300706662 5819283.1703782 35.2099990844727</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1337023">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">394012.076682038 5819253.01518395 35.0699996948242 394012.077996232 5819253.01712351 96.439374897578 394005.578945324 5819239.62297912 96.439374897578 394005.577631138 5819239.62103957 35.0699996948242 394012.076682038 5819253.01518395 35.0699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1337012">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">394005.29487255 5819301.56392443 35.1399993896484 394005.296007218 5819301.5655991 88.1262548943376 394007.217523965 5819305.54358065 88.1262548943376 394007.216389295 5819305.54190598 35.1399993896484 394005.29487255 5819301.56392443 35.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1336998">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">393996.21582712 5819264.60677627 35.1399993896484 393996.216961778 5819264.6084509 88.1262548943376 393997.943510824 5819268.170562 88.1262548943376 393997.942376164 5819268.16888737 35.1399993896484 393996.21582712 5819264.60677627 35.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1337053">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">393991.591163819 5819230.128829 35.0699996948242 393991.592477988 5819230.13076854 96.439374897578 394003.359275523 5819253.63724767 96.439374897578 394003.35796134 5819253.63530811 35.0699996948242 393991.591163819 5819230.128829 35.0699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1337040">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">393979.649194146 5819236.75365489 35.0699996948242 393979.650508299 5819236.75559443 96.439374897578 393984.500296654 5819246.75246434 96.439374897578 393984.498982494 5819246.75052479 35.0699996948242 393979.649194146 5819236.75365489 35.0699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1337010">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">393999.255912195 5819287.76494605 35.1399993896484 393999.257046856 5819287.7666207 88.1262548943376 393998.750880305 5819288.0112131 88.1262548943376 393998.749745644 5819288.00953844 35.1399993896484 393999.255912195 5819287.76494605 35.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1337043">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">393986.97803203 5819245.54838127 35.0699996948242 393986.979346192 5819245.55032082 96.439374897578 393988.133449104 5819244.99039044 96.439374897578 393988.13213494 5819244.98845089 35.0699996948242 393986.97803203 5819245.54838127 35.0699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1336992">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">394010.964534474 5819278.70420732 35.1399993896484 394010.965669148 5819278.70588196 88.1262548943376 394011.636596498 5819278.38003646 88.1262548943376 394011.635461823 5819278.37836182 35.1399993896484 394010.964534474 5819278.70420732 35.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1336994">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">394012.122867737 5819278.1420606 35.1399993896484 394012.124002412 5819278.14373524 88.1262548943376 394003.780502584 5819260.94102716 88.1262548943376 394003.779367918 5819260.93935254 35.1399993896484 394012.122867737 5819278.1420606 35.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1337030">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">394001.719708205 5819238.34170288 35.0699996948242 394001.721022386 5819238.34364242 96.439374897578 393996.864976511 5819228.33865259 96.439374897578 393996.863662336 5819228.33671305 35.0699996948242 394001.719708205 5819238.34170288 35.0699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1336986">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">394021.887033322 5819293.39805477 35.1399993896484 394021.888168008 5819293.39972943 88.1262548943376 394022.344116057 5819293.17806234 88.1262548943376 394022.342981371 5819293.17638768 35.1399993896484 394021.887033322 5819293.39805477 35.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1337026">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">394002.957094628 5819240.89270241 35.0699996948242 394002.95840881 5819240.89464195 96.439374897578 394001.813076516 5819241.45043783 96.439374897578 394001.811762335 5819241.44849827 35.0699996948242 394002.957094628 5819240.89270241 35.0699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1337038">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">393977.244315236 5819231.79679687 35.0699996948242 393977.245629387 5819231.79873641 96.439374897578 393977.456846415 5819232.2347432 96.439374897578 393977.455532264 5819232.23280366 35.0699996948242 393977.244315236 5819231.79679687 35.0699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1337020">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">394004.587799806 5819255.66656922 35.0699996948242 394004.58911399 5819255.66850878 96.439374897578 394004.975307652 5819256.46400858 96.439374897578 394004.973993467 5819256.46206902 35.0699996948242 394004.587799806 5819255.66656922 35.0699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1337028">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">394000.574393322 5819238.89841433 35.0699996948242 394000.575707501 5819238.90035387 96.439374897578 394001.27807433 5819238.55895872 96.439374897578 394001.27676015 5819238.55701917 35.0699996948242 394000.574393322 5819238.89841433 35.0699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1337035">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">393987.804958709 5819227.21123234 35.0699996948242 393987.806272873 5819227.21317188 96.439374897578 393984.86584266 5819228.64013739 96.439374897578 393984.8645285 5819228.63819786 35.0699996948242 393987.804958709 5819227.21123234 35.0699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1337024">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">394005.577631138 5819239.62103957 35.0699996948242 394005.578945324 5819239.62297912 96.439374897578 394005.122082639 5819239.84466356 96.439374897578 394005.120768454 5819239.84272401 35.0699996948242 394005.577631138 5819239.62103957 35.0699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1337050">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">394002.248518559 5819257.78363582 35.0699996948242 394002.249832741 5819257.78557539 96.439374897578 394001.863640073 5819256.99007557 96.439374897578 394001.862325892 5819256.988136 35.0699996948242 394002.248518559 5819257.78363582 35.0699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1337011">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">393998.749745644 5819288.00953844 35.1399993896484 393998.750880305 5819288.0112131 88.1262548943376 394005.296007218 5819301.5655991 88.1262548943376 394005.29487255 5819301.56392443 35.1399993896484 393998.749745644 5819288.00953844 35.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1337005">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">393995.344956837 5819286.32855171 35.1399993896484 393995.346091494 5819286.33022637 88.1262548943376 393995.795116576 5819286.11357353 88.1262548943376 393995.793981918 5819286.11189887 35.1399993896484 393995.344956837 5819286.32855171 35.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1336987">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">394022.342981371 5819293.17638768 35.1399993896484 394022.344116057 5819293.17806234 88.1262548943376 394015.79984535 5819279.684691 88.1262548943376 394015.798710671 5819279.68301635 35.1399993896484 394022.342981371 5819293.17638768 35.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1337039">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">393977.455532264 5819232.23280366 35.0699996948242 393977.456846415 5819232.2347432 96.439374897578 393979.650508299 5819236.75559443 96.439374897578 393979.649194146 5819236.75365489 35.0699996948242 393977.455532264 5819232.23280366 35.0699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1337022">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">394011.620124212 5819253.2368616 35.0699996948242 394011.621438405 5819253.23880116 96.439374897578 394012.077996232 5819253.01712351 96.439374897578 394012.076682038 5819253.01518395 35.0699996948242 394011.620124212 5819253.2368616 35.0699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1337016">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">393995.411259183 5819273.22077933 35.1399993896484 393995.41239384 5819273.22245397 88.1262548943376 394002.438122635 5819269.74747214 88.1262548943376 394002.43698797 5819269.74579751 35.1399993896484 393995.411259183 5819273.22077933 35.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1337047">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">393988.214485537 5819248.0984827 35.0699996948242 393988.215799701 5819248.10042225 96.439374897578 393996.015812365 5819264.19333674 96.439374897578 393996.014498191 5819264.19139717 35.0699996948242 393988.214485537 5819248.0984827 35.0699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1337068">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">393989.518702704 5819243.33076363 35.2400016784668 393989.520133417 5819243.33287519 102.051980539221 393996.670606883 5819256.90142564 102.051980539221 393996.669176161 5819256.89931407 35.2400016784668 393989.518702704 5819243.33076363 35.2400016784668</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1337044">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">393988.13213494 5819244.98845089 35.0699996948242 393988.133449104 5819244.99039044 96.439374897578 393989.369902605 5819247.54049187 96.439374897578 393989.36858844 5819247.53855231 35.0699996948242 393988.13213494 5819244.98845089 35.0699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1337041">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">393984.498982494 5819246.75052479 35.0699996948242 393984.500296654 5819246.75246434 96.439374897578 393984.962897726 5819246.527619 96.439374897578 393984.961583566 5819246.52567945 35.0699996948242 393984.498982494 5819246.75052479 35.0699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1337015">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">394006.832042151 5819296.46107695 35.1399993896484 394006.83317682 5819296.46275161 88.1262548943376 393995.41239384 5819273.22245397 88.1262548943376 393995.411259183 5819273.22077933 35.1399993896484 394006.832042151 5819296.46107695 35.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1336985">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">394012.100242959 5819298.14374942 35.1399993896484 394012.101377634 5819298.14542408 88.1262548943376 394021.888168008 5819293.39972943 88.1262548943376 394021.887033322 5819293.39805477 35.1399993896484 394012.100242959 5819298.14374942 35.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1336993">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">394011.635461823 5819278.37836182 35.1399993896484 394011.636596498 5819278.38003646 88.1262548943376 394012.124002412 5819278.14373524 88.1262548943376 394012.122867737 5819278.1420606 35.1399993896484 394011.635461823 5819278.37836182 35.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1337013">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">394007.216389295 5819305.54190598 35.1399993896484 394007.217523965 5819305.54358065 88.1262548943376 394014.011170538 5819302.0845701 88.1262548943376 394014.01003586 5819302.08289543 35.1399993896484 394007.216389295 5819305.54190598 35.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1336990">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">394013.4288283 5819280.83303886 35.1399993896484 394013.429962976 5819280.83471351 88.1262548943376 394012.270709331 5819281.39657277 88.1262548943376 394012.269574656 5819281.39489812 35.1399993896484 394013.4288283 5819280.83303886 35.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_1337067">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">393996.505131259 5819239.94543839 35.2400016784668 393996.506561981 5819239.94754994 102.051980539221 393989.520133417 5819243.33287519 102.051980539221 393989.518702704 5819243.33076363 35.2400016784668 393996.505131259 5819239.94543839 35.2400016784668</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface gml:id="GEOM_1336978">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">394014.01003586 5819302.08289543 35.1399993896484 394012.100242959 5819298.14374942 35.1399993896484 394021.887033322 5819293.39805477 35.1399993896484 394022.342981371 5819293.17638768 35.1399993896484 394015.798710671 5819279.68301635 35.1399993896484 394015.34306745 5819279.90467663 35.1399993896484 394013.4288283 5819280.83303886 35.1399993896484 394012.269574656 5819281.39489812 35.1399993896484 394010.964534474 5819278.70420732 35.1399993896484 394011.635461823 5819278.37836182 35.1399993896484 394012.122867737 5819278.1420606 35.1399993896484 394003.779367918 5819260.93935254 35.1399993896484 394003.526184011 5819260.41743514 35.1399993896484 393996.01449969 5819264.19139938 35.1399993896484 393996.21582712 5819264.60677627 35.1399993896484 393997.942376164 5819268.16888737 35.1399993896484 393998.327349404 5819268.96441038 35.1399993896484 393995.64632176 5819270.26438166 35.1399993896484 393995.260392314 5819269.46674102 35.1399993896484 393989.167744994 5819272.41624606 35.1399993896484 393988.719042159 5819272.63380767 35.1399993896484 393995.344956837 5819286.32855171 35.1399993896484 393995.793981918 5819286.11189887 35.1399993896484 393997.447638964 5819285.31268932 35.1399993896484 393998.717580051 5819284.69837501 35.1399993896484 394000.021826582 5819287.3954894 35.1399993896484 393999.255912195 5819287.76494605 35.1399993896484 393998.749745644 5819288.00953844 35.1399993896484 394005.29487255 5819301.56392443 35.1399993896484 394007.216389295 5819305.54190598 35.1399993896484 394014.01003586 5819302.08289543 35.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
									<gml:interior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">394013.789604533 5819292.98739151 35.1399993896484 394006.832042151 5819296.46107695 35.1399993896484 393995.411259183 5819273.22077933 35.1399993896484 394002.43698797 5819269.74579751 35.1399993896484 394013.789604533 5819292.98739151 35.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:interior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface gml:id="GEOM_1336979">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">394002.248518559 5819257.78363582 35.0699996948242 394001.862325892 5819256.988136 35.0699996948242 394004.587799806 5819255.66656922 35.0699996948242 394004.973993467 5819256.46206902 35.0699996948242 394011.620124212 5819253.2368616 35.0699996948242 394012.076682038 5819253.01518395 35.0699996948242 394005.577631138 5819239.62103957 35.0699996948242 394005.120768454 5819239.84272401 35.0699996948242 394002.957094628 5819240.89270241 35.0699996948242 394001.811762335 5819241.44849827 35.0699996948242 394000.574393322 5819238.89841433 35.0699996948242 394001.27676015 5819238.55701917 35.0699996948242 394001.719708205 5819238.34170288 35.0699996948242 393996.863662336 5819228.33671305 35.0699996948242 393994.694787168 5819223.86757079 35.0699996948242 393994.469918228 5819223.40375021 35.0699996948242 393987.579180992 5819226.74773502 35.0699996948242 393987.804958709 5819227.21123234 35.0699996948242 393984.8645285 5819228.63819786 35.0699996948242 393984.653310499 5819228.20219209 35.0699996948242 393977.244315236 5819231.79679687 35.0699996948242 393977.455532264 5819232.23280366 35.0699996948242 393979.649194146 5819236.75365489 35.0699996948242 393984.498982494 5819246.75052479 35.0699996948242 393984.961583566 5819246.52567945 35.0699996948242 393986.97803203 5819245.54838127 35.0699996948242 393988.13213494 5819244.98845089 35.0699996948242 393989.36858844 5819247.53855231 35.0699996948242 393988.660160952 5819247.88219858 35.0699996948242 393988.214485537 5819248.0984827 35.0699996948242 393996.014498191 5819264.19139717 35.0699996948242 394003.526182512 5819260.41743293 35.0699996948242 394002.248518559 5819257.78363582 35.0699996948242</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
									<gml:interior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">394003.35796134 5819253.63530811 35.0699996948242 393996.66917252 5819256.89930869 35.0699996948242 393984.50115849 5819233.80960459 35.0699996948242 393991.591163819 5819230.128829 35.0699996948242 394003.35796134 5819253.63530811 35.0699996948242</gml:posList>
										</gml:LinearRing>
									</gml:interior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface gml:id="GEOM_1336981">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">394013.789606033 5819292.98739372 35.2099990844727 394007.274491452 5819279.64935461 35.2099990844727 394000.300706662 5819283.1703782 35.2099990844727 394006.83204365 5819296.46107916 35.2099990844727 394013.789606033 5819292.98739372 35.2099990844727</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface gml:id="GEOM_1336983">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">394003.357964981 5819253.63531348 35.2400016784668 393996.505131259 5819239.94543839 35.2400016784668 393989.518702704 5819243.33076363 35.2400016784668 393996.669176161 5819256.89931407 35.2400016784668 394003.357964981 5819253.63531348 35.2400016784668</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface gml:id="GEOM_1336980">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">394000.300707733 5819283.17037978 35.2599983215332 394007.274492523 5819279.64935619 35.2599983215332 394002.43699054 5819269.7458013 35.2599983215332 393995.411261753 5819273.22078313 35.2599983215332 394000.300707733 5819283.17037978 35.2599983215332</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface gml:id="GEOM_1336982">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">393989.518704845 5819243.33076679 35.3400001525879 393996.5051334 5819239.94544155 35.3400001525879 393991.591169601 5819230.12883754 35.3400001525879 393984.501164272 5819233.80961312 35.3400001525879 393989.518704845 5819243.33076679 35.3400001525879</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
		</bldg:Building>
	</cityObjectMember>
</CityModel>