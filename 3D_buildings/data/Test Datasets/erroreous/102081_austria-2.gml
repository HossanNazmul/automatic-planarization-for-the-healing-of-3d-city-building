<?xml version="1.0" encoding="UTF-8"?>
<core:CityModel xmlns:bldg="http://www.opengis.net/citygml/building/1.0" xmlns:luse="http://www.opengis.net/citygml/landuse/1.0" xmlns:app="http://www.opengis.net/citygml/appearance/1.0" xmlns:xAL="urn:oasis:names:tc:ciq:xsdschema:xAL:2.0" xmlns:base="http://www.citygml.org/citygml/profiles/base/1.0" xmlns:frn="http://www.opengis.net/citygml/cityfurniture/1.0" xmlns:smil20lang="http://www.w3.org/2001/SMIL20/Language" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:tex="http://www.opengis.net/citygml/texturedsurface/1.0" xmlns:wtr="http://www.opengis.net/citygml/waterbody/1.0" xmlns:smil20="http://www.w3.org/2001/SMIL20/" xmlns:grp="http://www.opengis.net/citygml/cityobjectgroup/1.0" xmlns:core="http://www.opengis.net/citygml/1.0" xmlns:veg="http://www.opengis.net/citygml/vegetation/1.0" xmlns:sch="http://www.ascc.net/xml/schematron" xmlns:gen="http://www.opengis.net/citygml/generics/1.0" xmlns:dem="http://www.opengis.net/citygml/relief/1.0" xmlns:gml="http://www.opengis.net/gml" xmlns:tran="http://www.opengis.net/citygml/transportation/1.0">
<gml:boundedBy>
<gml:Envelope srsName="EPSG:31256" srsDimension="3">
<gml:lowerCorner>1167.28 340881.92 31.37</gml:lowerCorner>
<gml:upperCorner>1187.267 340905.652 60.6</gml:upperCorner>
</gml:Envelope>
</gml:boundedBy>
<core:cityObjectMember>
<bldg:Building gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b">
<gml:name>011523</gml:name>
<core:creationDate>2016-09-29</core:creationDate>
<gen:stringAttribute name="Blattnummer">
<gen:value>102081</gen:value>
</gen:stringAttribute>
<gen:stringAttribute name="FP_ID">
<gen:value>9973</gen:value>
</gen:stringAttribute>
<gen:stringAttribute name="HoeheDach">
<gen:value>61</gen:value>
</gen:stringAttribute>
<gen:stringAttribute name="HoeheGrund">
<gen:value>31</gen:value>
</gen:stringAttribute>
<gen:stringAttribute name="NiedrigsteTraufeDesGebaeudes">
<gen:value>55</gen:value>
</gen:stringAttribute>
<bldg:roofType>FLACHDACH</bldg:roofType>
<bldg:measuredHeight uom="urn:ogc:def:uom:UCUM:m">29.23</bldg:measuredHeight>
<bldg:lod2Solid>
<gml:Solid gml:id="UUID_97d2eb54-7e90-422c-9cc1-a6e997e72352" srsName="EPSG:31256" srsDimension="3">
<gml:exterior>
<gml:CompositeSurface gml:id="UUID_4af0a9d8-580c-483d-99d3-d23e801a9299">
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_36f054d6-84f1-45c5-9a76-e7e7a888bb12_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_36f054d6-84f1-45c5-9a76-e7e7a888bb12_poly_0_">
<gml:posList>1170.803 340883.002 59 1170.989 340884.464 59 1167.431 340884.926 56.521 1167.365 340884.331 56.527 1167.28 340883.56 56.536 1167.28 340883.46 56.545 1170.803 340883.002 59</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_195dad7c-720d-493d-897a-1707dae1365d_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_195dad7c-720d-493d-897a-1707dae1365d_poly_0_">
<gml:posList>1170.989 340884.464 59 1171.251 340886.519 58.52 1167.659 340886.985 58.52 1167.431 340884.926 59 1170.989 340884.464 59</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_f03e4a2a-19bc-4f44-8e4e-d6274602a8d3_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_f03e4a2a-19bc-4f44-8e4e-d6274602a8d3_poly_0_">
<gml:posList>1171.251 340886.519 59 1171.41 340887.769 56.32 1167.798 340888.237 56.32 1167.659 340886.985 59 1171.251 340886.519 59</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_be4718c3-8e73-408b-8f95-742dd6bba823_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_be4718c3-8e73-408b-8f95-742dd6bba823_poly_0_">
<gml:posList>1171.41 340887.769 59 1171.737 340890.332 59 1167.942 340890.829 58.526 1167.86 340890.18 58.526 1168.01 340890.15 58.545 1167.973 340889.817 58.546 1167.798 340888.237 58.549 1171.41 340887.769 59</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_1144a46b-1f08-4e10-bd76-4a09172029aa_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_1144a46b-1f08-4e10-bd76-4a09172029aa_poly_0_">
<gml:posList>1171.737 340890.332 59 1171.868 340891.367 59 1168.073 340891.859 56.355 1167.942 340890.829 56.355 1171.737 340890.332 59</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_f469bd61-679d-4a56-b833-c711fd648c9b_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_f469bd61-679d-4a56-b833-c711fd648c9b_poly_0_">
<gml:posList>1171.868 340891.367 59 1172.144 340893.531 59 1168.57 340893.995 58.554 1168.43 340892.94 58.553 1168.21 340892.94 58.526 1168.131 340892.321 58.526 1168.073 340891.859 58.526 1171.868 340891.367 59</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_fc3e32fc-a9bb-4b49-91b6-8b2188d708ef_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_fc3e32fc-a9bb-4b49-91b6-8b2188d708ef_poly_0_">
<gml:posList>1172.144 340893.531 59 1172.282 340894.613 59 1168.714 340895.076 56.513 1168.57 340893.995 56.509 1172.144 340893.531 59</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_fbb335b2-8097-490d-8f3a-dd6e58898df7_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_fbb335b2-8097-490d-8f3a-dd6e58898df7_poly_0_">
<gml:posList>1172.282 340894.613 59 1172.548 340896.698 59 1168.99 340897.16 58.556 1168.97 340897.008 58.556 1168.714 340895.076 58.555 1172.282 340894.613 59</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_bbe4a66d-01fc-417b-813c-eecf32a7d398_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_bbe4a66d-01fc-417b-813c-eecf32a7d398_poly_0_">
<gml:posList>1172.548 340896.698 59 1172.809 340898.747 59 1171.989 340898.952 58.42 1169.176 340899.657 56.43 1169.16 340899.54 56.429 1169.3 340899.49 56.53 1169.053 340897.628 56.523 1168.99 340897.16 56.521 1172.548 340896.698 59</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_4df85bc7-60e8-4c90-a4dd-6ad1d28399ea_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_4df85bc7-60e8-4c90-a4dd-6ad1d28399ea_poly_0_">
<gml:posList>1171.989 340898.952 58.42 1172.809 340898.747 58.42 1175.059 340898.184 58.42 1172.426 340901.972 60.2 1169.198 340899.815 58.513 1169.176 340899.657 58.42 1171.989 340898.952 58.42</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_8fba28b4-745b-4f92-89aa-71056294b209_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_8fba28b4-745b-4f92-89aa-71056294b209_poly_0_">
<gml:posList>1172.426 340901.972 60.2 1169.99 340905.476 58.553 1169.198 340899.815 58.513 1172.426 340901.972 60.2</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_6f99c3cd-8e26-4661-b779-cb5f9cb5a06b_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_6f99c3cd-8e26-4661-b779-cb5f9cb5a06b_poly_0_">
<gml:posList>1172.426 340901.972 60.2 1175.775 340904.21 58.449 1170.192 340905.608 58.449 1170.015 340905.652 58.45 1169.99 340905.476 58.553 1172.426 340901.972 60.2</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_16504be6-fcd4-46cd-aa09-5ef82962414f_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_16504be6-fcd4-46cd-aa09-5ef82962414f_poly_0_">
<gml:posList>1175.059 340898.184 58.42 1175.249 340899.68 58.42 1175.825 340904.197 58.42 1175.775 340904.21 58.449 1172.426 340901.972 60.2 1175.059 340898.184 58.42</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_d5a3adb3-45bb-44f4-b3d5-2558a4ab3d79_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_d5a3adb3-45bb-44f4-b3d5-2558a4ab3d79_poly_0_">
<gml:posList>1177.59 340882.12 56.519 1179.16 340881.92 55.096 1179.6 340885.33 55.091 1179.85 340885.4 54.877 1175.675 340885.946 58.66 1173.535 340886.223 60.6 1173.273 340884.168 60.6 1173.086 340882.705 60.6 1177.59 340882.12 56.519</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_c0bc9dd8-cf0f-4723-862e-c0da1cf7151f_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_c0bc9dd8-cf0f-4723-862e-c0da1cf7151f_poly_0_">
<gml:posList>1167.28 340883.46 31.37 1167.28 340883.46 56.545 1167.28 340883.56 56.536 1167.28 340883.56 31.37 1167.28 340883.46 31.37</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_636325af-030c-4039-8aa8-d2b4a323c10d_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_636325af-030c-4039-8aa8-d2b4a323c10d_poly_0_">
<gml:posList>1167.28 340883.56 31.37 1167.28 340883.56 56.536 1167.365 340884.331 56.527 1167.365 340884.331 31.37 1167.28 340883.56 31.37</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_a4611d0a-9bb2-4710-8dcd-3686dfe24fc2_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_a4611d0a-9bb2-4710-8dcd-3686dfe24fc2_poly_0_">
<gml:posList>1170.989 340884.464 59 1167.431 340884.926 59 1167.431 340884.926 56.521 1170.989 340884.464 59</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_9cab1cb7-b37a-46ea-b4c0-609fc637980f_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_9cab1cb7-b37a-46ea-b4c0-609fc637980f_poly_0_">
<gml:posList>1171.251 340886.519 58.52 1171.251 340886.519 59 1167.659 340886.985 59 1167.659 340886.985 58.52 1171.251 340886.519 58.52</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_e4ea8541-afdd-4cc4-89c1-97243ecc80c0_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_e4ea8541-afdd-4cc4-89c1-97243ecc80c0_poly_0_">
<gml:posList>1167.973 340889.817 31.37 1167.973 340889.817 58.546 1168.01 340890.15 58.545 1168.01 340890.15 31.37 1167.973 340889.817 31.37</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_45d429d7-5908-46a0-96a9-998fddf8456e_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_45d429d7-5908-46a0-96a9-998fddf8456e_poly_0_">
<gml:posList>1168.01 340890.15 31.37 1168.01 340890.15 58.545 1167.86 340890.18 58.526 1167.86 340890.18 31.37 1168.01 340890.15 31.37</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_c76dd42c-4290-45f6-9407-b7e07bdd29d9_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_c76dd42c-4290-45f6-9407-b7e07bdd29d9_poly_0_">
<gml:posList>1167.86 340890.18 31.37 1167.86 340890.18 58.526 1167.942 340890.829 58.526 1167.942 340890.829 56.355 1167.942 340890.829 31.37 1167.86 340890.18 31.37</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_fae5d9c6-e1c7-4aa5-ba44-1d3698e37a27_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_fae5d9c6-e1c7-4aa5-ba44-1d3698e37a27_poly_0_">
<gml:posList>1167.942 340890.829 56.355 1167.942 340890.829 58.526 1171.737 340890.332 59 1167.942 340890.829 56.355</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_85e916f8-1362-4900-82e3-433a5fa5684a_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_85e916f8-1362-4900-82e3-433a5fa5684a_poly_0_">
<gml:posList>1171.41 340887.769 56.32 1171.41 340887.769 59 1167.798 340888.237 58.549 1167.798 340888.237 56.32 1171.41 340887.769 56.32</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_6e150c6f-fa6d-44c3-9076-e7ff55bb52c1_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_6e150c6f-fa6d-44c3-9076-e7ff55bb52c1_poly_0_">
<gml:posList>1168.131 340892.321 31.37 1168.131 340892.321 58.526 1168.21 340892.94 58.526 1168.21 340892.94 31.37 1168.131 340892.321 31.37</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_2bb8f8ca-7c6e-4077-bab6-8089ca17490f_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_2bb8f8ca-7c6e-4077-bab6-8089ca17490f_poly_0_">
<gml:posList>1168.21 340892.94 31.37 1168.21 340892.94 58.526 1168.43 340892.94 58.553 1168.43 340892.94 31.37 1168.21 340892.94 31.37</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_0c8821ad-af9f-4093-b8b4-341159f43a1d_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_0c8821ad-af9f-4093-b8b4-341159f43a1d_poly_0_">
<gml:posList>1168.57 340893.995 56.509 1168.57 340893.995 58.554 1172.144 340893.531 59 1168.57 340893.995 56.509</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_3e8a1737-86df-489d-9435-33a1ca36c764_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_3e8a1737-86df-489d-9435-33a1ca36c764_poly_0_">
<gml:posList>1171.868 340891.367 59 1168.073 340891.859 58.526 1168.073 340891.859 56.355 1171.868 340891.367 59</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_2309b5be-42aa-49eb-9312-7fad5a0ac724_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_2309b5be-42aa-49eb-9312-7fad5a0ac724_poly_0_">
<gml:posList>1168.99 340897.16 56.521 1168.99 340897.16 58.556 1172.548 340896.698 59 1168.99 340897.16 56.521</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_7a9df863-08a8-48d6-b29d-4e3d1be3cd86_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_7a9df863-08a8-48d6-b29d-4e3d1be3cd86_poly_0_">
<gml:posList>1172.282 340894.613 59 1168.714 340895.076 58.555 1168.714 340895.076 56.513 1172.282 340894.613 59</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_d1424a23-7180-42e6-8055-36ae8f2f45c9_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_d1424a23-7180-42e6-8055-36ae8f2f45c9_poly_0_">
<gml:posList>1169.053 340897.628 31.37 1169.053 340897.628 56.523 1169.3 340899.49 56.53 1169.3 340899.49 31.37 1169.053 340897.628 31.37</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_db389721-f655-45c8-a701-67dba421a4d7_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_db389721-f655-45c8-a701-67dba421a4d7_poly_0_">
<gml:posList>1169.3 340899.49 31.37 1169.3 340899.49 56.53 1169.16 340899.54 56.429 1169.16 340899.54 31.37 1169.3 340899.49 31.37</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_7dfa8151-73ef-4d0d-a779-ecf65e866e20_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_7dfa8151-73ef-4d0d-a779-ecf65e866e20_poly_0_">
<gml:posList>1171.989 340898.952 58.42 1169.176 340899.657 58.42 1169.176 340899.657 56.43 1171.989 340898.952 58.42</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_a454cb96-8957-4264-afa6-93cbc1258904_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_a454cb96-8957-4264-afa6-93cbc1258904_poly_0_">
<gml:posList>1170.015 340905.652 31.37 1170.015 340905.652 58.45 1170.192 340905.608 58.449 1170.192 340905.608 31.37 1170.015 340905.652 31.37</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_af17799d-4ea8-4a4e-8935-f590afbe733a_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_af17799d-4ea8-4a4e-8935-f590afbe733a_poly_0_">
<gml:posList>1170.989 340884.464 59 1171.251 340886.519 59 1171.251 340886.519 58.52 1170.989 340884.464 59</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_b4f4faa0-1b00-4a7b-8128-ae0fd9994ce8_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_b4f4faa0-1b00-4a7b-8128-ae0fd9994ce8_poly_0_">
<gml:posList>1171.251 340886.519 59 1171.41 340887.769 59 1171.41 340887.769 56.32 1171.251 340886.519 59</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_1e17538e-c8a8-47cc-859d-cfa1990d23d7_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_1e17538e-c8a8-47cc-859d-cfa1990d23d7_poly_0_">
<gml:posList>1173.535 340886.223 58.66 1173.535 340886.223 60.6 1175.675 340885.946 58.66 1173.535 340886.223 58.66</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_27fe4c61-de22-42ad-8b1c-93a34840e70a_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_27fe4c61-de22-42ad-8b1c-93a34840e70a_poly_0_">
<gml:posList>1179.85 340885.4 31.37 1179.85 340885.4 54.877 1179.6 340885.33 55.091 1179.6 340885.33 31.37 1179.85 340885.4 31.37</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_e81d5271-4c19-4389-a189-64d442549f16_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_e81d5271-4c19-4389-a189-64d442549f16_poly_0_">
<gml:posList>1179.6 340885.33 31.37 1179.6 340885.33 55.091 1179.16 340881.92 55.096 1179.16 340881.92 31.37 1179.6 340885.33 31.37</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_cdb52aa6-084a-45b3-93e4-6978ea4704a6_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_cdb52aa6-084a-45b3-93e4-6978ea4704a6_poly_0_">
<gml:posList>1179.16 340881.92 31.37 1179.16 340881.92 55.096 1177.59 340882.12 56.519 1177.59 340882.12 31.37 1179.16 340881.92 31.37</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_dd396f71-57f2-4091-9db7-fa871c8cc90a_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_dd396f71-57f2-4091-9db7-fa871c8cc90a_poly_0_">
<gml:posList>1181.37 340886.44 31.37 1181.37 340886.44 58.66 1181.19 340886.25 58.66 1181.19 340886.25 31.37 1181.37 340886.44 31.37</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_d256b8df-7f6e-44b6-ace0-e03ec07fbb40_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_d256b8df-7f6e-44b6-ace0-e03ec07fbb40_poly_0_">
<gml:posList>1181.19 340886.25 31.37 1181.19 340886.25 58.66 1181 340886.07 58.66 1181 340886.07 31.37 1181.19 340886.25 31.37</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_3ab3f0ba-55cd-4607-bf9d-9fa8a0359cdd_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_3ab3f0ba-55cd-4607-bf9d-9fa8a0359cdd_poly_0_">
<gml:posList>1181 340886.07 31.37 1181 340886.07 58.66 1180.79 340885.9 58.66 1180.79 340885.9 31.37 1181 340886.07 31.37</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_cb25e7a6-d476-4766-b701-2662a0e150e8_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_cb25e7a6-d476-4766-b701-2662a0e150e8_poly_0_">
<gml:posList>1180.79 340885.9 31.37 1180.79 340885.9 58.66 1180.57 340885.75 58.66 1180.57 340885.75 31.37 1180.79 340885.9 31.37</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_c33e5c5f-7b0c-45b7-9c8b-c15233e8c38f_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_c33e5c5f-7b0c-45b7-9c8b-c15233e8c38f_poly_0_">
<gml:posList>1180.57 340885.75 31.37 1180.57 340885.75 58.66 1180.34 340885.62 58.66 1180.34 340885.62 31.37 1180.57 340885.75 31.37</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_3512b908-754d-4bee-bd7e-8e906bdc357d_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_3512b908-754d-4bee-bd7e-8e906bdc357d_poly_0_">
<gml:posList>1180.34 340885.62 31.37 1180.34 340885.62 58.66 1180.1 340885.5 58.66 1180.1 340885.5 31.37 1180.34 340885.62 31.37</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_8b5c6850-5b7f-4cce-a670-c6a3f95e2019_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_8b5c6850-5b7f-4cce-a670-c6a3f95e2019_poly_0_">
<gml:posList>1180.1 340885.5 31.37 1180.1 340885.5 58.66 1179.85 340885.4 58.66 1179.85 340885.4 54.877 1179.85 340885.4 31.37 1180.1 340885.5 31.37</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_fc1b666a-fb13-4ac9-aeda-ab355da592f6_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_fc1b666a-fb13-4ac9-aeda-ab355da592f6_poly_0_">
<gml:posList>1179.85 340885.4 54.877 1179.85 340885.4 58.66 1175.675 340885.946 58.66 1179.85 340885.4 54.877</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_cd00af68-afda-4750-bc2e-761860dd93a8_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_cd00af68-afda-4750-bc2e-761860dd93a8_poly_0_">
<gml:posList>1182.24 340888.62 31.37 1182.24 340888.62 58.66 1182.22 340888.36 58.66 1182.22 340888.36 31.37 1182.24 340888.62 31.37</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_2d048f26-44af-464f-ab77-b86cdb5e56b7_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_2d048f26-44af-464f-ab77-b86cdb5e56b7_poly_0_">
<gml:posList>1182.22 340888.36 31.37 1182.22 340888.36 58.66 1182.17 340888.1 58.66 1182.17 340888.1 31.37 1182.22 340888.36 31.37</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_2a0a73bf-7ed2-48e6-98c2-a334069af09c_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_2a0a73bf-7ed2-48e6-98c2-a334069af09c_poly_0_">
<gml:posList>1182.17 340888.1 31.37 1182.17 340888.1 58.66 1182.11 340887.84 58.66 1182.11 340887.84 31.37 1182.17 340888.1 31.37</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_cd191ba3-f613-4cc7-8418-5b7c187931c5_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_cd191ba3-f613-4cc7-8418-5b7c187931c5_poly_0_">
<gml:posList>1182.11 340887.84 31.37 1182.11 340887.84 58.66 1182.04 340887.58 58.66 1182.04 340887.58 31.37 1182.11 340887.84 31.37</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_fde63830-fe63-4a7d-8778-20a3dd6f3f6a_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_fde63830-fe63-4a7d-8778-20a3dd6f3f6a_poly_0_">
<gml:posList>1182.04 340887.58 31.37 1182.04 340887.58 58.66 1181.94 340887.34 58.66 1181.94 340887.34 31.37 1182.04 340887.58 31.37</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_9d66a0f5-8da7-4d44-8557-03ed8a7c4082_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_9d66a0f5-8da7-4d44-8557-03ed8a7c4082_poly_0_">
<gml:posList>1181.94 340887.34 31.37 1181.94 340887.34 58.66 1181.82 340887.1 58.66 1181.82 340887.1 31.37 1181.94 340887.34 31.37</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_0ba6f978-1395-4b4f-a8b3-c9e81bb9a9b5_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_0ba6f978-1395-4b4f-a8b3-c9e81bb9a9b5_poly_0_">
<gml:posList>1181.82 340887.1 31.37 1181.82 340887.1 58.66 1181.69 340886.87 58.66 1181.69 340886.87 31.37 1181.82 340887.1 31.37</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_1d0bbd3f-3fca-45dd-9b16-a49b96cf72c8_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_1d0bbd3f-3fca-45dd-9b16-a49b96cf72c8_poly_0_">
<gml:posList>1181.69 340886.87 31.37 1181.69 340886.87 58.66 1181.54 340886.65 58.66 1181.54 340886.65 31.37 1181.69 340886.87 31.37</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_e45b4ff8-35c0-47b5-913e-72e66c25b300_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_e45b4ff8-35c0-47b5-913e-72e66c25b300_poly_0_">
<gml:posList>1182.05 340889.93 31.37 1182.05 340889.93 58.66 1182.12 340889.68 58.66 1182.12 340889.68 31.37 1182.05 340889.93 31.37</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_9ab85a85-5d10-413a-a73f-b66376705949_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_9ab85a85-5d10-413a-a73f-b66376705949_poly_0_">
<gml:posList>1182.12 340889.68 31.37 1182.12 340889.68 58.66 1182.18 340889.42 58.66 1182.18 340889.42 31.37 1182.12 340889.68 31.37</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_ef4b4ee3-2901-43c4-819c-d6ce81fc973a_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_ef4b4ee3-2901-43c4-819c-d6ce81fc973a_poly_0_">
<gml:posList>1182.18 340889.42 31.37 1182.18 340889.42 58.66 1182.22 340889.15 58.66 1182.22 340889.15 31.37 1182.18 340889.42 31.37</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_55476d25-d440-40a0-af2e-99e0a971e183_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_55476d25-d440-40a0-af2e-99e0a971e183_poly_0_">
<gml:posList>1182.02 340890.69 31.37 1182.02 340890.69 58.66 1181.84 340890.42 58.66 1181.84 340890.42 31.37 1182.02 340890.69 31.37</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_605b649f-9a4d-42e6-9797-7629eab857e0_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_605b649f-9a4d-42e6-9797-7629eab857e0_poly_0_">
<gml:posList>1181.84 340890.42 31.37 1181.84 340890.42 58.66 1181.95 340890.18 58.66 1181.95 340890.18 31.37 1181.84 340890.42 31.37</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_f90cae25-f3b6-4fa4-8662-6713be70aed3_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_f90cae25-f3b6-4fa4-8662-6713be70aed3_poly_0_">
<gml:posList>1185.83 340890.19 31.37 1185.83 340890.19 58.66 1185.59 340890.221 58.66 1185.59 340890.221 31.37 1185.83 340890.19 31.37</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_4f97218f-e7cf-4f65-992e-82a60ea7f530_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_4f97218f-e7cf-4f65-992e-82a60ea7f530_poly_0_">
<gml:posList>1167.365 340884.331 56.527 1167.431 340884.926 56.521 1167.431 340884.926 59 1167.659 340886.985 58.52 1167.659 340886.985 59 1167.798 340888.237 56.32 1167.798 340888.237 58.549 1167.973 340889.817 58.546 1167.973 340889.817 31.37 1167.798 340888.237 31.37 1167.659 340886.985 31.37 1167.431 340884.926 31.37 1167.365 340884.331 31.37 1167.365 340884.331 56.527</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_7cf48b23-0ca0-4c45-8f0e-7c33bc72b376_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_7cf48b23-0ca0-4c45-8f0e-7c33bc72b376_poly_0_">
<gml:posList>1169.16 340899.54 56.429 1169.176 340899.657 56.43 1169.176 340899.657 58.42 1169.198 340899.815 58.513 1169.99 340905.476 58.553 1170.015 340905.652 58.45 1170.015 340905.652 31.37 1169.99 340905.476 31.37 1169.198 340899.815 31.37 1169.176 340899.657 31.37 1169.16 340899.54 31.37 1169.16 340899.54 56.429</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_ece4c58f-44bf-4383-bf7c-ff4d16fe270a_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_ece4c58f-44bf-4383-bf7c-ff4d16fe270a_poly_0_">
<gml:posList>1173.535 340886.223 58.66 1173.694 340887.473 58.66 1174.02 340890.036 58.66 1174.118 340890.804 58.66 1174.152 340891.071 58.66 1174.428 340893.235 58.66 1174.566 340894.317 58.66 1174.832 340896.402 58.66 1175.059 340898.184 58.66 1175.059 340898.184 60.6 1174.832 340896.402 60.6 1174.566 340894.317 60.6 1174.428 340893.235 60.6 1174.152 340891.071 60.6 1174.118 340890.804 60.6 1174.02 340890.036 60.6 1173.694 340887.473 60.6 1173.535 340886.223 60.6 1173.535 340886.223 58.66</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_3d8d270a-e163-43dd-85bb-deb93ab4ceb3_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_3d8d270a-e163-43dd-85bb-deb93ab4ceb3_poly_0_">
<gml:posList>1175.775 340904.21 58.449 1175.825 340904.197 58.42 1175.825 340904.197 58.435 1183.978 340902.156 58.435 1187.267 340901.332 58.435 1187.267 340901.332 31.37 1183.978 340902.156 31.37 1175.825 340904.197 31.37 1175.775 340904.21 31.37 1170.192 340905.608 31.37 1170.192 340905.608 58.449 1175.775 340904.21 58.449</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_91d9885c-3da0-4896-9a31-f3a61d78e307_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_91d9885c-3da0-4896-9a31-f3a61d78e307_poly_0_">
<gml:posList>1186.493 340895.32 58.66 1186.439 340894.898 58.66 1186.17 340892.813 58.66 1186.09 340892.19 58.66 1186.09 340892.19 31.37 1186.17 340892.813 31.37 1186.439 340894.898 31.37 1186.493 340895.32 31.37 1186.686 340896.816 31.37 1187.267 340901.332 31.37 1187.267 340901.332 58.435 1186.686 340896.816 59.8 1186.493 340895.32 60.6 1186.493 340895.32 58.66</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_8c34d31f-f661-4b7a-b9e7-dc5f51e92eab_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_8c34d31f-f661-4b7a-b9e7-dc5f51e92eab_poly_0_">
<gml:posList>1186.686 340896.816 31.37 1186.493 340895.32 31.37 1186.439 340894.898 31.37 1186.17 340892.813 31.37 1186.09 340892.19 31.37 1186.03 340891.731 31.37 1185.83 340890.19 31.37 1185.59 340890.221 31.37 1182.509 340890.626 31.37 1182.02 340890.69 31.37 1181.84 340890.42 31.37 1181.95 340890.18 31.37 1182.001 340890.054 31.37 1182.05 340889.93 31.37 1182.12 340889.68 31.37 1182.18 340889.42 31.37 1182.22 340889.15 31.37 1182.234 340888.971 31.37 1182.24 340888.89 31.37 1182.24 340888.77 31.37 1182.24 340888.62 31.37 1182.22 340888.36 31.37 1182.17 340888.1 31.37 1182.11 340887.84 31.37 1182.04 340887.58 31.37 1181.94 340887.34 31.37 1181.82 340887.1 31.37 1181.69 340886.87 31.37 1181.54 340886.65 31.37 1181.398 340886.474 31.37 1181.37 340886.44 31.37 1181.19 340886.25 31.37 1181 340886.07 31.37 1180.79 340885.9 31.37 1180.57 340885.75 31.37 1180.34 340885.62 31.37 1180.1 340885.5 31.37 1179.85 340885.4 31.37 1179.6 340885.33 31.37 1179.16 340881.92 31.37 1177.59 340882.12 31.37 1173.086 340882.705 31.37 1170.803 340883.002 31.37 1167.28 340883.46 31.37 1167.28 340883.56 31.37 1167.365 340884.331 31.37 1167.431 340884.926 31.37 1167.659 340886.985 31.37 1167.798 340888.237 31.37 1167.973 340889.817 31.37 1168.01 340890.15 31.37 1167.86 340890.18 31.37 1167.942 340890.829 31.37 1168.073 340891.859 31.37 1168.131 340892.321 31.37 1168.21 340892.94 31.37 1168.43 340892.94 31.37 1168.57 340893.995 31.37 1168.714 340895.076 31.37 1168.97 340897.008 31.37 1168.99 340897.16 31.37 1169.053 340897.628 31.37 1169.3 340899.49 31.37 1169.16 340899.54 31.37 1169.176 340899.657 31.37 1169.198 340899.815 31.37 1169.99 340905.476 31.37 1170.015 340905.652 31.37 1170.192 340905.608 31.37 1175.775 340904.21 31.37 1175.825 340904.197 31.37 1183.978 340902.156 31.37 1187.267 340901.332 31.37 1186.686 340896.816 31.37</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_797f3140-b115-4a71-8b94-e549356d5c4f_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_797f3140-b115-4a71-8b94-e549356d5c4f_poly_0_">
<gml:posList>1175.675 340885.946 58.66 1179.85 340885.4 58.66 1180.1 340885.5 58.66 1180.34 340885.62 58.66 1180.57 340885.75 58.66 1180.79 340885.9 58.66 1181 340886.07 58.66 1181.19 340886.25 58.66 1181.37 340886.44 58.66 1181.398 340886.474 58.66 1181.54 340886.65 58.66 1181.69 340886.87 58.66 1181.82 340887.1 58.66 1181.94 340887.34 58.66 1182.04 340887.58 58.66 1182.11 340887.84 58.66 1182.17 340888.1 58.66 1182.22 340888.36 58.66 1182.24 340888.62 58.66 1182.24 340888.77 58.66 1182.24 340888.89 58.66 1182.234 340888.971 58.66 1182.22 340889.15 58.66 1182.18 340889.42 58.66 1182.12 340889.68 58.66 1182.05 340889.93 58.66 1182.001 340890.054 58.66 1181.95 340890.18 58.66 1181.84 340890.42 58.66 1182.02 340890.69 58.66 1182.509 340890.626 58.66 1185.59 340890.221 58.66 1185.83 340890.19 58.66 1186.03 340891.731 58.66 1186.09 340892.19 58.66 1186.17 340892.813 58.66 1186.439 340894.898 58.66 1186.493 340895.32 58.66 1183.212 340896.142 58.66 1175.059 340898.184 58.66 1174.832 340896.402 58.66 1174.566 340894.317 58.66 1174.428 340893.235 58.66 1174.152 340891.071 58.66 1174.118 340890.804 58.66 1174.02 340890.036 58.66 1173.694 340887.473 58.66 1173.535 340886.223 58.66 1175.675 340885.946 58.66</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_c59a044f-f98f-473a-8879-40409c5ddd91_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_c59a044f-f98f-473a-8879-40409c5ddd91_poly_0_">
<gml:posList>1173.086 340882.705 60.6 1173.273 340884.168 60.6 1173.535 340886.223 60.6 1173.694 340887.473 60.6 1174.02 340890.036 60.6 1174.118 340890.804 60.6 1174.152 340891.071 60.6 1174.428 340893.235 60.6 1174.566 340894.317 60.6 1174.832 340896.402 60.6 1175.059 340898.184 60.6 1172.809 340898.747 59 1172.548 340896.698 59 1172.282 340894.613 59 1172.144 340893.531 59 1171.868 340891.367 59 1171.737 340890.332 59 1171.41 340887.769 59 1171.251 340886.519 59 1170.989 340884.464 59 1170.803 340883.002 59 1173.086 340882.705 60.6</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_4e1fa5b9-a6b8-4522-839d-7679f9bccd33_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_4e1fa5b9-a6b8-4522-839d-7679f9bccd33_poly_0_">
<gml:posList>1170.803 340883.002 31.37 1173.086 340882.705 31.37 1177.59 340882.12 31.37 1177.59 340882.12 56.519 1173.086 340882.705 60.6 1170.803 340883.002 59 1167.28 340883.46 56.545 1167.28 340883.46 31.37 1170.803 340883.002 31.37</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_3492bba1-191e-4558-a47a-ce4db125d963_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_3492bba1-191e-4558-a47a-ce4db125d963_poly_0_">
<gml:posList>1168.43 340892.94 58.553 1168.57 340893.995 58.554 1168.57 340893.995 56.509 1168.714 340895.076 56.513 1168.714 340895.076 58.555 1168.97 340897.008 58.556 1168.97 340897.008 31.37 1168.714 340895.076 31.37 1168.57 340893.995 31.37 1168.43 340892.94 31.37 1168.43 340892.94 58.553</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_d90c047b-f92c-453b-b108-978ddafb10dc_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_d90c047b-f92c-453b-b108-978ddafb10dc_poly_0_">
<gml:posList>1185.83 340890.19 31.37 1186.03 340891.731 31.37 1186.09 340892.19 31.37 1186.09 340892.19 58.66 1186.03 340891.731 58.66 1185.83 340890.19 58.66 1185.83 340890.19 31.37</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_2a8a9f7c-b79a-4c4d-ace1-b6e0eeb1974a_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_2a8a9f7c-b79a-4c4d-ace1-b6e0eeb1974a_poly_0_">
<gml:posList>1183.212 340896.142 58.66 1186.493 340895.32 58.66 1186.493 340895.32 60.6 1183.212 340896.142 60.6 1175.059 340898.184 60.6 1175.059 340898.184 58.66 1183.212 340896.142 58.66</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_29b96358-029c-4c24-9204-2c6e2aba7776_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_29b96358-029c-4c24-9204-2c6e2aba7776_poly_0_">
<gml:posList>1175.059 340898.184 58.66 1175.059 340898.184 60.6 1175.249 340899.68 59.8 1175.825 340904.197 58.435 1175.825 340904.197 58.42 1175.249 340899.68 58.42 1175.059 340898.184 58.42 1175.059 340898.184 58.66</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_091bdd79-0cd1-4062-bb32-310889c91866_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_091bdd79-0cd1-4062-bb32-310889c91866_poly_0_">
<gml:posList>1182.509 340890.626 31.37 1185.59 340890.221 31.37 1185.59 340890.221 58.66 1182.509 340890.626 58.66 1182.02 340890.69 58.66 1182.02 340890.69 31.37 1182.509 340890.626 31.37</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_907e4a72-e478-486c-a568-131c138b167a_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_907e4a72-e478-486c-a568-131c138b167a_poly_0_">
<gml:posList>1182.234 340888.971 58.66 1182.24 340888.89 58.66 1182.24 340888.89 31.37 1182.234 340888.971 31.37 1182.22 340889.15 31.37 1182.22 340889.15 58.66 1182.234 340888.971 58.66</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_89eef811-ad17-4cbf-b95b-1c6ab38d1f94_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_89eef811-ad17-4cbf-b95b-1c6ab38d1f94_poly_0_">
<gml:posList>1182.001 340890.054 58.66 1182.05 340889.93 58.66 1182.05 340889.93 31.37 1182.001 340890.054 31.37 1181.95 340890.18 31.37 1181.95 340890.18 58.66 1182.001 340890.054 58.66</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_5eb2f78e-0d0f-4de8-835e-4a7b82e82e0f_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_5eb2f78e-0d0f-4de8-835e-4a7b82e82e0f_poly_0_">
<gml:posList>1182.24 340888.77 58.66 1182.24 340888.62 58.66 1182.24 340888.62 31.37 1182.24 340888.77 31.37 1182.24 340888.89 31.37 1182.24 340888.89 58.66 1182.24 340888.77 58.66</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_eff64174-0f33-49b9-8612-3d09c5421b48_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_eff64174-0f33-49b9-8612-3d09c5421b48_poly_0_">
<gml:posList>1181.37 340886.44 31.37 1181.398 340886.474 31.37 1181.54 340886.65 31.37 1181.54 340886.65 58.66 1181.398 340886.474 58.66 1181.37 340886.44 58.66 1181.37 340886.44 31.37</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_f3e9569c-13c8-459b-a2f9-f98e665bef42_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_f3e9569c-13c8-459b-a2f9-f98e665bef42_poly_0_">
<gml:posList>1172.809 340898.747 59 1175.059 340898.184 60.6 1175.059 340898.184 58.66 1175.059 340898.184 58.42 1172.809 340898.747 58.42 1171.989 340898.952 58.42 1172.809 340898.747 59</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_70ac3d20-110d-44ef-8412-a559dfb64bda_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_70ac3d20-110d-44ef-8412-a559dfb64bda_poly_0_">
<gml:posList>1168.97 340897.008 58.556 1168.99 340897.16 58.556 1168.99 340897.16 56.521 1169.053 340897.628 56.523 1169.053 340897.628 31.37 1168.99 340897.16 31.37 1168.97 340897.008 31.37 1168.97 340897.008 58.556</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_c33cee57-d398-48f3-bb6e-1126f928748a_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_c33cee57-d398-48f3-bb6e-1126f928748a_poly_0_">
<gml:posList>1167.942 340890.829 56.355 1168.073 340891.859 56.355 1168.073 340891.859 58.526 1168.131 340892.321 58.526 1168.131 340892.321 31.37 1168.073 340891.859 31.37 1167.942 340890.829 31.37 1167.942 340890.829 56.355</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_f2de7e9e-7e55-44b0-beb9-02cbb97ede6c_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_f2de7e9e-7e55-44b0-beb9-02cbb97ede6c_poly_0_">
<gml:posList>1183.402 340897.639 59.8 1186.686 340896.816 59.8 1187.267 340901.332 58.435 1183.978 340902.156 58.435 1175.825 340904.197 58.435 1175.249 340899.68 59.8 1183.402 340897.639 59.8</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_60504ca9-c3de-46d0-a118-e2fed1b1c2b1_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011523-c567aff9-c2cb-455d-842b_60504ca9-c3de-46d0-a118-e2fed1b1c2b1_poly_0_">
<gml:posList>1183.212 340896.142 60.6 1186.493 340895.32 60.6 1186.686 340896.816 59.8 1183.402 340897.639 59.8 1175.249 340899.68 59.8 1175.059 340898.184 60.6 1183.212 340896.142 60.6</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:CompositeSurface>
</gml:exterior>
</gml:Solid>
</bldg:lod2Solid>
</bldg:Building>
</core:cityObjectMember>
</core:CityModel>
