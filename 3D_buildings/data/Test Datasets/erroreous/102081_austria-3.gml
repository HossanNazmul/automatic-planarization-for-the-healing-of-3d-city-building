<?xml version="1.0" encoding="UTF-8"?>
<core:CityModel xmlns:bldg="http://www.opengis.net/citygml/building/1.0" xmlns:luse="http://www.opengis.net/citygml/landuse/1.0" xmlns:app="http://www.opengis.net/citygml/appearance/1.0" xmlns:xAL="urn:oasis:names:tc:ciq:xsdschema:xAL:2.0" xmlns:base="http://www.citygml.org/citygml/profiles/base/1.0" xmlns:frn="http://www.opengis.net/citygml/cityfurniture/1.0" xmlns:smil20lang="http://www.w3.org/2001/SMIL20/Language" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:tex="http://www.opengis.net/citygml/texturedsurface/1.0" xmlns:wtr="http://www.opengis.net/citygml/waterbody/1.0" xmlns:smil20="http://www.w3.org/2001/SMIL20/" xmlns:grp="http://www.opengis.net/citygml/cityobjectgroup/1.0" xmlns:core="http://www.opengis.net/citygml/1.0" xmlns:veg="http://www.opengis.net/citygml/vegetation/1.0" xmlns:sch="http://www.ascc.net/xml/schematron" xmlns:gen="http://www.opengis.net/citygml/generics/1.0" xmlns:dem="http://www.opengis.net/citygml/relief/1.0" xmlns:gml="http://www.opengis.net/gml" xmlns:tran="http://www.opengis.net/citygml/transportation/1.0">
<gml:boundedBy>
<gml:Envelope srsName="EPSG:31256" srsDimension="3">
<gml:lowerCorner>1155.83 340683.01 38.55</gml:lowerCorner>
<gml:upperCorner>1167.13 340705.66 58.03</gml:upperCorner>
</gml:Envelope>
</gml:boundedBy>
<core:cityObjectMember>
<bldg:Building gml:id="UUID_LOD2_011517-67937c07-2de1-4ab4-a5e8">
<gml:name>011517</gml:name>
<core:creationDate>2016-09-29</core:creationDate>
<gen:stringAttribute name="Blattnummer">
<gen:value>102081</gen:value>
</gen:stringAttribute>
<gen:stringAttribute name="FP_ID">
<gen:value>9967</gen:value>
</gen:stringAttribute>
<gen:stringAttribute name="HoeheDach">
<gen:value>58</gen:value>
</gen:stringAttribute>
<gen:stringAttribute name="HoeheGrund">
<gen:value>39</gen:value>
</gen:stringAttribute>
<gen:stringAttribute name="NiedrigsteTraufeDesGebaeudes">
<gen:value>53</gen:value>
</gen:stringAttribute>
<bldg:roofType>PULTDACH</bldg:roofType>
<bldg:measuredHeight uom="urn:ogc:def:uom:UCUM:m">19.48</bldg:measuredHeight>
<bldg:lod2Solid>
<gml:Solid gml:id="UUID_28b76369-5196-43f5-95e6-6d268f90771d" srsName="EPSG:31256" srsDimension="3">
<gml:exterior>
<gml:CompositeSurface gml:id="UUID_4003edd8-fa89-499c-b8e8-9169b322a178">
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011517-67937c07-2de1-4ab4-a5e8_a3ac8f50-8092-4b11-90e8-e25e8ab36f14_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011517-67937c07-2de1-4ab4-a5e8_a3ac8f50-8092-4b11-90e8-e25e8ab36f14_poly_0_">
<gml:posList>1155.848 340695.86 55.69 1155.862 340693.68 55.69 1155.88 340690.94 55.69 1155.96 340690.93 55.69 1155.959 340690.731 55.69 1158.39 340690.534 55.69 1158.362 340698.099 55.69 1155.833 340698.206 55.69 1155.848 340695.86 55.69</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011517-67937c07-2de1-4ab4-a5e8_16b9e9fa-7e78-48f1-b63c-9d59a70b9d05_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011517-67937c07-2de1-4ab4-a5e8_16b9e9fa-7e78-48f1-b63c-9d59a70b9d05_poly_0_">
<gml:posList>1162.891 340683.29 56.53 1164.756 340683.131 54.83 1164.738 340688.08 54.83 1162.872 340688.238 56.53 1162.891 340683.29 56.53</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011517-67937c07-2de1-4ab4-a5e8_fbfc4e65-a60e-4301-9933-200671d0c28c_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011517-67937c07-2de1-4ab4-a5e8_fbfc4e65-a60e-4301-9933-200671d0c28c_poly_0_">
<gml:posList>1164.756 340683.131 53.14 1166.18 340683.01 53.14 1166.323 340686.369 53.14 1166.39 340687.94 53.14 1165.918 340687.98 53.14 1164.738 340688.08 53.14 1164.756 340683.131 53.14</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011517-67937c07-2de1-4ab4-a5e8_e72da564-8347-43c3-b0a2-27a60882f7a0_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011517-67937c07-2de1-4ab4-a5e8_e72da564-8347-43c3-b0a2-27a60882f7a0_poly_0_">
<gml:posList>1155.83 340698.65 38.55 1155.83 340698.65 52.984 1155.9 340698.65 53.058 1155.9 340698.65 38.55 1155.83 340698.65 38.55</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011517-67937c07-2de1-4ab4-a5e8_d6c06d93-3277-4ad2-bdf6-6a938d0de418_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011517-67937c07-2de1-4ab4-a5e8_d6c06d93-3277-4ad2-bdf6-6a938d0de418_poly_0_">
<gml:posList>1155.9 340698.65 38.55 1155.9 340698.65 53.058 1155.85 340705.66 53.032 1155.85 340705.66 38.55 1155.9 340698.65 38.55</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011517-67937c07-2de1-4ab4-a5e8_4ef578a7-5b67-418a-afff-1b91bb42822d_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011517-67937c07-2de1-4ab4-a5e8_4ef578a7-5b67-418a-afff-1b91bb42822d_poly_0_">
<gml:posList>1155.833 340698.206 52.985 1155.833 340698.206 55.69 1158.362 340698.099 55.69 1155.833 340698.206 52.985</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011517-67937c07-2de1-4ab4-a5e8_a5c21e33-50db-4bf2-998d-3db006081ddd_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011517-67937c07-2de1-4ab4-a5e8_a5c21e33-50db-4bf2-998d-3db006081ddd_poly_0_">
<gml:posList>1158.39 340690.534 55.69 1155.959 340690.731 55.69 1155.959 340690.731 53.091 1158.39 340690.534 55.69</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011517-67937c07-2de1-4ab4-a5e8_07233457-4ceb-4730-8b30-6004d4779d1e_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011517-67937c07-2de1-4ab4-a5e8_07233457-4ceb-4730-8b30-6004d4779d1e_poly_0_">
<gml:posList>1155.96 340690.93 38.55 1155.96 340690.93 55.69 1155.88 340690.94 55.69 1155.88 340690.94 38.55 1155.96 340690.93 38.55</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011517-67937c07-2de1-4ab4-a5e8_e13272ee-5f4e-4df1-a31e-b93ea0a70a9b_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011517-67937c07-2de1-4ab4-a5e8_e13272ee-5f4e-4df1-a31e-b93ea0a70a9b_poly_0_">
<gml:posList>1162.77 340697.923 56.53 1162.77 340697.923 58.03 1159.147 340698.076 58.03 1159.147 340698.076 56.53 1162.77 340697.923 56.53</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011517-67937c07-2de1-4ab4-a5e8_bff439e1-d68e-4d9c-9fc8-e73002516852_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011517-67937c07-2de1-4ab4-a5e8_bff439e1-d68e-4d9c-9fc8-e73002516852_poly_0_">
<gml:posList>1159.147 340698.076 56.53 1159.147 340698.076 58.03 1159.12 340705.519 58.03 1159.12 340705.519 56.53 1159.147 340698.076 56.53</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011517-67937c07-2de1-4ab4-a5e8_9f872656-aa53-44ef-9e65-840bada9b9df_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011517-67937c07-2de1-4ab4-a5e8_9f872656-aa53-44ef-9e65-840bada9b9df_poly_0_">
<gml:posList>1164.738 340688.08 53.14 1164.738 340688.08 54.83 1164.756 340683.131 54.83 1164.756 340683.131 53.14 1164.738 340688.08 53.14</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011517-67937c07-2de1-4ab4-a5e8_607a37e9-9de3-4677-9f73-bbe147ae3edf_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011517-67937c07-2de1-4ab4-a5e8_607a37e9-9de3-4677-9f73-bbe147ae3edf_poly_0_">
<gml:posList>1164.92 340705.27 38.55 1164.92 340705.27 58.03 1167.13 340705.18 58.03 1167.13 340705.18 38.55 1164.92 340705.27 38.55</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011517-67937c07-2de1-4ab4-a5e8_63877fe1-6610-4522-8de0-11557cf45f0d_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011517-67937c07-2de1-4ab4-a5e8_63877fe1-6610-4522-8de0-11557cf45f0d_poly_0_">
<gml:posList>1167.13 340705.18 38.55 1167.13 340705.18 58.03 1167.031 340702.85 58.03 1167.031 340702.85 38.55 1167.13 340705.18 38.55</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011517-67937c07-2de1-4ab4-a5e8_76461096-52ba-4d76-94ac-782a41609179_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011517-67937c07-2de1-4ab4-a5e8_76461096-52ba-4d76-94ac-782a41609179_poly_0_">
<gml:posList>1167.031 340702.85 38.55 1167.031 340702.85 58.03 1166.87 340699.03 58.03 1166.87 340699.03 38.55 1167.031 340702.85 38.55</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011517-67937c07-2de1-4ab4-a5e8_4c973b7b-3d05-4dd4-b841-3e863e226d4c_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011517-67937c07-2de1-4ab4-a5e8_4c973b7b-3d05-4dd4-b841-3e863e226d4c_poly_0_">
<gml:posList>1166.87 340699.03 38.55 1166.87 340699.03 58.03 1165.12 340699.11 58.03 1165.12 340699.11 38.55 1166.87 340699.03 38.55</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011517-67937c07-2de1-4ab4-a5e8_4dfe76b9-3a4d-4c1a-9d11-d8f4e9ab55d2_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011517-67937c07-2de1-4ab4-a5e8_4dfe76b9-3a4d-4c1a-9d11-d8f4e9ab55d2_poly_0_">
<gml:posList>1165.12 340699.11 38.55 1165.12 340699.11 58.03 1165.07 340697.87 58.03 1165.07 340697.87 38.55 1165.12 340699.11 38.55</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011517-67937c07-2de1-4ab4-a5e8_48d3f1fa-24e1-4974-8672-96faedec61bd_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011517-67937c07-2de1-4ab4-a5e8_48d3f1fa-24e1-4974-8672-96faedec61bd_poly_0_">
<gml:posList>1155.833 340698.206 52.985 1158.362 340698.099 55.69 1158.39 340690.534 55.69 1155.959 340690.731 53.091 1155.954 340688.876 53.078 1155.94 340683.88 53.043 1159.201 340683.603 56.53 1159.182 340688.602 56.53 1159.175 340690.457 56.53 1159.147 340698.076 56.53 1159.12 340705.519 56.53 1155.85 340705.66 53.032 1155.9 340698.65 53.058 1155.83 340698.65 52.984 1155.833 340698.206 52.985</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011517-67937c07-2de1-4ab4-a5e8_321f12fe-658d-4104-8ad7-9dc0efde1a98_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011517-67937c07-2de1-4ab4-a5e8_321f12fe-658d-4104-8ad7-9dc0efde1a98_poly_0_">
<gml:posList>1155.833 340698.206 38.55 1155.848 340695.86 38.55 1155.862 340693.68 38.55 1155.88 340690.94 38.55 1155.88 340690.94 55.69 1155.862 340693.68 55.69 1155.848 340695.86 55.69 1155.833 340698.206 55.69 1155.833 340698.206 52.985 1155.83 340698.65 52.984 1155.83 340698.65 38.55 1155.833 340698.206 38.55</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011517-67937c07-2de1-4ab4-a5e8_e98e7a3b-21b1-4274-af1f-666f9461c068_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011517-67937c07-2de1-4ab4-a5e8_e98e7a3b-21b1-4274-af1f-666f9461c068_poly_0_">
<gml:posList>1159.12 340705.519 56.53 1159.12 340705.519 58.03 1162.809 340705.361 58.03 1164.675 340705.281 58.03 1164.92 340705.27 58.03 1164.92 340705.27 38.55 1164.675 340705.281 38.55 1162.809 340705.361 38.55 1159.12 340705.519 38.55 1155.85 340705.66 38.55 1155.85 340705.66 53.032 1159.12 340705.519 56.53</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011517-67937c07-2de1-4ab4-a5e8_44656376-353a-4e21-aa71-2b4f16810492_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011517-67937c07-2de1-4ab4-a5e8_44656376-353a-4e21-aa71-2b4f16810492_poly_0_">
<gml:posList>1159.201 340683.603 38.55 1162.891 340683.29 38.55 1164.756 340683.131 38.55 1166.18 340683.01 38.55 1166.18 340683.01 53.14 1164.756 340683.131 53.14 1164.756 340683.131 54.83 1162.891 340683.29 56.53 1159.201 340683.603 56.53 1155.94 340683.88 53.043 1155.94 340683.88 38.55 1159.201 340683.603 38.55</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011517-67937c07-2de1-4ab4-a5e8_4615b181-b09f-4ca0-8d6a-fbd0d266d790_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011517-67937c07-2de1-4ab4-a5e8_4615b181-b09f-4ca0-8d6a-fbd0d266d790_poly_0_">
<gml:posList>1162.872 340688.238 56.53 1164.738 340688.08 54.83 1164.738 340688.08 53.14 1165.918 340687.98 53.14 1166.39 340687.94 53.14 1166.39 340687.94 38.55 1165.918 340687.98 38.55 1164.738 340688.08 38.55 1162.872 340688.238 38.55 1162.85 340688.24 38.55 1162.85 340688.24 56.53 1162.872 340688.238 56.53</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011517-67937c07-2de1-4ab4-a5e8_e8926e13-64de-4a6c-937f-671a88791f03_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011517-67937c07-2de1-4ab4-a5e8_e8926e13-64de-4a6c-937f-671a88791f03_poly_0_">
<gml:posList>1162.77 340697.923 58.03 1162.77 340697.923 56.53 1162.815 340692.478 56.53 1162.834 340690.146 56.53 1162.85 340688.29 56.53 1162.85 340688.24 56.53 1162.85 340688.24 38.55 1162.85 340688.29 38.55 1162.834 340690.146 38.55 1162.815 340692.478 38.55 1162.77 340697.923 38.55 1162.77 340697.97 38.55 1162.77 340697.97 58.03 1162.77 340697.923 58.03</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011517-67937c07-2de1-4ab4-a5e8_ee9f8d3f-7816-4534-a308-9bc48b122b27_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011517-67937c07-2de1-4ab4-a5e8_ee9f8d3f-7816-4534-a308-9bc48b122b27_poly_0_">
<gml:posList>1167.031 340702.85 38.55 1166.87 340699.03 38.55 1165.12 340699.11 38.55 1165.07 340697.87 38.55 1164.702 340697.886 38.55 1164.434 340697.898 38.55 1162.837 340697.967 38.55 1162.77 340697.97 38.55 1162.77 340697.923 38.55 1162.815 340692.478 38.55 1162.834 340690.146 38.55 1162.85 340688.29 38.55 1162.85 340688.24 38.55 1162.872 340688.238 38.55 1164.738 340688.08 38.55 1165.918 340687.98 38.55 1166.39 340687.94 38.55 1166.323 340686.369 38.55 1166.18 340683.01 38.55 1164.756 340683.131 38.55 1162.891 340683.29 38.55 1159.201 340683.603 38.55 1155.94 340683.88 38.55 1155.954 340688.876 38.55 1155.959 340690.731 38.55 1155.96 340690.93 38.55 1155.88 340690.94 38.55 1155.862 340693.68 38.55 1155.848 340695.86 38.55 1155.833 340698.206 38.55 1155.83 340698.65 38.55 1155.9 340698.65 38.55 1155.85 340705.66 38.55 1159.12 340705.519 38.55 1162.809 340705.361 38.55 1164.675 340705.281 38.55 1164.92 340705.27 38.55 1167.13 340705.18 38.55 1167.031 340702.85 38.55</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011517-67937c07-2de1-4ab4-a5e8_b64467ac-033f-4934-b21a-85da20355361_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011517-67937c07-2de1-4ab4-a5e8_b64467ac-033f-4934-b21a-85da20355361_poly_0_">
<gml:posList>1159.147 340698.076 58.03 1162.77 340697.923 58.03 1162.77 340697.97 58.03 1162.837 340697.967 58.03 1164.434 340697.898 58.03 1164.702 340697.886 58.03 1165.07 340697.87 58.03 1165.12 340699.11 58.03 1166.87 340699.03 58.03 1167.031 340702.85 58.03 1167.13 340705.18 58.03 1164.92 340705.27 58.03 1164.675 340705.281 58.03 1162.809 340705.361 58.03 1159.12 340705.519 58.03 1159.147 340698.076 58.03</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011517-67937c07-2de1-4ab4-a5e8_fddfdac2-f5ee-480a-ac18-4f5ac27b526d_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011517-67937c07-2de1-4ab4-a5e8_fddfdac2-f5ee-480a-ac18-4f5ac27b526d_poly_0_">
<gml:posList>1159.175 340690.457 56.53 1159.182 340688.602 56.53 1159.201 340683.603 56.53 1162.891 340683.29 56.53 1162.872 340688.238 56.53 1162.85 340688.24 56.53 1162.85 340688.29 56.53 1162.834 340690.146 56.53 1162.815 340692.478 56.53 1162.77 340697.923 56.53 1159.147 340698.076 56.53 1159.175 340690.457 56.53</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011517-67937c07-2de1-4ab4-a5e8_1f9f0cc9-ae14-443f-8b2a-a6ccde7c1a19_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011517-67937c07-2de1-4ab4-a5e8_1f9f0cc9-ae14-443f-8b2a-a6ccde7c1a19_poly_0_">
<gml:posList>1155.94 340683.88 53.043 1155.954 340688.876 53.078 1155.959 340690.731 53.091 1155.959 340690.731 55.69 1155.96 340690.93 55.69 1155.96 340690.93 38.55 1155.959 340690.731 38.55 1155.954 340688.876 38.55 1155.94 340683.88 38.55 1155.94 340683.88 53.043</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011517-67937c07-2de1-4ab4-a5e8_9ab4492c-6997-4035-abc6-d307c9bb9acd_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011517-67937c07-2de1-4ab4-a5e8_9ab4492c-6997-4035-abc6-d307c9bb9acd_poly_0_">
<gml:posList>1166.18 340683.01 38.55 1166.323 340686.369 38.55 1166.39 340687.94 38.55 1166.39 340687.94 53.14 1166.323 340686.369 53.14 1166.18 340683.01 53.14 1166.18 340683.01 38.55</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011517-67937c07-2de1-4ab4-a5e8_5a44f7f9-1673-48e4-ab94-4e2d534d967f_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011517-67937c07-2de1-4ab4-a5e8_5a44f7f9-1673-48e4-ab94-4e2d534d967f_poly_0_">
<gml:posList>1164.702 340697.886 38.55 1165.07 340697.87 38.55 1165.07 340697.87 58.03 1164.702 340697.886 58.03 1164.434 340697.898 58.03 1164.434 340697.898 38.55 1164.702 340697.886 38.55</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_LOD2_011517-67937c07-2de1-4ab4-a5e8_b86a0cda-51f4-41ee-a527-fe018460c267_poly">
<gml:exterior>
<gml:LinearRing gml:id="UUID_LOD2_011517-67937c07-2de1-4ab4-a5e8_b86a0cda-51f4-41ee-a527-fe018460c267_poly_0_">
<gml:posList>1162.837 340697.967 38.55 1164.434 340697.898 38.55 1164.434 340697.898 58.03 1162.837 340697.967 58.03 1162.77 340697.97 58.03 1162.77 340697.97 38.55 1162.837 340697.967 38.55</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:CompositeSurface>
</gml:exterior>
</gml:Solid>
</bldg:lod2Solid>
</bldg:Building>
</core:cityObjectMember>
</core:CityModel>
