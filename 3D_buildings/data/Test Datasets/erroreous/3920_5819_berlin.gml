<?xml version="1.0" encoding="UTF-8"?>
<CityModel xmlns:bldg="http://www.opengis.net/citygml/building/1.0" xmlns:app="http://www.opengis.net/citygml/appearance/1.0" xmlns:smil20="http://www.w3.org/2001/SMIL20/" xmlns:grp="http://www.opengis.net/citygml/cityobjectgroup/1.0" xmlns:base="http://www.citygml.org/citygml/profiles/base/1.0" xmlns:frn="http://www.opengis.net/citygml/cityfurniture/1.0" xmlns:smil20lang="http://www.w3.org/2001/SMIL20/Language" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:wtr="http://www.opengis.net/citygml/waterbody/1.0" xmlns:tex="http://www.opengis.net/citygml/texturedsurface/1.0" xmlns:veg="http://www.opengis.net/citygml/vegetation/1.0" xmlns:sch="http://www.ascc.net/xml/schematron" xmlns:gen="http://www.opengis.net/citygml/generics/1.0" xmlns:dem="http://www.opengis.net/citygml/relief/1.0" xmlns:gml="http://www.opengis.net/gml" xmlns:tran="http://www.opengis.net/citygml/transportation/1.0" xmlns:xAL="urn:oasis:names:tc:ciq:xsdschema:xAL:2.0" xmlns:luse="http://www.opengis.net/citygml/landuse/1.0" xmlns="http://www.opengis.net/citygml/1.0">
	<gml:boundedBy>
		<gml:Envelope srsName="EPSG:25833" srsDimension="3">
			<gml:lowerCorner>391950.534 5819809.911 34.536</gml:lowerCorner>
			<gml:upperCorner>392616.622 5820075.986 117.04</gml:upperCorner>
		</gml:Envelope>
	</gml:boundedBy>
	<cityObjectMember>
		<bldg:Building gml:id="DEB_5d0c2345-5938-4d">
			<gml:name>DEB_5d0c2345-5938-4d</gml:name>
			<bldg:roofType/>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-69df7d50-b68b-4749-ac18-154fe20cd4e0">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392562.805 5819916.942 54.14 392562.805 5819916.942 62.3 392571.185 5819910.781 62.3 392571.185 5819910.781 59.84 392571.185 5819910.781 54.14 392562.805 5819916.942 54.14</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-f2ec3588-8fac-49de-9c42-a56160ccdc63">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392539.719 5819968.621 54.14 392539.719 5819968.621 69.43 392562.805 5819916.942 69.43 392562.805 5819916.942 62.3 392562.805 5819916.942 54.14 392539.719 5819968.621 54.14</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="fme-gen-1129ae13-6071-4395-9111-d2f2a8acc3d2">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392514.027 5819899.444 57.61 392523.796 5819905.77 57.61 392512.397 5819921.814 57.61 392510.123 5819925.604 57.61 392501.242 5819919.075 57.61 392514.027 5819899.444 57.61</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="fme-gen-38739ee2-d66f-440a-b41d-f161b73fc874">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392483.928 5819950.279 57.61 392492.442 5819955.348 57.61 392483.517 5819972.999 57.61 392474.329 5819968.598 57.61 392483.928 5819950.279 57.61</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="fme-gen-4b88980c-0279-4a35-abde-8ae4cda6f93b">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392489.718 5819939.23 57.61 392496.318 5819926.637 57.61 392497.182 5819925.309 57.61 392507.007 5819930.796 57.61 392496.657 5819948.046 57.61 392487.601 5819943.271 57.61 392489.718 5819939.23 57.61</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="fme-gen-5d840bee-96e0-40a0-9afe-fd60556df817">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392470.868 5819975.202 57.61 392479.742 5819980.463 57.61 392474.484 5819990.861 57.61 392471.031 5819999.434 57.61 392460.743 5819994.523 57.61 392470.868 5819975.202 57.61</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-abf43845-5733-4016-ac9e-330701fc98ad">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392492.209 5820036.698 34.536 392492.209 5820036.698 60.59 392516.436 5820054.401 60.59 392516.436 5820054.401 34.536 392492.209 5820036.698 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-0abab21b-05f4-4d48-ac78-56350a9e1d8d">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392490.68 5820035.642 41.923 392490.68 5820035.642 54.48 392490.68 5820035.642 60.59 392492.182 5820036.737 60.59 392492.182 5820036.737 41.923 392490.68 5820035.642 41.923</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-12f0fd20-e19b-4336-a008-9201c9b786a4">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392464.232 5820016.316 54.52 392464.232 5820016.316 69.43 392487.468 5820033.296 60.59 392464.232 5820016.316 54.52</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-ad80a026-1d27-467f-8cd7-4d0fae399eff">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392487.468 5820033.296 69.43 392487.468 5820033.296 60.59 392464.232 5820016.316 69.43 392487.468 5820033.296 69.43</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_144a4014-9041-46d7-a0b6-e979c3e07597">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392441.919 5820000.012 54.48 392441.919 5820000.012 54.52 392464.232 5820016.316 54.48 392441.919 5820000.012 54.48</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_5c0ffe92-2828-4e9a-96fd-db60d4419c4d">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392487.468 5820033.296 54.48 392464.232 5820016.316 54.48 392441.919 5820000.012 54.52 392487.468 5820033.296 54.48</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_7d3a7443-10d9-4cd8-a1e5-db33dc2481e6">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392490.68 5820035.642 54.48 392487.468 5820033.296 54.48 392441.919 5820000.012 54.52 392490.68 5820035.642 54.48</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_cefbdb57-f7ec-4e44-b656-ce8d1590e2b1">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392464.232 5820016.316 54.52 392490.68 5820035.642 54.48 392441.919 5820000.012 54.52 392464.232 5820016.316 54.52</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-c5a040ab-d2f3-4553-a5ad-c3cf699c5646">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392490.68 5820035.642 60.59 392490.68 5820035.642 54.48 392464.232 5820016.316 54.52 392490.68 5820035.642 60.59</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-907078f1-f5b9-4bb1-b842-81c1f709dce0">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392487.468 5820033.296 60.59 392490.68 5820035.642 60.59 392464.232 5820016.316 54.52 392487.468 5820033.296 60.59</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="fme-gen-cc40c9b8-2c12-44cc-a34b-d18fe65895ba">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392450.695 5819985.181 58.162 392453.645 5819980.363 58.162 392456.298 5819975.855 58.162 392461.593 5819966.303 58.162 392465.983 5819957.73 58.162 392466.306 5819957.07 58.162 392471.011 5819946.981 58.162 392474.918 5819937.775 58.162 392489.718 5819939.23 58.162 392487.601 5819943.271 58.162 392483.928 5819950.279 58.162 392474.329 5819968.598 58.162 392470.868 5819975.202 58.162 392460.743 5819994.523 58.162 392456.929 5820001.802 58.162 392445.077 5819993.831 58.162 392450.695 5819985.181 58.162</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="fme-gen-15ecf54e-703d-4f70-81d1-36b28206d41f">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392484.88 5819909.794 58.14 392488.512 5819898.097 58.14 392493.752 5819873.273 58.14 392512.397 5819877.757 58.14 392519.434 5819891.141 58.14 392514.027 5819899.444 58.14 392501.242 5819919.075 58.14 392497.182 5819925.309 58.14 392496.318 5819926.637 58.14 392483.626 5819913.782 58.14 392484.88 5819909.794 58.14</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-8ac06588-012a-4df0-b3e7-cd5f26626efa">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392453.645 5819980.363 34.536 392453.645 5819980.363 58.162 392450.695 5819985.181 58.162 392450.695 5819985.181 34.536 392453.645 5819980.363 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-0a74a43d-6930-43c6-9939-791a5c1b45fc">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392456.298 5819975.855 34.536 392456.298 5819975.855 58.162 392453.645 5819980.363 58.162 392453.645 5819980.363 34.536 392456.298 5819975.855 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-5456f992-e291-456d-82d3-99256787ad04">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392444.746 5819994.342 34.536 392444.746 5819994.342 54.52 392442.66 5819997.395 54.52 392442.66 5819997.395 34.536 392444.746 5819994.342 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-d035eaf9-fb7e-43d3-872b-2926fba14dbf">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392450.695 5819985.181 58.162 392445.077 5819993.831 58.162 392450.695 5819985.181 34.536 392450.695 5819985.181 58.162</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-76f7fa5b-f9e2-4de9-94f7-d273398cb7b9">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392445.077 5819993.831 34.536 392450.695 5819985.181 34.536 392445.077 5819993.831 58.162 392445.077 5819993.831 54.52 392445.077 5819993.831 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-f7a52773-85af-482f-a74c-57a76abe4a6a">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392444.746 5819994.342 54.52 392445.077 5819993.831 34.536 392445.077 5819993.831 54.52 392444.746 5819994.342 54.52</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-0bccca2c-b2b2-4ef0-8128-67b6996aa482">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392444.746 5819994.342 34.536 392445.077 5819993.831 34.536 392444.746 5819994.342 54.52 392444.746 5819994.342 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-cf73ebb3-98e5-430e-ac0b-1d66b243f470">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392552.433 5820001.201 34.536 392552.433 5820001.201 58.74 392553.917 5819998.837 58.74 392553.917 5819998.837 34.536 392552.433 5820001.201 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-b8eebe29-ca69-4f2d-97ae-22efb369fdac">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392562.799 5819984.196 34.536 392562.799 5819984.196 58.74 392563.493 5819982.997 58.74 392563.493 5819982.997 34.536 392562.799 5819984.196 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-1ee2094e-c20c-4e32-bfc2-1293c8993982">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392553.917 5819998.837 34.536 392553.917 5819998.837 58.74 392560.643 5819987.831 58.74 392560.643 5819987.831 34.536 392553.917 5819998.837 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-1b891cfb-fa42-4a6e-876e-d7ca7dcd1193">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392560.643 5819987.831 34.536 392560.643 5819987.831 58.74 392561.537 5819986.329 58.74 392561.537 5819986.329 34.536 392560.643 5819987.831 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-970258a4-ded2-48ba-8918-231f38d0fb72">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392561.537 5819986.329 34.536 392561.537 5819986.329 58.74 392562.799 5819984.196 58.74 392562.799 5819984.196 34.536 392561.537 5819986.329 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="fme-gen-8646df09-d450-471f-ba17-891b12345b6e">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392487.601 5819943.271 54.53 392496.657 5819948.046 54.53 392493.335 5819953.584 54.53 392492.442 5819955.348 54.53 392483.928 5819950.279 54.53 392487.601 5819943.271 54.53</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="fme-gen-6cb9ea2a-4ba2-4d04-adaf-da8b78d121e7">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392441.919 5820000.012 54.48 392464.232 5820016.316 54.48 392487.468 5820033.296 54.48 392490.68 5820035.642 54.48 392481.232 5820043.661 54.48 392444.783 5820005.652 54.48 392442.567 5820006.578 54.48 392441.974 5820006.013 54.48 392439.194 5820003.366 54.48 392438.807 5820002.993 54.48 392441.919 5820000.012 54.48</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="UUID_b24feca0-3470-4646-9818-49ec76b036e0">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392404.953 5820043.546 54.48 392406.272 5820042.069 54.48 392406.654 5820041.659 54.48 392401.682 5820047.256 54.48 392404.953 5820043.546 54.48</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="fme-gen-a33b4e6d-e23a-47d8-a41c-8fe9c8643c94">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392501.242 5819919.075 54.495 392510.123 5819925.604 54.495 392507.007 5819930.796 54.495 392497.182 5819925.309 54.495 392501.242 5819919.075 54.495</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="fme-gen-38b5c55f-35e2-4ee5-be87-fa901037d018">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392443.844 5819998.168 54.52 392442.66 5819997.395 54.52 392444.746 5819994.342 54.52 392445.077 5819993.831 54.52 392456.929 5820001.802 54.52 392460.743 5819994.523 54.52 392471.031 5819999.434 54.52 392464.232 5820016.316 54.52 392441.919 5820000.012 54.52 392443.844 5819998.168 54.52</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="fme-gen-66200fb0-0504-443e-ab22-a061260c4a9d">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392475.36 5819936.734 54.14 392476.394 5819934.144 54.14 392477.067 5819932.424 54.14 392486.192 5819934.627 54.14 392486.285 5819934.276 54.14 392488.228 5819927.049 54.14 392488.592 5819925.712 54.14 392482.063 5819918.604 54.14 392483.038 5819915.652 54.14 392483.626 5819913.782 54.14 392496.318 5819926.637 54.14 392489.718 5819939.23 54.14 392474.918 5819937.775 54.14 392475.36 5819936.734 54.14</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="fme-gen-09f940a1-4bd1-4f2d-a371-c5bed5a4d1fd">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392474.329 5819968.598 54.399 392483.517 5819972.999 54.399 392479.742 5819980.463 54.399 392470.868 5819975.202 54.399 392474.329 5819968.598 54.399</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="fme-gen-d1e2e3c9-73fe-44d7-ac02-97d533f08d1d">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392512.608 5820007.381 54.14 392524.681 5819991.284 54.14 392539.719 5819968.621 54.14 392562.805 5819916.942 54.14 392571.185 5819910.781 54.14 392590.904 5819925.143 54.14 392600.64 5819902.407 54.14 392613.63 5819872.075 54.14 392613.843 5819872.194 54.14 392616.622 5819873.742 54.14 392616.276 5819874.67 54.14 392613.113 5819882.922 54.14 392611.465 5819887.055 54.14 392604.696 5819903 54.14 392598.03 5819917.352 54.14 392596.237 5819921.045 54.14 392593.331 5819926.91 54.14 392590.135 5819933.36 54.14 392588.118 5819932.238 54.14 392585.037 5819937.731 54.14 392587.293 5819938.966 54.14 392586.252 5819940.954 54.14 392566.348 5819930.051 54.14 392554.206 5819958.062 54.14 392537.096 5819987.452 54.14 392514.053 5820017.256 54.14 392523.822 5820022.937 54.14 392520.986 5820027.068 54.14 392509.009 5820018.345 54.14 392499.053 5820022.207 54.14 392512.608 5820007.381 54.14</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="fme-gen-f9098a9c-139e-4630-8c53-e60930273e5b">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392498.114 5819852.61 54.14 392509.102 5819854.848 54.14 392515.125 5819856.064 54.14 392514.485 5819859.216 54.14 392513.15 5819865.78 54.14 392522.395 5819867.674 54.14 392522.747 5819866.188 54.14 392524.391 5819866.525 54.14 392523.162 5819869.578 54.14 392532.467 5819877.561 54.14 392519.434 5819891.141 54.14 392512.397 5819877.757 54.14 392493.752 5819873.273 54.14 392498.114 5819852.61 54.14</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="fme-gen-57693064-714c-4503-8bbe-625b9f3cd427">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392524.727 5819858.331 54.14 392525.511 5819855.995 54.14 392528.411 5819856.543 54.14 392526.681 5819860.841 54.14 392525.229 5819860.031 54.14 392524.325 5819859.768 54.14 392524.727 5819858.331 54.14</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-76c30ec7-f140-444a-944b-0532324a9ecb">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392519.434 5819891.141 54.14 392519.434 5819891.141 58.14 392512.397 5819877.757 58.14 392512.397 5819877.757 54.14 392519.434 5819891.141 54.14</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-6e6ac913-4852-4ee6-a31b-175109992490">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392418.286 5820029.701 34.536 392418.286 5820029.701 66.13 392417.301 5820030.647 66.13 392417.301 5820030.647 34.536 392418.286 5820029.701 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-a13ae94d-4d48-4919-8e39-2dfd63fcbc95">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392406.272 5820042.069 34.536 392406.272 5820042.069 54.48 392404.953 5820043.546 54.48 392404.953 5820043.546 34.536 392406.272 5820042.069 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-ff1b6cd7-507c-4f5d-9e0f-35f4a155b10f">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392417.301 5820030.647 34.536 392417.301 5820030.647 66.13 392411.687 5820036.261 66.13 392411.687 5820036.261 34.536 392417.301 5820030.647 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-a378d0e1-1d3f-42e5-80f8-66be2faf8dfe">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392401.682 5820047.256 54.48 392406.654 5820041.659 54.48 392401.692 5820047.263 66.13 392401.682 5820047.256 54.48</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-00abfe19-3f04-4956-a66e-2d5f84d3f284">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392411.687 5820036.261 66.13 392406.654 5820041.659 66.13 392411.687 5820036.261 34.536 392411.687 5820036.261 66.13</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-42364db4-d13a-4c1e-bc32-d6899d2d902e">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392406.654 5820041.659 34.536 392411.687 5820036.261 34.536 392406.654 5820041.659 66.13 392406.654 5820041.659 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-7b19596a-6835-4a67-8f1b-c2f073d17e2d">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392406.272 5820042.069 54.48 392406.654 5820041.659 34.536 392406.654 5820041.659 54.48 392406.272 5820042.069 54.48</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-a758ff47-ba94-4912-9190-88ccb727547e">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392406.272 5820042.068 34.536 392406.654 5820041.659 34.536 392406.272 5820042.069 54.48 392406.272 5820042.068 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-6f739ff6-b44d-4cd0-a006-838f6dea7ec5">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392404.953 5820043.546 54.48 392401.682 5820047.256 54.48 392404.953 5820043.546 34.536 392404.953 5820043.546 54.48</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-2d4ea400-c50e-40fb-b9d1-50026a345d59">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392401.692 5820047.263 34.536 392404.953 5820043.546 34.536 392401.682 5820047.256 54.48 392401.692 5820047.263 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-b1cd25b3-ea38-428d-b86f-d4d9ef59a7ec">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392406.654 5820041.659 66.13 392401.692 5820047.263 66.13 392406.654 5820041.659 54.48 392406.654 5820041.659 66.13</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-25c53324-b0b9-48f3-861f-786b5feac395">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392588.118 5819932.238 34.536 392588.118 5819932.238 54.14 392590.135 5819933.36 54.14 392590.135 5819933.36 34.536 392588.118 5819932.238 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="fme-gen-a831e2c9-f60f-45a5-b195-3327e43ec735">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392499.053 5820022.207 60.59 392509.009 5820018.345 60.59 392520.986 5820027.068 60.59 392523.822 5820022.937 60.59 392525.111 5820021.06 60.59 392534.588 5820027.961 60.59 392533.84 5820029.045 60.59 392533.211 5820029.935 60.59 392516.818 5820053.844 60.59 392516.436 5820054.401 60.59 392492.209 5820036.698 60.59 392492.182 5820036.737 60.59 392490.68 5820035.642 60.59 392487.468 5820033.296 60.59 392499.053 5820022.207 60.59</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-0339d75f-fcc1-41e2-97b2-cc492b34834b">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392514.485 5819859.216 34.536 392514.485 5819859.216 54.14 392515.125 5819856.064 54.14 392515.125 5819856.064 34.536 392514.485 5819859.216 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-ffdbfe09-55e8-4867-b1d3-f4c0d716e385">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392513.15 5819865.78 34.536 392513.15 5819865.78 54.14 392514.485 5819859.216 54.14 392514.485 5819859.216 34.536 392513.15 5819865.78 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-cb25829b-d513-480e-ab75-888fdd89e2d0">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392489.718 5819939.23 54.14 392489.718 5819939.23 57.61 392489.718 5819939.23 58.162 392474.918 5819937.775 58.162 392474.918 5819937.775 54.14 392489.718 5819939.23 54.14</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-f4cb17a4-0bbd-46e4-a68e-bb55e9917de9">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392537.096 5819987.452 54.14 392537.096 5819987.452 58.74 392514.053 5820017.256 58.74 392514.053 5820017.256 54.14 392537.096 5819987.452 54.14</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-b0e5efbb-47ee-4826-a67b-d203f1cb313f">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392512.397 5819877.757 54.14 392512.397 5819877.757 58.14 392493.752 5819873.273 58.14 392493.752 5819873.273 54.14 392512.397 5819877.757 54.14</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-1521e303-1c2b-4e64-8685-39f8e11a6fb6">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392585.037 5819937.731 34.536 392585.037 5819937.731 54.14 392588.118 5819932.238 54.14 392588.118 5819932.238 34.536 392585.037 5819937.731 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-220383e5-4c22-4ffc-9c7f-0f58a94880a9">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392613.63 5819872.075 54.14 392600.64 5819902.407 54.14 392613.63 5819872.075 59.84 392613.63 5819872.075 54.14</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-c3b1f062-6659-4e4b-8504-b522f41eb8e5">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392600.64 5819902.407 59.84 392613.63 5819872.075 59.84 392600.64 5819902.407 54.14 392600.64 5819902.407 59.84</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-3d3efc16-5161-4f96-bea9-04d8e4b1c628">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392590.904 5819925.143 54.14 392600.64 5819902.407 59.84 392600.64 5819902.407 54.14 392590.904 5819925.143 54.14</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-76147049-fbfa-45cf-912a-2eeda157bab5">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392590.904 5819925.143 59.84 392600.64 5819902.407 59.84 392590.904 5819925.143 54.14 392590.904 5819925.143 59.84</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-15a669c7-e335-42a1-9245-1d979e545c68">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392524.727 5819858.331 34.536 392524.727 5819858.331 54.14 392524.325 5819859.768 54.14 392524.325 5819859.768 34.536 392524.727 5819858.331 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-dd5b2112-dff2-4323-8aa3-d15650bd09d1">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392527.38 5819850.621 34.536 392527.38 5819850.621 41.923 392526.847 5819852.011 41.923 392526.847 5819852.011 34.536 392527.38 5819850.621 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-65c8e44e-ecdf-42d2-a8d8-02748ff94339">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392525.511 5819855.995 54.14 392524.727 5819858.331 54.14 392525.511 5819855.995 41.923 392525.511 5819855.995 54.14</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-55715cb0-26c1-4c51-8b36-fc6185778e01">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392525.511 5819855.995 34.536 392525.511 5819855.995 41.923 392524.727 5819858.331 54.14 392525.511 5819855.995 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-f207fe95-e7f1-4b57-af0e-14a5bd8dcbf6">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392524.727 5819858.331 34.536 392525.511 5819855.995 34.536 392524.727 5819858.331 54.14 392524.727 5819858.331 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-bb9fe533-6c21-49fd-a537-e2bd027df320">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392526.847 5819852.011 41.923 392525.511 5819855.995 41.923 392526.847 5819852.011 34.536 392526.847 5819852.011 41.923</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-77b3a36a-8c1d-4503-bf9f-a7048774f545">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392525.511 5819855.995 34.536 392526.847 5819852.011 34.536 392525.511 5819855.995 41.923 392525.511 5819855.995 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="fme-gen-653d93e0-069a-4e29-a485-ac196e3ea7df">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392471.031 5819999.434 69.43 392474.484 5819990.861 69.43 392479.742 5819980.463 69.43 392483.517 5819972.999 69.43 392492.442 5819955.348 69.43 392493.335 5819953.584 69.43 392496.657 5819948.046 69.43 392507.007 5819930.796 69.43 392510.123 5819925.604 69.43 392512.397 5819921.814 69.43 392523.796 5819905.77 69.43 392529.552 5819897.668 69.43 392562.805 5819916.942 69.43 392539.719 5819968.621 69.43 392524.681 5819991.284 69.43 392512.608 5820007.381 69.43 392499.053 5820022.207 69.43 392487.468 5820033.296 69.43 392464.232 5820016.316 69.43 392471.031 5819999.434 69.43</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-64f6419e-9901-4f1c-8ac9-2ebc4d92f6f2">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392596.237 5819921.045 34.536 392596.237 5819921.045 54.14 392598.03 5819917.352 54.14 392598.03 5819917.352 34.536 392596.237 5819921.045 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-e6323bfd-584f-438d-b06d-5ecd9feca6ad">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392598.03 5819917.352 34.536 392598.03 5819917.352 54.14 392604.696 5819903 54.14 392604.696 5819903 34.536 392598.03 5819917.352 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-9a32aeb1-3efe-433f-bae7-db5405fc9c33">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392586.252 5819940.954 54.14 392586.252 5819940.954 58.74 392566.348 5819930.051 58.74 392566.348 5819930.051 54.14 392586.252 5819940.954 54.14</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="fme-gen-c2c94542-7be0-42a8-9db5-6bb03159ca2f">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392526.847 5819852.011 41.923 392527.38 5819850.621 41.923 392529.975 5819844.582 41.923 392530.63 5819843.25 41.923 392530.902 5819843.375 41.923 392535.069 5819835.336 41.923 392529.451 5819832.253 41.923 392532.449 5819826.823 41.923 392542.073 5819832.187 41.923 392528.411 5819856.543 41.923 392525.511 5819855.995 41.923 392526.847 5819852.011 41.923</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="fme-gen-7fe4564b-7324-4c53-b727-d06c7b77b347">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392490.68 5820035.642 41.923 392492.182 5820036.737 41.923 392484.849 5820047.434 41.923 392481.232 5820043.661 41.923 392490.68 5820035.642 41.923</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-4690768e-d913-4241-b6d3-360cd5ca3309">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392441.919 5820000.012 34.536 392443.844 5819998.168 34.536 392443.844 5819998.168 54.52 392441.919 5820000.012 54.52 392441.919 5820000.012 54.48 392438.807 5820002.993 54.48 392438.807 5820002.993 34.536 392441.919 5820000.012 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-af2021d1-889a-4e3f-89ab-87bc2bb451cb">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392507.007 5819930.796 54.495 392507.007 5819930.796 57.61 392497.182 5819925.309 57.61 392497.182 5819925.309 54.495 392507.007 5819930.796 54.495</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-4cdb1981-6da9-42e4-8518-e89577b840e2">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392532.467 5819877.561 54.14 392532.467 5819877.561 59.84 392532.467 5819877.561 62.3 392519.434 5819891.141 62.3 392519.434 5819891.141 58.14 392519.434 5819891.141 54.14 392532.467 5819877.561 54.14</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-27f367f4-12c7-4610-aac8-a3ba11db39eb">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392532.449 5819826.823 34.536 392532.449 5819826.823 41.923 392529.451 5819832.253 41.923 392529.451 5819832.253 34.536 392532.449 5819826.823 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface gml:id="UUID_eaeb233f-8389-41ed-908f-f4c88d14c129">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392613.843 5819872.194 34.536 392613.63 5819872.075 34.536 392602.985 5819866.14 34.536 392602.571 5819865.909 34.536 392560.119 5819842.247 34.536 392559.987 5819842.173 34.536 392542.611 5819832.487 34.536 392542.073 5819832.187 34.536 392532.449 5819826.823 34.536 392529.451 5819832.253 34.536 392535.069 5819835.336 34.536 392530.902 5819843.375 34.536 392530.63 5819843.25 34.536 392529.975 5819844.582 34.536 392527.38 5819850.621 34.536 392526.847 5819852.011 34.536 392525.511 5819855.995 34.536 392524.727 5819858.331 34.536 392524.325 5819859.768 34.536 392525.229 5819860.031 34.536 392526.681 5819860.841 34.536 392527.119 5819861.085 34.536 392524.754 5819866.599 34.536 392524.391 5819866.525 34.536 392522.747 5819866.188 34.536 392522.395 5819867.674 34.536 392513.15 5819865.78 34.536 392514.485 5819859.216 34.536 392515.125 5819856.064 34.536 392509.102 5819854.848 34.536 392498.114 5819852.61 34.536 392493.752 5819873.273 34.536 392488.512 5819898.097 34.536 392484.88 5819909.794 34.536 392483.626 5819913.782 34.536 392483.038 5819915.652 34.536 392482.063 5819918.604 34.536 392488.592 5819925.712 34.536 392488.228 5819927.049 34.536 392486.285 5819934.276 34.536 392486.192 5819934.627 34.536 392477.067 5819932.424 34.536 392476.394 5819934.144 34.536 392475.36 5819936.734 34.536 392474.918 5819937.775 34.536 392471.011 5819946.981 34.536 392466.306 5819957.07 34.536 392465.983 5819957.73 34.536 392461.593 5819966.303 34.536 392456.298 5819975.855 34.536 392453.645 5819980.363 34.536 392450.695 5819985.181 34.536 392445.077 5819993.831 34.536 392444.746 5819994.342 34.536 392442.66 5819997.395 34.536 392443.844 5819998.168 34.536 392441.919 5820000.012 34.536 392438.807 5820002.993 34.536 392439.194 5820003.366 34.536 392441.974 5820006.013 34.536 392442.567 5820006.578 34.536 392444.163 5820008.101 34.536 392444.031 5820008.196 34.536 392437.108 5820013.413 34.536 392430.37 5820018.869 34.536 392423.827 5820024.557 34.536 392418.286 5820029.701 34.536 392417.301 5820030.647 34.536 392411.687 5820036.261 34.536 392406.654 5820041.659 34.536 392406.272 5820042.069 34.536 392404.953 5820043.546 34.536 392401.692 5820047.263 34.536 392403.781 5820048.786 34.536 392441.115 5820075.986 34.536 392442.2 5820075.182 34.536 392444.941 5820073.17 34.536 392447.865 5820070.929 34.536 392453.245 5820066.573 34.536 392453.297 5820066.53 34.536 392458.528 5820061.982 34.536 392462.713 5820058.103 34.536 392463.244 5820058.466 34.536 392472.115 5820064.524 34.536 392472.809 5820064.996 34.536 392484.849 5820047.434 34.536 392492.182 5820036.737 34.536 392492.209 5820036.698 34.536 392516.436 5820054.401 34.536 392516.818 5820053.844 34.536 392533.211 5820029.935 34.536 392533.84 5820029.045 34.536 392534.588 5820027.961 34.536 392541.554 5820017.864 34.536 392548.481 5820007.376 34.536 392552.433 5820001.201 34.536 392553.917 5819998.837 34.536 392560.643 5819987.831 34.536 392561.537 5819986.329 34.536 392562.799 5819984.196 34.536 392563.493 5819982.997 34.536 392567.485 5819975.992 34.536 392567.592 5819975.799 34.536 392570.944 5819969.772 34.536 392574.036 5819964.124 34.536 392577.088 5819958.456 34.536 392586.252 5819940.954 34.536 392587.293 5819938.966 34.536 392585.037 5819937.731 34.536 392588.118 5819932.238 34.536 392590.135 5819933.36 34.536 392593.331 5819926.91 34.536 392596.237 5819921.045 34.536 392598.03 5819917.352 34.536 392604.696 5819903 34.536 392611.465 5819887.055 34.536 392613.113 5819882.922 34.536 392616.276 5819874.67 34.536 392616.622 5819873.742 34.536 392613.843 5819872.194 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-1751780f-4906-4ede-a8be-7b2354207093">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392514.053 5820017.256 54.14 392514.053 5820017.256 58.74 392523.822 5820022.937 58.74 392523.822 5820022.937 54.14 392514.053 5820017.256 54.14</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-fe769226-9393-49bf-b492-c56d64b9dc7e">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392604.696 5819903 34.536 392604.696 5819903 54.14 392611.465 5819887.055 54.14 392611.465 5819887.055 34.536 392604.696 5819903 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-6f4bca73-276f-4fa8-94f8-f762d7197de3">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392534.588 5820027.961 60.59 392534.588 5820027.961 58.74 392533.84 5820029.045 34.536 392534.588 5820027.961 60.59</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-1e0c220e-1f48-4e5c-a6a2-23a99db03c9f">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392541.554 5820017.864 34.536 392541.554 5820017.864 58.74 392548.481 5820007.376 58.74 392548.481 5820007.376 34.536 392541.554 5820017.864 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-7179ea2c-38f8-4575-869c-1c4a338a764b">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392548.481 5820007.376 34.536 392548.481 5820007.376 58.74 392552.433 5820001.201 58.74 392552.433 5820001.201 34.536 392548.481 5820007.376 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-d57a2e05-3a9f-4945-b176-414adf437609">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392533.211 5820029.935 34.536 392533.211 5820029.935 60.59 392533.84 5820029.045 60.59 392533.84 5820029.045 34.536 392533.211 5820029.935 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-635ef801-35e7-4f4f-8e06-9d09b7b82385">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392533.211 5820029.935 34.536 392516.818 5820053.844 34.536 392533.211 5820029.935 60.59 392533.211 5820029.935 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-226c0a52-29bf-413c-94f5-963ce978dd77">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392516.818 5820053.844 60.59 392533.211 5820029.935 60.59 392516.818 5820053.844 34.536 392516.818 5820053.844 60.59</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-2c4e7088-f97c-4e95-989a-974108f04166">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392516.436 5820054.401 34.536 392516.818 5820053.844 60.59 392516.818 5820053.844 34.536 392516.436 5820054.401 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-eb3b807e-b65a-4c09-801c-8b9e129293d9">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392516.436 5820054.401 60.59 392516.818 5820053.844 60.59 392516.436 5820054.401 34.536 392516.436 5820054.401 60.59</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-3d0101aa-4385-42eb-b84b-53ccc6ac47e0">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392541.554 5820017.864 34.536 392534.588 5820027.961 34.536 392541.554 5820017.864 58.74 392541.554 5820017.864 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-19e8e1b0-4766-4797-963d-cceb15738242">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392534.588 5820027.961 58.74 392541.554 5820017.864 58.74 392534.588 5820027.961 34.536 392534.588 5820027.961 58.74</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-b8bbdd2a-bce6-4233-8ceb-2a38768aca1c">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392533.84 5820029.045 34.536 392534.588 5820027.961 58.74 392534.588 5820027.961 34.536 392533.84 5820029.045 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-df6946f7-d5e7-4253-9d78-1128a81495ac">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392533.84 5820029.045 60.59 392534.588 5820027.961 60.59 392533.84 5820029.045 34.536 392533.84 5820029.045 60.59</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-ce56af74-cf25-441a-92b6-cd4a036ac679">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392460.743 5819994.523 54.52 392460.743 5819994.523 57.61 392471.031 5819999.434 57.61 392471.031 5819999.434 54.52 392460.743 5819994.523 54.52</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-8a9a4f95-87d6-49d8-834c-ee9afc7022f4">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392512.608 5820007.381 54.14 392512.608 5820007.381 69.43 392524.681 5819991.284 69.43 392524.681 5819991.284 54.14 392512.608 5820007.381 54.14</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-edd78863-a67f-43cc-86a6-16c50e79f22e">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392514.027 5819899.444 57.61 392514.027 5819899.444 58.14 392514.027 5819899.444 62.3 392523.796 5819905.77 62.3 392523.796 5819905.77 57.61 392514.027 5819899.444 57.61</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-4cc0e075-9adb-44c6-afb2-2510d5424772">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392616.276 5819874.67 34.536 392616.276 5819874.67 54.14 392616.622 5819873.742 54.14 392616.622 5819873.742 34.536 392616.276 5819874.67 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-238cc4b0-1e5a-4684-a918-3cd016cedaf5">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392611.465 5819887.055 34.536 392611.465 5819887.055 54.14 392613.113 5819882.922 54.14 392613.113 5819882.922 34.536 392611.465 5819887.055 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-d3f86481-d4f5-4dd5-be5a-e251a487908f">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392613.113 5819882.922 34.536 392613.113 5819882.922 54.14 392616.276 5819874.67 54.14 392616.276 5819874.67 34.536 392613.113 5819882.922 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-21ec0502-88cb-4dcb-9d73-0a2fdeb7d1cd">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392444.941 5820073.17 34.536 392444.941 5820073.17 66.13 392447.865 5820070.929 66.13 392447.865 5820070.929 34.536 392444.941 5820073.17 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-c91b4707-3124-4d16-877a-f3f27ed24c64">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392442.2 5820075.182 34.536 392442.2 5820075.182 66.13 392444.941 5820073.17 66.13 392444.941 5820073.17 34.536 392442.2 5820075.182 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-e5b2e378-b9da-4096-be9a-018509eab8b4">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392441.115 5820075.986 34.536 392441.115 5820075.986 66.13 392442.2 5820075.182 66.13 392442.2 5820075.182 34.536 392441.115 5820075.986 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-d3b0547a-dcaf-44cc-a0da-a8a89720c4d1">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392571.185 5819910.781 59.84 392571.185 5819910.781 62.3 392532.467 5819877.561 62.3 392532.467 5819877.561 59.84 392571.185 5819910.781 59.84</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-33010a61-3127-407d-bc94-14c2eaf89df3">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392523.162 5819869.578 54.14 392523.162 5819869.578 59.84 392532.467 5819877.561 59.84 392532.467 5819877.561 54.14 392523.162 5819869.578 54.14</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-bdaa5495-3760-42ac-b511-e1f218941619">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392481.232 5820043.661 54.48 392481.232 5820043.661 41.923 392484.849 5820047.434 41.923 392481.232 5820043.661 54.48</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-20236e20-f8e1-4d47-af31-f0a28c4c10e6">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392481.232 5820043.661 66.13 392481.232 5820043.661 54.48 392484.849 5820047.434 41.923 392481.232 5820043.661 66.13</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-1fa05471-be16-49c3-a958-24838e85a4f0">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392484.849 5820047.434 66.13 392481.232 5820043.661 66.13 392484.849 5820047.434 41.923 392484.849 5820047.434 66.13</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-3210e9fe-7f58-4a9d-b3ab-c59922bff1da">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392444.783 5820005.652 54.48 392481.232 5820043.661 54.48 392444.783 5820005.652 66.13 392444.783 5820005.652 54.48</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-698240ae-b9f2-4067-8908-0d12a25bab95">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392481.232 5820043.661 66.13 392444.783 5820005.652 66.13 392481.232 5820043.661 54.48 392481.232 5820043.661 66.13</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-2a1573ba-e262-421d-a296-b7d1c49d63aa">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392476.394 5819934.144 34.536 392476.394 5819934.144 54.14 392475.36 5819936.734 54.14 392475.36 5819936.734 34.536 392476.394 5819934.144 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-b6bde247-4861-44c5-9626-c80706a65cdb">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392471.011 5819946.981 34.536 392471.011 5819946.981 58.162 392466.306 5819957.07 58.162 392466.306 5819957.07 34.536 392471.011 5819946.981 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-98cb77e9-37ab-43dc-8671-b946a0ba1a76">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392466.306 5819957.07 34.536 392466.306 5819957.07 58.162 392465.983 5819957.73 58.162 392465.983 5819957.73 34.536 392466.306 5819957.07 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-10df7a6e-7b4e-415d-9e76-d724a49b6755">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392477.067 5819932.424 34.536 392477.067 5819932.424 54.14 392476.394 5819934.144 54.14 392476.394 5819934.144 34.536 392477.067 5819932.424 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-8961f8b6-9a56-452e-9b71-f71dc6883f2d">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392474.918 5819937.775 58.162 392471.01 5819946.981 58.162 392474.918 5819937.775 54.14 392474.918 5819937.775 58.162</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-8ce564bf-6220-4f5a-905d-bab60156d7b6">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392474.918 5819937.775 34.536 392474.918 5819937.775 54.14 392471.01 5819946.981 58.162 392474.918 5819937.775 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-01826d57-93eb-40ec-b6b9-f4198dd4bca0">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392471.011 5819946.981 34.536 392474.918 5819937.775 34.536 392471.01 5819946.981 58.162 392471.011 5819946.981 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-be4b8e37-2445-4147-ac45-063c0e8592a0">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392475.36 5819936.734 54.14 392474.918 5819937.775 54.14 392475.36 5819936.734 34.536 392475.36 5819936.734 54.14</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-a5b7ab4c-86a6-4531-bdb5-da57697c322e">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392474.918 5819937.775 34.536 392475.36 5819936.734 34.536 392474.918 5819937.775 54.14 392474.918 5819937.775 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="fme-gen-91cd1941-7448-4cee-9590-d5994f796cc7">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392524.391 5819866.525 59.84 392524.754 5819866.599 59.84 392527.119 5819861.085 59.84 392526.681 5819860.841 59.84 392528.411 5819856.543 59.84 392542.073 5819832.187 59.84 392542.611 5819832.487 59.84 392559.987 5819842.173 59.84 392544.409 5819869.34 59.84 392587.105 5819894.448 59.84 392602.985 5819866.14 59.84 392613.63 5819872.075 59.84 392600.64 5819902.407 59.84 392590.904 5819925.143 59.84 392571.185 5819910.781 59.84 392532.467 5819877.561 59.84 392523.162 5819869.578 59.84 392524.391 5819866.525 59.84</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-83a42758-44fc-4690-928d-b9cd1ce34035">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392524.325 5819859.768 34.536 392524.325 5819859.768 54.14 392525.229 5819860.031 54.14 392525.229 5819860.031 34.536 392524.325 5819859.768 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-68d26626-0482-4a56-9ca7-21728f85e836">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392525.229 5819860.031 34.536 392525.229 5819860.031 54.14 392526.681 5819860.841 34.536 392525.229 5819860.031 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-52fe9825-44b2-4276-844e-d89df674836e">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392527.119 5819861.085 34.536 392526.681 5819860.841 34.536 392525.229 5819860.031 54.14 392527.119 5819861.085 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-75df8d0d-a5e0-4353-8f02-cf467d2ec040">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392526.681 5819860.841 54.14 392527.119 5819861.085 34.536 392525.229 5819860.031 54.14 392526.681 5819860.841 54.14</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-74791af7-25ac-4dbd-8485-098428a92b4e">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392527.119 5819861.085 59.84 392527.119 5819861.085 34.536 392526.681 5819860.841 54.14 392527.119 5819861.085 59.84</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-74468890-e77d-4575-972f-ec13db0c46f5">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392526.681 5819860.841 59.84 392527.119 5819861.085 59.84 392526.681 5819860.841 54.14 392526.681 5819860.841 59.84</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-fd4d7a00-9dff-4f14-a7d0-9d7b384f012b">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392481.232 5820043.661 41.923 392481.232 5820043.661 54.48 392490.68 5820035.642 54.48 392490.68 5820035.642 41.923 392481.232 5820043.661 41.923</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-c3c15d66-68ce-4dd2-ada6-7ce82c5aa486">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392524.754 5819866.599 34.536 392524.754 5819866.599 59.84 392524.391 5819866.525 34.536 392524.754 5819866.599 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-7e9cb88b-673c-4a03-abc8-89cc3fd9c0ad">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392524.391 5819866.525 54.14 392524.391 5819866.525 34.536 392524.754 5819866.599 59.84 392524.391 5819866.525 54.14</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-3d64f0d4-e723-47c5-baa0-6073799379dc">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392522.747 5819866.188 34.536 392524.391 5819866.525 34.536 392524.391 5819866.525 54.14 392522.747 5819866.188 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-2ee312e9-5ce4-491c-ad84-860600bcee2b">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392524.391 5819866.525 59.84 392524.391 5819866.525 54.14 392524.754 5819866.599 59.84 392524.391 5819866.525 59.84</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-562a5981-e4a0-4294-b462-a8fc121c3da1">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392524.391 5819866.525 54.14 392522.747 5819866.188 54.14 392522.747 5819866.188 34.536 392524.391 5819866.525 54.14</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-49c0683a-702e-4e4c-a533-c3934b268f80">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392519.434 5819891.141 58.14 392519.434 5819891.141 62.3 392514.027 5819899.444 62.3 392514.027 5819899.444 58.14 392519.434 5819891.141 58.14</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-40b6e52c-7aa8-4620-aa52-b7ec9bb92f6d">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392501.242 5819919.075 54.495 392497.182 5819925.309 54.495 392501.242 5819919.075 57.61 392501.242 5819919.075 54.495</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-9ddfd0da-a352-40e5-aae9-bca43e578466">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392501.242 5819919.075 58.14 392501.242 5819919.075 57.61 392497.182 5819925.309 54.495 392501.242 5819919.075 58.14</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-35e0558c-6a57-4d7a-be25-200c8a6aba17">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392497.182 5819925.309 58.14 392501.242 5819919.075 58.14 392497.182 5819925.309 54.495 392497.182 5819925.309 58.14</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-a9ab3b43-2592-4d30-b7f7-33ac873db7b9">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392496.318 5819926.637 57.61 392497.182 5819925.309 58.14 392497.182 5819925.309 57.61 392496.318 5819926.637 57.61</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-3721f9c2-0a27-481d-852b-e1e6eecbe5e6">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392496.318 5819926.637 58.14 392497.182 5819925.309 58.14 392496.318 5819926.637 57.61 392496.318 5819926.637 58.14</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-f2921adf-8629-4bd2-b783-0b16ac58d201">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392514.027 5819899.444 57.61 392501.242 5819919.075 57.61 392514.027 5819899.444 58.14 392514.027 5819899.444 57.61</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-2e8f945d-e63e-4f97-ae3f-584a92795e33">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392501.242 5819919.075 58.14 392514.027 5819899.444 58.14 392501.242 5819919.075 57.61 392501.242 5819919.075 58.14</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-623b96ad-1522-42e3-8da4-f09e643466cb">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392509.102 5819854.848 34.536 392509.102 5819854.848 54.14 392498.114 5819852.61 54.14 392498.114 5819852.61 34.536 392509.102 5819854.848 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-88e2753d-f8c2-4d0c-aac8-9c10a72bc7d8">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392515.125 5819856.064 34.536 392515.125 5819856.064 54.14 392509.102 5819854.848 54.14 392509.102 5819854.848 34.536 392515.125 5819856.064 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-058c304f-9104-4c74-b621-fbecdbf73983">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392529.552 5819897.668 69.43 392523.796 5819905.77 69.43 392529.552 5819897.668 62.3 392529.552 5819897.668 69.43</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-05db4fa9-1b3d-4231-8e2f-700c0912601a">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392523.796 5819905.77 62.3 392529.552 5819897.668 62.3 392523.796 5819905.77 69.43 392523.796 5819905.77 62.3</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-e1a48dc6-3cd4-4ee7-89b8-a250e691eb2a">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392512.397 5819921.814 69.43 392523.796 5819905.77 62.3 392523.796 5819905.77 69.43 392512.397 5819921.814 69.43</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-748fea45-d533-4c7f-97d3-a8b1edcec064">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392523.796 5819905.77 57.61 392523.796 5819905.77 62.3 392512.397 5819921.814 69.43 392523.796 5819905.77 57.61</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-50c3db64-4b5e-4bab-a71f-44b77ca51793">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392512.397 5819921.814 57.61 392523.796 5819905.77 57.61 392512.397 5819921.814 69.43 392512.397 5819921.814 57.61</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-51e0761f-8027-4576-9a03-decbcc1cba9e">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392566.348 5819930.051 54.14 392566.348 5819930.051 58.74 392554.206 5819958.062 58.74 392554.206 5819958.062 54.14 392566.348 5819930.051 54.14</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-ca1f8835-6203-44bc-9074-0bda1378ce7c">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392483.626 5819913.782 34.536 392484.88 5819909.794 34.536 392484.88 5819909.794 58.14 392483.626 5819913.782 58.14 392483.626 5819913.782 54.14 392483.038 5819915.652 54.14 392483.038 5819915.652 34.536 392483.626 5819913.782 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-9796d437-72da-4ec1-8a84-f13462bb834c">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392488.512 5819898.097 34.536 392488.512 5819898.097 58.14 392484.88 5819909.794 58.14 392484.88 5819909.794 34.536 392488.512 5819898.097 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-f3a21f32-cc0e-4df0-98ed-d3fa77eb660c">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392483.038 5819915.652 34.536 392483.038 5819915.652 54.14 392482.063 5819918.604 54.14 392482.063 5819918.604 34.536 392483.038 5819915.652 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-125753ff-784f-4c3a-907b-182e2cdf537f">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392437.108 5820013.413 34.536 392437.108 5820013.413 66.13 392430.37 5820018.869 66.13 392430.37 5820018.869 34.536 392437.108 5820013.413 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-f4bc253b-bd9d-43bd-bcb0-5edc729a1a2d">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392444.031 5820008.196 34.536 392444.031 5820008.196 66.13 392437.108 5820013.413 66.13 392437.108 5820013.413 34.536 392444.031 5820008.196 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="fme-gen-d4ede1d1-790a-4448-8a95-70d80c744d97">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392519.434 5819891.141 62.3 392532.467 5819877.561 62.3 392571.185 5819910.781 62.3 392562.805 5819916.942 62.3 392529.552 5819897.668 62.3 392523.796 5819905.77 62.3 392514.027 5819899.444 62.3 392519.434 5819891.141 62.3</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-4e75ef42-d304-4187-90d1-127fd198b0ea">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392492.442 5819955.348 54.53 392492.442 5819955.348 57.61 392483.928 5819950.279 57.61 392483.928 5819950.279 54.53 392492.442 5819955.348 54.53</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="fme-gen-5d36a8cf-7408-493a-8720-3658e19eb2a6">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392537.096 5819987.452 58.74 392554.206 5819958.062 58.74 392566.348 5819930.051 58.74 392586.252 5819940.954 58.74 392577.088 5819958.456 58.74 392574.036 5819964.124 58.74 392570.944 5819969.772 58.74 392567.592 5819975.799 58.74 392567.485 5819975.992 58.74 392563.493 5819982.997 58.74 392562.799 5819984.196 58.74 392561.537 5819986.329 58.74 392560.643 5819987.831 58.74 392553.917 5819998.837 58.74 392552.433 5820001.201 58.74 392548.481 5820007.376 58.74 392541.554 5820017.864 58.74 392534.588 5820027.961 58.74 392525.111 5820021.06 58.74 392523.822 5820022.937 58.74 392514.053 5820017.256 58.74 392537.096 5819987.452 58.74</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-4b43355a-521a-4673-8155-203f1c13862a">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392483.626 5819913.782 54.14 392483.626 5819913.782 58.14 392496.318 5819926.637 58.14 392496.318 5819926.637 57.61 392496.318 5819926.637 54.14 392483.626 5819913.782 54.14</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-2e91cf82-74e5-43e9-be2c-896669ad1d83">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392567.592 5819975.799 34.536 392567.592 5819975.799 58.74 392570.944 5819969.772 58.74 392570.944 5819969.772 34.536 392567.592 5819975.799 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-ffd43926-a45d-4f07-a356-667b79f6ac93">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392570.944 5819969.772 34.536 392570.944 5819969.772 58.74 392574.036 5819964.124 58.74 392574.036 5819964.124 34.536 392570.944 5819969.772 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-5dd6f69c-04ee-48f4-bb35-7947a267eb1e">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392574.036 5819964.124 34.536 392574.036 5819964.124 58.74 392577.088 5819958.456 58.74 392577.088 5819958.456 34.536 392574.036 5819964.124 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-1bc6f7d7-1d05-4026-a489-a69d93374d61">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392567.485 5819975.992 34.536 392567.485 5819975.992 58.74 392567.592 5819975.799 58.74 392567.592 5819975.799 34.536 392567.485 5819975.992 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-8f81a70c-03d1-4f2f-943a-c6857290636c">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392587.293 5819938.966 34.536 392586.252 5819940.954 34.536 392587.293 5819938.966 54.14 392587.293 5819938.966 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-25178aa5-d866-49c3-855c-e38fa85b1565">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392586.252 5819940.954 54.14 392587.293 5819938.966 54.14 392586.252 5819940.954 34.536 392586.252 5819940.954 54.14</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-b9e4ad50-9ef2-462e-b8c4-23ce192075f4">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392577.088 5819958.456 34.536 392586.252 5819940.954 54.14 392586.252 5819940.954 34.536 392577.088 5819958.456 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-f98ca477-f1c9-4105-aa43-b86c98172d30">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392586.252 5819940.954 58.74 392586.252 5819940.954 54.14 392577.088 5819958.456 34.536 392586.252 5819940.954 58.74</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-e3b9b59e-3f95-4f6a-a2d0-d41724828256">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392577.088 5819958.456 58.74 392586.252 5819940.954 58.74 392577.088 5819958.456 34.536 392577.088 5819958.456 58.74</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-6a713ba3-1b70-4d4d-8ae3-6c0d3cc5870e">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392596.237 5819921.045 34.536 392593.331 5819926.91 34.536 392596.237 5819921.045 54.14 392596.237 5819921.045 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-816edd0c-240b-46e5-ab1d-4848be9506e2">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392593.331 5819926.91 54.14 392596.237 5819921.045 54.14 392593.331 5819926.91 34.536 392593.331 5819926.91 54.14</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-57455c9e-95e7-42b1-a877-669c006cb87b">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392590.135 5819933.36 34.536 392593.331 5819926.91 54.14 392593.331 5819926.91 34.536 392590.135 5819933.36 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-e5ed656d-d7ee-421e-8cad-de05f84776d6">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392590.135 5819933.36 54.14 392593.331 5819926.91 54.14 392590.135 5819933.36 34.536 392590.135 5819933.36 54.14</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-81f7a164-71d5-4e5e-91a2-d92509e1b5f6">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392529.451 5819832.253 34.536 392529.451 5819832.253 41.923 392535.069 5819835.336 41.923 392535.069 5819835.336 34.536 392529.451 5819832.253 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-44524613-ca6b-4843-9ea4-11ea624c656f">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392524.681 5819991.284 54.14 392524.681 5819991.284 69.43 392539.719 5819968.621 69.43 392539.719 5819968.621 54.14 392524.681 5819991.284 54.14</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-220c4142-d286-465c-9a78-9115f3ed3459">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392487.601 5819943.271 54.53 392487.601 5819943.271 57.61 392496.657 5819948.046 57.61 392496.657 5819948.046 54.53 392487.601 5819943.271 54.53</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="fme-gen-9518aae4-470c-4b2c-880c-e56598965450">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392406.654 5820041.659 66.13 392411.687 5820036.261 66.13 392417.301 5820030.647 66.13 392418.286 5820029.701 66.13 392423.827 5820024.557 66.13 392430.37 5820018.869 66.13 392437.108 5820013.413 66.13 392444.031 5820008.196 66.13 392444.163 5820008.101 66.13 392442.567 5820006.578 66.13 392444.783 5820005.652 66.13 392481.232 5820043.661 66.13 392484.849 5820047.434 66.13 392472.809 5820064.996 66.13 392472.115 5820064.524 66.13 392463.244 5820058.466 66.13 392462.713 5820058.103 66.13 392458.528 5820061.982 66.13 392453.297 5820066.53 66.13 392453.245 5820066.573 66.13 392447.865 5820070.929 66.13 392444.941 5820073.17 66.13 392442.2 5820075.182 66.13 392441.115 5820075.986 66.13 392403.781 5820048.786 66.13 392401.692 5820047.263 66.13 392406.654 5820041.659 66.13</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-05f1c867-6957-4f1b-96ff-3b8d68b41422">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392445.077 5819993.831 54.52 392445.077 5819993.831 58.162 392456.929 5820001.802 58.162 392456.929 5820001.802 54.52 392445.077 5819993.831 54.52</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-06796f6a-f863-4419-9375-4ab4a085ec43">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392462.713 5820058.103 34.536 392462.713 5820058.103 66.13 392463.244 5820058.466 34.536 392462.713 5820058.103 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_72295864-3fb7-41f7-8917-edbb050dad65">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392472.115 5820064.524 66.13 392463.244 5820058.466 66.13 392462.713 5820058.103 66.13 392472.115 5820064.524 66.13</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-3106ecc9-f42b-426c-b1c8-749c3eb815eb">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392472.115 5820064.524 34.536 392463.244 5820058.466 34.536 392462.713 5820058.103 66.13 392472.115 5820064.524 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-cf166266-e205-4d33-8555-3aab2d67f423">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392472.115 5820064.524 34.536 392472.115 5820064.524 66.13 392472.809 5820064.996 66.13 392472.809 5820064.996 34.536 392472.115 5820064.524 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-d19e638e-8362-4f78-83b7-cc2cf023c730">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392472.115 5820064.524 66.13 392472.115 5820064.524 34.536 392462.713 5820058.103 66.13 392472.115 5820064.524 66.13</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-290f86eb-f13b-412e-9dfb-195338244107">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392563.493 5819982.997 34.536 392563.493 5819982.997 58.74 392567.485 5819975.992 58.74 392567.485 5819975.992 34.536 392563.493 5819982.997 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-8ac69ee9-5b4a-481f-8c93-d8728f91a42d">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392444.163 5820008.101 34.536 392444.163 5820008.101 66.13 392444.031 5820008.196 66.13 392444.031 5820008.196 34.536 392444.163 5820008.101 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-7b35e2be-845f-44b9-9be6-1a91533db2e3">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392438.807 5820002.993 34.536 392438.807 5820002.993 54.48 392439.194 5820003.366 54.48 392439.194 5820003.366 34.536 392438.807 5820002.993 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-37ce6b15-2f84-4f9d-9e93-8dba309430ef">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392439.194 5820003.366 34.536 392439.194 5820003.366 54.48 392441.974 5820006.013 54.48 392441.974 5820006.013 34.536 392439.194 5820003.366 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-c1dcf924-163e-4326-a895-cdd73034f923">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392441.974 5820006.013 34.536 392441.974 5820006.013 54.48 392442.567 5820006.578 34.536 392441.974 5820006.013 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-60eb0f52-ec53-4b80-8f9c-7321c3192c33">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392444.163 5820008.101 34.536 392442.567 5820006.578 34.536 392441.974 5820006.013 54.48 392444.163 5820008.101 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-32af7469-5911-4fc2-8b51-bb04090115b5">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392442.567 5820006.578 54.48 392444.163 5820008.101 34.536 392441.974 5820006.013 54.48 392442.567 5820006.578 54.48</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-d9c5b2cb-8557-4ac7-a3fd-fccc56055dd1">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392444.163 5820008.101 66.13 392444.163 5820008.101 34.536 392442.567 5820006.578 54.48 392444.163 5820008.101 66.13</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-0e15c105-43da-4a76-af70-60c51720c0fc">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392442.567 5820006.578 66.13 392444.163 5820008.101 66.13 392442.567 5820006.578 54.48 392442.567 5820006.578 66.13</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-97064f53-8431-4c8a-9938-3d87e3666a99">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392562.805 5819916.942 62.3 392562.805 5819916.942 69.43 392529.552 5819897.668 69.43 392529.552 5819897.668 62.3 392562.805 5819916.942 62.3</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-73289085-7d54-4400-ac9e-c8a8d76074df">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392544.409 5819869.34 59.84 392544.409 5819869.34 63.638 392587.105 5819894.448 63.638 392587.105 5819894.448 59.84 392544.409 5819869.34 59.84</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-1953bed5-ca77-4f13-8776-83576c0263d5">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392528.411 5819856.543 54.14 392528.411 5819856.543 59.84 392526.681 5819860.841 59.84 392526.681 5819860.841 54.14 392528.411 5819856.543 54.14</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-6f105f13-eef0-4d15-b241-8284ae3eb2c9">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392524.391 5819866.525 54.14 392524.391 5819866.525 59.84 392523.162 5819869.578 59.84 392523.162 5819869.578 54.14 392524.391 5819866.525 54.14</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-807206ea-6600-495a-b1e6-2ac24acb485e">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392527.119 5819861.085 34.536 392527.119 5819861.085 59.84 392524.754 5819866.599 59.84 392524.754 5819866.599 34.536 392527.119 5819861.085 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-8c32f57b-bb19-40b4-8bd6-866a73172d0e">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392474.484 5819990.861 69.43 392471.031 5819999.434 69.43 392474.484 5819990.861 57.61 392474.484 5819990.861 69.43</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-698c995b-8191-41a3-a2c6-3786582bf080">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392471.031 5819999.434 57.61 392474.484 5819990.861 57.61 392471.031 5819999.434 69.43 392471.031 5819999.434 57.61</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-b1076a01-82c9-4dee-9b8d-9af3316f80dc">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392464.232 5820016.316 69.43 392471.031 5819999.434 57.61 392471.031 5819999.434 69.43 392464.232 5820016.316 69.43</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-93984d42-dd9e-4345-8c11-77e8f8a81047">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392471.031 5819999.434 54.52 392471.031 5819999.434 57.61 392464.232 5820016.316 69.43 392471.031 5819999.434 54.52</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-900cc1d4-6ba0-49ab-8f07-0fcd7964d0c9">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392464.232 5820016.316 54.52 392471.031 5819999.434 54.52 392464.232 5820016.316 69.43 392464.232 5820016.316 54.52</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="fme-gen-81cee2eb-0c53-4c10-993a-054665a8eabd">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392559.987 5819842.173 63.638 392560.119 5819842.247 63.638 392602.571 5819865.909 63.638 392602.985 5819866.14 63.638 392587.105 5819894.448 63.638 392544.409 5819869.34 63.638 392559.987 5819842.173 63.638</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-3956fdca-6fc4-4225-ba75-ce562ea48f6b">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392501.242 5819919.075 54.495 392501.242 5819919.075 57.61 392510.123 5819925.604 57.61 392510.123 5819925.604 54.495 392501.242 5819919.075 54.495</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_e4b6da32-4cbb-4ecd-a4fa-12c5fc6b0267">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392453.245 5820066.573 34.536 392453.245 5820066.573 66.13 392453.297 5820066.53 66.13 392453.297 5820066.53 34.536 392453.245 5820066.573 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-6f82e229-1c32-4ef0-8b96-c16308d44e3e">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392493.752 5819873.273 58.14 392488.512 5819898.097 58.14 392493.752 5819873.273 54.14 392493.752 5819873.273 58.14</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-62d28bd5-4e15-41b7-9008-f73d8ed4a465">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392493.752 5819873.273 34.536 392493.752 5819873.273 54.14 392488.512 5819898.097 58.14 392493.752 5819873.273 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-b2aa0a85-d14d-4838-813c-a7ecc1250e4c">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392488.512 5819898.097 34.536 392493.752 5819873.273 34.536 392488.512 5819898.097 58.14 392488.512 5819898.097 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-4ef7a54c-871e-49b6-bf22-8f918fd003d1">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392498.114 5819852.61 54.14 392493.752 5819873.273 54.14 392498.114 5819852.61 34.536 392498.114 5819852.61 54.14</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-7a67e56e-7937-4c1d-92c2-54a2f35b6a86">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392493.752 5819873.273 34.536 392498.114 5819852.61 34.536 392493.752 5819873.273 54.14 392493.752 5819873.273 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-e0d65768-706b-457c-a45a-fe0cadb2806e">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392616.622 5819873.742 34.536 392616.622 5819873.742 54.14 392613.843 5819872.194 54.14 392613.843 5819872.194 34.536 392616.622 5819873.742 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-8c4e764f-6ac6-45d3-9b9a-53d1353fdf55">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392602.985 5819866.14 59.84 392602.985 5819866.14 63.638 392559.987 5819842.173 59.84 392602.985 5819866.14 59.84</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-ee0ad88a-fa6a-4694-baae-dd8811304edc">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392559.987 5819842.173 63.638 392559.987 5819842.173 59.84 392602.985 5819866.14 63.638 392559.987 5819842.173 63.638</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_7734470f-34b3-40b2-a6b7-5e0160df0048">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392602.571 5819865.909 63.638 392559.987 5819842.173 63.638 392602.985 5819866.14 63.638 392602.571 5819865.909 63.638</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_25daa7ec-4365-4a5b-b115-0b0386590f7d">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392559.987 5819842.173 63.638 392560.119 5819842.247 63.638 392602.571 5819865.909 63.638 392559.987 5819842.173 63.638</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-a60ac03e-c8b9-4f8e-becd-b706afbe8a7b">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392613.843 5819872.194 34.536 392613.843 5819872.194 54.14 392613.63 5819872.075 34.536 392613.843 5819872.194 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-cdb89c40-b724-487b-91f4-d3d15f4e1b24">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392602.985 5819866.14 34.536 392613.63 5819872.075 34.536 392613.843 5819872.194 54.14 392602.985 5819866.14 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-39614ac6-d7f9-406a-bb78-55fb6d4f00ac">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392602.571 5819865.909 34.536 392602.985 5819866.14 34.536 392613.843 5819872.194 54.14 392602.571 5819865.909 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-dfafe827-b655-488e-ae6a-8331bf6bf19e">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392560.119 5819842.247 34.536 392602.571 5819865.909 34.536 392613.843 5819872.194 54.14 392560.119 5819842.247 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-4825a41f-fde9-4cc5-b005-77ce9ac16db7">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392559.987 5819842.173 34.536 392560.119 5819842.247 34.536 392613.843 5819872.194 54.14 392559.987 5819842.173 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-47e06a43-3b85-4e94-9409-78c1ecd7afe8">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392542.611 5819832.487 34.536 392559.987 5819842.173 34.536 392613.843 5819872.194 54.14 392542.611 5819832.487 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-cb772832-5e8d-4c03-9705-a41037bde91a">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392542.073 5819832.187 34.536 392542.611 5819832.487 34.536 392613.843 5819872.194 54.14 392542.073 5819832.187 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-473ddfed-b96a-4d24-9456-ed58056b7c6d">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392532.449 5819826.823 34.536 392542.073 5819832.187 34.536 392613.843 5819872.194 54.14 392532.449 5819826.823 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-6e16d364-844e-49e7-8b0e-c1e9c6059b7b">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392542.073 5819832.187 41.923 392532.449 5819826.823 34.536 392613.843 5819872.194 54.14 392542.073 5819832.187 41.923</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-e2c88c2b-7d99-4d83-bab6-d475bab881d4">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392613.63 5819872.075 54.14 392542.073 5819832.187 41.923 392613.843 5819872.194 54.14 392613.63 5819872.075 54.14</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-a45eaf95-34a6-4d9f-99ed-a1929c1192cd">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392542.073 5819832.187 59.84 392542.073 5819832.187 41.923 392613.63 5819872.075 54.14 392542.073 5819832.187 59.84</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-1071f630-6a5e-4c25-a0b7-3985e4f6ccb6">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392613.63 5819872.075 59.84 392542.073 5819832.187 59.84 392613.63 5819872.075 54.14 392613.63 5819872.075 59.84</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_9216d5b7-d94f-47c5-a8c6-2c27de0da1a9">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392602.985 5819866.14 59.84 392542.073 5819832.187 59.84 392613.63 5819872.075 59.84 392602.985 5819866.14 59.84</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_76892b19-0b30-489f-909c-7c21566d9e3b">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392542.073 5819832.187 59.84 392559.987 5819842.173 59.84 392602.985 5819866.14 59.84 392542.073 5819832.187 59.84</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_f3185a0b-b5fe-4a3d-84d7-f42295898a88">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392542.073 5819832.187 59.84 392542.611 5819832.487 59.84 392559.987 5819842.173 59.84 392542.073 5819832.187 59.84</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-822a3b57-06d2-463e-8815-d4a9a1255249">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392542.073 5819832.187 41.923 392532.449 5819826.823 41.923 392532.449 5819826.823 34.536 392542.073 5819832.187 41.923</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-1e12b122-f90e-43f9-a914-31e60bc024f8">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392559.987 5819842.173 59.84 392559.987 5819842.173 63.638 392544.409 5819869.34 63.638 392544.409 5819869.34 59.84 392559.987 5819842.173 59.84</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-54347936-a5f6-4037-989b-56c7f5083284">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392507.007 5819930.796 69.43 392496.657 5819948.046 69.43 392507.007 5819930.796 57.61 392507.007 5819930.796 69.43</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-64b0452d-df34-4376-ba88-0126c5125900">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392496.657 5819948.046 57.61 392507.007 5819930.796 57.61 392496.657 5819948.046 69.43 392496.657 5819948.046 57.61</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-8179507a-a4f6-4432-be21-2c6f624a1890">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392493.335 5819953.584 69.43 392496.657 5819948.046 57.61 392496.657 5819948.046 69.43 392493.335 5819953.584 69.43</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-07ce07d4-6ccd-4324-b46c-c49d68c26d26">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392496.657 5819948.046 54.53 392496.657 5819948.046 57.61 392493.335 5819953.584 69.43 392496.657 5819948.046 54.53</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-1ebd491b-fb3a-489c-92e4-40185c13a965">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392493.335 5819953.584 54.53 392496.657 5819948.046 54.53 392493.335 5819953.584 69.43 392493.335 5819953.584 54.53</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-73ec8bd2-7ae8-4339-ac53-9002a62a735a">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392512.397 5819921.814 69.43 392510.123 5819925.604 69.43 392512.397 5819921.814 57.61 392512.397 5819921.814 69.43</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-89d4efe1-aff5-4060-a95e-911caac5b636">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392510.123 5819925.604 57.61 392512.397 5819921.814 57.61 392510.123 5819925.604 69.43 392510.123 5819925.604 57.61</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-dfdf0fde-ad6a-4c5a-9119-a69737962c41">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392507.007 5819930.796 69.43 392510.123 5819925.604 57.61 392510.123 5819925.604 69.43 392507.007 5819930.796 69.43</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-b22fdc87-aed9-48d0-ac24-f67d7be92d31">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392510.123 5819925.604 54.495 392510.123 5819925.604 57.61 392507.007 5819930.796 69.43 392510.123 5819925.604 54.495</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-1a1dfb4f-c0f4-4943-a3b4-f9586965ed67">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392507.007 5819930.796 54.495 392510.123 5819925.604 54.495 392507.007 5819930.796 69.43 392507.007 5819930.796 54.495</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-5964ccd2-4048-476a-ba70-5d9ec107a8ea">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392522.747 5819866.188 34.536 392522.747 5819866.188 54.14 392522.395 5819867.674 54.14 392522.395 5819867.674 34.536 392522.747 5819866.188 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-3ba73be3-9c4c-47d9-a8ff-14c5a8dca921">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392542.073 5819832.187 41.923 392542.073 5819832.187 59.84 392528.411 5819856.543 59.84 392528.411 5819856.543 54.14 392528.411 5819856.543 41.923 392542.073 5819832.187 41.923</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-7920093f-bfcd-46af-8647-d73642ac4c91">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392534.588 5820027.961 58.74 392534.588 5820027.961 60.59 392525.111 5820021.06 60.59 392525.111 5820021.06 58.74 392534.588 5820027.961 58.74</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-20f32c83-9700-46f0-b2ad-2f92588ea0e1">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392488.228 5819927.049 34.536 392488.228 5819927.049 54.14 392486.285 5819934.276 54.14 392486.285 5819934.276 34.536 392488.228 5819927.049 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-98e8fcee-222b-402d-8ec1-e1709936b571">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392488.592 5819925.712 34.536 392488.592 5819925.712 54.14 392488.228 5819927.049 54.14 392488.228 5819927.049 34.536 392488.592 5819925.712 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-4c5f500f-8322-497d-8844-605173497382">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392447.865 5820070.929 34.536 392447.865 5820070.929 66.13 392453.245 5820066.573 66.13 392453.245 5820066.573 34.536 392447.865 5820070.929 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-03887083-46d1-44bb-9bce-f0a25312f86f">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392487.468 5820033.296 60.59 392487.468 5820033.296 69.43 392499.053 5820022.207 69.43 392499.053 5820022.207 60.59 392487.468 5820033.296 60.59</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-2b27a327-2cad-4000-8b4d-911ef3246b6d">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392486.192 5819934.627 34.536 392486.192 5819934.627 54.14 392477.067 5819932.424 54.14 392477.067 5819932.424 34.536 392486.192 5819934.627 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-d3f68d1e-00ce-4a8f-b8b0-db5ce0aaf633">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392486.285 5819934.276 34.536 392486.285 5819934.276 54.14 392486.192 5819934.627 54.14 392486.192 5819934.627 34.536 392486.285 5819934.276 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-462773bc-fbf3-46f6-a9a1-5c568bd46e4c">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392554.206 5819958.062 54.14 392554.206 5819958.062 58.74 392537.096 5819987.452 58.74 392537.096 5819987.452 54.14 392554.206 5819958.062 54.14</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-ec790c0d-2794-4425-a512-f22b53f35005">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392496.318 5819926.637 54.14 392496.318 5819926.637 57.61 392489.718 5819939.23 57.61 392489.718 5819939.23 54.14 392496.318 5819926.637 54.14</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-6b6300f8-cd87-4345-bbe8-692934039e05">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392460.743 5819994.523 54.52 392456.929 5820001.802 54.52 392460.743 5819994.523 57.61 392460.743 5819994.523 54.52</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-22b7b398-3620-4d4f-8f4f-cbf481626d9e">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392460.743 5819994.523 58.162 392460.743 5819994.523 57.61 392456.929 5820001.802 54.52 392460.743 5819994.523 58.162</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-1b81a30e-82da-4e23-9a05-0e581b79578e">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392456.929 5820001.802 58.162 392460.743 5819994.523 58.162 392456.929 5820001.802 54.52 392456.929 5820001.802 58.162</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-20bbc730-8e0f-44ea-985b-a4186ce16a14">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392474.329 5819968.598 54.399 392470.868 5819975.202 54.399 392474.329 5819968.598 57.61 392474.329 5819968.598 54.399</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-35e1e090-d17e-43b9-a41e-6799d85640c8">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392474.329 5819968.598 58.162 392474.329 5819968.598 57.61 392470.868 5819975.202 54.399 392474.329 5819968.598 58.162</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-cf2c427e-5a6a-4cc2-a7a8-c02212a2bcbf">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392470.868 5819975.202 58.162 392474.329 5819968.598 58.162 392470.868 5819975.202 54.399 392470.868 5819975.202 57.61 392470.868 5819975.202 58.162</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-c2dceacd-59f3-49c6-b862-4348a193d0c0">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392460.743 5819994.523 57.61 392470.868 5819975.202 58.162 392470.868 5819975.202 57.61 392460.743 5819994.523 57.61</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-f1f3e58a-6ae8-4c3a-8a72-ef160f7faf2a">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392460.743 5819994.523 58.162 392470.868 5819975.202 58.162 392460.743 5819994.523 57.61 392460.743 5819994.523 58.162</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-99f2e6ec-08e0-4286-b382-9ba4d85c07da">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392487.601 5819943.271 54.53 392483.928 5819950.279 54.53 392487.601 5819943.271 57.61 392487.601 5819943.271 54.53</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-04d00218-9e9b-488e-9e79-e01819c6417e">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392487.601 5819943.271 58.162 392487.601 5819943.271 57.61 392483.928 5819950.279 54.53 392487.601 5819943.271 58.162</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-78be0e0b-ca25-4708-8086-26f67c4c6051">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392483.928 5819950.279 58.162 392487.601 5819943.271 58.162 392483.928 5819950.279 54.53 392483.928 5819950.279 57.61 392483.928 5819950.279 58.162</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-20779898-f460-43f2-8c19-b14304fd7d4a">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392474.329 5819968.598 57.61 392483.928 5819950.279 58.162 392483.928 5819950.279 57.61 392474.329 5819968.598 57.61</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-8b43eeab-28ba-4d45-843d-39ecfe593ec8">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392474.329 5819968.598 58.162 392483.928 5819950.279 58.162 392474.329 5819968.598 57.61 392474.329 5819968.598 58.162</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-4d398ccc-d037-40e3-9a27-311a6130c35c">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392489.718 5819939.23 57.61 392487.601 5819943.271 57.61 392489.718 5819939.23 58.162 392489.718 5819939.23 57.61</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-a72b89ad-7d72-4ad7-b339-d54962b640cd">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392487.601 5819943.271 58.162 392489.718 5819939.23 58.162 392487.601 5819943.271 57.61 392487.601 5819943.271 58.162</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-bf00321f-c724-47ec-b9ed-0b592796adfd">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392529.975 5819844.582 34.536 392529.975 5819844.582 41.923 392527.38 5819850.621 41.923 392527.38 5819850.621 34.536 392529.975 5819844.582 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-6fa2a375-21c0-4aa4-a2c0-556e5d2e10f7">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392530.63 5819843.25 34.536 392530.63 5819843.25 41.923 392529.975 5819844.582 41.923 392529.975 5819844.582 34.536 392530.63 5819843.25 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-effffd59-bc01-4bf4-b19f-565dcbc344e6">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392530.902 5819843.375 34.536 392530.902 5819843.375 41.923 392530.63 5819843.25 41.923 392530.63 5819843.25 34.536 392530.902 5819843.375 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-99bd2978-d579-4ac2-aad2-cc24c30f5aa5">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392403.781 5820048.786 34.536 392403.781 5820048.786 66.13 392441.115 5820075.986 66.13 392441.115 5820075.986 34.536 392403.781 5820048.786 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-aaa7f6d7-c714-4eef-9611-2bececfecbcc">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392401.692 5820047.263 34.536 392401.682 5820047.256 54.48 392403.781 5820048.786 34.536 392401.692 5820047.263 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-756f97b2-4ad8-4bca-9999-a37a9983dbdc">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392403.781 5820048.786 66.13 392403.781 5820048.786 34.536 392401.682 5820047.256 54.48 392403.781 5820048.786 66.13</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-988ec8af-90c1-469b-ad3b-35b243fe00e4">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392401.692 5820047.263 66.13 392403.781 5820048.786 66.13 392401.682 5820047.256 54.48 392401.692 5820047.263 66.13</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-b52a5701-11ca-434a-a264-8ec3fa0949dc">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392587.105 5819894.448 59.84 392587.105 5819894.448 63.638 392602.985 5819866.14 63.638 392602.985 5819866.14 59.84 392587.105 5819894.448 59.84</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-2f196687-d872-48e9-a967-3164837782d3">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392528.411 5819856.543 41.923 392528.411 5819856.543 54.14 392525.511 5819855.995 54.14 392525.511 5819855.995 41.923 392528.411 5819856.543 41.923</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-62467c9c-123b-4da2-a588-9f86f3a1d712">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392453.297 5820066.53 34.536 392453.297 5820066.53 66.13 392458.528 5820061.982 66.13 392458.528 5820061.982 34.536 392453.297 5820066.53 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-04eaed40-326c-4529-b0ef-c88281fc03d0">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392458.528 5820061.982 34.536 392458.528 5820061.982 66.13 392462.713 5820058.103 66.13 392462.713 5820058.103 34.536 392458.528 5820061.982 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-472c10b5-cee8-498b-937b-6ae4f6fe9c90">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392442.66 5819997.395 34.536 392442.66 5819997.395 54.52 392443.844 5819998.168 54.52 392443.844 5819998.168 34.536 392442.66 5819997.395 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-5e2d5daa-fc31-4fa1-9cf6-236b6a0a00ba">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392492.182 5820036.737 34.536 392484.849 5820047.434 34.536 392492.182 5820036.737 41.923 392492.182 5820036.737 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-9d49ff86-a97b-45a3-8a00-662d2e977fff">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392484.849 5820047.434 34.536 392484.849 5820047.434 41.923 392492.182 5820036.737 41.923 392484.849 5820047.434 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-47b1ada5-f622-4d8b-ba5c-280003359834">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392472.809 5820064.996 34.536 392484.849 5820047.434 41.923 392484.849 5820047.434 34.536 392472.809 5820064.996 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-76b233c1-6010-4a50-9dfb-d403307e0d6f">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392484.849 5820047.434 66.13 392484.849 5820047.434 41.923 392472.809 5820064.996 34.536 392484.849 5820047.434 66.13</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-6fc8a25f-ea54-45c8-96d4-b142d19edf71">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392472.809 5820064.996 66.13 392484.849 5820047.434 66.13 392472.809 5820064.996 34.536 392472.809 5820064.996 66.13</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_288b4697-e740-4ab3-956a-0930dcc6c6ae">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392492.209 5820036.698 34.536 392492.182 5820036.737 34.536 392492.209 5820036.698 60.59 392492.209 5820036.698 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_6e9718f5-1b84-477c-903c-b32fcf6b29d6">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392492.182 5820036.737 60.59 392492.209 5820036.698 60.59 392492.182 5820036.737 34.536 392492.182 5820036.737 41.923 392492.182 5820036.737 60.59</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-c7a39c98-4b09-4c70-85b5-6a0eff4b03f1">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392520.986 5820027.068 54.14 392520.986 5820027.068 60.59 392509.009 5820018.345 60.59 392509.009 5820018.345 54.14 392520.986 5820027.068 54.14</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-d953b231-86b8-43dc-ab76-4c6649316632">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392479.742 5819980.463 69.43 392474.484 5819990.861 69.43 392479.742 5819980.463 57.61 392479.742 5819980.463 69.43</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-1292fe8d-bfd3-44e8-9bad-29061e101683">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392474.484 5819990.861 57.61 392479.742 5819980.463 57.61 392474.484 5819990.861 69.43 392474.484 5819990.861 57.61</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-27c4defd-b970-47a4-9cc6-a54b56313867">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392492.442 5819955.348 69.43 392483.517 5819972.999 69.43 392492.442 5819955.348 57.61 392492.442 5819955.348 69.43</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-b0dde77d-4485-4e4a-8047-11c5024e1a5c">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392483.517 5819972.999 57.61 392492.442 5819955.348 57.61 392483.517 5819972.999 69.43 392483.517 5819972.999 57.61</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-32b49576-6546-45d1-b19b-8c13f2daacac">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392479.742 5819980.463 69.43 392483.517 5819972.999 57.61 392483.517 5819972.999 69.43 392479.742 5819980.463 69.43</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-5194a488-089d-4ed7-9477-3c9067c29ca4">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392483.517 5819972.999 54.399 392483.517 5819972.999 57.61 392479.742 5819980.463 69.43 392483.517 5819972.999 54.399</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-81848d2f-6adf-493e-897f-a64214b2c235">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392479.742 5819980.463 54.399 392483.517 5819972.999 54.399 392479.742 5819980.463 69.43 392479.742 5819980.463 57.61 392479.742 5819980.463 54.399</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-f172cf75-a3d3-4015-9f61-d4028646a1dc">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392493.335 5819953.584 69.43 392492.442 5819955.348 69.43 392493.335 5819953.584 54.53 392493.335 5819953.584 69.43</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-c6703605-6e45-419b-bc73-b4523927e17f">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392492.442 5819955.348 54.53 392493.335 5819953.584 54.53 392492.442 5819955.348 69.43 392492.442 5819955.348 54.53</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-1969e8bd-be58-43aa-a1d5-ddc89ee16b7d">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392479.742 5819980.463 54.399 392479.742 5819980.463 57.61 392470.868 5819975.202 57.61 392470.868 5819975.202 54.399 392479.742 5819980.463 54.399</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-56fa7cbb-a520-4754-a8a3-654bc30e605e">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392535.069 5819835.336 34.536 392535.069 5819835.336 41.923 392530.902 5819843.375 41.923 392530.902 5819843.375 34.536 392535.069 5819835.336 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-d7f2e3a6-6f45-4c2c-958e-91d18750c8b2">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392587.293 5819938.966 34.536 392587.293 5819938.966 54.14 392585.037 5819937.731 54.14 392585.037 5819937.731 34.536 392587.293 5819938.966 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_400b72c4-2e6d-4482-acdb-ab4859901465">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392525.111 5820021.06 60.59 392523.822 5820022.937 60.59 392525.111 5820021.06 58.74 392525.111 5820021.06 60.59</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_e2c15ce1-98d1-4b9c-b69d-8bd7d64cb891">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392523.822 5820022.937 58.74 392525.111 5820021.06 58.74 392523.822 5820022.937 60.59 392523.822 5820022.937 58.74</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_1c34ca4e-2ea3-450f-8a17-bde9f41a79c4">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392520.986 5820027.068 60.59 392523.822 5820022.937 58.74 392523.822 5820022.937 60.59 392520.986 5820027.068 60.59</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_8a546131-ece5-482a-b1ce-72d28781677e">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392523.822 5820022.937 54.14 392523.822 5820022.937 58.74 392520.986 5820027.068 60.59 392523.822 5820022.937 54.14</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_47d3cfa3-5831-49b5-9ab2-e4ad5e986219">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392520.986 5820027.068 54.14 392523.822 5820022.937 54.14 392520.986 5820027.068 60.59 392520.986 5820027.068 54.14</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-75b155a6-cd6e-484b-847e-69da4d561097">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392499.053 5820022.207 54.14 392499.053 5820022.207 60.59 392499.053 5820022.207 69.43 392512.608 5820007.381 69.43 392512.608 5820007.381 54.14 392499.053 5820022.207 54.14</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-bce12490-776a-45e8-9339-25294d469d21">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392571.185 5819910.781 54.14 392571.185 5819910.781 59.84 392590.904 5819925.143 59.84 392590.904 5819925.143 54.14 392571.185 5819910.781 54.14</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-9f345d25-4400-4c46-b9f2-d758b5ba6334">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392444.783 5820005.652 54.48 392444.783 5820005.652 66.13 392442.567 5820006.578 66.13 392442.567 5820006.578 54.48 392444.783 5820005.652 54.48</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-b5c1ac4c-caa5-46e7-a2c3-1d4de289382f">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392430.37 5820018.869 34.536 392430.37 5820018.869 66.13 392423.827 5820024.557 66.13 392423.827 5820024.557 34.536 392430.37 5820018.869 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-76d3336c-3f5c-40e2-aa8c-e7c1b4b13e54">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392423.827 5820024.557 34.536 392423.827 5820024.557 66.13 392418.286 5820029.701 66.13 392418.286 5820029.701 34.536 392423.827 5820024.557 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-fe0db8ac-5786-4161-8101-5355365531f6">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392509.009 5820018.345 54.14 392509.009 5820018.345 60.59 392499.053 5820022.207 60.59 392499.053 5820022.207 54.14 392509.009 5820018.345 54.14</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-eba1e68f-592d-4243-b7ca-e59dfca85e74">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392482.063 5819918.604 34.536 392482.063 5819918.604 54.14 392488.592 5819925.712 54.14 392488.592 5819925.712 34.536 392482.063 5819918.604 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-1de0ee12-6df4-4b39-8f61-934f03499cda">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392461.593 5819966.303 34.536 392461.593 5819966.303 58.162 392456.298 5819975.855 58.162 392456.298 5819975.855 34.536 392461.593 5819966.303 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-80052508-b1b0-4b28-961c-365e627b72af">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392465.983 5819957.73 34.536 392465.983 5819957.73 58.162 392461.593 5819966.303 58.162 392461.593 5819966.303 34.536 392465.983 5819957.73 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-cbd5d6bb-2663-4dff-9817-ff9a9d96dd80">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392522.395 5819867.674 34.536 392522.395 5819867.674 54.14 392513.15 5819865.78 54.14 392513.15 5819865.78 34.536 392522.395 5819867.674 34.536</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="fme-gen-b8cbf1f2-92b7-45aa-a281-a93ec927d035">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392474.329 5819968.598 54.399 392474.329 5819968.598 57.61 392483.517 5819972.999 57.61 392483.517 5819972.999 54.399 392474.329 5819968.598 54.399</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
		</bldg:Building>
	</cityObjectMember>
	<cityObjectMember>
		<bldg:Building gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3">
			<gml:name>Rotes_Rathaus</gml:name>
			<bldg:roofType>1130</bldg:roofType>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_2baad404-ea8e-4d9f-8d3e-7de3d3a8a7d4_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391996.939 5819914.34 60.61 392016.733 5819929.868 60.61 392023.152 5819934.903 60.61 392023.152 5819934.903 35.367 392016.733 5819929.868 35.367 391996.939 5819914.34 35.367 391996.939 5819914.34 60.61</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_46db71f9-b9ec-407a-ab50-86e4aa6baa94_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392004.481 5819874.906 53.205 392009.109 5819868.954 53.205 392013.737 5819863.002 53.205 392026.858 5819846.126 53.205 392026.858 5819846.126 60.69 392026.858 5819846.126 60.734 392013.737 5819863.002 60.745 392013.737 5819863.002 61.09 392009.109 5819868.954 61.75 392004.481 5819874.906 61.09 392004.481 5819874.906 60.687 391992.95 5819889.736 60.691 391992.95 5819889.736 53.205 392004.481 5819874.906 53.205</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_cda34dd2-fecc-450b-ac0c-26c8fcf55d6d_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392022.892 5819884.293 35.367 392022.892 5819884.293 60.806 392023.364 5819884.663 60.806 392023.364 5819884.663 35.367 392022.892 5819884.293 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_3fe1a9ec-f8c5-4b90-973a-cbf8560bdf68_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392021.728 5819886.748 35.367 392021.728 5819886.748 60.806 392021.256 5819886.378 60.806 392021.256 5819886.378 35.367 392021.728 5819886.748 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_6328c690-cbb8-4753-ba4f-c92c919e164e_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392023.615 5819880.334 61.75 392028.268 5819874.402 61.09 392028.268 5819874.402 60.806 392023.615 5819880.334 60.806 392018.961 5819886.266 60.806 392018.961 5819886.266 61.09 392023.615 5819880.334 61.75</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_50df5396-97e5-4b12-934b-b539ccf275b1_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392077.616 5819875.256 35.367 392077.616 5819875.256 60.69 392079.02 5819875.087 60.69 392079.02 5819875.087 35.367 392077.616 5819875.256 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_efe8c0a3-cfce-4ab2-a938-04cc5e2ac09a_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392038.145 5819894.767 35.367 392038.145 5819894.767 60.806 392035.52 5819898.109 60.806 392035.52 5819898.109 35.367 392038.145 5819894.767 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_2c362751-1d90-4e80-b2dd-b40af82ca8b4_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392036.454 5819857.243 61.81 392038.191 5819855.029 61.81 392041.455 5819857.593 60.74 392029.904 5819872.317 60.74 392028.268 5819874.402 60.74 392027.2 5819873.564 61.09 392025.003 5819871.84 61.81 392036.454 5819857.243 61.81</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_3a6c5587-d1f6-433d-a67e-88e432d90ef5_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392009.864 5819814.403 35.367 392009.864 5819814.403 60.81 392009.695 5819812.999 60.81 392009.695 5819812.999 35.367 392009.864 5819814.403 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_a506073d-91ea-4a7c-9e33-8d347a06bc4e_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391988.116 5819833.381 35.367 391988.116 5819833.381 61.074 391986.858 5819832.393 60.656 391986.858 5819832.393 35.367 391988.116 5819833.381 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_2a3ce0ad-4b2c-4644-b6bd-03cca034cb97_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392042.638 5819887.427 35.367 392042.638 5819887.427 60.806 392043.423 5819888.044 60.806 392043.423 5819888.044 35.367 392042.638 5819887.427 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_e48da57f-b686-4d56-9550-76ef3daa1562_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392017.577 5819888.031 53.205 392017.577 5819888.031 60.69 392017.577 5819888.031 60.809 392018.108 5819888.448 60.81 392018.108 5819888.448 53.205 392017.577 5819888.031 53.205</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_9a37c264-a4dc-48d8-8eb5-19501ac9bca0_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392046.712 5819882.237 35.367 392046.712 5819882.237 53.205 392045.309 5819882.406 53.205 392045.309 5819882.406 35.367 392046.712 5819882.237 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_f137611d-eec9-4961-aa90-10bd8b472a5a_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392027.497 5819879.394 35.367 392027.497 5819879.394 60.806 392027.025 5819879.023 60.806 392027.025 5819879.023 35.367 392027.497 5819879.394 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_2e8bc7f9-265e-4bae-bd86-efb30363ced4_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391967.919 5819855.403 35.367 391967.919 5819855.403 60.473 391968.038 5819856.385 60.656 391968.038 5819856.385 35.367 391967.919 5819855.403 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_dadfdbfc-0dca-48e7-ba22-e8cefe790c33_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392005.765 5819809.911 35.367 392005.765 5819809.911 60.81 392004.378 5819810.095 60.81 392004.378 5819810.095 35.367 392005.765 5819809.911 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_81b898c1-3cb7-46bf-94cd-1f4751ccf74e_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392038.769 5819901.804 60.941 392043.061 5819905.172 61.111 392050.695 5819911.16 60.812 392050.695 5819911.16 60.61 392043.061 5819905.172 60.61 392038.769 5819901.804 60.61 392038.769 5819901.804 60.808 392035.388 5819899.152 60.807 392038.769 5819901.804 60.941</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_699e5b7d-b720-4c0c-b6b2-ed3ab3e08513_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391986.858 5819832.393 35.367 391986.858 5819832.393 60.656 391985.875 5819832.513 60.473 391985.875 5819832.513 35.367 391986.858 5819832.393 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_768a203a-0a54-4041-9e30-c952c2f9760a_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392019.078 5819887.211 35.367 392019.078 5819887.211 60.807 392020.493 5819888.322 60.807 392020.493 5819888.322 35.367 392019.078 5819887.211 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_e377bb94-b6ed-4dfa-98b5-99ff49af4f8e_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391992.84 5819888.761 35.367 391992.84 5819888.761 53.205 391993.312 5819889.131 53.205 391993.312 5819889.131 35.367 391992.84 5819888.761 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_717756f1-c451-44d9-915c-aab67c6666d0_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392047.58 5819844.795 35.367 392047.58 5819844.795 60.69 392047.951 5819844.323 60.69 392047.951 5819844.323 35.367 392047.58 5819844.795 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_867873eb-f20e-4118-84d9-f63d71935453_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392023.152 5819934.903 35.367 392023.152 5819934.903 60.61 392022.782 5819935.375 60.61 392022.782 5819935.375 35.367 392023.152 5819934.903 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_4b7156ce-054e-43a1-8292-1be64af23c9e_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392030.407 5819848.914 64.29 392038.191 5819855.029 64.29 392036.454 5819857.243 64.29 392032.56 5819854.188 64.29 392028.666 5819851.133 64.29 392030.407 5819848.914 64.29</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_c41b2090-340c-4bcc-84cb-85bda274c8ec_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392028.666 5819851.133 61.813 392032.56 5819854.188 63.72 392021.109 5819868.785 63.72 392017.215 5819865.731 61.804 392028.666 5819851.133 61.813</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_1c37a486-a663-4b15-abba-66a7a94f60fb_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392030.735 5819933.989 35.367 392030.735 5819933.989 60.61 392048.692 5819911.121 60.61 392048.692 5819911.121 35.367 392030.735 5819933.989 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_3d81bcdd-8404-4a00-9f09-8214edafeb2c_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391978.56 5819878.447 35.367 391979.702 5819879.343 35.367 391992.881 5819889.681 35.367 391992.881 5819889.681 53.205 391992.881 5819889.681 60.93 391979.702 5819879.343 60.93 391979.702 5819879.343 60.88 391978.56 5819878.447 60.88 391978.56 5819878.447 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_cbea9d78-b709-4849-bf6f-e8d611ae9fcd_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392030.407 5819848.914 61.815 392030.407 5819848.914 64.29 392028.666 5819851.133 64.29 392028.666 5819851.133 61.813 392030.407 5819848.914 61.815</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_8ac148a8-1bf9-45b9-8d48-bea52009871d_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392026.884 5819939.864 35.367 392026.884 5819939.864 60.61 392028.288 5819939.694 60.61 392028.288 5819939.694 35.367 392026.884 5819939.864 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_cc425b06-de34-468c-a957-f2fbe98e4adb_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391997.234 5819883.155 35.367 391999.946 5819879.694 35.367 391999.946 5819879.694 53.205 391997.234 5819883.155 53.205 391992.84 5819888.761 53.205 391992.84 5819888.761 35.367 391997.234 5819883.155 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_d3cd8cd7-dd06-4836-8f63-ba355994e1f9_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392027.025 5819879.023 35.367 392027.025 5819879.023 60.806 392027.303 5819878.67 60.806 392027.303 5819878.67 35.367 392027.025 5819879.023 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_fab5442e-1a64-42c0-830c-db188cd50a29_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391985.53 5819870.532 60.683 391985.53 5819870.532 60.88 391984.938 5819870.066 60.88 391985.53 5819870.532 60.683</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_def040f2-b630-457e-885b-ccfc19622902_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392053.87 5819907.116 35.367 392053.87 5819907.116 60.812 392056.526 5819903.735 60.812 392056.526 5819903.735 35.367 392053.87 5819907.116 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_c5a1e9ec-4abc-4445-a716-dda9544b117e_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392030.366 5819875.736 35.367 392030.366 5819875.736 60.806 392028.951 5819874.625 60.806 392028.951 5819874.625 35.367 392030.366 5819875.736 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_5748b0e1-567e-4f87-a071-c4d1cae5e03f_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392037.377 5819903.577 53.205 392037.377 5819903.577 60.61 392035.249 5819906.284 60.61 392035.249 5819906.284 53.205 392037.377 5819903.577 53.205</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_0b389d7e-3604-404d-9d12-fdaa37169267_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392017.461 5819855.518 35.367 392017.461 5819855.518 53.205 392017.088 5819855.958 53.205 392017.088 5819855.958 35.367 392017.461 5819855.518 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_3628b409-85e1-45ed-9d97-3440555ba1f2_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391984.389 5819835.54 35.367 391984.389 5819835.54 60.656 391985.176 5819836.157 60.918 391985.176 5819836.157 35.367 391984.389 5819835.54 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_e8b0d531-b1e8-46ac-b668-d22510d4c4a1_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392014.874 5819863.895 61.09 392013.737 5819863.002 61.09 392013.737 5819863.002 60.745 392014.874 5819863.895 61.09</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_f5dd8cb5-3a77-413d-a98a-d3d711b341da_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391988.708 5819832.626 35.367 392001.918 5819815.787 35.367 392001.918 5819815.787 60.81 391988.708 5819832.626 60.81 391988.708 5819832.626 61.074 391988.116 5819833.381 61.074 391988.116 5819833.381 35.367 391988.708 5819832.626 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_30bb40f4-0fff-4c08-9679-a2e14b6cad7e_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392016.982 5819884.713 61.09 392018.961 5819886.266 61.09 392018.961 5819886.266 60.806 392018.961 5819886.266 60.69 392016.982 5819884.713 61.09</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_a5556c5f-88bd-4aba-8b22-e718f4d18461_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392011.802 5819880.649 63.26 392015.696 5819883.704 61.35 392004.102 5819898.484 61.35 392000.208 5819895.429 63.26 392011.802 5819880.649 63.26</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_4e67f8d0-b6d3-488a-b5f8-5b69e25e1943_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392052.852 5819908.413 35.367 392052.852 5819908.413 60.812 392053.87 5819907.116 60.812 392053.87 5819907.116 35.367 392052.852 5819908.413 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_138bb069-cede-485d-acb5-fa50e40a1114_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391982.384 5819905.781 60.93 391986.275 5819900.821 60.93 391994.201 5819890.717 60.93 391994.201 5819890.717 108.27 391986.275 5819900.821 108.27 391986.275 5819900.821 68.32 391982.384 5819905.781 68.32 391981.798 5819906.527 68.32 391981.798 5819906.527 60.93 391982.384 5819905.781 60.93</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_9003deb9-7783-4197-9599-1ffd56ceb064_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392049.164 5819911.491 35.367 392049.164 5819911.491 60.61 392050.568 5819911.322 60.61 392050.568 5819911.322 35.367 392049.164 5819911.491 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_2dd0e1be-1fe9-4ae1-8395-777f7e5a8ea5_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391991.154 5819911.834 35.367 391991.154 5819911.834 68.32 391992.57 5819912.945 68.32 391992.57 5819912.945 35.367 391991.154 5819911.834 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_10d2b26b-a118-4c3b-aafc-4887ccc7b00b_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392004.481 5819874.906 60.687 392006.558 5819876.535 61.091 392007.908 5819877.595 61.353 391996.314 5819892.374 61.345 391994.201 5819890.717 60.93 391994.173 5819890.695 60.93 391992.95 5819889.736 60.691 392004.481 5819874.906 60.687</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_ddffc220-8148-4a1c-b84c-8c1c544d71c0_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391995.511 5819916.079 68.32 391995.581 5819916.07 68.32 391995.581 5819916.07 35.367 391995.511 5819916.079 35.367 391994.178 5819916.24 35.367 391994.178 5819916.24 68.32 391995.511 5819916.079 68.32</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_a089853c-6f07-4149-bc8c-24c8cd33b0f4_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392059.188 5819897.754 35.367 392059.188 5819897.754 60.69 392077.144 5819874.886 60.69 392077.144 5819874.886 35.367 392059.188 5819897.754 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_d8636efc-ca4d-4d19-9814-59d677ae01bc_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391999.041 5819908.098 108.27 392005.638 5819899.689 108.27 392005.638 5819899.689 61.04 392005.638 5819899.689 60.81 391999.041 5819908.098 60.81 391997.711 5819909.793 60.81 391997.711 5819909.793 68.32 391997.711 5819909.793 108.27 391999.041 5819908.098 108.27</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_cf2bbf9a-592e-4e09-8238-280b044328d5_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392028.29 5819877.411 60.806 392028.66 5819876.939 60.806 392028.66 5819876.939 35.367 392028.29 5819877.411 35.367 392027.303 5819878.67 35.367 392027.303 5819878.67 60.806 392028.29 5819877.411 60.806</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_e16f08b3-57fb-4ca5-889d-944ebc431a63_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392044.336 5819883.645 60.811 392049.714 5819887.866 60.812 392048.081 5819889.945 60.808 392044.695 5819887.288 60.806 392042.703 5819885.725 60.806 392044.336 5819883.645 60.811</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_6e0763c9-30a3-4267-bca4-7db38513bad7_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392057.544 5819902.437 35.367 392057.544 5819902.437 60.812 392056.916 5819901.943 60.836 392056.916 5819901.943 35.367 392057.544 5819902.437 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_76b626e5-af1c-4c65-802c-c6ee7d87afb5_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392018.354 5819856.224 35.367 392018.354 5819856.224 53.205 392017.461 5819855.518 53.205 392017.461 5819855.518 35.367 392018.354 5819856.224 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_a3eb128d-decb-49b9-a331-ef4bc106f598_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392009.493 5819814.875 35.367 392022.351 5819824.975 35.367 392038.147 5819837.384 35.367 392038.147 5819837.384 60.69 392022.351 5819824.975 60.69 392022.351 5819824.975 60.81 392009.493 5819814.875 60.81 392009.493 5819814.875 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_b64d39db-67b0-4bab-88cf-4c0231e3a554_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392047.58 5819844.795 35.367 392069.817 5819862.264 35.367 392076.233 5819867.305 35.367 392076.233 5819867.305 60.69 392069.817 5819862.264 60.69 392047.58 5819844.795 60.69 392047.58 5819844.795 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_3a1d40c2-84f7-48a9-9aaa-00d98531b45c_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392004.481 5819874.906 60.687 392004.481 5819874.906 61.09 392006.558 5819876.535 61.091 392004.481 5819874.906 60.687</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_afc69fa5-a1f1-4ad2-810f-e46e6e10d467_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391979.16 5819843.827 35.367 391979.16 5819843.827 60.918 391977.308 5819846.187 60.918 391977.308 5819846.187 35.367 391979.16 5819843.827 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_4add6656-8c72-4800-b755-708d123273f8_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392028.666 5819851.133 64.29 392032.56 5819854.188 64.29 392036.454 5819857.243 64.29 392036.454 5819857.243 61.81 392032.56 5819854.188 63.72 392028.666 5819851.133 61.813 392028.666 5819851.133 64.29</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_bcdfabbe-c911-4c81-b53e-c781cc914ffe_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391969.096 5819857.628 35.367 391969.297 5819857.372 35.367 391969.297 5819857.372 61.075 391969.096 5819857.628 61.075 391969.096 5819857.628 60.88 391955.495 5819874.966 60.88 391955.495 5819874.966 35.367 391969.096 5819857.628 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_584fa070-961c-4eb9-b717-7c0636e86d10_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392044.695 5819887.288 60.806 392048.081 5819889.945 60.941 392052.371 5819893.31 61.111 392043.061 5819905.172 61.111 392038.769 5819901.804 60.941 392035.388 5819899.152 60.807 392044.695 5819887.288 60.806</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_90d9b1e1-bdf5-4101-b904-d48becb876f3_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392059.057 5819900.511 35.367 392059.057 5819900.511 60.812 392059.828 5819899.528 60.812 392059.828 5819899.528 35.367 392059.057 5819900.511 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_92aa3581-c0c0-455a-b933-d2e5a0a46af5_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392030.435 5819872.733 53.205 392030.435 5819872.733 60.81 392029.904 5819872.317 60.809 392029.904 5819872.317 60.74 392029.904 5819872.317 53.205 392030.435 5819872.733 53.205</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_933093cd-f096-4b8e-b86a-7404a4728882_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392056.916 5819901.943 35.367 392056.916 5819901.943 60.836 392058.428 5819900.017 60.836 392058.428 5819900.017 35.367 392056.916 5819901.943 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_d1e2fb9b-040b-4576-b007-da31a7ab6b69_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392076.604 5819866.833 35.367 392076.604 5819866.833 60.69 392076.233 5819867.305 60.69 392076.233 5819867.305 35.367 392076.604 5819866.833 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_3f1f4aa4-fc5c-4417-b959-dd088eda37a8_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392030.659 5819902.681 35.367 392030.659 5819902.681 53.205 392035.249 5819906.284 53.205 392035.249 5819906.284 35.367 392030.659 5819902.681 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_1ee6cae0-9dfc-450c-b48c-bd1f6e0b47a6_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392056.526 5819903.735 35.367 392056.526 5819903.735 60.812 392057.544 5819902.437 60.812 392057.544 5819902.437 35.367 392056.526 5819903.735 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_5571837e-e67e-490c-9fe0-27d2e575e237_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392003.322 5819899.477 108.27 391997.505 5819906.894 108.27 391996.519 5819900.131 117.04 392003.322 5819899.477 108.27</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_fab9f515-304c-42bd-a8b3-67c0ece8d0ce_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392036.454 5819857.243 61.81 392036.454 5819857.243 64.29 392038.191 5819855.029 64.29 392038.191 5819855.029 61.81 392036.454 5819857.243 61.81</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_bdc4abed-b102-40ae-9c86-7264a11daaf0_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392021.256 5819886.378 35.367 392021.256 5819886.378 60.806 392021.626 5819885.906 60.806 392021.626 5819885.906 35.367 392021.256 5819886.378 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_f16c9003-e689-4046-8eb3-86d1d02f8052_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392042.703 5819885.725 35.367 392044.336 5819883.645 35.367 392045.309 5819882.406 35.367 392045.309 5819882.406 53.205 392044.336 5819883.645 53.205 392044.336 5819883.645 60.811 392042.703 5819885.725 60.806 392042.468 5819886.024 60.806 392042.468 5819886.024 35.367 392042.703 5819885.725 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_1e306269-a19a-4984-a336-de7547b17296_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391994.173 5819890.695 60.93 391992.95 5819889.736 60.93 391992.95 5819889.736 60.691 391994.173 5819890.695 60.93</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_08a6e7a4-d066-48ea-aa95-b88bb48a8d88_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391979.702 5819879.343 60.93 391992.881 5819889.681 60.93 391992.95 5819889.736 60.93 391994.173 5819890.695 60.93 391994.201 5819890.717 60.93 391986.275 5819900.821 60.93 391982.384 5819905.781 60.93 391981.798 5819906.527 60.93 391981.436 5819906.243 60.93 391981.275 5819904.91 60.93 391981.266 5819904.84 60.93 391981.636 5819904.368 60.93 391982.624 5819903.109 60.93 391982.58 5819903.074 60.93 391969.274 5819892.636 60.93 391979.702 5819879.343 60.93</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_e00280e3-51ec-4b45-9e6e-8fb195c315db_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392077.144 5819874.886 35.367 392077.144 5819874.886 60.69 392077.616 5819875.256 60.69 392077.616 5819875.256 35.367 392077.144 5819874.886 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_712bfca1-4a0f-4925-8344-3987637f91f7_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392048.081 5819889.945 60.808 392048.081 5819889.945 60.69 392052.371 5819893.31 60.69 392059.779 5819899.122 60.69 392059.779 5819899.122 60.82 392052.371 5819893.31 61.111 392048.081 5819889.945 60.941 392044.695 5819887.288 60.806 392048.081 5819889.945 60.808</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_bf82fcc5-c7b1-487b-95fe-de27fc5d86ad_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392001.003 5819850.807 35.367 392001.674 5819851.334 35.367 392003.48 5819852.751 35.367 392005.524 5819854.356 35.367 392009.85 5819857.752 35.367 392009.85 5819857.752 53.205 392005.524 5819854.356 53.205 392003.48 5819852.751 53.205 392001.674 5819851.334 53.205 392001.674 5819851.334 60.46 392001.003 5819850.807 60.683 392001.003 5819850.807 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_858a86df-20a8-49ca-91cb-859ab82eb635_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391956.04 5819883.018 35.367 391956.04 5819883.018 60.88 391956.411 5819882.546 60.88 391956.411 5819882.546 35.367 391956.04 5819883.018 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_73d3845c-58d2-409a-922a-cc93b833644b_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392017.088 5819855.958 35.367 392017.088 5819855.958 53.205 392014.01 5819860.154 53.205 392014.01 5819860.154 35.367 392017.088 5819855.958 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_fab5b869-72bd-4698-a42d-2602a8e08209_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392001.446 5819815.417 35.367 392001.446 5819815.417 60.81 392001.918 5819815.787 60.81 392001.918 5819815.787 35.367 392001.446 5819815.417 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_d4c927bd-18fa-4e45-84a6-396fdefda479_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392005.638 5819899.689 60.81 392005.638 5819899.689 61.04 392006.773 5819900.58 60.81 392005.638 5819899.689 60.81</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_909384fb-bc4b-4c51-8922-e374f20a8692_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392013.164 5819835.304 60.81 392013.835 5819835.831 60.81 392013.835 5819835.831 60.69 392026.529 5819845.802 60.69 392026.529 5819845.802 35.367 392013.835 5819835.831 35.367 392013.164 5819835.304 35.367 392013.164 5819835.304 60.81</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_6bd39061-f06a-4337-b6ba-a9d66c4087ea_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392023.364 5819884.663 35.367 392023.364 5819884.663 60.806 392024.505 5819883.209 60.806 392024.505 5819883.209 35.367 392023.364 5819884.663 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_12390a07-2260-4e50-a597-c3496fd59db8_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392001.003 5819850.807 35.367 392001.003 5819850.807 60.683 392001.398 5819850.303 60.683 392001.398 5819850.303 35.367 392001.003 5819850.807 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_6d061971-f107-47ab-a237-fbc96cfa31ec_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391994.201 5819890.717 60.93 391996.314 5819892.374 61.345 392000.208 5819895.429 63.26 392004.102 5819898.484 61.35 392005.638 5819899.689 61.04 392005.638 5819899.689 108.27 392004.102 5819898.484 108.27 392000.208 5819895.429 108.27 391996.314 5819892.374 108.27 391994.201 5819890.717 108.27 391994.201 5819890.717 60.93</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_4b36882c-aa6e-45f2-96d8-164f43e3a02d_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392045.309 5819882.406 53.205 392046.712 5819882.237 53.205 392051.302 5819885.843 53.205 392049.714 5819887.866 53.205 392044.336 5819883.645 53.205 392045.309 5819882.406 53.205</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_41097b98-24ad-4a23-8552-b6ad69e72e37_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391992.57 5819912.945 35.367 391992.57 5819912.945 68.32 391993.435 5819913.623 68.32 391993.435 5819913.623 35.367 391992.57 5819912.945 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_3f065004-0013-418f-92b6-bb453a8cbe47_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392029.904 5819872.317 60.74 392029.904 5819872.317 60.809 392028.268 5819874.402 60.806 392028.268 5819874.402 60.74 392029.904 5819872.317 60.74</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_fa86106d-6845-4b85-beda-844427dc8537_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391974.532 5819849.726 35.367 391974.532 5819849.726 60.918 391971.293 5819853.856 60.918 391971.293 5819853.856 35.367 391974.532 5819849.726 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_45475743-7316-44b0-83be-0230c2d8e54e_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392007.908 5819877.595 61.353 392011.802 5819880.649 63.26 392000.208 5819895.429 63.26 391996.314 5819892.374 61.345 392007.908 5819877.595 61.353</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_9298c0e4-d384-40fb-b686-0d8706196356_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391971.293 5819853.856 35.367 391971.293 5819853.856 60.918 391970.506 5819853.239 60.656 391970.506 5819853.239 35.367 391971.293 5819853.856 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_340de1ee-850d-498a-a80d-626b98924ae4_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392029.904 5819872.317 53.205 392029.904 5819872.317 60.74 392041.455 5819857.593 60.74 392041.455 5819857.593 60.69 392041.455 5819857.593 53.205 392029.904 5819872.317 53.205</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_17c48ae1-58a4-4ebf-997e-af2f80a8bd39_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391978.56 5819878.447 35.367 391978.56 5819878.447 60.88 391978.991 5819877.897 60.88 391978.991 5819877.897 35.367 391978.56 5819878.447 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_badd4856-9f8c-4c8d-ab7b-4ff684c2b9ef_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392030.435 5819872.733 60.81 392030.435 5819872.733 53.205 392041.985 5819858.009 53.205 392041.985 5819858.009 60.69 392042.016 5819857.969 60.69 392042.016 5819857.969 35.367 392041.985 5819858.009 35.367 392030.435 5819872.733 35.367 392028.951 5819874.625 35.367 392028.951 5819874.625 60.806 392030.435 5819872.733 60.81</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_149e38a7-e810-4c5f-8405-f4902047990a_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391988.208 5819907.49 35.367 391988.208 5819907.49 68.32 391991.354 5819909.958 68.32 391991.354 5819909.958 35.367 391988.208 5819907.49 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_b0516a39-4fa3-42af-bb04-009fd0181b91_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392058.428 5819900.017 35.367 392058.428 5819900.017 60.836 392059.057 5819900.511 60.812 392059.057 5819900.511 35.367 392058.428 5819900.017 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_b3abd91e-ca52-4a4f-b625-9527f1933215_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392026.858 5819846.126 60.734 392030.407 5819848.914 61.815 392028.666 5819851.133 61.813 392017.215 5819865.731 61.804 392014.874 5819863.895 61.09 392013.737 5819863.002 60.745 392026.858 5819846.126 60.734</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_66c24ff4-f38c-476d-8261-ccc0074fbb7a_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392032 5819899.355 53.205 392037.377 5819903.577 53.205 392035.249 5819906.284 53.205 392030.659 5819902.681 53.205 392030.49 5819901.277 53.205 392032 5819899.355 53.205</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_a6fcdba8-0b6f-43d9-b17c-5c7458eb9029_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391969.523 5819853.357 60.473 391970.506 5819853.239 60.656 391971.293 5819853.856 60.918 391974.532 5819849.726 60.918 391975.89 5819847.996 60.918 391977.308 5819846.187 60.918 391979.16 5819843.827 60.918 391980.579 5819842.017 60.918 391981.937 5819840.287 60.918 391985.176 5819836.157 60.918 391984.389 5819835.54 60.656 391984.27 5819834.557 60.473 391985.875 5819832.513 60.473 391986.858 5819832.393 60.656 391988.116 5819833.381 61.074 391988.708 5819832.626 61.074 391996.339 5819838.612 63.61 391976.724 5819863.617 63.61 391969.096 5819857.628 61.075 391969.297 5819857.372 61.075 391968.038 5819856.385 60.656 391967.919 5819855.403 60.473 391969.523 5819853.357 60.473</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_06d39247-5dd7-4a38-b64f-bc0e6cdf2529_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392002.547 5819875.404 35.367 392002.547 5819875.404 53.205 392003.019 5819875.775 53.205 392003.019 5819875.775 35.367 392002.547 5819875.404 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_c2f507a0-d059-4680-96a0-98fc983c6161_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391988.708 5819832.626 60.81 391996.339 5819838.612 60.81 392004.766 5819845.223 60.81 391996.339 5819838.612 63.61 391988.708 5819832.626 61.074 391988.708 5819832.626 60.81</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_e31a4214-a65c-4b7a-bd89-e89630e759c9_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391981.937 5819840.287 35.367 391981.937 5819840.287 60.918 391980.579 5819842.017 60.918 391980.579 5819842.017 35.367 391981.937 5819840.287 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_89dbde4d-b71d-47fa-a173-7e4b7a164e48_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392041.455 5819857.593 53.205 392041.985 5819858.009 53.205 392030.435 5819872.733 53.205 392029.904 5819872.317 53.205 392041.455 5819857.593 53.205</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_7a3f4cc8-3b40-4ccc-91dc-a5e485eb1f3b_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392026.858 5819846.126 60.69 392030.407 5819848.914 60.69 392038.191 5819855.029 60.69 392041.455 5819857.593 60.69 392041.455 5819857.593 60.74 392038.191 5819855.029 61.81 392038.191 5819855.029 64.29 392030.407 5819848.914 64.29 392030.407 5819848.914 61.815 392026.858 5819846.126 60.734 392026.858 5819846.126 60.69</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_f01f8030-2486-429f-9c71-0e80e769498d_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391950.704 5819880.102 35.367 391950.704 5819880.102 60.88 391954.636 5819883.187 60.88 391954.636 5819883.187 35.367 391950.704 5819880.102 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_d023c3dd-4f13-4a29-901d-a6639370e485_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392028.268 5819874.402 60.74 392028.268 5819874.402 60.806 392028.268 5819874.402 61.09 392027.2 5819873.564 61.09 392028.268 5819874.402 60.74</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_54f06cd5-9c70-48d2-88d4-5c3c6557d467_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391954.636 5819883.187 35.367 391954.636 5819883.187 60.88 391956.04 5819883.018 60.88 391956.04 5819883.018 35.367 391954.636 5819883.187 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_98a14907-706d-4ef4-bc74-23372f3fd691_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392018.108 5819888.448 53.205 392018.108 5819888.448 60.81 392019.078 5819887.211 60.807 392019.078 5819887.211 35.367 392018.108 5819888.448 35.367 392007.898 5819901.462 35.367 392007.898 5819901.462 53.205 392018.108 5819888.448 53.205</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_9dcc88d0-c1bd-4757-b859-d9ac1a8184fa_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392049.714 5819887.866 53.205 392049.714 5819887.866 60.69 392049.714 5819887.866 60.812 392044.336 5819883.645 60.811 392044.336 5819883.645 53.205 392049.714 5819887.866 53.205</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_e2c6b471-d34f-4871-83b0-f37abef7f4d6_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391999.041 5819908.098 60.81 392005.638 5819899.689 60.81 392006.773 5819900.58 60.81 392007.367 5819901.046 60.81 391999.441 5819911.15 60.81 391997.711 5819909.793 60.81 391999.041 5819908.098 60.81</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_a3153869-9a28-4a7d-b58b-0ef9b726b829_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391953.619 5819874.765 35.367 391953.619 5819874.765 60.88 391950.534 5819878.698 60.88 391950.534 5819878.698 35.367 391953.619 5819874.765 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_64c973f3-0ca8-43f2-94f7-c615b6cc2faf_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391989.28 5819865.751 60.683 391989.951 5819866.278 60.46 391989.951 5819866.278 53.205 391991.756 5819867.696 53.205 391991.756 5819867.696 35.367 391989.951 5819866.278 35.367 391989.28 5819865.751 35.367 391989.28 5819865.751 60.683</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_06138514-6d99-4557-acc4-9aea680ef41c_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392014.912 5819860.612 35.367 392014.912 5819860.612 53.205 392014.372 5819861.301 53.205 392014.372 5819861.301 35.367 392014.912 5819860.612 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_212a5a6f-b77a-4741-9efd-330cab1c97e2_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391994.201 5819890.717 108.27 391996.314 5819892.374 108.27 392000.208 5819895.429 108.27 392004.102 5819898.484 108.27 392005.638 5819899.689 108.27 391999.041 5819908.098 108.27 391997.711 5819909.793 108.27 391988.387 5819902.478 108.27 391986.275 5819900.821 108.27 391994.201 5819890.717 108.27</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
									<gml:interior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391997.505 5819906.894 108.27 392003.322 5819899.477 108.27 391995.534 5819893.368 108.27 391989.717 5819900.784 108.27 391997.505 5819906.894 108.27</gml:posList>
										</gml:LinearRing>
									</gml:interior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_9acb3870-35d5-4440-8333-d084d34e701e_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391983.783 5819906.814 35.367 391983.783 5819906.814 68.32 391984.153 5819906.342 68.32 391984.153 5819906.342 35.367 391983.783 5819906.814 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_072c52a0-5024-466d-ae4e-3b1969d2df10_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392010.764 5819858.469 35.367 392010.764 5819858.469 53.205 392009.85 5819857.752 53.205 392009.85 5819857.752 35.367 392010.764 5819858.469 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_e2a593ea-2e78-4707-af6d-ee0d22443ec5_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391984.153 5819906.342 35.367 391984.153 5819906.342 68.32 391985.018 5819907.021 68.32 391985.018 5819907.021 35.367 391984.153 5819906.342 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_993ed3f8-e8ef-44bc-8be8-57b8b8c6781a_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392009.493 5819814.875 35.367 392009.493 5819814.875 60.81 392009.864 5819814.403 60.81 392009.864 5819814.403 35.367 392009.493 5819814.875 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_45c16061-ef8e-403a-a248-f52d14a28d5b_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392081.938 5819869.752 35.367 392081.938 5819869.752 60.69 392078.008 5819866.664 60.69 392078.008 5819866.664 35.367 392081.938 5819869.752 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_f3b00f6b-7964-40d6-9a8a-1f54a5ca3e8c_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392001.276 5819814.013 35.367 392001.276 5819814.013 60.81 392001.446 5819815.417 60.81 392001.446 5819815.417 35.367 392001.276 5819814.013 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_892c5731-144a-4533-b06a-e741a9311696_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391969.523 5819853.357 35.367 391969.523 5819853.357 60.473 391967.919 5819855.403 60.473 391967.919 5819855.403 35.367 391969.523 5819853.357 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_87734e80-57d1-4b47-8c76-19caf06ec3f5_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391982.624 5819903.109 35.367 391982.624 5819903.109 60.93 391981.636 5819904.368 60.93 391981.636 5819904.368 35.367 391982.624 5819903.109 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_3c1ab1cc-2b14-4220-95fd-0571c0993698_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391981.636 5819904.368 35.367 391981.636 5819904.368 60.93 391981.266 5819904.84 60.93 391981.266 5819904.84 35.367 391981.636 5819904.368 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_b0edca0f-a919-4caf-9a16-7399a5fbdbb6_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392031.206 5819934.359 35.367 392031.206 5819934.359 60.61 392030.735 5819933.989 60.61 392030.735 5819933.989 35.367 392031.206 5819934.359 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_6df9f069-f843-4c1a-bbb2-6acbdb3806d2_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391993.065 5819914.095 68.32 391993.074 5819914.167 68.32 391993.235 5819915.499 68.32 391993.235 5819915.499 35.367 391993.074 5819914.167 35.367 391993.065 5819914.095 35.367 391993.065 5819914.095 68.32</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_4baa5a7e-8998-4a2e-8fa4-a1054bdb8130_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392018.845 5819855.598 35.367 392022.491 5819850.95 35.367 392026.497 5819845.842 35.367 392026.529 5819845.802 35.367 392026.529 5819845.802 60.69 392026.497 5819845.842 60.69 392026.497 5819845.842 53.205 392022.491 5819850.95 53.205 392018.845 5819855.598 53.205 392018.354 5819856.224 53.205 392018.354 5819856.224 35.367 392018.845 5819855.598 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_68eb98f9-2e17-44ca-99fd-2c22d1c13dec_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392032.56 5819854.188 63.72 392036.454 5819857.243 61.81 392025.003 5819871.84 61.81 392021.109 5819868.785 63.72 392032.56 5819854.188 63.72</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_b0d8c95b-d55f-4d6d-87dd-62b3fd8b6ab5_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392013.737 5819863.002 61.09 392014.874 5819863.895 61.09 392017.215 5819865.731 61.09 392021.109 5819868.785 61.09 392025.003 5819871.84 61.09 392027.2 5819873.564 61.09 392028.268 5819874.402 61.09 392023.615 5819880.334 61.75 392009.109 5819868.954 61.75 392013.737 5819863.002 61.09</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_63420b3a-9297-41fb-9484-eb27ce40887d_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392051.302 5819885.843 53.205 392046.712 5819882.237 53.205 392046.712 5819882.237 35.367 392051.302 5819885.843 35.367 392051.388 5819885.911 35.367 392051.388 5819885.911 60.69 392051.302 5819885.843 60.69 392051.302 5819885.843 53.205</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_879c6dfa-21e7-4417-820e-f0cf2ba8dda0_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392003.322 5819899.477 108.27 391996.519 5819900.131 117.04 391995.534 5819893.368 108.27 392003.322 5819899.477 108.27</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_badcd5ea-69d1-4fe5-838b-6b5cb733cf84_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391983.712 5819906.823 68.32 391983.783 5819906.814 68.32 391983.783 5819906.814 35.367 391983.712 5819906.823 35.367 391982.38 5819906.984 35.367 391982.38 5819906.984 68.32 391983.712 5819906.823 68.32</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_75c04905-16e0-4992-ae50-c560f777e0df_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392047.951 5819844.323 35.367 392047.951 5819844.323 60.69 392047.782 5819842.919 60.69 392047.782 5819842.919 35.367 392047.951 5819844.323 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_44654616-de01-4c0d-ba39-4577ace69786_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392007.908 5819877.595 61.09 392011.802 5819880.649 61.09 392015.696 5819883.704 61.09 392016.982 5819884.713 61.09 392015.696 5819883.704 61.35 392011.802 5819880.649 63.26 392007.908 5819877.595 61.353 392006.558 5819876.535 61.091 392007.908 5819877.595 61.09</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_33555d84-6df3-401b-b865-4bb06dcc04de_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392042.016 5819857.969 60.69 392061.297 5819873.115 60.69 392061.439 5819873.226 60.69 392061.439 5819873.226 35.367 392061.297 5819873.115 35.367 392042.016 5819857.969 35.367 392042.016 5819857.969 60.69</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_9c7d30c2-8234-4adb-b70b-d7f9a7e32ba3_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391991.296 5819864.563 60.46 392000.603 5819852.699 60.46 392001.674 5819851.334 60.46 392001.674 5819851.334 53.205 392000.603 5819852.699 53.205 391991.296 5819864.563 53.205 391989.951 5819866.278 53.205 391989.951 5819866.278 60.46 391991.296 5819864.563 60.46</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_f4af31a8-69f9-423e-a82d-c29bdd7df5b1_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391956.411 5819882.546 60.88 391969.274 5819892.636 60.88 391969.274 5819892.636 60.93 391982.58 5819903.074 60.93 391982.624 5819903.109 60.93 391982.624 5819903.109 35.367 391982.58 5819903.074 35.367 391969.274 5819892.636 35.367 391956.411 5819882.546 35.367 391956.411 5819882.546 60.88</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_932d2e8a-0965-4f11-85d8-21e42495b47e_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392032 5819899.355 53.205 392032 5819899.355 60.811 392037.377 5819903.577 60.812 392037.377 5819903.577 60.61 392037.377 5819903.577 53.205 392032 5819899.355 53.205</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_1c1691ce-46cb-46bc-a9ac-103d2dd6f104_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392039.92 5819836.743 35.367 392039.92 5819836.743 60.69 392038.517 5819836.912 60.69 392038.517 5819836.912 35.367 392039.92 5819836.743 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_df507c77-ee4e-4d13-88e4-092e1801e62f_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391977.308 5819846.187 35.367 391977.308 5819846.187 60.918 391975.89 5819847.996 60.918 391975.89 5819847.996 35.367 391977.308 5819846.187 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_a0c48497-6eb6-4ab7-b2c0-5e247341e3cd_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392007.367 5819901.046 60.69 392007.367 5819901.046 60.81 392006.773 5819900.58 60.81 392007.367 5819901.046 60.69</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_b6d71179-a1d3-48d3-adfc-d9bf1574c843_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391981.266 5819904.84 60.93 391981.275 5819904.91 60.93 391981.436 5819906.243 60.93 391981.436 5819906.243 35.367 391981.275 5819904.91 35.367 391981.266 5819904.84 35.367 391981.266 5819904.84 60.93</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_71653b52-cac8-483d-81f9-2da2ba833f29_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392048.692 5819911.121 35.367 392048.692 5819911.121 60.61 392049.164 5819911.491 60.61 392049.164 5819911.491 35.367 392048.692 5819911.121 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_12ca5e9c-08d4-40b5-8dcc-35a657625cb5_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392014.01 5819860.154 35.367 392014.01 5819860.154 53.205 392014.912 5819860.612 53.205 392014.912 5819860.612 35.367 392014.01 5819860.154 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_c9928c7b-9f83-4324-8883-9235c5a51fb7_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392018.961 5819886.266 60.69 392018.961 5819886.266 60.806 392017.577 5819888.031 60.809 392017.577 5819888.031 60.69 392018.961 5819886.266 60.69</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_dd8609e1-6115-43b1-92bf-2456fb269a34_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391996.519 5819900.131 117.04 391997.505 5819906.894 108.27 391989.717 5819900.784 108.27 391996.519 5819900.131 117.04</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_b5fe2a36-a1d5-4acd-b8a5-df0e74dda9c4_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392026.497 5819845.842 53.205 392026.497 5819845.842 60.69 392026.858 5819846.126 60.69 392026.858 5819846.126 53.205 392026.497 5819845.842 53.205</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_623b45be-0f36-4007-98a8-63686833143a_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391953.619 5819874.765 60.88 391955.023 5819874.596 60.88 391955.495 5819874.966 60.88 391969.096 5819857.628 60.88 391976.724 5819863.617 60.88 391984.938 5819870.066 60.88 391985.53 5819870.532 60.88 391979.463 5819878.267 60.88 391978.991 5819877.897 60.88 391978.56 5819878.447 60.88 391979.702 5819879.343 60.88 391969.274 5819892.636 60.88 391956.411 5819882.546 60.88 391956.04 5819883.018 60.88 391954.636 5819883.187 60.88 391950.704 5819880.102 60.88 391950.534 5819878.698 60.88 391953.619 5819874.765 60.88</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_8bc8fe41-d420-4171-b651-3873adcba035_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392078.008 5819866.664 35.367 392078.008 5819866.664 60.69 392076.604 5819866.833 60.69 392076.604 5819866.833 35.367 392078.008 5819866.664 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_5144be8a-4eaf-4cf7-9f8e-3f3595104974_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392037.377 5819903.577 60.61 392037.377 5819903.577 60.812 392038.769 5819901.804 60.808 392038.769 5819901.804 60.61 392037.377 5819903.577 60.61</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_5dfde9df-bc97-4681-9b55-60f318733efb_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391996.339 5819838.612 63.61 392004.766 5819845.223 60.81 392005.148 5819845.522 60.683 392004.822 5819845.938 60.683 392005.344 5819846.276 60.521 392001.998 5819850.742 60.489 392001.398 5819850.303 60.683 392001.003 5819850.807 60.683 392001.674 5819851.334 60.46 392000.603 5819852.699 60.46 391991.296 5819864.563 60.46 391989.951 5819866.278 60.46 391989.28 5819865.751 60.683 391985.53 5819870.532 60.683 391984.938 5819870.066 60.88 391976.724 5819863.617 63.61 391996.339 5819838.612 63.61</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_5e6241f8-7b83-4742-a7a5-35a9c1b4b336_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392013.835 5819835.831 60.69 392013.835 5819835.831 60.81 392022.351 5819824.975 60.81 392022.351 5819824.975 60.69 392013.835 5819835.831 60.69</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_cdd8bf0c-147d-4198-8827-e945244673bb_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391991.296 5819864.563 53.205 392000.603 5819852.699 53.205 392001.674 5819851.334 53.205 392003.48 5819852.751 53.205 392005.524 5819854.356 53.205 392009.85 5819857.752 53.205 392010.764 5819858.469 53.205 392011.894 5819859.357 53.205 392014.372 5819861.301 53.205 392014.912 5819860.612 53.205 392014.01 5819860.154 53.205 392017.088 5819855.958 53.205 392017.461 5819855.518 53.205 392018.354 5819856.224 53.205 392018.845 5819855.598 53.205 392022.491 5819850.95 53.205 392026.497 5819845.842 53.205 392026.858 5819846.126 53.205 392013.737 5819863.002 53.205 392009.109 5819868.954 53.205 392004.481 5819874.906 53.205 391992.95 5819889.736 53.205 391992.881 5819889.681 53.205 391993.312 5819889.131 53.205 391992.84 5819888.761 53.205 391997.234 5819883.155 53.205 391999.946 5819879.694 53.205 392003.019 5819875.775 53.205 392002.547 5819875.404 53.205 392002.176 5819875.876 53.205 392000.17 5819874.301 53.205 391998.126 5819872.696 53.205 391993.801 5819869.301 53.205 391991.756 5819867.696 53.205 391989.951 5819866.278 53.205 391991.296 5819864.563 53.205</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_4306b62f-eb41-49e1-92b2-648be036f634_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392051.302 5819885.843 53.205 392051.302 5819885.843 60.69 392049.714 5819887.866 60.69 392049.714 5819887.866 53.205 392051.302 5819885.843 53.205</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_ef7496f9-ed84-43d7-83c2-4c8cfb0f4cfc_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392018.961 5819886.266 60.806 392023.615 5819880.334 60.806 392028.268 5819874.402 60.806 392029.904 5819872.317 60.809 392030.435 5819872.733 60.81 392028.951 5819874.625 60.806 392030.366 5819875.736 60.806 392030.215 5819875.929 60.806 392029.132 5819877.309 60.806 392028.66 5819876.939 60.806 392028.29 5819877.411 60.806 392027.303 5819878.67 60.806 392027.025 5819879.023 60.806 392027.497 5819879.394 60.806 392026.356 5819880.849 60.806 392024.505 5819883.209 60.806 392023.364 5819884.663 60.806 392022.892 5819884.293 60.806 392022.613 5819884.648 60.806 392021.626 5819885.906 60.806 392021.256 5819886.378 60.806 392021.728 5819886.748 60.806 392020.908 5819887.793 60.806 392020.493 5819888.322 60.807 392019.078 5819887.211 60.807 392018.108 5819888.448 60.81 392017.577 5819888.031 60.809 392018.961 5819886.266 60.806</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_7fc1ccc8-0705-4691-9c98-9015809d5bdf_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391950.534 5819878.698 35.367 391950.534 5819878.698 60.88 391950.704 5819880.102 60.88 391950.704 5819880.102 35.367 391950.534 5819878.698 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_544911b1-4ef6-40c5-b0ff-51c34e82013f_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392033.33 5819897.661 60.807 392033.466 5819897.645 60.807 392034.734 5819897.492 60.806 392035.52 5819898.109 60.806 392038.145 5819894.767 60.806 392040.799 5819891.386 60.806 392043.423 5819888.044 60.806 392042.638 5819887.427 60.806 392042.468 5819886.024 60.806 392042.703 5819885.725 60.806 392044.695 5819887.288 60.806 392035.388 5819899.152 60.807 392038.769 5819901.804 60.808 392037.377 5819903.577 60.812 392032 5819899.355 60.811 392033.33 5819897.661 60.807</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_056675d0-9f6a-419d-88ae-ce0dc0fbcbc2_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391985.176 5819836.157 35.367 391985.176 5819836.157 60.918 391981.937 5819840.287 60.918 391981.937 5819840.287 35.367 391985.176 5819836.157 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_1ea9cdee-7d4c-4d49-b521-d8e62fe43df7_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391978.991 5819877.897 35.367 391978.991 5819877.897 60.88 391979.463 5819878.267 60.88 391979.463 5819878.267 35.367 391978.991 5819877.897 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_bfcf410b-1b67-4b98-a57e-58a9ffa623da_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392005.148 5819845.522 60.683 392005.148 5819845.522 60.81 392013.164 5819835.304 60.81 392013.164 5819835.304 35.367 392005.148 5819845.522 35.367 392004.822 5819845.938 35.367 392004.822 5819845.938 60.683 392005.148 5819845.522 60.683</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_0c8b3d95-abf4-4fe8-be2f-445d2cf2f39a_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392042.468 5819886.024 35.367 392042.468 5819886.024 60.806 392042.638 5819887.427 60.806 392042.638 5819887.427 35.367 392042.468 5819886.024 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_e337a032-971f-4d58-92fe-1df0e10b5fdf_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392022.351 5819824.975 60.69 392038.147 5819837.384 60.69 392038.517 5819836.912 60.69 392039.92 5819836.743 60.69 392047.782 5819842.919 60.69 392047.951 5819844.323 60.69 392047.58 5819844.795 60.69 392069.817 5819862.264 60.69 392076.233 5819867.305 60.69 392076.604 5819866.833 60.69 392078.008 5819866.664 60.69 392081.938 5819869.752 60.69 392082.05 5819871.121 60.69 392081.737 5819871.628 60.69 392079.02 5819875.087 60.69 392077.616 5819875.256 60.69 392077.144 5819874.886 60.69 392059.188 5819897.754 60.69 392059.66 5819898.125 60.69 392059.779 5819899.122 60.69 392052.371 5819893.31 60.69 392048.081 5819889.945 60.69 392049.714 5819887.866 60.69 392051.302 5819885.843 60.69 392051.388 5819885.911 60.69 392061.439 5819873.226 60.69 392061.297 5819873.115 60.69 392042.016 5819857.969 60.69 392041.985 5819858.009 60.69 392041.455 5819857.593 60.69 392038.191 5819855.029 60.69 392030.407 5819848.914 60.69 392026.858 5819846.126 60.69 392026.497 5819845.842 60.69 392026.529 5819845.802 60.69 392013.835 5819835.831 60.69 392022.351 5819824.975 60.69</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_5df8a552-8ef1-4830-a0ff-5a1bc45f2df7_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392007.367 5819901.046 53.205 392007.898 5819901.462 53.205 392007.898 5819901.462 35.367 392027.103 5819916.527 35.367 392027.103 5819916.527 60.61 392007.898 5819901.462 60.61 392007.367 5819901.046 60.61 392007.367 5819901.046 53.205</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_c2ea1143-535b-4644-9585-9ce87f8aa9bd_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392026.356 5819880.849 60.806 392027.497 5819879.394 60.806 392027.497 5819879.394 35.367 392026.356 5819880.849 35.367 392024.505 5819883.209 35.367 392024.505 5819883.209 60.806 392026.356 5819880.849 60.806</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_cd2054fc-f1a2-4656-bcd2-3e50516eb9f2_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391991.354 5819909.958 35.367 391991.354 5819909.958 68.32 391991.154 5819911.834 68.32 391991.154 5819911.834 35.367 391991.354 5819909.958 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_b2495bf6-8b34-4ac9-be78-cda528359a38_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392004.378 5819810.095 35.367 392004.378 5819810.095 60.81 392003.991 5819810.552 60.81 392003.991 5819810.552 35.367 392004.378 5819810.095 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_3db1c703-cef6-4c36-aa5a-17abfa7276b9_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391991.756 5819867.696 53.205 391993.801 5819869.301 53.205 391998.126 5819872.696 53.205 392000.17 5819874.301 53.205 392002.176 5819875.876 53.205 392002.176 5819875.876 35.367 392000.17 5819874.301 35.367 391998.126 5819872.696 35.367 391993.801 5819869.301 35.367 391991.756 5819867.696 35.367 391991.756 5819867.696 53.205</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_1f17c1df-e95a-4e28-886d-0c3d5f3d2c40_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391955.495 5819874.966 35.367 391955.495 5819874.966 60.88 391955.023 5819874.596 60.88 391955.023 5819874.596 35.367 391955.495 5819874.966 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_a68f22e9-389c-45b4-b6ed-4c89f6b4d6cd_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391982.384 5819905.781 68.32 391986.275 5819900.821 68.32 391988.387 5819902.478 68.32 391997.711 5819909.793 68.32 391999.441 5819911.15 68.32 391996.939 5819914.34 68.32 391995.952 5819915.598 68.32 391995.581 5819916.07 68.32 391995.511 5819916.079 68.32 391994.178 5819916.24 68.32 391993.235 5819915.499 68.32 391993.074 5819914.167 68.32 391993.065 5819914.095 68.32 391993.435 5819913.623 68.32 391992.57 5819912.945 68.32 391991.154 5819911.834 68.32 391991.354 5819909.958 68.32 391988.208 5819907.49 68.32 391986.434 5819908.132 68.32 391985.018 5819907.021 68.32 391984.153 5819906.342 68.32 391983.783 5819906.814 68.32 391983.712 5819906.823 68.32 391982.38 5819906.984 68.32 391981.798 5819906.527 68.32 391982.384 5819905.781 68.32</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_3c2f2d40-6570-4dd2-828d-02efc56a579f_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392005.344 5819846.276 35.367 392005.344 5819846.276 60.521 392004.822 5819845.938 60.683 392004.822 5819845.938 35.367 392005.344 5819846.276 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_aa5c6105-5486-4044-86fe-6f328cd65fcd_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392001.998 5819850.742 35.367 392001.998 5819850.742 60.489 392005.344 5819846.276 60.521 392005.344 5819846.276 35.367 392001.998 5819850.742 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_2a8a1274-0417-4478-9c71-e8247ad32afa_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391995.581 5819916.07 35.367 391995.581 5819916.07 68.32 391995.952 5819915.598 68.32 391995.952 5819915.598 35.367 391995.581 5819916.07 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_355fca3c-8571-4329-b87f-a947fa6f75a3_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391980.579 5819842.017 35.367 391980.579 5819842.017 60.918 391979.16 5819843.827 60.918 391979.16 5819843.827 35.367 391980.579 5819842.017 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_ecd0f2ec-dffb-4cc6-8b60-bda5b1828be5_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391986.275 5819900.821 108.27 391988.387 5819902.478 108.27 391997.711 5819909.793 108.27 391997.711 5819909.793 68.32 391988.387 5819902.478 68.32 391986.275 5819900.821 68.32 391986.275 5819900.821 108.27</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_edd0810c-3037-4f29-a55e-38cc99224b00_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391999.441 5819911.15 60.61 392007.367 5819901.046 60.61 392007.898 5819901.462 60.61 392027.103 5819916.527 60.61 392035.249 5819906.284 60.61 392037.377 5819903.577 60.61 392038.769 5819901.804 60.61 392043.061 5819905.172 60.61 392050.695 5819911.16 60.61 392050.568 5819911.322 60.61 392049.164 5819911.491 60.61 392048.692 5819911.121 60.61 392030.735 5819933.989 60.61 392031.206 5819934.359 60.61 392031.376 5819935.763 60.61 392028.288 5819939.694 60.61 392026.884 5819939.864 60.61 392022.952 5819936.779 60.61 392022.782 5819935.375 60.61 392023.152 5819934.903 60.61 392016.733 5819929.868 60.61 391996.939 5819914.34 60.61 391999.441 5819911.15 60.61</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_ce6ec05b-f4b3-4d0e-922c-095c0589f856_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392041.455 5819857.593 53.205 392041.455 5819857.593 60.69 392041.985 5819858.009 60.69 392041.985 5819858.009 53.205 392041.455 5819857.593 53.205</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_ee2d4951-f0b4-49c0-b4d6-81109ee9a31f_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392079.02 5819875.087 35.367 392079.02 5819875.087 60.69 392081.737 5819871.628 60.69 392081.737 5819871.628 35.367 392079.02 5819875.087 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_5b441372-6d9a-4889-9ff6-057c039c3839_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391975.89 5819847.996 35.367 391975.89 5819847.996 60.918 391974.532 5819849.726 60.918 391974.532 5819849.726 35.367 391975.89 5819847.996 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_ff7ab273-2873-4e66-b036-bca5bf218923_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392009.109 5819868.954 61.75 392023.615 5819880.334 61.75 392018.961 5819886.266 61.09 392016.982 5819884.713 61.09 392015.696 5819883.704 61.09 392011.802 5819880.649 61.09 392007.908 5819877.595 61.09 392006.558 5819876.535 61.091 392004.481 5819874.906 61.09 392009.109 5819868.954 61.75</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_5dbf2552-e1e9-494a-be94-1a9e2f14c4fc_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392032 5819899.355 35.367 392033.33 5819897.661 35.367 392033.33 5819897.661 60.807 392032 5819899.355 60.811 392032 5819899.355 53.205 392030.49 5819901.277 53.205 392030.49 5819901.277 35.367 392032 5819899.355 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_bcf0d11e-cb3d-4cf5-aa9d-a9fe0b7d61b6_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392003.991 5819810.552 35.367 392003.991 5819810.552 60.81 392001.276 5819814.013 60.81 392001.276 5819814.013 35.367 392003.991 5819810.552 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_adad325e-18a8-4346-ab06-2ad7715bb50d_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392051.339 5819910.34 60.812 392051.339 5819910.34 35.367 392050.695 5819911.16 35.367 392050.568 5819911.322 35.367 392050.568 5819911.322 60.61 392050.695 5819911.16 60.61 392050.695 5819911.16 60.812 392051.339 5819910.34 60.812</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_16e1f2db-f1f1-4898-86c1-42eaf4e588dc_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392059.66 5819898.125 35.367 392059.66 5819898.125 60.69 392059.188 5819897.754 60.69 392059.188 5819897.754 35.367 392059.66 5819898.125 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_3c510991-be27-44d0-bf25-81289734e3dd_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391995.534 5819893.368 108.27 391996.519 5819900.131 117.04 391989.717 5819900.784 108.27 391995.534 5819893.368 108.27</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_1f64ffab-e831-4c6c-88ce-fc0dfc66fc8f_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392021.626 5819885.906 35.367 392021.626 5819885.906 60.806 392022.613 5819884.648 60.806 392022.613 5819884.648 35.367 392021.626 5819885.906 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_39087292-4c67-4efc-9054-6f55d6276ac2_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392081.938 5819869.752 35.367 392078.008 5819866.664 35.367 392076.604 5819866.833 35.367 392076.233 5819867.305 35.367 392069.817 5819862.264 35.367 392047.58 5819844.795 35.367 392047.951 5819844.323 35.367 392047.782 5819842.919 35.367 392039.92 5819836.743 35.367 392038.517 5819836.912 35.367 392038.147 5819837.384 35.367 392022.351 5819824.975 35.367 392009.493 5819814.875 35.367 392009.864 5819814.403 35.367 392009.695 5819812.999 35.367 392005.765 5819809.911 35.367 392004.378 5819810.095 35.367 392003.991 5819810.552 35.367 392001.276 5819814.013 35.367 392001.446 5819815.417 35.367 392001.918 5819815.787 35.367 391988.708 5819832.626 35.367 391988.116 5819833.381 35.367 391986.858 5819832.393 35.367 391985.875 5819832.513 35.367 391984.27 5819834.557 35.367 391984.389 5819835.54 35.367 391985.176 5819836.157 35.367 391981.937 5819840.287 35.367 391980.579 5819842.017 35.367 391979.16 5819843.827 35.367 391977.308 5819846.187 35.367 391975.89 5819847.996 35.367 391974.532 5819849.726 35.367 391971.293 5819853.856 35.367 391970.506 5819853.239 35.367 391969.523 5819853.357 35.367 391967.919 5819855.403 35.367 391968.038 5819856.385 35.367 391969.297 5819857.372 35.367 391969.096 5819857.628 35.367 391955.495 5819874.966 35.367 391955.023 5819874.596 35.367 391953.619 5819874.765 35.367 391950.534 5819878.698 35.367 391950.704 5819880.102 35.367 391954.636 5819883.187 35.367 391956.04 5819883.018 35.367 391956.411 5819882.546 35.367 391969.274 5819892.636 35.367 391982.58 5819903.074 35.367 391982.624 5819903.109 35.367 391981.636 5819904.368 35.367 391981.266 5819904.84 35.367 391981.275 5819904.91 35.367 391981.436 5819906.243 35.367 391981.798 5819906.527 35.367 391982.38 5819906.984 35.367 391983.712 5819906.823 35.367 391983.783 5819906.814 35.367 391984.153 5819906.342 35.367 391985.018 5819907.021 35.367 391986.434 5819908.132 35.367 391988.208 5819907.49 35.367 391991.354 5819909.958 35.367 391991.154 5819911.834 35.367 391992.57 5819912.945 35.367 391993.435 5819913.623 35.367 391993.065 5819914.095 35.367 391993.074 5819914.167 35.367 391993.235 5819915.499 35.367 391994.178 5819916.24 35.367 391995.511 5819916.079 35.367 391995.581 5819916.07 35.367 391995.952 5819915.598 35.367 391996.939 5819914.34 35.367 392016.733 5819929.868 35.367 392023.152 5819934.903 35.367 392022.782 5819935.375 35.367 392022.952 5819936.779 35.367 392026.884 5819939.864 35.367 392028.288 5819939.694 35.367 392031.376 5819935.763 35.367 392031.206 5819934.359 35.367 392030.735 5819933.989 35.367 392048.692 5819911.121 35.367 392049.164 5819911.491 35.367 392050.568 5819911.322 35.367 392050.695 5819911.16 35.367 392051.339 5819910.34 35.367 392050.71 5819909.846 35.367 392052.223 5819907.919 35.367 392052.852 5819908.413 35.367 392053.87 5819907.116 35.367 392056.526 5819903.735 35.367 392057.544 5819902.437 35.367 392056.916 5819901.943 35.367 392058.428 5819900.017 35.367 392059.057 5819900.511 35.367 392059.828 5819899.528 35.367 392059.779 5819899.122 35.367 392059.66 5819898.125 35.367 392059.188 5819897.754 35.367 392077.144 5819874.886 35.367 392077.616 5819875.256 35.367 392079.02 5819875.087 35.367 392081.737 5819871.628 35.367 392082.05 5819871.121 35.367 392081.938 5819869.752 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
									<gml:interior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392051.388 5819885.911 35.367 392051.302 5819885.843 35.367 392046.712 5819882.237 35.367 392045.309 5819882.406 35.367 392044.336 5819883.645 35.367 392042.703 5819885.725 35.367 392042.468 5819886.024 35.367 392042.638 5819887.427 35.367 392043.423 5819888.044 35.367 392040.799 5819891.386 35.367 392038.145 5819894.767 35.367 392035.52 5819898.109 35.367 392034.734 5819897.492 35.367 392033.466 5819897.645 35.367 392033.33 5819897.661 35.367 392032 5819899.355 35.367 392030.49 5819901.277 35.367 392030.659 5819902.681 35.367 392035.249 5819906.284 35.367 392027.103 5819916.527 35.367 392007.898 5819901.462 35.367 392018.108 5819888.448 35.367 392019.078 5819887.211 35.367 392020.493 5819888.322 35.367 392020.908 5819887.793 35.367 392021.728 5819886.748 35.367 392021.256 5819886.378 35.367 392021.626 5819885.906 35.367 392022.613 5819884.648 35.367 392022.892 5819884.293 35.367 392023.364 5819884.663 35.367 392024.505 5819883.209 35.367 392026.356 5819880.849 35.367 392027.497 5819879.394 35.367 392027.025 5819879.023 35.367 392027.303 5819878.67 35.367 392028.29 5819877.411 35.367 392028.66 5819876.939 35.367 392029.132 5819877.309 35.367 392030.215 5819875.929 35.367 392030.366 5819875.736 35.367 392028.951 5819874.625 35.367 392030.435 5819872.733 35.367 392041.985 5819858.009 35.367 392042.016 5819857.969 35.367 392061.297 5819873.115 35.367 392061.439 5819873.226 35.367 392051.388 5819885.911 35.367</gml:posList>
										</gml:LinearRing>
									</gml:interior>
									<gml:interior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392026.497 5819845.842 35.367 392022.491 5819850.95 35.367 392018.845 5819855.598 35.367 392018.354 5819856.224 35.367 392017.461 5819855.518 35.367 392017.088 5819855.958 35.367 392014.01 5819860.154 35.367 392014.912 5819860.612 35.367 392014.372 5819861.301 35.367 392011.894 5819859.357 35.367 392010.764 5819858.469 35.367 392009.85 5819857.752 35.367 392005.524 5819854.356 35.367 392003.48 5819852.751 35.367 392001.674 5819851.334 35.367 392001.003 5819850.807 35.367 392001.398 5819850.303 35.367 392001.998 5819850.742 35.367 392005.344 5819846.276 35.367 392004.822 5819845.938 35.367 392005.148 5819845.522 35.367 392013.164 5819835.304 35.367 392013.835 5819835.831 35.367 392026.529 5819845.802 35.367 392026.497 5819845.842 35.367</gml:posList>
										</gml:LinearRing>
									</gml:interior>
									<gml:interior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391999.946 5819879.694 35.367 391997.234 5819883.155 35.367 391992.84 5819888.761 35.367 391993.312 5819889.131 35.367 391992.881 5819889.681 35.367 391979.702 5819879.343 35.367 391978.56 5819878.447 35.367 391978.991 5819877.897 35.367 391979.463 5819878.267 35.367 391985.53 5819870.532 35.367 391989.28 5819865.751 35.367 391989.951 5819866.278 35.367 391991.756 5819867.696 35.367 391993.801 5819869.301 35.367 391998.126 5819872.696 35.367 392000.17 5819874.301 35.367 392002.176 5819875.876 35.367 392002.547 5819875.404 35.367 392003.019 5819875.775 35.367 391999.946 5819879.694 35.367</gml:posList>
										</gml:LinearRing>
									</gml:interior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_0cac2085-c326-45f0-b058-e1433d64fb2d_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392001.918 5819815.787 60.81 392001.446 5819815.417 60.81 392001.276 5819814.013 60.81 392003.991 5819810.552 60.81 392004.378 5819810.095 60.81 392005.765 5819809.911 60.81 392009.695 5819812.999 60.81 392009.864 5819814.403 60.81 392009.493 5819814.875 60.81 392022.351 5819824.975 60.81 392013.835 5819835.831 60.81 392013.164 5819835.304 60.81 392005.148 5819845.522 60.81 392004.766 5819845.223 60.81 391996.339 5819838.612 60.81 391988.708 5819832.626 60.81 392001.918 5819815.787 60.81</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_bb7a3411-e288-4461-87dd-4a6285ac15cd_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391985.875 5819832.513 35.367 391985.875 5819832.513 60.473 391984.27 5819834.557 60.473 391984.27 5819834.557 35.367 391985.875 5819832.513 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_1eb6ee90-951a-409e-9317-49acde93e8fb_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391985.018 5819907.021 35.367 391985.018 5819907.021 68.32 391986.434 5819908.132 68.32 391986.434 5819908.132 35.367 391985.018 5819907.021 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_a33d8b7c-efa1-4fe6-9f40-1d9afb066ef4_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392033.466 5819897.645 35.367 392034.734 5819897.492 35.367 392034.734 5819897.492 60.806 392033.466 5819897.645 60.807 392033.33 5819897.661 60.807 392033.33 5819897.661 35.367 392033.466 5819897.645 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_7ce09eca-d40c-4a50-9d5b-b01f2448cda4_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392081.737 5819871.628 35.367 392081.737 5819871.628 60.69 392082.05 5819871.121 60.69 392082.05 5819871.121 35.367 392081.737 5819871.628 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_042cb68a-3e03-4c9f-8ef7-e609efd40efe_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392051.339 5819910.34 35.367 392051.339 5819910.34 60.812 392050.71 5819909.846 60.836 392050.71 5819909.846 35.367 392051.339 5819910.34 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_4a9abc85-5200-40ef-857f-0d65f265fcfc_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392011.894 5819859.357 35.367 392011.894 5819859.357 53.205 392010.764 5819858.469 53.205 392010.764 5819858.469 35.367 392011.894 5819859.357 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_75ef012d-6101-4d4b-86f7-777ff706581d_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392030.49 5819901.277 35.367 392030.49 5819901.277 53.205 392030.659 5819902.681 53.205 392030.659 5819902.681 35.367 392030.49 5819901.277 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_f8bd4e69-d893-4cc5-9574-390770cea1ea_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391992.95 5819889.736 53.205 391992.95 5819889.736 60.691 391992.95 5819889.736 60.93 391992.881 5819889.681 60.93 391992.881 5819889.681 53.205 391992.95 5819889.736 53.205</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_16ef2a15-4f59-43b0-bc30-6f616668b4b9_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392022.782 5819935.375 35.367 392022.782 5819935.375 60.61 392022.952 5819936.779 60.61 392022.952 5819936.779 35.367 392022.782 5819935.375 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_d9781162-4fcc-44d1-ac45-26605009ddb4_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391979.702 5819879.343 60.88 391979.702 5819879.343 60.93 391969.274 5819892.636 60.93 391969.274 5819892.636 60.88 391979.702 5819879.343 60.88</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_172e8998-e4c4-4cd5-b84b-6b3b2d2a11c1_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391996.939 5819914.34 68.32 391999.441 5819911.15 68.32 391999.441 5819911.15 60.81 392007.367 5819901.046 60.81 392007.367 5819901.046 60.69 392017.577 5819888.031 60.69 392017.577 5819888.031 53.205 392007.367 5819901.046 53.205 392007.367 5819901.046 60.61 391999.441 5819911.15 60.61 391996.939 5819914.34 60.61 391996.939 5819914.34 35.367 391995.952 5819915.598 35.367 391995.952 5819915.598 68.32 391996.939 5819914.34 68.32</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_013031c0-488b-4113-b775-b8e9388c079f_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392022.613 5819884.648 35.367 392022.613 5819884.648 60.806 392022.892 5819884.293 60.806 392022.892 5819884.293 35.367 392022.613 5819884.648 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_bfcb53ca-f468-4d97-8672-a8c24bc6de5d_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392020.908 5819887.793 60.806 392021.728 5819886.748 60.806 392021.728 5819886.748 35.367 392020.908 5819887.793 35.367 392020.493 5819888.322 35.367 392020.493 5819888.322 60.807 392020.908 5819887.793 60.806</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_925a51b4-8f56-4d36-b8c3-689997d8ea83_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392040.799 5819891.386 35.367 392043.423 5819888.044 35.367 392043.423 5819888.044 60.806 392040.799 5819891.386 60.806 392038.145 5819894.767 60.806 392038.145 5819894.767 35.367 392040.799 5819891.386 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_5df225a2-d2a9-444f-a260-961b588f622a_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392015.696 5819883.704 61.35 392016.982 5819884.713 61.09 392018.961 5819886.266 60.69 392017.577 5819888.031 60.69 392007.367 5819901.046 60.69 392006.773 5819900.58 60.81 392005.638 5819899.689 61.04 392004.102 5819898.484 61.35 392015.696 5819883.704 61.35</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_fce54bb1-f37a-47e5-8dfc-9df4fa240f55_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392001.398 5819850.303 35.367 392001.398 5819850.303 60.683 392001.998 5819850.742 60.489 392001.998 5819850.742 35.367 392001.398 5819850.303 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_f244f2b7-6fc4-431f-b349-7ced245d2871_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392048.081 5819889.945 60.69 392048.081 5819889.945 60.808 392049.714 5819887.866 60.812 392049.714 5819887.866 60.69 392048.081 5819889.945 60.69</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_7fc296d1-b7da-4904-ba90-f031806e6874_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392014.372 5819861.301 35.367 392014.372 5819861.301 53.205 392011.894 5819859.357 53.205 392011.894 5819859.357 35.367 392014.372 5819861.301 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_37ce93af-c85f-46fb-ac42-be7e186fdf48_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391985.53 5819870.532 60.88 391985.53 5819870.532 60.683 391989.28 5819865.751 60.683 391989.28 5819865.751 35.367 391985.53 5819870.532 35.367 391979.463 5819878.267 35.367 391979.463 5819878.267 60.88 391985.53 5819870.532 60.88</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_adfb6319-7f8e-4e71-a8cf-33a50d2af525_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392052.371 5819893.31 61.111 392059.779 5819899.122 60.82 392059.828 5819899.528 60.812 392059.057 5819900.511 60.812 392058.428 5819900.017 60.836 392056.916 5819901.943 60.836 392057.544 5819902.437 60.812 392056.526 5819903.735 60.812 392053.87 5819907.116 60.812 392052.852 5819908.413 60.812 392052.223 5819907.919 60.836 392050.71 5819909.846 60.836 392051.339 5819910.34 60.812 392050.695 5819911.16 60.812 392043.061 5819905.172 61.111 392052.371 5819893.31 61.111</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_6385fba8-fee3-4082-a459-6537729eb720_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392017.215 5819865.731 61.804 392021.109 5819868.785 63.72 392025.003 5819871.84 61.81 392027.2 5819873.564 61.09 392025.003 5819871.84 61.09 392021.109 5819868.785 61.09 392017.215 5819865.731 61.09 392014.874 5819863.895 61.09 392017.215 5819865.731 61.804</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_8cc1bc68-929b-495e-9c81-dabcd34d2d2b_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392052.223 5819907.919 35.367 392052.223 5819907.919 60.836 392052.852 5819908.413 60.812 392052.852 5819908.413 35.367 392052.223 5819907.919 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_e8377c54-a872-4cc0-81fd-e1c0ce186fde_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392003.019 5819875.775 35.367 392003.019 5819875.775 53.205 391999.946 5819879.694 53.205 391999.946 5819879.694 35.367 392003.019 5819875.775 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_503c4d7c-70d5-48e1-84cb-137b6015feaa_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391969.096 5819857.628 61.075 391976.724 5819863.617 63.61 391984.938 5819870.066 60.88 391976.724 5819863.617 60.88 391969.096 5819857.628 60.88 391969.096 5819857.628 61.075</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_72cc9767-272a-4feb-a060-dd80469d422e_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392050.71 5819909.846 35.367 392050.71 5819909.846 60.836 392052.223 5819907.919 60.836 392052.223 5819907.919 35.367 392050.71 5819909.846 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_4852c484-1c4e-4ccb-ab57-af53fb1f6b7e_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391993.435 5819913.623 35.367 391993.435 5819913.623 68.32 391993.065 5819914.095 68.32 391993.065 5819914.095 35.367 391993.435 5819913.623 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_0597532b-476f-4729-a620-f44b9488d8fe_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392028.66 5819876.939 35.367 392028.66 5819876.939 60.806 392029.132 5819877.309 60.806 392029.132 5819877.309 35.367 392028.66 5819876.939 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_a8a57e07-3a6e-4616-acfd-bd122e3f7ff8_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392082.05 5819871.121 35.367 392082.05 5819871.121 60.69 392081.938 5819869.752 60.69 392081.938 5819869.752 35.367 392082.05 5819871.121 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_abb61c17-38f0-4ace-be13-d1f8230a9b9f_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392035.52 5819898.109 35.367 392035.52 5819898.109 60.806 392034.734 5819897.492 60.806 392034.734 5819897.492 35.367 392035.52 5819898.109 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_86b1385b-8bb6-47fc-aef5-c0a7030e65db_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392038.517 5819836.912 35.367 392038.517 5819836.912 60.69 392038.147 5819837.384 60.69 392038.147 5819837.384 35.367 392038.517 5819836.912 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_9d0451ed-6e72-4e6f-a58f-f674fbd19140_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392061.439 5819873.226 35.367 392061.439 5819873.226 60.69 392051.388 5819885.911 60.69 392051.388 5819885.911 35.367 392061.439 5819873.226 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_2c987b5e-0713-44b3-8ba1-131478162c9e_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391981.436 5819906.243 60.93 391981.798 5819906.527 60.93 391981.798 5819906.527 68.32 391982.38 5819906.984 68.32 391982.38 5819906.984 35.367 391981.798 5819906.527 35.367 391981.436 5819906.243 35.367 391981.436 5819906.243 60.93</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_ce813fe1-475f-4e6e-8c9a-edde8d4aabd0_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392004.766 5819845.223 60.81 392005.148 5819845.522 60.81 392005.148 5819845.522 60.683 392004.766 5819845.223 60.81</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_d1c2f443-c5fe-4440-93c1-fe0f6cc994d9_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392059.779 5819899.122 60.69 392059.66 5819898.125 60.69 392059.66 5819898.125 35.367 392059.779 5819899.122 35.367 392059.828 5819899.528 35.367 392059.828 5819899.528 60.812 392059.779 5819899.122 60.82 392059.779 5819899.122 60.69</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_9cfe4507-6870-4102-92f3-359603056bac_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392047.782 5819842.919 35.367 392047.782 5819842.919 60.69 392039.92 5819836.743 60.69 392039.92 5819836.743 35.367 392047.782 5819842.919 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_57ddf4e0-c831-4e5a-975e-b251c50238f8_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391986.434 5819908.132 35.367 391986.434 5819908.132 68.32 391988.208 5819907.49 68.32 391988.208 5819907.49 35.367 391986.434 5819908.132 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_a9dbe100-a098-48ce-b9b2-df7fc6f755bc_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391970.506 5819853.239 35.367 391970.506 5819853.239 60.656 391969.523 5819853.357 60.473 391969.523 5819853.357 35.367 391970.506 5819853.239 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_d1e54b48-9c2a-4eff-9f29-ba7d789f2f03_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392030.215 5819875.929 60.806 392030.366 5819875.736 60.806 392030.366 5819875.736 35.367 392030.215 5819875.929 35.367 392029.132 5819877.309 35.367 392029.132 5819877.309 60.806 392030.215 5819875.929 60.806</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_12e1376b-b07e-4d3c-851e-006d2ecd7440_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391999.441 5819911.15 60.81 391999.441 5819911.15 68.32 391997.711 5819909.793 68.32 391997.711 5819909.793 60.81 391999.441 5819911.15 60.81</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_0ff5860c-d74d-452a-8c98-ea12af6632e7_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391993.235 5819915.499 35.367 391993.235 5819915.499 68.32 391994.178 5819916.24 68.32 391994.178 5819916.24 35.367 391993.235 5819915.499 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_1238326d-325a-4cba-8d0b-ca46e4444243_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392031.376 5819935.763 35.367 392031.376 5819935.763 60.61 392031.206 5819934.359 60.61 392031.206 5819934.359 35.367 392031.376 5819935.763 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_a2193237-3d5e-42a9-aedf-491306d1527c_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392017.577 5819888.031 53.205 392018.108 5819888.448 53.205 392007.898 5819901.462 53.205 392007.367 5819901.046 53.205 392017.577 5819888.031 53.205</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_4841d8f7-3f66-4bb6-bfb6-e2c54d7fd2a0_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391993.312 5819889.131 35.367 391993.312 5819889.131 53.205 391992.881 5819889.681 53.205 391992.881 5819889.681 35.367 391993.312 5819889.131 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_8136ef23-9794-4fc0-a164-1608a69f2568_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391955.023 5819874.596 35.367 391955.023 5819874.596 60.88 391953.619 5819874.765 60.88 391953.619 5819874.765 35.367 391955.023 5819874.596 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_fb7d9580-9dd1-4c6f-ba18-d225318412dd_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392022.952 5819936.779 35.367 392022.952 5819936.779 60.61 392026.884 5819939.864 60.61 392026.884 5819939.864 35.367 392022.952 5819936.779 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_b5057583-bb26-4ff8-a788-c631918e4e76_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392009.695 5819812.999 35.367 392009.695 5819812.999 60.81 392005.765 5819809.911 60.81 392005.765 5819809.911 35.367 392009.695 5819812.999 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_818f2c49-e173-44df-9fe1-0fd3b163c9e4_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392035.249 5819906.284 35.367 392035.249 5819906.284 53.205 392035.249 5819906.284 60.61 392027.103 5819916.527 60.61 392027.103 5819916.527 35.367 392035.249 5819906.284 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_a8d281ad-6deb-4e16-be36-1a8eff349d9d_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391968.038 5819856.385 35.367 391968.038 5819856.385 60.656 391969.297 5819857.372 61.075 391969.297 5819857.372 35.367 391968.038 5819856.385 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_523906d6-c1fb-428e-b874-ac713150c1c2_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392002.176 5819875.876 35.367 392002.176 5819875.876 53.205 392002.547 5819875.404 53.205 392002.547 5819875.404 35.367 392002.176 5819875.876 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_ecb269c7-f29c-4a60-ac12-a4e6eba3004a_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">391984.27 5819834.557 35.367 391984.27 5819834.557 60.473 391984.389 5819835.54 60.656 391984.389 5819835.54 35.367 391984.27 5819834.557 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="DEB_LOD2_UUID_2bd330e6-dab8-4398-ae2c-419e62bc44f3_d085367f-a2d8-4a53-862e-45a4c04a5cc5_poly">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="3">392028.288 5819939.694 35.367 392028.288 5819939.694 60.61 392031.376 5819935.763 60.61 392031.376 5819935.763 35.367 392028.288 5819939.694 35.367</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
		</bldg:Building>
	</cityObjectMember>
</CityModel>