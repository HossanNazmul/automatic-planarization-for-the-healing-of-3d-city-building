<?xml version="1.0" encoding="UTF-8"?>
<core:CityModel xmlns:bldg="http://www.opengis.net/citygml/building/1.0" xmlns:luse="http://www.opengis.net/citygml/landuse/1.0" xmlns:app="http://www.opengis.net/citygml/appearance/1.0" xmlns:xAL="urn:oasis:names:tc:ciq:xsdschema:xAL:2.0" xmlns:base="http://www.citygml.org/citygml/profiles/base/1.0" xmlns:frn="http://www.opengis.net/citygml/cityfurniture/1.0" xmlns:smil20lang="http://www.w3.org/2001/SMIL20/Language" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:tex="http://www.opengis.net/citygml/texturedsurface/1.0" xmlns:wtr="http://www.opengis.net/citygml/waterbody/1.0" xmlns:smil20="http://www.w3.org/2001/SMIL20/" xmlns:grp="http://www.opengis.net/citygml/cityobjectgroup/1.0" xmlns:core="http://www.opengis.net/citygml/1.0" xmlns:veg="http://www.opengis.net/citygml/vegetation/1.0" xmlns:sch="http://www.ascc.net/xml/schematron" xmlns:gen="http://www.opengis.net/citygml/generics/1.0" xmlns:dem="http://www.opengis.net/citygml/relief/1.0" xmlns:gml="http://www.opengis.net/gml" xmlns:tran="http://www.opengis.net/citygml/transportation/1.0">
<gml:boundedBy>
<gml:Envelope srsDimension="3">
<gml:lowerCorner>72605.709 350978.432 259.373</gml:lowerCorner>
<gml:upperCorner>72616.787 350989.208 262.105</gml:upperCorner>
</gml:Envelope>
</gml:boundedBy>
<core:cityObjectMember>
<bldg:Building gml:id="ID_32063">
<gml:name>ID_32063</gml:name>
<gen:stringAttribute name="citygrid_UnitID">
<gen:value>ID_32063</gen:value>
</gen:stringAttribute>
<bldg:lod3Solid>
<gml:Solid srsDimension="3">
<gml:exterior>
<gml:CompositeSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_999ba67d-4418-4bc4-9ec7730d46fd76d8">
<gml:exterior>
<gml:LinearRing gml:id="UUID_6e1d70f9-4a31-4ea2-a89d718e8ac00e61">
<gml:posList>72605.709 350984.727 262.101 72605.903 350984.502 262.105 72612.782 350978.432 262.101 72616.787 350982.97 261.379 72609.717 350989.208 261.384 72609.414 350988.865 261.438 72605.709 350984.727 262.101</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_62886aa2-fa80-48c9-b32f3ea6f5f8e952">
<gml:exterior>
<gml:LinearRing gml:id="UUID_1b844857-e9a6-4edc-80237aebf9b5220f">
<gml:posList>72609.717 350989.208 261.384 72609.717 350989.208 259.373 72609.414 350988.865 259.373 72605.709 350984.727 259.373 72605.709 350984.727 262.101 72609.414 350988.865 261.438 72609.717 350989.208 261.384</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_0e28c855-1f7f-409c-a873ee2d69a8f743">
<gml:exterior>
<gml:LinearRing gml:id="UUID_47b49483-834f-4bef-b77478fafc23542c">
<gml:posList>72605.709 350984.727 262.101 72605.709 350984.727 259.373 72605.903 350984.502 259.373 72605.903 350984.502 262.105 72605.709 350984.727 262.101</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_7d8b29ef-05a4-4a18-a5eba22b6ca7560c">
<gml:exterior>
<gml:LinearRing gml:id="UUID_ea78927d-a64f-434b-a6e3e3a749033ea8">
<gml:posList>72605.903 350984.502 262.105 72605.903 350984.502 259.373 72612.782 350978.432 259.373 72612.782 350978.432 262.101 72605.903 350984.502 262.105</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_bdafd5a1-8905-4a49-824ec24492b449c9">
<gml:exterior>
<gml:LinearRing gml:id="UUID_f1391d28-cb11-412e-b4faa6d70dcce5d4">
<gml:posList>72612.782 350978.432 262.101 72612.782 350978.432 259.373 72616.787 350982.97 259.373 72616.787 350982.97 261.379 72612.782 350978.432 262.101</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_305923da-2bdd-4220-a4504ededa470d52">
<gml:exterior>
<gml:LinearRing gml:id="UUID_95e462a4-6f9b-4786-9c50e28c176b5d17">
<gml:posList>72616.787 350982.97 261.379 72616.787 350982.97 259.373 72609.717 350989.208 259.373 72609.717 350989.208 261.384 72616.787 350982.97 261.379</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="UUID_964890e8-0972-4590-bfa09d440b2b41db">
<gml:exterior>
<gml:LinearRing gml:id="UUID_3a612886-04fd-4f8d-86eed3492e6dfe7d">
<gml:posList>72609.414 350988.865 259.373 72609.717 350989.208 259.373 72616.787 350982.97 259.373 72612.782 350978.432 259.373 72605.903 350984.502 259.373 72605.709 350984.727 259.373 72609.414 350988.865 259.373</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:CompositeSurface>
</gml:exterior>
</gml:Solid>
</bldg:lod3Solid>
</bldg:Building>
</core:cityObjectMember>
</core:CityModel>
