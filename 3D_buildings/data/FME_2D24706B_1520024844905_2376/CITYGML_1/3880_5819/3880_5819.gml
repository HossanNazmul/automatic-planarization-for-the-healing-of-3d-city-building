<?xml version="1.0" encoding="UTF-8"?>
<CityModel xmlns:bldg="http://www.opengis.net/citygml/building/1.0" xmlns:app="http://www.opengis.net/citygml/appearance/1.0" xmlns:smil20="http://www.w3.org/2001/SMIL20/" xmlns:grp="http://www.opengis.net/citygml/cityobjectgroup/1.0" xmlns:base="http://www.citygml.org/citygml/profiles/base/1.0" xmlns:frn="http://www.opengis.net/citygml/cityfurniture/1.0" xmlns:smil20lang="http://www.w3.org/2001/SMIL20/Language" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:wtr="http://www.opengis.net/citygml/waterbody/1.0" xmlns:tex="http://www.opengis.net/citygml/texturedsurface/1.0" xmlns:veg="http://www.opengis.net/citygml/vegetation/1.0" xmlns:sch="http://www.ascc.net/xml/schematron" xmlns:gen="http://www.opengis.net/citygml/generics/1.0" xmlns:dem="http://www.opengis.net/citygml/relief/1.0" xmlns:gml="http://www.opengis.net/gml" xmlns:tran="http://www.opengis.net/citygml/transportation/1.0" xmlns:xAL="urn:oasis:names:tc:ciq:xsdschema:xAL:2.0" xmlns:luse="http://www.opengis.net/citygml/landuse/1.0" xmlns="http://www.opengis.net/citygml/1.0">
	<gml:boundedBy>
		<gml:Envelope srsName="EPSG:25833" srsDimension="3">
			<gml:lowerCorner>388026.660031001 5819531.58046452 36.1399993896484</gml:lowerCorner>
			<gml:upperCorner>388053.888191965 5819558.84700281 98.5662508010864</gml:upperCorner>
		</gml:Envelope>
	</gml:boundedBy>
	<cityObjectMember>
		<bldg:Building gml:id="BLDG_0003000f0010a95a">
			<gml:name>Siegessäule</gml:name>
			<bldg:roofType>1000</bldg:roofType>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="GEOM_6948556">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388040.350640367 5819542.20662375 98.5662508010864 388040.786761151 5819542.24595615 98.5662508010864 388041.352157223 5819542.38598371 98.5662508010864 388041.884648006 5819542.62206352 98.5662508010864 388042.368054037 5819542.94702241 98.5662508010864 388042.787687268 5819543.35098666 98.5662508010864 388043.130797358 5819543.82168204 98.5662508010864 388043.386959079 5819544.34480669 98.5662508010864 388043.548389089 5819544.90446575 98.5662508010864 388043.610182417 5819545.48365424 98.5662508010864 388043.570461506 5819546.06477383 98.5662508010864 388043.430433255 5819546.6301675 98.5662508010864 388043.194352351 5819547.16265603 98.5662508010864 388042.869391992 5819547.64606004 98.5662508010864 388042.465425935 5819548.06569154 98.5662508010864 388041.994728483 5819548.40880024 98.5662508010864 388041.47160154 5819548.66496095 98.5662508010864 388040.911940052 5819548.82639036 98.5662508010864 388040.332749827 5819548.88818351 98.5662508010864 388039.751626961 5819548.84846288 98.5662508010864 388039.186230885 5819548.70843533 98.5662508010864 388038.653740095 5819548.47235553 98.5662508010864 388038.170334055 5819548.14739664 98.5662508010864 388037.750700814 5819547.74343239 98.5662508010864 388037.407590715 5819547.27273701 98.5662508010864 388037.151428986 5819546.74961235 98.5662508010864 388036.989998971 5819546.18995329 98.5662508010864 388036.92820564 5819545.61076479 98.5662508010864 388036.967926553 5819545.02964518 98.5662508010864 388037.107954809 5819544.46425151 98.5662508010864 388037.34403572 5819543.93176297 98.5662508010864 388037.668996088 5819543.44835896 98.5662508010864 388038.072962155 5819543.02872746 98.5662508010864 388038.543659616 5819542.68561877 98.5662508010864 388039.066786568 5819542.42945806 98.5662508010864 388039.62644806 5819542.26802865 98.5662508010864 388040.205639063 5819542.2062355 98.5662508010864 388040.350640367 5819542.20662375 98.5662508010864</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="GEOM_6948554">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388047.194749935 5819558.84700281 43.1977320994831 388031.032699293 5819557.37437505 43.1977320994831 388031.171968891 5819555.84388764 43.1977320994831 388027.902815876 5819555.54025952 43.1977320994831 388028.194046504 5819552.27379761 43.1977320994831 388026.660181292 5819552.13697317 43.1977320994831 388028.101613463 5819535.95477361 43.1977320994831 388029.63468555 5819536.09161319 43.1977320994831 388029.93277483 5819532.82636351 43.1977320994831 388033.200407052 5819533.11421383 43.1977320994831 388033.33900862 5819531.58068762 43.1977320994831 388049.528315033 5819533.04559624 43.1977320994831 388049.389690266 5819534.57790222 43.1977320994831 388052.654583569 5819534.8789872 43.1977320994831 388052.354822558 5819538.14939508 43.1977320994831 388053.888191965 5819538.28903639 43.1977320994831 388052.410154762 5819554.47193135 43.1977320994831 388050.886627475 5819554.33314038 43.1977320994831 388050.601551796 5819557.59911885 43.1977320994831 388047.334586668 5819557.31424654 43.1977320994831 388047.194749935 5819558.84700281 43.1977320994831</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
									<gml:interior>
										<gml:LinearRing>
											<gml:posList>388040.041865498 5819537.79986602 43.1977320994831 388038.874813867 5819537.93782756 43.1977320994831 388037.57672638 5819538.31224939 43.1977320994831 388036.363377668 5819538.90639247 43.1977320994831 388035.271634718 5819539.70220406 43.1977320994831 388034.334669591 5819540.67550381 43.1977320994831 388033.5809515 5819541.79671852 43.1977320994831 388033.03338179 5819543.03178064 43.1977320994831 388032.708598091 5819544.34316344 43.1977320994831 388032.616468793 5819545.69102119 43.1977320994831 388032.759793198 5819547.03439994 43.1977320994831 388033.134216466 5819548.33248179 43.1977320994831 388033.728361934 5819549.54582518 43.1977320994831 388034.524176792 5819550.63756329 43.1977320994831 388035.497480605 5819551.5745242 43.1977320994831 388036.618700027 5819552.32823883 43.1977320994831 388037.853767371 5819552.87580595 43.1977320994831 388039.165155738 5819553.200588 43.1977320994831 388040.51314636 5819553.29271423 43.1977320994831 388041.856403774 5819553.14939263 43.1977320994831 388043.154491234 5819552.77497075 43.1977320994831 388044.367839905 5819552.18082764 43.1977320994831 388045.459582804 5819551.38501604 43.1977320994831 388046.396547878 5819550.41171629 43.1977320994831 388047.150265921 5819549.2905016 43.1977320994831 388047.697835592 5819548.05543951 43.1977320994831 388048.022619267 5819546.74405676 43.1977320994831 388048.114748558 5819545.39619905 43.1977320994831 388047.971424163 5819544.05282035 43.1977320994831 388047.597000923 5819542.75473854 43.1977320994831 388047.002855496 5819541.54139517 43.1977320994831 388046.207040688 5819540.44965708 43.1977320994831 388045.233736927 5819539.51269617 43.1977320994831 388044.112517555 5819538.75898152 43.1977320994831 388042.87745025 5819538.21141437 43.1977320994831 388041.566061907 5819537.88663228 43.1977320994831 388040.218198396 5819537.79450358 43.1977320994831 388040.041865498 5819537.79986602 43.1977320994831</gml:posList>
										</gml:LinearRing>
									</gml:interior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="GEOM_6948555">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388040.394904586 5819537.7934896 53.7984091079596 388041.56629032 5819537.88696602 53.7984091079596 388042.877682483 5819538.21174907 53.7984091079596 388044.112753385 5819538.75931781 53.7984091079596 388045.233976024 5819539.51303466 53.7984091079596 388046.207282619 5819540.4499983 53.7984091079596 388047.003099745 5819541.54173957 53.7984091079596 388047.597246902 5819542.75508646 53.7984091079596 388047.971671233 5819544.05317205 53.7984091079596 388048.114996045 5819545.39655467 53.7984091079596 388048.022866485 5819546.7444163 53.7984091079596 388047.698081865 5819548.05580287 53.7984091079596 388047.150510599 5819549.29086856 53.7984091079596 388046.396790362 5819550.41208651 53.7984091079596 388045.459822558 5819551.3853891 53.7984091079596 388044.368076478 5819552.18120302 53.7984091079596 388043.154724273 5819552.77534786 53.7984091079596 388041.856633033 5819553.14977083 53.7984091079596 388040.513245332 5819553.29309526 53.7984091079596 388039.165377158 5819553.20096635 53.7984091079596 388037.853984971 5819552.87618335 53.7984091079596 388036.61891403 5819552.32861464 53.7984091079596 388035.497691343 5819551.57489781 53.7984091079596 388034.524384695 5819550.63793417 53.7984091079596 388033.728567519 5819549.54619289 53.7984091079596 388033.13442032 5819548.33284596 53.7984091079596 388032.759995961 5819547.03476033 53.7984091079596 388032.616671139 5819545.69137767 53.7984091079596 388032.708800705 5819544.34351599 53.7984091079596 388033.03358535 5819543.03212938 53.7984091079596 388033.581156655 5819541.79706366 53.7984091079596 388034.334876941 5819540.67584568 53.7984091079596 388035.271844798 5819539.70254309 53.7984091079596 388036.363590927 5819538.90672919 53.7984091079596 388037.576943174 5819538.31258438 53.7984091079596 388038.875034442 5819537.93816145 53.7984091079596 388040.218422883 5819537.79483705 53.7984091079596 388040.394904586 5819537.7934896 53.7984091079596</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948540">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388042.868066902 5819547.64409301 36.3400001525879 388042.869391992 5819547.64606004 98.5662508010864 388043.194352351 5819547.16265603 98.5662508010864 388043.19302726 5819547.160689 36.3400001525879 388042.868066902 5819547.64409301 36.3400001525879</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948523">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388037.342710637 5819543.92979595 36.3400001525879 388037.34403572 5819543.93176297 98.5662508010864 388037.107954809 5819544.46425151 98.5662508010864 388037.106629726 5819544.46228448 36.3400001525879 388037.342710637 5819543.92979595 36.3400001525879</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948535">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388040.33142474 5819548.88621648 36.3400001525879 388040.332749827 5819548.88818351 98.5662508010864 388040.911940052 5819548.82639036 98.5662508010864 388040.910614964 5819548.82442333 36.3400001525879 388040.33142474 5819548.88621648 36.3400001525879</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948548">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388042.786362178 5819543.34901964 36.3400001525879 388042.787687268 5819543.35098666 98.5662508010864 388042.368054037 5819542.94702241 98.5662508010864 388042.366728947 5819542.94505538 36.3400001525879 388042.786362178 5819543.34901964 36.3400001525879</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948527">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388036.988673888 5819546.18798626 36.3400001525879 388036.989998971 5819546.18995329 98.5662508010864 388037.151428986 5819546.74961235 98.5662508010864 388037.150103903 5819546.74764532 36.3400001525879 388036.988673888 5819546.18798626 36.3400001525879</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948529">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388037.406265632 5819547.27076998 36.3400001525879 388037.407590715 5819547.27273701 98.5662508010864 388037.750700814 5819547.74343239 98.5662508010864 388037.74937573 5819547.74146535 36.3400001525879 388037.406265632 5819547.27076998 36.3400001525879</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948539">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388042.464100846 5819548.0637245 36.3400001525879 388042.465425935 5819548.06569154 98.5662508010864 388042.869391992 5819547.64606004 98.5662508010864 388042.868066902 5819547.64409301 36.3400001525879 388042.464100846 5819548.0637245 36.3400001525879</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948541">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388043.19302726 5819547.160689 36.3400001525879 388043.194352351 5819547.16265603 98.5662508010864 388043.430433255 5819546.6301675 98.5662508010864 388043.429108164 5819546.62820047 36.3400001525879 388043.19302726 5819547.160689 36.3400001525879</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948542">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388043.429108164 5819546.62820047 36.3400001525879 388043.430433255 5819546.6301675 98.5662508010864 388043.570461506 5819546.06477383 98.5662508010864 388043.569136415 5819546.0628068 36.3400001525879 388043.429108164 5819546.62820047 36.3400001525879</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948474">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388034.334519299 5819540.67528071 36.1399993896484 388034.334669591 5819540.67550381 43.1977320994831 388035.271634718 5819539.70220406 43.1977320994831 388035.271484426 5819539.70198095 36.1399993896484 388034.334519299 5819540.67528071 36.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948543">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388043.569136415 5819546.0628068 36.3400001525879 388043.570461506 5819546.06477383 98.5662508010864 388043.610182417 5819545.48365424 98.5662508010864 388043.608857327 5819545.48168721 36.3400001525879 388043.569136415 5819546.0628068 36.3400001525879</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948446">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388044.112367261 5819538.75875842 36.1399993896484 388044.112517555 5819538.75898152 43.1977320994831 388045.233736927 5819539.51269617 43.1977320994831 388045.233586634 5819539.51247307 36.1399993896484 388044.112367261 5819538.75875842 36.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948501">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388044.367703639 5819552.18064956 36.2900009155273 388044.368076478 5819552.18120302 53.7984091079596 388045.459822558 5819551.3853891 53.7984091079596 388045.459449718 5819551.38483564 36.2900009155273 388044.367703639 5819552.18064956 36.2900009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948544">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388043.608857327 5819545.48168721 36.3400001525879 388043.610182417 5819545.48365424 98.5662508010864 388043.548389089 5819544.90446575 98.5662508010864 388043.547063998 5819544.90249872 36.3400001525879 388043.608857327 5819545.48168721 36.3400001525879</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948545">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388043.547063998 5819544.90249872 36.3400001525879 388043.548389089 5819544.90446575 98.5662508010864 388043.386959079 5819544.34480669 98.5662508010864 388043.385633989 5819544.34283966 36.3400001525879 388043.547063998 5819544.90249872 36.3400001525879</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948428">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388052.354672263 5819538.14917197 36.1399993896484 388052.354822558 5819538.14939508 43.1977320994831 388052.654583569 5819534.8789872 43.1977320994831 388052.654433274 5819534.8787641 36.1399993896484 388052.354672263 5819538.14917197 36.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948546">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388043.385633989 5819544.34283966 36.3400001525879 388043.386959079 5819544.34480669 98.5662508010864 388043.130797358 5819543.82168204 98.5662508010864 388043.129472268 5819543.81971501 36.3400001525879 388043.385633989 5819544.34283966 36.3400001525879</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948547">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388043.129472268 5819543.81971501 36.3400001525879 388043.130797358 5819543.82168204 98.5662508010864 388042.787687268 5819543.35098666 98.5662508010864 388042.786362178 5819543.34901964 36.3400001525879 388043.129472268 5819543.81971501 36.3400001525879</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948476">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388036.363227375 5819538.90616937 36.1399993896484 388036.363377668 5819538.90639247 43.1977320994831 388037.57672638 5819538.31224939 43.1977320994831 388037.576576087 5819538.31202629 36.1399993896484 388036.363227375 5819538.90616937 36.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948530">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388037.74937573 5819547.74146535 36.3400001525879 388037.750700814 5819547.74343239 98.5662508010864 388038.170334055 5819548.14739664 98.5662508010864 388038.169008971 5819548.14542961 36.3400001525879 388037.74937573 5819547.74146535 36.3400001525879</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948493">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388034.524011859 5819550.63738071 36.2900009155273 388034.524384695 5819550.63793417 53.7984091079596 388035.497691343 5819551.57489781 53.7984091079596 388035.497318506 5819551.57434435 36.2900009155273 388034.524011859 5819550.63738071 36.2900009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948549">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388042.366728947 5819542.94505538 36.3400001525879 388042.368054037 5819542.94702241 98.5662508010864 388041.884648006 5819542.62206352 98.5662508010864 388041.883322917 5819542.6200965 36.3400001525879 388042.366728947 5819542.94505538 36.3400001525879</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948427">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388053.888041669 5819538.28881329 36.1399993896484 388053.888191965 5819538.28903639 43.1977320994831 388052.354822558 5819538.14939508 43.1977320994831 388052.354672263 5819538.14917197 36.1399993896484 388053.888041669 5819538.28881329 36.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948430">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388049.389539972 5819534.57767912 36.1399993896484 388049.389690266 5819534.57790222 43.1977320994831 388049.528315033 5819533.04559624 43.1977320994831 388049.528164739 5819533.04537313 36.1399993896484 388049.389539972 5819534.57767912 36.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948524">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388037.106629726 5819544.46228448 36.3400001525879 388037.107954809 5819544.46425151 98.5662508010864 388036.967926553 5819545.02964518 98.5662508010864 388036.96660147 5819545.02767815 36.3400001525879 388037.106629726 5819544.46228448 36.3400001525879</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948550">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388041.883322917 5819542.6200965 36.3400001525879 388041.884648006 5819542.62206352 98.5662508010864 388041.352157223 5819542.38598371 98.5662508010864 388041.350832135 5819542.38401669 36.3400001525879 388041.883322917 5819542.6200965 36.3400001525879</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948551">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388041.350832135 5819542.38401669 36.3400001525879 388041.352157223 5819542.38598371 98.5662508010864 388040.786761151 5819542.24595615 98.5662508010864 388040.785436064 5819542.24398912 36.3400001525879 388041.350832135 5819542.38401669 36.3400001525879</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948552">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388040.785436064 5819542.24398912 36.3400001525879 388040.786761151 5819542.24595615 98.5662508010864 388040.350640367 5819542.20662375 98.5662508010864 388040.34931528 5819542.20465672 36.3400001525879 388040.785436064 5819542.24398912 36.3400001525879</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948481">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388038.874661604 5819537.937608 36.2900009155273 388038.875034442 5819537.93816145 53.7984091079596 388037.576943174 5819538.31258438 53.7984091079596 388037.576570336 5819538.31203092 36.2900009155273 388038.874661604 5819537.937608 36.2900009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948426">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388052.410004467 5819554.47170824 36.1399993896484 388052.410154762 5819554.47193135 43.1977320994831 388053.888191965 5819538.28903639 43.1977320994831 388053.888041669 5819538.28881329 36.1399993896484 388052.410004467 5819554.47170824 36.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948425">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388050.88647718 5819554.33291727 36.1399993896484 388050.886627475 5819554.33314038 43.1977320994831 388052.410154762 5819554.47193135 43.1977320994831 388052.410004467 5819554.47170824 36.1399993896484 388050.88647718 5819554.33291727 36.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948475">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388035.271484426 5819539.70198095 36.1399993896484 388035.271634718 5819539.70220406 43.1977320994831 388036.363377668 5819538.90639247 43.1977320994831 388036.363227375 5819538.90616937 36.1399993896484 388035.271484426 5819539.70198095 36.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948422">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388047.194599641 5819558.8467797 36.1399993896484 388047.194749935 5819558.84700281 43.1977320994831 388047.334586668 5819557.31424654 43.1977320994831 388047.334436373 5819557.31402343 36.1399993896484 388047.194599641 5819558.8467797 36.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948424">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388050.601401502 5819557.59889575 36.1399993896484 388050.601551796 5819557.59911885 43.1977320994831 388050.886627475 5819554.33314038 43.1977320994831 388050.88647718 5819554.33291727 36.1399993896484 388050.601401502 5819557.59889575 36.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948423">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388047.334436373 5819557.31402343 36.1399993896484 388047.334586668 5819557.31424654 43.1977320994831 388050.601551796 5819557.59911885 43.1977320994831 388050.601401502 5819557.59889575 36.1399993896484 388047.334436373 5819557.31402343 36.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948500">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388043.154351434 5819552.77479439 36.2900009155273 388043.154724273 5819552.77534786 53.7984091079596 388044.368076478 5819552.18120302 53.7984091079596 388044.367703639 5819552.18064956 36.2900009155273 388043.154351434 5819552.77479439 36.2900009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948477">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388037.576576087 5819538.31202629 36.1399993896484 388037.57672638 5819538.31224939 43.1977320994831 388038.874813867 5819537.93782756 43.1977320994831 388038.874663574 5819537.93760445 36.1399993896484 388037.576576087 5819538.31202629 36.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948490">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388032.759623126 5819547.03420687 36.2900009155273 388032.759995961 5819547.03476033 53.7984091079596 388033.13442032 5819548.33284596 53.7984091079596 388033.134047484 5819548.3322925 36.2900009155273 388032.759623126 5819547.03420687 36.2900009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948431">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388049.528164739 5819533.04537313 36.1399993896484 388049.528315033 5819533.04559624 43.1977320994831 388033.33900862 5819531.58068762 43.1977320994831 388033.338858328 5819531.58046452 36.1399993896484 388049.528164739 5819533.04537313 36.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948525">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388036.96660147 5819545.02767815 36.3400001525879 388036.967926553 5819545.02964518 98.5662508010864 388036.92820564 5819545.61076479 98.5662508010864 388036.926880558 5819545.60879775 36.3400001525879 388036.96660147 5819545.02767815 36.3400001525879</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948437">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388026.660031001 5819552.13675007 36.1399993896484 388026.660181292 5819552.13697317 43.1977320994831 388028.194046504 5819552.27379761 43.1977320994831 388028.193896212 5819552.27357451 36.1399993896484 388026.660031001 5819552.13675007 36.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948526">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388036.926880558 5819545.60879775 36.3400001525879 388036.92820564 5819545.61076479 98.5662508010864 388036.989998971 5819546.18995329 98.5662508010864 388036.988673888 5819546.18798626 36.3400001525879 388036.926880558 5819545.60879775 36.3400001525879</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948538">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388041.993403394 5819548.4068332 36.3400001525879 388041.994728483 5819548.40880024 98.5662508010864 388042.465425935 5819548.06569154 98.5662508010864 388042.464100846 5819548.0637245 36.3400001525879 388041.993403394 5819548.4068332 36.3400001525879</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948489">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388032.616298303 5819545.69082421 36.2900009155273 388032.616671139 5819545.69137767 53.7984091079596 388032.759995961 5819547.03476033 53.7984091079596 388032.759623126 5819547.03420687 36.2900009155273 388032.616298303 5819545.69082421 36.2900009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948488">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388032.70842787 5819544.34296253 36.2900009155273 388032.708800705 5819544.34351599 53.7984091079596 388032.616671139 5819545.69137767 53.7984091079596 388032.616298303 5819545.69082421 36.2900009155273 388032.70842787 5819544.34296253 36.2900009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948445">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388042.877299956 5819538.21119127 36.1399993896484 388042.87745025 5819538.21141437 43.1977320994831 388044.112517555 5819538.75898152 43.1977320994831 388044.112367261 5819538.75875842 36.1399993896484 388042.877299956 5819538.21119127 36.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948487">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388033.033212514 5819543.03157592 36.2900009155273 388033.03358535 5819543.03212938 53.7984091079596 388032.708800705 5819544.34351599 53.7984091079596 388032.70842787 5819544.34296253 36.2900009155273 388033.033212514 5819543.03157592 36.2900009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948432">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388033.338858328 5819531.58046452 36.1399993896484 388033.33900862 5819531.58068762 43.1977320994831 388033.200407052 5819533.11421383 43.1977320994831 388033.20025676 5819533.11399073 36.1399993896484 388033.338858328 5819531.58046452 36.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948492">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388033.728194683 5819549.54563942 36.2900009155273 388033.728567519 5819549.54619289 53.7984091079596 388034.524384695 5819550.63793417 53.7984091079596 388034.524011859 5819550.63738071 36.2900009155273 388033.728194683 5819549.54563942 36.2900009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948435">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388029.634535258 5819536.09139009 36.1399993896484 388029.63468555 5819536.09161319 43.1977320994831 388028.101613463 5819535.95477361 43.1977320994831 388028.101463171 5819535.95455051 36.1399993896484 388029.634535258 5819536.09139009 36.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948433">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388033.20025676 5819533.11399073 36.1399993896484 388033.200407052 5819533.11421383 43.1977320994831 388029.93277483 5819532.82636351 43.1977320994831 388029.932624538 5819532.82614041 36.1399993896484 388033.20025676 5819533.11399073 36.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948434">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388029.932624538 5819532.82614041 36.1399993896484 388029.93277483 5819532.82636351 43.1977320994831 388029.63468555 5819536.09161319 43.1977320994831 388029.634535258 5819536.09139009 36.1399993896484 388029.932624538 5819532.82614041 36.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948491">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388033.134047484 5819548.3322925 36.2900009155273 388033.13442032 5819548.33284596 53.7984091079596 388033.728567519 5819549.54619289 53.7984091079596 388033.728194683 5819549.54563942 36.2900009155273 388033.134047484 5819548.3322925 36.2900009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948436">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388028.101463171 5819535.95455051 36.1399993896484 388028.101613463 5819535.95477361 43.1977320994831 388026.660181292 5819552.13697317 43.1977320994831 388026.660031001 5819552.13675007 36.1399993896484 388028.101463171 5819535.95455051 36.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948494">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388035.497318506 5819551.57434435 36.2900009155273 388035.497691343 5819551.57489781 53.7984091079596 388036.61891403 5819552.32861464 53.7984091079596 388036.618541193 5819552.32806117 36.2900009155273 388035.497318506 5819551.57434435 36.2900009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948444">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388041.565911614 5819537.88640917 36.1399993896484 388041.566061907 5819537.88663228 43.1977320994831 388042.87745025 5819538.21141437 43.1977320994831 388042.877299956 5819538.21119127 36.1399993896484 388041.565911614 5819537.88640917 36.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948440">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388031.171818599 5819555.84366454 36.1399993896484 388031.171968891 5819555.84388764 43.1977320994831 388031.032699293 5819557.37437505 43.1977320994831 388031.032549001 5819557.37415195 36.1399993896484 388031.171818599 5819555.84366454 36.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948499">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388041.856260194 5819553.14921736 36.2900009155273 388041.856633033 5819553.14977083 53.7984091079596 388043.154724273 5819552.77534786 53.7984091079596 388043.154351434 5819552.77479439 36.2900009155273 388041.856260194 5819553.14921736 36.2900009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948479">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388040.394531748 5819537.79293614 36.2900009155273 388040.394904586 5819537.7934896 53.7984091079596 388040.218422883 5819537.79483705 53.7984091079596 388040.218050045 5819537.7942836 36.2900009155273 388040.394531748 5819537.79293614 36.2900009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948438">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388028.193896212 5819552.27357451 36.1399993896484 388028.194046504 5819552.27379761 43.1977320994831 388027.902815876 5819555.54025952 43.1977320994831 388027.902665584 5819555.54003642 36.1399993896484 388028.193896212 5819552.27357451 36.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948439">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388027.902665584 5819555.54003642 36.1399993896484 388027.902815876 5819555.54025952 43.1977320994831 388031.171968891 5819555.84388764 43.1977320994831 388031.171818599 5819555.84366454 36.1399993896484 388027.902665584 5819555.54003642 36.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948516">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388040.34931528 5819542.20465672 36.3400001525879 388040.350640367 5819542.20662375 98.5662508010864 388040.205639063 5819542.2062355 98.5662508010864 388040.204313977 5819542.20426847 36.3400001525879 388040.34931528 5819542.20465672 36.3400001525879</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948498">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388040.512872494 5819553.29254179 36.2900009155273 388040.513245332 5819553.29309526 53.7984091079596 388041.856633033 5819553.14977083 53.7984091079596 388041.856260194 5819553.14921736 36.2900009155273 388040.512872494 5819553.29254179 36.2900009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948531">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388038.169008971 5819548.14542961 36.3400001525879 388038.170334055 5819548.14739664 98.5662508010864 388038.653740095 5819548.47235553 98.5662508010864 388038.65241501 5819548.4703885 36.3400001525879 388038.169008971 5819548.14542961 36.3400001525879</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948495">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388036.618541193 5819552.32806117 36.2900009155273 388036.61891403 5819552.32861464 53.7984091079596 388037.853984971 5819552.87618335 53.7984091079596 388037.853612134 5819552.87562989 36.2900009155273 388036.618541193 5819552.32806117 36.2900009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948480">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388040.218050045 5819537.7942836 36.2900009155273 388040.218422883 5819537.79483705 53.7984091079596 388038.875034442 5819537.93816145 53.7984091079596 388038.874661604 5819537.937608 36.2900009155273 388040.218050045 5819537.7942836 36.2900009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948441">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388031.032549001 5819557.37415195 36.1399993896484 388031.032699293 5819557.37437505 43.1977320994831 388047.194749935 5819558.84700281 43.1977320994831 388047.194599641 5819558.8467797 36.1399993896484 388031.032549001 5819557.37415195 36.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948443">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388040.218048103 5819537.79428047 36.1399993896484 388040.218198396 5819537.79450358 43.1977320994831 388041.566061907 5819537.88663228 43.1977320994831 388041.565911614 5819537.88640917 36.1399993896484 388040.218048103 5819537.79428047 36.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948497">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388039.16500432 5819553.20041289 36.2900009155273 388039.165377158 5819553.20096635 53.7984091079596 388040.513245332 5819553.29309526 53.7984091079596 388040.512872494 5819553.29254179 36.2900009155273 388039.16500432 5819553.20041289 36.2900009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948496">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388037.853612134 5819552.87562989 36.2900009155273 388037.853984971 5819552.87618335 53.7984091079596 388039.165377158 5819553.20096635 53.7984091079596 388039.16500432 5819553.20041289 36.2900009155273 388037.853612134 5819552.87562989 36.2900009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948442">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388040.041715205 5819537.79964292 36.1399993896484 388040.041865498 5819537.79986602 43.1977320994831 388040.218198396 5819537.79450358 43.1977320994831 388040.218048103 5819537.79428047 36.1399993896484 388040.041715205 5819537.79964292 36.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948528">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388037.150103903 5819546.74764532 36.3400001525879 388037.151428986 5819546.74961235 98.5662508010864 388037.407590715 5819547.27273701 98.5662508010864 388037.406265632 5819547.27076998 36.3400001525879 388037.150103903 5819546.74764532 36.3400001525879</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948482">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388037.576570336 5819538.31203092 36.2900009155273 388037.576943174 5819538.31258438 53.7984091079596 388036.363590927 5819538.90672919 53.7984091079596 388036.36321809 5819538.90617573 36.2900009155273 388037.576570336 5819538.31203092 36.2900009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948447">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388045.233586634 5819539.51247307 36.1399993896484 388045.233736927 5819539.51269617 43.1977320994831 388046.207040688 5819540.44965708 43.1977320994831 388046.206890394 5819540.44943398 36.1399993896484 388045.233586634 5819539.51247307 36.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948522">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388037.667671004 5819543.44639194 36.3400001525879 388037.668996088 5819543.44835896 98.5662508010864 388037.34403572 5819543.93176297 98.5662508010864 388037.342710637 5819543.92979595 36.3400001525879 388037.667671004 5819543.44639194 36.3400001525879</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948502">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388045.459449718 5819551.38483564 36.2900009155273 388045.459822558 5819551.3853891 53.7984091079596 388046.396790362 5819550.41208651 53.7984091079596 388046.396417521 5819550.41153305 36.2900009155273 388045.459449718 5819551.38483564 36.2900009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948534">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388039.750301876 5819548.84649585 36.3400001525879 388039.751626961 5819548.84846288 98.5662508010864 388040.332749827 5819548.88818351 98.5662508010864 388040.33142474 5819548.88621648 36.3400001525879 388039.750301876 5819548.84649585 36.3400001525879</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948505">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388047.697709024 5819548.05524941 36.2900009155273 388047.698081865 5819548.05580287 53.7984091079596 388048.022866485 5819546.7444163 53.7984091079596 388048.022493644 5819546.74386284 36.2900009155273 388047.697709024 5819548.05524941 36.2900009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948473">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388033.580801208 5819541.79649542 36.1399993896484 388033.5809515 5819541.79671852 43.1977320994831 388034.334669591 5819540.67550381 43.1977320994831 388034.334519299 5819540.67528071 36.1399993896484 388033.580801208 5819541.79649542 36.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948503">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388046.396417521 5819550.41153305 36.2900009155273 388046.396790362 5819550.41208651 53.7984091079596 388047.150510599 5819549.29086856 53.7984091079596 388047.150137759 5819549.29031509 36.2900009155273 388046.396417521 5819550.41153305 36.2900009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948456">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388046.396397584 5819550.41149318 36.1399993896484 388046.396547878 5819550.41171629 43.1977320994831 388045.459582804 5819551.38501604 43.1977320994831 388045.45943251 5819551.38479294 36.1399993896484 388046.396397584 5819550.41149318 36.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948518">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388039.625122974 5819542.26606163 36.3400001525879 388039.62644806 5819542.26802865 98.5662508010864 388039.066786568 5819542.42945806 98.5662508010864 388039.065461482 5819542.42749104 36.3400001525879 388039.625122974 5819542.26606163 36.3400001525879</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948486">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388033.580783819 5819541.7965102 36.2900009155273 388033.581156655 5819541.79706366 53.7984091079596 388033.03358535 5819543.03212938 53.7984091079596 388033.033212514 5819543.03157592 36.2900009155273 388033.580783819 5819541.7965102 36.2900009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948504">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388047.150137759 5819549.29031509 36.2900009155273 388047.150510599 5819549.29086856 53.7984091079596 388047.698081865 5819548.05580287 53.7984091079596 388047.697709024 5819548.05524941 36.2900009155273 388047.150137759 5819549.29031509 36.2900009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948448">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388046.206890394 5819540.44943398 36.1399993896484 388046.207040688 5819540.44965708 43.1977320994831 388047.002855496 5819541.54139517 43.1977320994831 388047.002705202 5819541.54117207 36.1399993896484 388046.206890394 5819540.44943398 36.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948517">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388040.204313977 5819542.20426847 36.3400001525879 388040.205639063 5819542.2062355 98.5662508010864 388039.62644806 5819542.26802865 98.5662508010864 388039.625122974 5819542.26606163 36.3400001525879 388040.204313977 5819542.20426847 36.3400001525879</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948455">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388047.150115627 5819549.29027849 36.1399993896484 388047.150265921 5819549.2905016 43.1977320994831 388046.396547878 5819550.41171629 43.1977320994831 388046.396397584 5819550.41149318 36.1399993896484 388047.150115627 5819549.29027849 36.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948449">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388047.002705202 5819541.54117207 36.1399993896484 388047.002855496 5819541.54139517 43.1977320994831 388047.597000923 5819542.75473854 43.1977320994831 388047.596850629 5819542.75451544 36.1399993896484 388047.002705202 5819541.54117207 36.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948454">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388047.697685298 5819548.0552164 36.1399993896484 388047.697835592 5819548.05543951 43.1977320994831 388047.150265921 5819549.2905016 43.1977320994831 388047.150115627 5819549.29027849 36.1399993896484 388047.697685298 5819548.0552164 36.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948451">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388047.971273869 5819544.05259725 36.1399993896484 388047.971424163 5819544.05282035 43.1977320994831 388048.114748558 5819545.39619905 43.1977320994831 388048.114598263 5819545.39597595 36.1399993896484 388047.971273869 5819544.05259725 36.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948450">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388047.596850629 5819542.75451544 36.1399993896484 388047.597000923 5819542.75473854 43.1977320994831 388047.971424163 5819544.05282035 43.1977320994831 388047.971273869 5819544.05259725 36.1399993896484 388047.596850629 5819542.75451544 36.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948453">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388048.022468972 5819546.74383366 36.1399993896484 388048.022619267 5819546.74405676 43.1977320994831 388047.697835592 5819548.05543951 43.1977320994831 388047.697685298 5819548.0552164 36.1399993896484 388048.022468972 5819546.74383366 36.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948484">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388035.271471961 5819539.70198963 36.2900009155273 388035.271844798 5819539.70254309 53.7984091079596 388034.334876941 5819540.67584568 53.7984091079596 388034.334504105 5819540.67529222 36.2900009155273 388035.271471961 5819539.70198963 36.2900009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948485">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388034.334504105 5819540.67529222 36.2900009155273 388034.334876941 5819540.67584568 53.7984091079596 388033.581156655 5819541.79706366 53.7984091079596 388033.580783819 5819541.7965102 36.2900009155273 388034.334504105 5819540.67529222 36.2900009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948483">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388036.36321809 5819538.90617573 36.2900009155273 388036.363590927 5819538.90672919 53.7984091079596 388035.271844798 5819539.70254309 53.7984091079596 388035.271471961 5819539.70198963 36.2900009155273 388036.36321809 5819538.90617573 36.2900009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948452">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388048.114598263 5819545.39597595 36.1399993896484 388048.114748558 5819545.39619905 43.1977320994831 388048.022619267 5819546.74405676 43.1977320994831 388048.022468972 5819546.74383366 36.1399993896484 388048.114598263 5819545.39597595 36.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948461">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388040.512996067 5819553.29249113 36.1399993896484 388040.51314636 5819553.29271423 43.1977320994831 388039.165155738 5819553.200588 43.1977320994831 388039.165005445 5819553.2003649 36.1399993896484 388040.512996067 5819553.29249113 36.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948509">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388047.596874062 5819542.75453301 36.2900009155273 388047.597246902 5819542.75508646 53.7984091079596 388047.003099745 5819541.54173957 53.7984091079596 388047.002726904 5819541.54118611 36.2900009155273 388047.596874062 5819542.75453301 36.2900009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948521">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388038.071637071 5819543.02676044 36.3400001525879 388038.072962155 5819543.02872746 98.5662508010864 388037.668996088 5819543.44835896 98.5662508010864 388037.667671004 5819543.44639194 36.3400001525879 388038.071637071 5819543.02676044 36.3400001525879</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948506">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388048.022493644 5819546.74386284 36.2900009155273 388048.022866485 5819546.7444163 53.7984091079596 388048.114996045 5819545.39655467 53.7984091079596 388048.114623204 5819545.39600121 36.2900009155273 388048.022493644 5819546.74386284 36.2900009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948536">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388040.910614964 5819548.82442333 36.3400001525879 388040.911940052 5819548.82639036 98.5662508010864 388041.47160154 5819548.66496095 98.5662508010864 388041.470276451 5819548.66299391 36.3400001525879 388040.910614964 5819548.82442333 36.3400001525879</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948457">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388045.45943251 5819551.38479294 36.1399993896484 388045.459582804 5819551.38501604 43.1977320994831 388044.367839905 5819552.18082764 43.1977320994831 388044.367689611 5819552.18060453 36.1399993896484 388045.45943251 5819551.38479294 36.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948508">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388047.971298392 5819544.05261859 36.2900009155273 388047.971671233 5819544.05317205 53.7984091079596 388047.597246902 5819542.75508646 53.7984091079596 388047.596874062 5819542.75453301 36.2900009155273 388047.971298392 5819544.05261859 36.2900009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948429">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388052.654433274 5819534.8787641 36.1399993896484 388052.654583569 5819534.8789872 43.1977320994831 388049.389690266 5819534.57790222 43.1977320994831 388049.389539972 5819534.57767912 36.1399993896484 388052.654433274 5819534.8787641 36.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948460">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388041.856253481 5819553.14916952 36.1399993896484 388041.856403774 5819553.14939263 43.1977320994831 388040.51314636 5819553.29271423 43.1977320994831 388040.512996067 5819553.29249113 36.1399993896484 388041.856253481 5819553.14916952 36.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948533">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388039.1849058 5819548.70646829 36.3400001525879 388039.186230885 5819548.70843533 98.5662508010864 388039.751626961 5819548.84846288 98.5662508010864 388039.750301876 5819548.84649585 36.3400001525879 388039.1849058 5819548.70646829 36.3400001525879</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948459">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388043.15434094 5819552.77474764 36.1399993896484 388043.154491234 5819552.77497075 43.1977320994831 388041.856403774 5819553.14939263 43.1977320994831 388041.856253481 5819553.14916952 36.1399993896484 388043.15434094 5819552.77474764 36.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948458">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388044.367689611 5819552.18060453 36.1399993896484 388044.367839905 5819552.18082764 43.1977320994831 388043.154491234 5819552.77497075 43.1977320994831 388043.15434094 5819552.77474764 36.1399993896484 388044.367689611 5819552.18060453 36.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948465">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388035.497330313 5819551.57430109 36.1399993896484 388035.497480605 5819551.5745242 43.1977320994831 388034.524176792 5819550.63756329 43.1977320994831 388034.5240265 5819550.63734018 36.1399993896484 388035.497330313 5819551.57430109 36.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948507">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388048.114623204 5819545.39600121 36.2900009155273 388048.114996045 5819545.39655467 53.7984091079596 388047.971671233 5819544.05317205 53.7984091079596 388047.971298392 5819544.05261859 36.2900009155273 388048.114623204 5819545.39600121 36.2900009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948472">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388033.033231498 5819543.03155754 36.1399993896484 388033.03338179 5819543.03178064 43.1977320994831 388033.5809515 5819541.79671852 43.1977320994831 388033.580801208 5819541.79649542 36.1399993896484 388033.033231498 5819543.03155754 36.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948537">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388041.470276451 5819548.66299391 36.3400001525879 388041.47160154 5819548.66496095 98.5662508010864 388041.994728483 5819548.40880024 98.5662508010864 388041.993403394 5819548.4068332 36.3400001525879 388041.470276451 5819548.66299391 36.3400001525879</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948464">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388036.618549734 5819552.32801572 36.1399993896484 388036.618700027 5819552.32823883 43.1977320994831 388035.497480605 5819551.5745242 43.1977320994831 388035.497330313 5819551.57430109 36.1399993896484 388036.618549734 5819552.32801572 36.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948512">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388045.233603184 5819539.5124812 36.2900009155273 388045.233976024 5819539.51303466 53.7984091079596 388044.112753385 5819538.75931781 53.7984091079596 388044.112380545 5819538.75876435 36.2900009155273 388045.233603184 5819539.5124812 36.2900009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948513">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388044.112380545 5819538.75876435 36.2900009155273 388044.112753385 5819538.75931781 53.7984091079596 388042.877682483 5819538.21174907 53.7984091079596 388042.877309644 5819538.21119561 36.2900009155273 388044.112380545 5819538.75876435 36.2900009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948510">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388047.002726904 5819541.54118611 36.2900009155273 388047.003099745 5819541.54173957 53.7984091079596 388046.207282619 5819540.4499983 53.7984091079596 388046.206909778 5819540.44944484 36.2900009155273 388047.002726904 5819541.54118611 36.2900009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948511">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388046.206909778 5819540.44944484 36.2900009155273 388046.207282619 5819540.4499983 53.7984091079596 388045.233976024 5819539.51303466 53.7984091079596 388045.233603184 5819539.5124812 36.2900009155273 388046.206909778 5819540.44944484 36.2900009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948463">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388037.853617078 5819552.87558284 36.1399993896484 388037.853767371 5819552.87580595 43.1977320994831 388036.618700027 5819552.32823883 43.1977320994831 388036.618549734 5819552.32801572 36.1399993896484 388037.853617078 5819552.87558284 36.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948462">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388039.165005445 5819553.2003649 36.1399993896484 388039.165155738 5819553.200588 43.1977320994831 388037.853767371 5819552.87580595 43.1977320994831 388037.853617078 5819552.87558284 36.1399993896484 388039.165005445 5819553.2003649 36.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948466">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388034.5240265 5819550.63734018 36.1399993896484 388034.524176792 5819550.63756329 43.1977320994831 388033.728361934 5819549.54582518 43.1977320994831 388033.728211643 5819549.54560208 36.1399993896484 388034.5240265 5819550.63734018 36.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948514">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388042.877309644 5819538.21119561 36.2900009155273 388042.877682483 5819538.21174907 53.7984091079596 388041.56629032 5819537.88696602 53.7984091079596 388041.565917482 5819537.88641256 36.2900009155273 388042.877309644 5819538.21119561 36.2900009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948532">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388038.65241501 5819548.4703885 36.3400001525879 388038.653740095 5819548.47235553 98.5662508010864 388039.186230885 5819548.70843533 98.5662508010864 388039.1849058 5819548.70646829 36.3400001525879 388038.65241501 5819548.4703885 36.3400001525879</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948471">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388032.708447799 5819544.34294033 36.1399993896484 388032.708598091 5819544.34316344 43.1977320994831 388033.03338179 5819543.03178064 43.1977320994831 388033.033231498 5819543.03155754 36.1399993896484 388032.708447799 5819544.34294033 36.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948470">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388032.616318501 5819545.69079809 36.1399993896484 388032.616468793 5819545.69102119 43.1977320994831 388032.708598091 5819544.34316344 43.1977320994831 388032.708447799 5819544.34294033 36.1399993896484 388032.616318501 5819545.69079809 36.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948469">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388032.759642906 5819547.03417683 36.1399993896484 388032.759793198 5819547.03439994 43.1977320994831 388032.616468793 5819545.69102119 43.1977320994831 388032.616318501 5819545.69079809 36.1399993896484 388032.759642906 5819547.03417683 36.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948520">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388038.542334532 5819542.68365174 36.3400001525879 388038.543659616 5819542.68561877 98.5662508010864 388038.072962155 5819543.02872746 98.5662508010864 388038.071637071 5819543.02676044 36.3400001525879 388038.542334532 5819542.68365174 36.3400001525879</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948468">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388033.134066174 5819548.33225868 36.1399993896484 388033.134216466 5819548.33248179 43.1977320994831 388032.759793198 5819547.03439994 43.1977320994831 388032.759642906 5819547.03417683 36.1399993896484 388033.134066174 5819548.33225868 36.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948467">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388033.728211643 5819549.54560208 36.1399993896484 388033.728361934 5819549.54582518 43.1977320994831 388033.134216466 5819548.33248179 43.1977320994831 388033.134066174 5819548.33225868 36.1399993896484 388033.728211643 5819549.54560208 36.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948515">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388041.565917482 5819537.88641256 36.2900009155273 388041.56629032 5819537.88696602 53.7984091079596 388040.394904586 5819537.7934896 53.7984091079596 388040.394531748 5819537.79293614 36.2900009155273 388041.565917482 5819537.88641256 36.2900009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948478">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388038.874663574 5819537.93760445 36.1399993896484 388038.874813867 5819537.93782756 43.1977320994831 388040.041865498 5819537.79986602 43.1977320994831 388040.041715205 5819537.79964292 36.1399993896484 388038.874663574 5819537.93760445 36.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_6948519">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388039.065461482 5819542.42749104 36.3400001525879 388039.066786568 5819542.42945806 98.5662508010864 388038.543659616 5819542.68561877 98.5662508010864 388038.542334532 5819542.68365174 36.3400001525879 388039.065461482 5819542.42749104 36.3400001525879</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface gml:id="GEOM_6948418">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388031.032549001 5819557.37415195 36.1399993896484 388047.194599641 5819558.8467797 36.1399993896484 388047.334436373 5819557.31402343 36.1399993896484 388050.601401502 5819557.59889575 36.1399993896484 388050.88647718 5819554.33291727 36.1399993896484 388052.410004467 5819554.47170824 36.1399993896484 388053.888041669 5819538.28881329 36.1399993896484 388052.354672263 5819538.14917197 36.1399993896484 388052.654433274 5819534.8787641 36.1399993896484 388049.389539972 5819534.57767912 36.1399993896484 388049.528164739 5819533.04537313 36.1399993896484 388033.338858328 5819531.58046452 36.1399993896484 388033.20025676 5819533.11399073 36.1399993896484 388029.932624538 5819532.82614041 36.1399993896484 388029.634535258 5819536.09139009 36.1399993896484 388028.101463171 5819535.95455051 36.1399993896484 388026.660031001 5819552.13675007 36.1399993896484 388028.193896212 5819552.27357451 36.1399993896484 388027.902665584 5819555.54003642 36.1399993896484 388031.171818599 5819555.84366454 36.1399993896484 388031.032549001 5819557.37415195 36.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
									<gml:interior>
										<gml:LinearRing>
											<gml:posList>388038.874663574 5819537.93760445 36.1399993896484 388040.041715205 5819537.79964292 36.1399993896484 388040.218048103 5819537.79428047 36.1399993896484 388041.565911614 5819537.88640917 36.1399993896484 388042.877299956 5819538.21119127 36.1399993896484 388044.112367261 5819538.75875842 36.1399993896484 388045.233586634 5819539.51247307 36.1399993896484 388046.206890394 5819540.44943398 36.1399993896484 388047.002705202 5819541.54117207 36.1399993896484 388047.596850629 5819542.75451544 36.1399993896484 388047.971273869 5819544.05259725 36.1399993896484 388048.114598263 5819545.39597595 36.1399993896484 388048.022468972 5819546.74383366 36.1399993896484 388047.697685298 5819548.0552164 36.1399993896484 388047.150115627 5819549.29027849 36.1399993896484 388046.396397584 5819550.41149318 36.1399993896484 388045.45943251 5819551.38479294 36.1399993896484 388044.367689611 5819552.18060453 36.1399993896484 388043.15434094 5819552.77474764 36.1399993896484 388041.856253481 5819553.14916952 36.1399993896484 388040.512996067 5819553.29249113 36.1399993896484 388039.165005445 5819553.2003649 36.1399993896484 388037.853617078 5819552.87558284 36.1399993896484 388036.618549734 5819552.32801572 36.1399993896484 388035.497330313 5819551.57430109 36.1399993896484 388034.5240265 5819550.63734018 36.1399993896484 388033.728211643 5819549.54560208 36.1399993896484 388033.134066174 5819548.33225868 36.1399993896484 388032.759642906 5819547.03417683 36.1399993896484 388032.616318501 5819545.69079809 36.1399993896484 388032.708447799 5819544.34294033 36.1399993896484 388033.033231498 5819543.03155754 36.1399993896484 388033.580801208 5819541.79649542 36.1399993896484 388034.334519299 5819540.67528071 36.1399993896484 388035.271484426 5819539.70198095 36.1399993896484 388036.363227375 5819538.90616937 36.1399993896484 388037.576576087 5819538.31202629 36.1399993896484 388038.874663574 5819537.93760445 36.1399993896484</gml:posList>
										</gml:LinearRing>
									</gml:interior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface gml:id="GEOM_6948420">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388040.785436064 5819542.24398912 36.3400001525879 388040.34931528 5819542.20465672 36.3400001525879 388040.204313977 5819542.20426847 36.3400001525879 388039.625122974 5819542.26606163 36.3400001525879 388039.065461482 5819542.42749104 36.3400001525879 388038.542334532 5819542.68365174 36.3400001525879 388038.071637071 5819543.02676044 36.3400001525879 388037.667671004 5819543.44639194 36.3400001525879 388037.342710637 5819543.92979595 36.3400001525879 388037.106629726 5819544.46228448 36.3400001525879 388036.96660147 5819545.02767815 36.3400001525879 388036.926880558 5819545.60879775 36.3400001525879 388036.988673888 5819546.18798626 36.3400001525879 388037.150103903 5819546.74764532 36.3400001525879 388037.406265632 5819547.27076998 36.3400001525879 388037.74937573 5819547.74146535 36.3400001525879 388038.169008971 5819548.14542961 36.3400001525879 388038.65241501 5819548.4703885 36.3400001525879 388039.1849058 5819548.70646829 36.3400001525879 388039.750301876 5819548.84649585 36.3400001525879 388040.33142474 5819548.88621648 36.3400001525879 388040.910614964 5819548.82442333 36.3400001525879 388041.470276451 5819548.66299391 36.3400001525879 388041.993403394 5819548.4068332 36.3400001525879 388042.464100846 5819548.0637245 36.3400001525879 388042.868066902 5819547.64409301 36.3400001525879 388043.19302726 5819547.160689 36.3400001525879 388043.429108164 5819546.62820047 36.3400001525879 388043.569136415 5819546.0628068 36.3400001525879 388043.608857327 5819545.48168721 36.3400001525879 388043.547063998 5819544.90249872 36.3400001525879 388043.385633989 5819544.34283966 36.3400001525879 388043.129472268 5819543.81971501 36.3400001525879 388042.786362178 5819543.34901964 36.3400001525879 388042.366728947 5819542.94505538 36.3400001525879 388041.883322917 5819542.6200965 36.3400001525879 388041.350832135 5819542.38401669 36.3400001525879 388040.785436064 5819542.24398912 36.3400001525879</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface gml:id="GEOM_6948419">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>388041.565917482 5819537.88641256 36.2900009155273 388040.394531748 5819537.79293614 36.2900009155273 388040.218050045 5819537.7942836 36.2900009155273 388038.874661604 5819537.937608 36.2900009155273 388037.576570336 5819538.31203092 36.2900009155273 388036.36321809 5819538.90617573 36.2900009155273 388035.271471961 5819539.70198963 36.2900009155273 388034.334504105 5819540.67529222 36.2900009155273 388033.580783819 5819541.7965102 36.2900009155273 388033.033212514 5819543.03157592 36.2900009155273 388032.70842787 5819544.34296253 36.2900009155273 388032.616298303 5819545.69082421 36.2900009155273 388032.759623126 5819547.03420687 36.2900009155273 388033.134047484 5819548.3322925 36.2900009155273 388033.728194683 5819549.54563942 36.2900009155273 388034.524011859 5819550.63738071 36.2900009155273 388035.497318506 5819551.57434435 36.2900009155273 388036.618541193 5819552.32806117 36.2900009155273 388037.853612134 5819552.87562989 36.2900009155273 388039.16500432 5819553.20041289 36.2900009155273 388040.512872494 5819553.29254179 36.2900009155273 388041.856260194 5819553.14921736 36.2900009155273 388043.154351434 5819552.77479439 36.2900009155273 388044.367703639 5819552.18064956 36.2900009155273 388045.459449718 5819551.38483564 36.2900009155273 388046.396417521 5819550.41153305 36.2900009155273 388047.150137759 5819549.29031509 36.2900009155273 388047.697709024 5819548.05524941 36.2900009155273 388048.022493644 5819546.74386284 36.2900009155273 388048.114623204 5819545.39600121 36.2900009155273 388047.971298392 5819544.05261859 36.2900009155273 388047.596874062 5819542.75453301 36.2900009155273 388047.002726904 5819541.54118611 36.2900009155273 388046.206909778 5819540.44944484 36.2900009155273 388045.233603184 5819539.5124812 36.2900009155273 388044.112380545 5819538.75876435 36.2900009155273 388042.877309644 5819538.21119561 36.2900009155273 388041.565917482 5819537.88641256 36.2900009155273</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
		</bldg:Building>
	</cityObjectMember>
</CityModel>