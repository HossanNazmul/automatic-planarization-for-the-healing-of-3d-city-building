<?xml version="1.0" encoding="UTF-8"?>
<CityModel xmlns:bldg="http://www.opengis.net/citygml/building/1.0" xmlns:app="http://www.opengis.net/citygml/appearance/1.0" xmlns:smil20="http://www.w3.org/2001/SMIL20/" xmlns:grp="http://www.opengis.net/citygml/cityobjectgroup/1.0" xmlns:base="http://www.citygml.org/citygml/profiles/base/1.0" xmlns:frn="http://www.opengis.net/citygml/cityfurniture/1.0" xmlns:smil20lang="http://www.w3.org/2001/SMIL20/Language" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:wtr="http://www.opengis.net/citygml/waterbody/1.0" xmlns:tex="http://www.opengis.net/citygml/texturedsurface/1.0" xmlns:veg="http://www.opengis.net/citygml/vegetation/1.0" xmlns:sch="http://www.ascc.net/xml/schematron" xmlns:gen="http://www.opengis.net/citygml/generics/1.0" xmlns:dem="http://www.opengis.net/citygml/relief/1.0" xmlns:gml="http://www.opengis.net/gml" xmlns:tran="http://www.opengis.net/citygml/transportation/1.0" xmlns:xAL="urn:oasis:names:tc:ciq:xsdschema:xAL:2.0" xmlns:luse="http://www.opengis.net/citygml/landuse/1.0" xmlns="http://www.opengis.net/citygml/1.0">
	<gml:boundedBy>
		<gml:Envelope srsName="EPSG:25833" srsDimension="3">
			<gml:lowerCorner>389653.721821351 5820081.35095776 31.1499996185303</gml:lowerCorner>
			<gml:upperCorner>389854.268553405 5820188.00157623 59.8097336249404</gml:upperCorner>
		</gml:Envelope>
	</gml:boundedBy>
	<cityObjectMember>
		<bldg:Building gml:id="BLDG_0003000f000ebde4">
			<gml:name>Paul_Loebe_Haus</gml:name>
			<bldg:roofType>1000</bldg:roofType>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365583">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389819.457296224 5820108.87201279 31.1499996185303 389819.457907551 5820108.87291906 59.8097336249404 389821.656645533 5820110.49904744 59.8097336249404 389821.656034205 5820110.49814117 31.1499996185303 389819.457296224 5820108.87201279 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365575">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389812.655946841 5820183.83794732 31.1499996185303 389812.656558165 5820183.83885363 59.8097336249404 389811.881579722 5820149.67908332 59.8097336249404 389811.880968399 5820149.67817703 31.1499996185303 389812.655946841 5820183.83794732 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365594">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389836.334093759 5820091.19759854 31.1499996185303 389836.334705096 5820091.1985048 59.8097336249404 389835.613735199 5820090.54637992 59.8097336249404 389835.613123863 5820090.54547367 31.1499996185303 389836.334093759 5820091.19759854 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365599">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389824.717238568 5820087.49191137 31.1499996185303 389824.717849899 5820087.49281762 59.8097336249404 389824.66427369 5820085.25402607 59.8097336249404 389824.663662359 5820085.25311982 31.1499996185303 389824.717238568 5820087.49191137 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365526">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389674.537493569 5820158.38125067 31.1499996185303 389674.538104814 5820158.38215697 59.8097336249404 389677.243932821 5820160.1005585 59.8097336249404 389677.243321575 5820160.09965221 31.1499996185303 389674.537493569 5820158.38125067 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365625">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389775.540592412 5820118.15530402 31.1499996185303 389775.541203715 5820118.15621029 59.8097336249404 389774.716012029 5820083.21742563 59.8097336249404 389774.715400728 5820083.21651938 31.1499996185303 389775.540592412 5820118.15530402 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365514">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389676.057379312 5820112.83899396 31.1499996185303 389676.057990557 5820112.83990023 59.8097336249404 389673.415307473 5820114.69728216 59.8097336249404 389673.41469623 5820114.69637589 31.1499996185303 389676.057379312 5820112.83899396 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365543">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389721.75846939 5820157.28211518 31.1499996185303 389721.759080662 5820157.28302147 59.8097336249404 389723.479226878 5820154.57228756 59.8097336249404 389723.478615606 5820154.57138126 31.1499996185303 389721.75846939 5820157.28211518 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365586">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389828.725047058 5820111.70937631 31.1499996185303 389828.725658391 5820111.71028258 59.8097336249404 389832.156316431 5820110.49825889 59.8097336249404 389832.155705097 5820110.49735262 31.1499996185303 389828.725047058 5820111.70937631 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365505">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389704.36345244 5820119.83895489 31.1499996185303 389704.364063701 5820119.83986116 59.8097336249404 389703.54796032 5820084.86550173 59.8097336249404 389703.54734906 5820084.86459548 31.1499996185303 389704.36345244 5820119.83895489 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365578">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389810.82027009 5820103.16216092 31.1499996185303 389810.820881412 5820103.16306718 59.8097336249404 389813.279672993 5820103.03589049 59.8097336249404 389813.279061669 5820103.03498423 31.1499996185303 389810.82027009 5820103.16216092 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365581">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389815.485438542 5820108.71199796 31.1499996185303 389815.486049867 5820108.71290423 59.8097336249404 389817.472940771 5820111.00890258 59.8097336249404 389817.472329445 5820111.00799631 31.1499996185303 389815.485438542 5820108.71199796 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365499">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389718.019015007 5820111.9143751 31.1499996185303 389718.019626276 5820111.91528137 59.8097336249404 389714.860619174 5820111.36629648 59.8097336249404 389714.860007906 5820111.36539021 31.1499996185303 389718.019015007 5820111.9143751 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365614">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389796.712197757 5820110.55889232 31.1499996185303 389796.712809071 5820110.55979859 59.8097336249404 389796.887137157 5820117.67017142 59.8097336249404 389796.886525843 5820117.66926515 31.1499996185303 389796.712197757 5820110.55889232 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365498">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389720.727248643 5820113.63090112 31.1499996185303 389720.727859914 5820113.63180739 59.8097336249404 389718.019626276 5820111.91528137 59.8097336249404 389718.019015007 5820111.9143751 31.1499996185303 389720.727248643 5820113.63090112 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365558">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389759.589606625 5820150.58570906 31.1499996185303 389759.590217918 5820150.58661535 59.8097336249404 389762.059043164 5820150.52988353 59.8097336249404 389762.058431869 5820150.52897724 31.1499996185303 389759.589606625 5820150.58570906 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365611">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389810.361082835 5820084.79956389 31.1499996185303 389810.361694157 5820084.80047014 59.8097336249404 389810.303624511 5820082.38965777 59.8097336249404 389810.303013189 5820082.38875151 31.1499996185303 389810.361082835 5820084.79956389 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365602">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389851.081600166 5820081.93155346 31.1499996185303 389851.082211512 5820081.93245971 59.8097336249404 389853.795941414 5820081.85275965 59.8097336249404 389853.795330067 5820081.8518534 31.1499996185303 389851.081600166 5820081.93155346 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365501">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389711.730881455 5820112.06634813 31.1499996185303 389711.731492721 5820112.0672544 59.8097336249404 389709.109332275 5820113.91264968 59.8097336249404 389709.10872101 5820113.91174341 31.1499996185303 389711.730881455 5820112.06634813 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365553">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389748.403824148 5820158.43950242 31.1499996185303 389748.404435434 5820158.44040872 59.8097336249404 389751.562903213 5820158.99306519 59.8097336249404 389751.562291924 5820158.99215889 31.1499996185303 389748.403824148 5820158.43950242 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365540">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389712.835305267 5820159.27667749 31.1499996185303 389712.835916533 5820159.27758378 59.8097336249404 389715.998643145 5820159.82954932 59.8097336249404 389715.998031877 5820159.82864303 31.1499996185303 389712.835305267 5820159.27667749 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365624">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389777.971086835 5820118.10479316 31.1499996185303 389777.971698139 5820118.10569943 59.8097336249404 389775.541203715 5820118.15621029 59.8097336249404 389775.540592412 5820118.15530402 31.1499996185303 389777.971086835 5820118.10479316 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365533">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389690.881435895 5820152.18821859 31.1499996185303 389690.882047148 5820152.18912488 59.8097336249404 389691.68809247 5820187.1478099 59.8097336249404 389691.687481216 5820187.14690359 31.1499996185303 389690.881435895 5820152.18821859 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365616">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389794.448816187 5820117.72540484 31.1499996185303 389794.4494275 5820117.72631111 59.8097336249404 389793.74951402 5820114.58802334 59.8097336249404 389793.748902707 5820114.58711707 31.1499996185303 389794.448816187 5820117.72540484 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365597">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389830.495497074 5820087.83910145 31.1499996185303 389830.496108408 5820087.84000771 59.8097336249404 389827.625808264 5820087.3569367 59.8097336249404 389827.625196932 5820087.35603044 31.1499996185303 389830.495497074 5820087.83910145 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365522">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389670.339367799 5820187.64702423 31.1499996185303 389670.33997904 5820187.64793054 59.8097336249404 389669.53876708 5820152.68671106 59.8097336249404 389669.538155839 5820152.68580477 31.1499996185303 389670.339367799 5820187.64702423 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365623">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389778.52541369 5820114.93776175 31.1499996185303 389778.526024994 5820114.93866802 59.8097336249404 389777.971698139 5820118.10569943 59.8097336249404 389777.971086835 5820118.10479316 31.1499996185303 389778.52541369 5820114.93776175 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365510">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389686.975542697 5820117.06330458 31.1499996185303 389686.976153948 5820117.06421085 59.8097336249404 389685.119375472 5820114.42152498 59.8097336249404 389685.118764221 5820114.42061871 31.1499996185303 389686.975542697 5820117.06330458 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365497">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389722.572661731 5820116.2536626 31.1499996185303 389722.573273003 5820116.25456887 59.8097336249404 389720.727859914 5820113.63180739 59.8097336249404 389720.727248643 5820113.63090112 31.1499996185303 389722.572661731 5820116.2536626 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365615">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389796.886525843 5820117.66926515 31.1499996185303 389796.887137157 5820117.67017142 59.8097336249404 389794.4494275 5820117.72631111 59.8097336249404 389794.448816187 5820117.72540484 31.1499996185303 389796.886525843 5820117.66926515 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365601">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389851.170736893 5820084.62800567 31.1499996185303 389851.171348239 5820084.62891192 59.8097336249404 389851.082211512 5820081.93245971 59.8097336249404 389851.081600166 5820081.93155346 31.1499996185303 389851.170736893 5820084.62800567 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365617">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389793.748902707 5820114.58711707 31.1499996185303 389793.74951402 5820114.58802334 59.8097336249404 389791.902095989 5820111.95614516 59.8097336249404 389791.901484677 5820111.95523889 31.1499996185303 389793.748902707 5820114.58711707 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365551">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389743.854355596 5820154.09525352 31.1499996185303 389743.85496688 5820154.09615981 59.8097336249404 389745.697365271 5820156.72081019 59.8097336249404 389745.696753985 5820156.7199039 31.1499996185303 389743.854355596 5820154.09525352 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365495">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389725.722244389 5820119.32276668 31.1499996185303 389725.722855663 5820119.32367295 59.8097336249404 389723.274220405 5820119.38307326 59.8097336249404 389723.273609132 5820119.38216699 31.1499996185303 389725.722244389 5820119.32276668 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365618">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389791.901484677 5820111.95523889 31.1499996185303 389791.902095989 5820111.95614516 59.8097336249404 389789.188222414 5820110.23179334 59.8097336249404 389789.187611104 5820110.23088708 31.1499996185303 389791.901484677 5820111.95523889 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365621">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389782.881647245 5820110.37587587 31.1499996185303 389782.882258551 5820110.37678214 59.8097336249404 389780.2503833 5820112.22480274 59.8097336249404 389780.249771995 5820112.22389647 31.1499996185303 389782.881647245 5820110.37587587 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365593">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389834.352682907 5820093.32802068 31.1499996185303 389834.353294243 5820093.32892694 59.8097336249404 389836.334705096 5820091.1985048 59.8097336249404 389836.334093759 5820091.19759854 31.1499996185303 389834.352682907 5820093.32802068 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365494">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389724.890039708 5820084.36824474 31.1499996185303 389724.890650981 5820084.36915099 59.8097336249404 389725.722855663 5820119.32367295 59.8097336249404 389725.722244389 5820119.32276668 31.1499996185303 389724.890039708 5820084.36824474 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365531">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389687.871554721 5820155.4037977 31.1499996185303 389687.872165973 5820155.40470399 59.8097336249404 389688.424225854 5820152.24686928 59.8097336249404 389688.423614602 5820152.24596299 31.1499996185303 389687.871554721 5820155.4037977 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365532">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389688.423614602 5820152.24596299 31.1499996185303 389688.424225854 5820152.24686928 59.8097336249404 389690.882047148 5820152.18912488 59.8097336249404 389690.881435895 5820152.18821859 31.1499996185303 389688.423614602 5820152.24596299 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365619">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389789.187611104 5820110.23088708 31.1499996185303 389789.188222414 5820110.23179334 59.8097336249404 389786.020569212 5820109.67748047 59.8097336249404 389786.019957904 5820109.6765742 31.1499996185303 389789.187611104 5820110.23088708 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365620">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389786.019957904 5820109.6765742 31.1499996185303 389786.020569212 5820109.67748047 59.8097336249404 389782.882258551 5820110.37678214 59.8097336249404 389782.881647245 5820110.37587587 31.1499996185303 389786.019957904 5820109.6765742 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365622">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389780.249771995 5820112.22389647 31.1499996185303 389780.2503833 5820112.22480274 59.8097336249404 389778.526024994 5820114.93866802 59.8097336249404 389778.52541369 5820114.93776175 31.1499996185303 389780.249771995 5820112.22389647 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365545">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389724.031193335 5820151.40865535 31.1499996185303 389724.031804608 5820151.40956164 59.8097336249404 389726.46095123 5820151.35236276 59.8097336249404 389726.460339956 5820151.35145647 31.1499996185303 389724.031193335 5820151.40865535 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365632">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389753.608172928 5820111.07131869 31.1499996185303 389753.608784218 5820111.07222495 59.8097336249404 389750.442926071 5820110.51604603 59.8097336249404 389750.442314784 5820110.51513976 31.1499996185303 389753.608172928 5820111.07131869 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365548">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389741.51025566 5820185.98728234 31.1499996185303 389741.510866943 5820185.98818865 59.8097336249404 389740.687012513 5820151.0237439 59.8097336249404 389740.686401231 5820151.02283761 31.1499996185303 389741.51025566 5820185.98728234 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365580">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389813.713574074 5820105.07900651 31.1499996185303 389813.714185398 5820105.07991277 59.8097336249404 389815.486049867 5820108.71290423 59.8097336249404 389815.485438542 5820108.71199796 31.1499996185303 389813.713574074 5820105.07900651 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365496">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389723.273609132 5820119.38216699 31.1499996185303 389723.274220405 5820119.38307326 59.8097336249404 389722.573273003 5820116.25456887 59.8097336249404 389722.572661731 5820116.2536626 31.1499996185303 389723.273609132 5820119.38216699 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365556">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389757.316733015 5820156.45123789 31.1499996185303 389757.317344307 5820156.45214418 59.8097336249404 389759.036960886 5820153.74569322 59.8097336249404 389759.036349593 5820153.74478693 31.1499996185303 389757.316733015 5820156.45123789 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365608">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389819.155477958 5820082.70919409 31.1499996185303 389819.156089285 5820082.71010034 59.8097336249404 389812.179504571 5820082.86052229 59.8097336249404 389812.178893248 5820082.85961604 31.1499996185303 389819.155477958 5820082.70919409 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365546">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389726.460339956 5820151.35145647 31.1499996185303 389726.46095123 5820151.35236276 59.8097336249404 389727.275653725 5820186.31698275 59.8097336249404 389727.27504245 5820186.31607644 31.1499996185303 389726.460339956 5820151.35145647 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365525">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389672.69451992 5820155.75844267 31.1499996185303 389672.695131163 5820155.75934896 59.8097336249404 389674.538104814 5820158.38215697 59.8097336249404 389674.537493569 5820158.38125067 31.1499996185303 389672.69451992 5820155.75844267 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365500">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389714.860007906 5820111.36539021 31.1499996185303 389714.860619174 5820111.36629648 59.8097336249404 389711.731492721 5820112.0672544 59.8097336249404 389711.730881455 5820112.06634813 31.1499996185303 389714.860007906 5820111.36539021 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365595">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389835.613123863 5820090.54547367 31.1499996185303 389835.613735199 5820090.54637992 59.8097336249404 389833.200153698 5820088.9194571 59.8097336249404 389833.199542362 5820088.91855084 31.1499996185303 389835.613123863 5820090.54547367 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365605">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389820.98427341 5820082.1165865 31.1499996185303 389820.984884738 5820082.11749275 59.8097336249404 389821.046172922 5820084.56913357 59.8097336249404 389821.045561593 5820084.56822731 31.1499996185303 389820.98427341 5820082.1165865 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365539">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389710.124562845 5820157.55653873 31.1499996185303 389710.125174109 5820157.55744503 59.8097336249404 389712.835916533 5820159.27758378 59.8097336249404 389712.835305267 5820159.27667749 31.1499996185303 389710.124562845 5820157.55653873 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365541">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389715.998031877 5820159.82864303 31.1499996185303 389715.998643145 5820159.82954932 59.8097336249404 389719.132051234 5820159.12912056 59.8097336249404 389719.131439964 5820159.12821427 31.1499996185303 389715.998031877 5820159.82864303 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365549">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389740.686401231 5820151.02283761 31.1499996185303 389740.687012513 5820151.0237439 59.8097336249404 389743.156436 5820150.96638974 59.8097336249404 389743.155824716 5820150.96548345 31.1499996185303 389740.686401231 5820151.02283761 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365552">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389745.696753985 5820156.7199039 31.1499996185303 389745.697365271 5820156.72081019 59.8097336249404 389748.404435434 5820158.44040872 59.8097336249404 389748.403824148 5820158.43950242 31.1499996185303 389745.696753985 5820156.7199039 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365627">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389760.48027098 5820083.54958121 31.1499996185303 389760.480882274 5820083.55048746 59.8097336249404 389761.301598369 5820118.51071869 59.8097336249404 389761.300987075 5820118.50981242 31.1499996185303 389760.48027098 5820083.54958121 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365554">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389751.562291924 5820158.99215889 31.1499996185303 389751.562903213 5820158.99306519 59.8097336249404 389754.693306757 5820158.29513445 59.8097336249404 389754.692695467 5820158.29422815 31.1499996185303 389751.562291924 5820158.99215889 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365557">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389759.036349593 5820153.74478693 31.1499996185303 389759.036960886 5820153.74569322 59.8097336249404 389759.590217918 5820150.58661535 59.8097336249404 389759.589606625 5820150.58570906 31.1499996185303 389759.036349593 5820153.74478693 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365559">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389762.058431869 5820150.52897724 31.1499996185303 389762.059043164 5820150.52988353 59.8097336249404 389762.853939117 5820185.44788423 59.8097336249404 389762.853327822 5820185.44697792 31.1499996185303 389762.058431869 5820150.52897724 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365562">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389776.288646831 5820150.19417588 31.1499996185303 389776.289258133 5820150.19508216 59.8097336249404 389778.709275642 5820150.13927787 59.8097336249404 389778.708664338 5820150.13837159 31.1499996185303 389776.288646831 5820150.19417588 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365564">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389779.40924687 5820153.27969844 31.1499996185303 389779.409858174 5820153.28060473 59.8097336249404 389781.259750265 5820155.9142666 59.8097336249404 389781.25913896 5820155.91336031 31.1499996185303 389779.40924687 5820153.27969844 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365571">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389795.20221026 5820149.75624036 31.1499996185303 389795.202821573 5820149.75714665 59.8097336249404 389797.651513389 5820149.70079686 59.8097336249404 389797.650902074 5820149.69989057 31.1499996185303 389795.20221026 5820149.75624036 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365574">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389812.675684655 5820184.33008543 31.1499996185303 389812.676295978 5820184.33099174 59.8097336249404 389812.656558165 5820183.83885363 59.8097336249404 389812.655946841 5820183.83794732 31.1499996185303 389812.675684655 5820184.33008543 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365576">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389811.880968399 5820149.67817703 31.1499996185303 389811.881579722 5820149.67908332 59.8097336249404 389811.135911721 5820116.9956892 59.8097336249404 389811.135300398 5820116.99478293 31.1499996185303 389811.880968399 5820149.67817703 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365579">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389813.279061669 5820103.03498423 31.1499996185303 389813.279672993 5820103.03589049 59.8097336249404 389813.714185398 5820105.07991277 59.8097336249404 389813.713574074 5820105.07900651 31.1499996185303 389813.279061669 5820103.03498423 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365626">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389774.715400728 5820083.21651938 31.1499996185303 389774.716012029 5820083.21742563 59.8097336249404 389760.480882274 5820083.55048746 59.8097336249404 389760.48027098 5820083.54958121 31.1499996185303 389774.715400728 5820083.21651938 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365636">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389742.948262537 5820115.77021587 31.1499996185303 389742.948873821 5820115.77112214 59.8097336249404 389742.393292635 5820118.93634682 59.8097336249404 389742.392681352 5820118.93544054 31.1499996185303 389742.948262537 5820115.77021587 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365631">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389756.319618435 5820112.79632678 31.1499996185303 389756.320229726 5820112.79723305 59.8097336249404 389753.608784218 5820111.07222495 59.8097336249404 389753.608172928 5820111.07131869 31.1499996185303 389756.319618435 5820112.79632678 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365584">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389821.656034205 5820110.49814117 31.1499996185303 389821.656645533 5820110.49904744 59.8097336249404 389825.087025621 5820111.71054112 59.8097336249404 389825.08641429 5820111.70963485 31.1499996185303 389821.656034205 5820110.49814117 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365588">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389834.986134601 5820108.21101869 31.1499996185303 389834.986745938 5820108.21192496 59.8097336249404 389836.894208238 5820105.11374813 59.8097336249404 389836.8935969 5820105.11284186 31.1499996185303 389834.986134601 5820108.21101869 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365590">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389837.659608533 5820101.55607165 31.1499996185303 389837.66021987 5820101.55697791 59.8097336249404 389837.197913304 5820097.94789848 59.8097336249404 389837.197301966 5820097.94699222 31.1499996185303 389837.659608533 5820101.55607165 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365591">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389837.197301966 5820097.94699222 31.1499996185303 389837.197913304 5820097.94789848 59.8097336249404 389835.559694063 5820094.69929771 59.8097336249404 389835.559082727 5820094.69839145 31.1499996185303 389837.197301966 5820097.94699222 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365596">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389833.199542362 5820088.91855084 31.1499996185303 389833.200153698 5820088.9194571 59.8097336249404 389830.496108408 5820087.84000771 59.8097336249404 389830.495497074 5820087.83910145 31.1499996185303 389833.199542362 5820088.91855084 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365604">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389854.267942058 5820081.35095776 31.1499996185303 389854.268553405 5820081.35186402 59.8097336249404 389820.984884738 5820082.11749275 59.8097336249404 389820.98427341 5820082.1165865 31.1499996185303 389854.267942058 5820081.35095776 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365613">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389796.060481783 5820082.71768246 31.1499996185303 389796.061093098 5820082.71858871 59.8097336249404 389796.712809071 5820110.55979859 59.8097336249404 389796.712197757 5820110.55889232 31.1499996185303 389796.060481783 5820082.71768246 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365524">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389671.996000476 5820152.62928083 31.1499996185303 389671.996611718 5820152.63018712 59.8097336249404 389672.695131163 5820155.75934896 59.8097336249404 389672.69451992 5820155.75844267 31.1499996185303 389671.996000476 5820152.62928083 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365582">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389817.472329445 5820111.00799631 31.1499996185303 389817.472940771 5820111.00890258 59.8097336249404 389819.457907551 5820108.87291906 59.8097336249404 389819.457296224 5820108.87201279 31.1499996185303 389817.472329445 5820111.00799631 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365629">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389758.862654853 5820118.56535401 31.1499996185303 389758.863266146 5820118.56626028 59.8097336249404 389758.16520599 5820115.4291569 59.8097336249404 389758.164594698 5820115.42825063 31.1499996185303 389758.862654853 5820118.56535401 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365609">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389812.178893248 5820082.85961604 31.1499996185303 389812.179504571 5820082.86052229 59.8097336249404 389812.220515766 5820084.75961513 59.8097336249404 389812.219904442 5820084.75870888 31.1499996185303 389812.178893248 5820082.85961604 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365628">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389761.300987075 5820118.50981242 31.1499996185303 389761.301598369 5820118.51071869 59.8097336249404 389758.863266146 5820118.56626028 59.8097336249404 389758.862654853 5820118.56535401 31.1499996185303 389761.300987075 5820118.50981242 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365630">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389758.164594698 5820115.42825063 31.1499996185303 389758.16520599 5820115.4291569 59.8097336249404 389756.320229726 5820112.79723305 59.8097336249404 389756.319618435 5820112.79632678 31.1499996185303 389758.164594698 5820115.42825063 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365523">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389669.538155839 5820152.68580477 31.1499996185303 389669.53876708 5820152.68671106 59.8097336249404 389671.996611718 5820152.63018712 59.8097336249404 389671.996000476 5820152.62928083 31.1499996185303 389669.538155839 5820152.68580477 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365635">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389744.673266554 5820113.05816894 31.1499996185303 389744.673877838 5820113.05907521 59.8097336249404 389742.948873821 5820115.77112214 59.8097336249404 389742.948262537 5820115.77021587 31.1499996185303 389744.673266554 5820113.05816894 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365530">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389686.153767021 5820158.1102143 31.1499996185303 389686.154378272 5820158.1111206 59.8097336249404 389687.872165973 5820155.40470399 59.8097336249404 389687.871554721 5820155.4037977 31.1499996185303 389686.153767021 5820158.1102143 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365633">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389750.442314784 5820110.51513976 31.1499996185303 389750.442926071 5820110.51604603 59.8097336249404 389747.305811357 5820111.2141041 59.8097336249404 389747.305200071 5820111.21319784 31.1499996185303 389750.442314784 5820110.51513976 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365634">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389747.305200071 5820111.21319784 31.1499996185303 389747.305811357 5820111.2141041 59.8097336249404 389744.673877838 5820113.05907521 59.8097336249404 389744.673266554 5820113.05816894 31.1499996185303 389747.305200071 5820111.21319784 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365592">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389835.559082727 5820094.69839145 31.1499996185303 389835.559694063 5820094.69929771 59.8097336249404 389834.353294243 5820093.32892694 59.8097336249404 389834.352682907 5820093.32802068 31.1499996185303 389835.559082727 5820094.69839145 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365607">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389819.196489132 5820084.6082859 31.1499996185303 389819.19710046 5820084.60919215 59.8097336249404 389819.156089285 5820082.71010034 59.8097336249404 389819.155477958 5820082.70919409 31.1499996185303 389819.196489132 5820084.6082859 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365639">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389739.130122322 5820084.03875036 31.1499996185303 389739.130733603 5820084.03965662 59.8097336249404 389724.890650981 5820084.36915099 59.8097336249404 389724.890039708 5820084.36824474 31.1499996185303 389739.130122322 5820084.03875036 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365507">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389689.31768395 5820085.19633251 31.1499996185303 389689.318295202 5820085.19723877 59.8097336249404 389690.130494516 5820120.15885762 59.8097336249404 389690.129883263 5820120.15795135 31.1499996185303 389689.31768395 5820085.19633251 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365503">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389707.392188436 5820116.61996669 31.1499996185303 389707.392799699 5820116.62087296 59.8097336249404 389706.84320274 5820119.77988154 59.8097336249404 389706.842591477 5820119.77897526 31.1499996185303 389707.392188436 5820116.61996669 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365638">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389739.953738273 5820118.99099375 31.1499996185303 389739.954349555 5820118.99190003 59.8097336249404 389739.130733603 5820084.03965662 59.8097336249404 389739.130122322 5820084.03875036 31.1499996185303 389739.953738273 5820118.99099375 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365502">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389709.10872101 5820113.91174341 31.1499996185303 389709.109332275 5820113.91264968 59.8097336249404 389707.392799699 5820116.62087296 59.8097336249404 389707.392188436 5820116.61996669 31.1499996185303 389709.10872101 5820113.91174341 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365637">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389742.392681352 5820118.93544054 31.1499996185303 389742.393292635 5820118.93634682 59.8097336249404 389739.954349555 5820118.99190003 59.8097336249404 389739.953738273 5820118.99099375 31.1499996185303 389742.392681352 5820118.93544054 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365555">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389754.692695467 5820158.29422815 31.1499996185303 389754.693306757 5820158.29513445 59.8097336249404 389757.317344307 5820156.45214418 59.8097336249404 389757.316733015 5820156.45123789 31.1499996185303 389754.692695467 5820158.29422815 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365521">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389656.113168984 5820188.00066992 31.1499996185303 389656.113780217 5820188.00157623 59.8097336249404 389670.33997904 5820187.64793054 59.8097336249404 389670.339367799 5820187.64702423 31.1499996185303 389656.113168984 5820188.00066992 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365544">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389723.478615606 5820154.57138126 31.1499996185303 389723.479226878 5820154.57228756 59.8097336249404 389724.031804608 5820151.40956164 59.8097336249404 389724.031193335 5820151.40865535 31.1499996185303 389723.478615606 5820154.57138126 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365529">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389683.530339706 5820159.95319326 31.1499996185303 389683.530950955 5820159.95409956 59.8097336249404 389686.154378272 5820158.1111206 59.8097336249404 389686.153767021 5820158.1102143 31.1499996185303 389683.530339706 5820159.95319326 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365560">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389762.853327822 5820185.44697792 31.1499996185303 389762.853939117 5820185.44788423 59.8097336249404 389777.099213433 5820185.13476548 59.8097336249404 389777.098602129 5820185.13385917 31.1499996185303 389762.853327822 5820185.44697792 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365565">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389781.25913896 5820155.91336031 31.1499996185303 389781.259750265 5820155.9142666 59.8097336249404 389783.976099828 5820157.64040192 59.8097336249404 389783.975488521 5820157.63949563 31.1499996185303 389781.25913896 5820155.91336031 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365516">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389671.683870105 5820117.42318087 31.1499996185303 389671.684481348 5820117.42408714 59.8097336249404 389671.129224046 5820120.6063948 59.8097336249404 389671.128612804 5820120.60548853 31.1499996185303 389671.683870105 5820117.42318087 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365573">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389798.438481513 5820184.65037401 31.1499996185303 389798.439092829 5820184.65128032 59.8097336249404 389812.676295978 5820184.33099174 59.8097336249404 389812.675684655 5820184.33008543 31.1499996185303 389798.438481513 5820184.65037401 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365561">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389777.098602129 5820185.13385917 31.1499996185303 389777.099213433 5820185.13476548 59.8097336249404 389776.289258133 5820150.19508216 59.8097336249404 389776.288646831 5820150.19417588 31.1499996185303 389777.098602129 5820185.13385917 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365577">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389811.135300398 5820116.99478293 31.1499996185303 389811.135911721 5820116.9956892 59.8097336249404 389810.820881412 5820103.16306718 59.8097336249404 389810.82027009 5820103.16216092 31.1499996185303 389811.135300398 5820116.99478293 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365570">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389794.647342587 5820152.92694369 31.1499996185303 389794.6479539 5820152.92784998 59.8097336249404 389795.202821573 5820149.75714665 59.8097336249404 389795.20221026 5820149.75624036 31.1499996185303 389794.647342587 5820152.92694369 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365512">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389682.391340063 5820112.6898108 31.1499996185303 389682.391951311 5820112.69071706 59.8097336249404 389679.210242139 5820112.13545156 59.8097336249404 389679.209630892 5820112.13454529 31.1499996185303 389682.391340063 5820112.6898108 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365528">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389680.401777362 5820160.65169904 31.1499996185303 389680.402388609 5820160.65260534 59.8097336249404 389683.530950955 5820159.95409956 59.8097336249404 389683.530339706 5820159.95319326 31.1499996185303 389680.401777362 5820160.65169904 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365598">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389827.625196932 5820087.35603044 31.1499996185303 389827.625808264 5820087.3569367 59.8097336249404 389824.717849899 5820087.49281762 59.8097336249404 389824.717238568 5820087.49191137 31.1499996185303 389827.625196932 5820087.35603044 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365563">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389778.708664338 5820150.13837159 31.1499996185303 389778.709275642 5820150.13927787 59.8097336249404 389779.409858174 5820153.28060473 59.8097336249404 389779.40924687 5820153.27969844 31.1499996185303 389778.708664338 5820150.13837159 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365612">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389810.303013189 5820082.38875151 31.1499996185303 389810.303624511 5820082.38965777 59.8097336249404 389796.061093098 5820082.71858871 59.8097336249404 389796.060481783 5820082.71768246 31.1499996185303 389810.303013189 5820082.38875151 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365527">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389677.243321575 5820160.09965221 31.1499996185303 389677.243932821 5820160.1005585 59.8097336249404 389680.402388609 5820160.65260534 59.8097336249404 389680.401777362 5820160.65169904 31.1499996185303 389677.243321575 5820160.09965221 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365534">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389691.687481216 5820187.14690359 31.1499996185303 389691.68809247 5820187.1478099 59.8097336249404 389705.933294495 5820186.79868533 59.8097336249404 389705.932683232 5820186.79777902 31.1499996185303 389691.687481216 5820187.14690359 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365566">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389783.975488521 5820157.63949563 31.1499996185303 389783.976099828 5820157.64040192 59.8097336249404 389787.146814371 5820158.19526616 59.8097336249404 389787.146203062 5820158.19435987 31.1499996185303 389783.975488521 5820157.63949563 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365603">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389853.795330067 5820081.8518534 31.1499996185303 389853.795941414 5820081.85275965 59.8097336249404 389854.268553405 5820081.35186402 59.8097336249404 389854.267942058 5820081.35095776 31.1499996185303 389853.795330067 5820081.8518534 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365513">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389679.209630892 5820112.13454529 31.1499996185303 389679.210242139 5820112.13545156 59.8097336249404 389676.057990557 5820112.83990023 59.8097336249404 389676.057379312 5820112.83899396 31.1499996185303 389679.209630892 5820112.13454529 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365550">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389743.155824716 5820150.96548345 31.1499996185303 389743.156436 5820150.96638974 59.8097336249404 389743.85496688 5820154.09615981 59.8097336249404 389743.854355596 5820154.09525352 31.1499996185303 389743.155824716 5820150.96548345 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365517">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389671.128612804 5820120.60548853 31.1499996185303 389671.129224046 5820120.6063948 59.8097336249404 389668.800717728 5820120.66045701 59.8097336249404 389668.800106487 5820120.65955073 31.1499996185303 389671.128612804 5820120.60548853 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365606">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389821.045561593 5820084.56822731 31.1499996185303 389821.046172922 5820084.56913357 59.8097336249404 389819.19710046 5820084.60919215 59.8097336249404 389819.196489132 5820084.6082859 31.1499996185303 389821.045561593 5820084.56822731 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365515">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389673.41469623 5820114.69637589 31.1499996185303 389673.415307473 5820114.69728216 59.8097336249404 389671.684481348 5820117.42408714 59.8097336249404 389671.683870105 5820117.42318087 31.1499996185303 389673.41469623 5820114.69637589 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365568">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389790.287530473 5820157.49317062 31.1499996185303 389790.288141783 5820157.49407691 59.8097336249404 389792.922433466 5820155.6447882 59.8097336249404 389792.921822153 5820155.64388191 31.1499996185303 389790.287530473 5820157.49317062 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365567">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389787.146203062 5820158.19435987 31.1499996185303 389787.146814371 5820158.19526616 59.8097336249404 389790.288141783 5820157.49407691 59.8097336249404 389790.287530473 5820157.49317062 31.1499996185303 389787.146203062 5820158.19435987 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365585">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389825.08641429 5820111.70963485 31.1499996185303 389825.087025621 5820111.71054112 59.8097336249404 389828.725658391 5820111.71028258 59.8097336249404 389828.725047058 5820111.70937631 31.1499996185303 389825.08641429 5820111.70963485 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365569">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389792.921822153 5820155.64388191 31.1499996185303 389792.922433466 5820155.6447882 59.8097336249404 389794.6479539 5820152.92784998 59.8097336249404 389794.647342587 5820152.92694369 31.1499996185303 389792.921822153 5820155.64388191 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365506">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389703.54734906 5820084.86459548 31.1499996185303 389703.54796032 5820084.86550173 59.8097336249404 389689.318295202 5820085.19723877 59.8097336249404 389689.31768395 5820085.19633251 31.1499996185303 389703.54734906 5820084.86459548 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365511">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389685.118764221 5820114.42061871 31.1499996185303 389685.119375472 5820114.42152498 59.8097336249404 389682.391951311 5820112.69071706 59.8097336249404 389682.391340063 5820112.6898108 31.1499996185303 389685.118764221 5820114.42061871 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365600">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389824.663662359 5820085.25311982 31.1499996185303 389824.66427369 5820085.25402607 59.8097336249404 389851.171348239 5820084.62891192 59.8097336249404 389851.170736893 5820084.62800567 31.1499996185303 389824.663662359 5820085.25311982 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365610">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389812.219904442 5820084.75870888 31.1499996185303 389812.220515766 5820084.75961513 59.8097336249404 389810.361694157 5820084.80047014 59.8097336249404 389810.361082835 5820084.79956389 31.1499996185303 389812.219904442 5820084.75870888 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365537">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389707.57742754 5820151.79674333 31.1499996185303 389707.578038803 5820151.79764962 59.8097336249404 389708.27845899 5820154.93043575 59.8097336249404 389708.277847726 5820154.92952946 31.1499996185303 389707.57742754 5820151.79674333 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365520">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389653.721821351 5820086.04012535 31.1499996185303 389653.722432583 5820086.0410316 59.8097336249404 389656.113780217 5820188.00157623 59.8097336249404 389656.113168984 5820188.00066992 31.1499996185303 389653.721821351 5820086.04012535 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365504">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389706.842591477 5820119.77897526 31.1499996185303 389706.84320274 5820119.77988154 59.8097336249404 389704.364063701 5820119.83986116 59.8097336249404 389704.36345244 5820119.83895489 31.1499996185303 389706.842591477 5820119.77897526 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365538">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389708.277847726 5820154.92952946 31.1499996185303 389708.27845899 5820154.93043575 59.8097336249404 389710.125174109 5820157.55744503 59.8097336249404 389710.124562845 5820157.55653873 31.1499996185303 389708.277847726 5820154.92952946 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365535">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389705.932683232 5820186.79777902 31.1499996185303 389705.933294495 5820186.79868533 59.8097336249404 389705.1202186 5820151.85539299 59.8097336249404 389705.119607338 5820151.8544867 31.1499996185303 389705.932683232 5820186.79777902 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365572">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389797.650902074 5820149.69989057 31.1499996185303 389797.651513389 5820149.70079686 59.8097336249404 389798.439092829 5820184.65128032 59.8097336249404 389798.438481513 5820184.65037401 31.1499996185303 389797.650902074 5820149.69989057 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365547">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389727.27504245 5820186.31607644 31.1499996185303 389727.275653725 5820186.31698275 59.8097336249404 389741.510866943 5820185.98818865 59.8097336249404 389741.51025566 5820185.98728234 31.1499996185303 389727.27504245 5820186.31607644 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365519">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389667.970767536 5820085.6952045 31.1499996185303 389667.971378776 5820085.69611076 59.8097336249404 389653.722432583 5820086.0410316 59.8097336249404 389653.721821351 5820086.04012535 31.1499996185303 389667.970767536 5820085.6952045 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365536">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389705.119607338 5820151.8544867 31.1499996185303 389705.1202186 5820151.85539299 59.8097336249404 389707.578038803 5820151.79764962 59.8097336249404 389707.57742754 5820151.79674333 31.1499996185303 389705.119607338 5820151.8544867 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365518">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389668.800106487 5820120.65955073 31.1499996185303 389668.800717728 5820120.66045701 59.8097336249404 389667.971378776 5820085.69611076 59.8097336249404 389667.970767536 5820085.6952045 31.1499996185303 389668.800106487 5820120.65955073 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365542">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389719.131439964 5820159.12821427 31.1499996185303 389719.132051234 5820159.12912056 59.8097336249404 389721.759080662 5820157.28302147 59.8097336249404 389721.75846939 5820157.28211518 31.1499996185303 389719.131439964 5820159.12821427 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365508">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389690.129883263 5820120.15795135 31.1499996185303 389690.130494516 5820120.15885762 59.8097336249404 389687.680604739 5820120.21645095 59.8097336249404 389687.679993487 5820120.21554467 31.1499996185303 389690.129883263 5820120.15795135 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365587">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389832.155705097 5820110.49735262 31.1499996185303 389832.156316431 5820110.49825889 59.8097336249404 389834.986745938 5820108.21192496 59.8097336249404 389834.986134601 5820108.21101869 31.1499996185303 389832.155705097 5820110.49735262 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365589">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389836.8935969 5820105.11284186 31.1499996185303 389836.894208238 5820105.11374813 59.8097336249404 389837.66021987 5820101.55697791 59.8097336249404 389837.659608533 5820101.55607165 31.1499996185303 389836.8935969 5820105.11284186 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_365509">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389687.679993487 5820120.21554467 31.1499996185303 389687.680604739 5820120.21645095 59.8097336249404 389686.976153948 5820117.06421085 59.8097336249404 389686.975542697 5820117.06330458 31.1499996185303 389687.679993487 5820120.21554467 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="GEOM_365641">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389724.890650981 5820084.36915099 59.8097336249404 389739.130733603 5820084.03965662 59.8097336249404 389739.954349555 5820118.99190003 59.8097336249404 389742.393292635 5820118.93634682 59.8097336249404 389742.948873821 5820115.77112214 59.8097336249404 389744.673877838 5820113.05907521 59.8097336249404 389747.305811357 5820111.2141041 59.8097336249404 389750.442926071 5820110.51604603 59.8097336249404 389753.608784218 5820111.07222495 59.8097336249404 389756.320229726 5820112.79723305 59.8097336249404 389758.16520599 5820115.4291569 59.8097336249404 389758.863266146 5820118.56626028 59.8097336249404 389761.301598369 5820118.51071869 59.8097336249404 389760.480882274 5820083.55048746 59.8097336249404 389774.716012029 5820083.21742563 59.8097336249404 389775.541203715 5820118.15621029 59.8097336249404 389777.971698139 5820118.10569943 59.8097336249404 389778.526024994 5820114.93866802 59.8097336249404 389780.2503833 5820112.22480274 59.8097336249404 389782.882258551 5820110.37678214 59.8097336249404 389786.020569212 5820109.67748047 59.8097336249404 389789.188222414 5820110.23179334 59.8097336249404 389791.902095989 5820111.95614516 59.8097336249404 389793.74951402 5820114.58802334 59.8097336249404 389794.4494275 5820117.72631111 59.8097336249404 389796.887137157 5820117.67017142 59.8097336249404 389796.712809071 5820110.55979859 59.8097336249404 389796.061093098 5820082.71858871 59.8097336249404 389810.303624511 5820082.38965777 59.8097336249404 389810.361694157 5820084.80047014 59.8097336249404 389812.220515766 5820084.75961513 59.8097336249404 389812.179504571 5820082.86052229 59.8097336249404 389819.156089285 5820082.71010034 59.8097336249404 389819.19710046 5820084.60919215 59.8097336249404 389821.046172922 5820084.56913357 59.8097336249404 389820.984884738 5820082.11749275 59.8097336249404 389854.268553405 5820081.35186402 59.8097336249404 389853.795941414 5820081.85275965 59.8097336249404 389851.082211512 5820081.93245971 59.8097336249404 389851.171348239 5820084.62891192 59.8097336249404 389824.66427369 5820085.25402607 59.8097336249404 389824.717849899 5820087.49281762 59.8097336249404 389827.625808264 5820087.3569367 59.8097336249404 389830.496108408 5820087.84000771 59.8097336249404 389833.200153698 5820088.9194571 59.8097336249404 389835.613735199 5820090.54637992 59.8097336249404 389836.334705096 5820091.1985048 59.8097336249404 389834.353294243 5820093.32892694 59.8097336249404 389835.559694063 5820094.69929771 59.8097336249404 389837.197913304 5820097.94789848 59.8097336249404 389837.66021987 5820101.55697791 59.8097336249404 389836.894208238 5820105.11374813 59.8097336249404 389834.986745938 5820108.21192496 59.8097336249404 389832.156316431 5820110.49825889 59.8097336249404 389828.725658391 5820111.71028258 59.8097336249404 389825.087025621 5820111.71054112 59.8097336249404 389821.656645533 5820110.49904744 59.8097336249404 389819.457907551 5820108.87291906 59.8097336249404 389817.472940771 5820111.00890258 59.8097336249404 389815.486049867 5820108.71290423 59.8097336249404 389813.714185398 5820105.07991277 59.8097336249404 389813.279672993 5820103.03589049 59.8097336249404 389810.820881412 5820103.16306718 59.8097336249404 389811.135911721 5820116.9956892 59.8097336249404 389811.881579722 5820149.67908332 59.8097336249404 389812.656558165 5820183.83885363 59.8097336249404 389812.676295978 5820184.33099174 59.8097336249404 389798.439092829 5820184.65128032 59.8097336249404 389797.651513389 5820149.70079686 59.8097336249404 389795.202821573 5820149.75714665 59.8097336249404 389794.6479539 5820152.92784998 59.8097336249404 389792.922433466 5820155.6447882 59.8097336249404 389790.288141783 5820157.49407691 59.8097336249404 389787.146814371 5820158.19526616 59.8097336249404 389783.976099828 5820157.64040192 59.8097336249404 389781.259750265 5820155.9142666 59.8097336249404 389779.409858174 5820153.28060473 59.8097336249404 389778.709275642 5820150.13927787 59.8097336249404 389776.289258133 5820150.19508216 59.8097336249404 389777.099213433 5820185.13476548 59.8097336249404 389762.853939117 5820185.44788423 59.8097336249404 389762.059043164 5820150.52988353 59.8097336249404 389759.590217918 5820150.58661535 59.8097336249404 389759.036960886 5820153.74569322 59.8097336249404 389757.317344307 5820156.45214418 59.8097336249404 389754.693306757 5820158.29513445 59.8097336249404 389751.562903213 5820158.99306519 59.8097336249404 389748.404435434 5820158.44040872 59.8097336249404 389745.697365271 5820156.72081019 59.8097336249404 389743.85496688 5820154.09615981 59.8097336249404 389743.156436 5820150.96638974 59.8097336249404 389740.687012513 5820151.0237439 59.8097336249404 389741.510866943 5820185.98818865 59.8097336249404 389727.275653725 5820186.31698275 59.8097336249404 389726.46095123 5820151.35236276 59.8097336249404 389724.031804608 5820151.40956164 59.8097336249404 389723.479226878 5820154.57228756 59.8097336249404 389721.759080662 5820157.28302147 59.8097336249404 389719.132051234 5820159.12912056 59.8097336249404 389715.998643145 5820159.82954932 59.8097336249404 389712.835916533 5820159.27758378 59.8097336249404 389710.125174109 5820157.55744503 59.8097336249404 389708.27845899 5820154.93043575 59.8097336249404 389707.578038803 5820151.79764962 59.8097336249404 389705.1202186 5820151.85539299 59.8097336249404 389705.933294495 5820186.79868533 59.8097336249404 389691.68809247 5820187.1478099 59.8097336249404 389690.882047148 5820152.18912488 59.8097336249404 389688.424225854 5820152.24686928 59.8097336249404 389687.872165973 5820155.40470399 59.8097336249404 389686.154378272 5820158.1111206 59.8097336249404 389683.530950955 5820159.95409956 59.8097336249404 389680.402388609 5820160.65260534 59.8097336249404 389677.243932821 5820160.1005585 59.8097336249404 389674.538104814 5820158.38215697 59.8097336249404 389672.695131163 5820155.75934896 59.8097336249404 389671.996611718 5820152.63018712 59.8097336249404 389669.53876708 5820152.68671106 59.8097336249404 389670.33997904 5820187.64793054 59.8097336249404 389656.113780217 5820188.00157623 59.8097336249404 389653.722432583 5820086.0410316 59.8097336249404 389667.971378776 5820085.69611076 59.8097336249404 389668.800717728 5820120.66045701 59.8097336249404 389671.129224046 5820120.6063948 59.8097336249404 389671.684481348 5820117.42408714 59.8097336249404 389673.415307473 5820114.69728216 59.8097336249404 389676.057990557 5820112.83990023 59.8097336249404 389679.210242139 5820112.13545156 59.8097336249404 389682.391951311 5820112.69071706 59.8097336249404 389685.119375472 5820114.42152498 59.8097336249404 389686.976153948 5820117.06421085 59.8097336249404 389687.680604739 5820120.21645095 59.8097336249404 389690.130494516 5820120.15885762 59.8097336249404 389689.318295202 5820085.19723877 59.8097336249404 389703.54796032 5820084.86550173 59.8097336249404 389704.364063701 5820119.83986116 59.8097336249404 389706.84320274 5820119.77988154 59.8097336249404 389707.392799699 5820116.62087296 59.8097336249404 389709.109332275 5820113.91264968 59.8097336249404 389711.731492721 5820112.0672544 59.8097336249404 389714.860619174 5820111.36629648 59.8097336249404 389718.019626276 5820111.91528137 59.8097336249404 389720.727859914 5820113.63180739 59.8097336249404 389722.573273003 5820116.25456887 59.8097336249404 389723.274220405 5820119.38307326 59.8097336249404 389725.722855663 5820119.32367295 59.8097336249404 389724.890650981 5820084.36915099 59.8097336249404</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface gml:id="GEOM_365492">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>389739.130122322 5820084.03875036 31.1499996185303 389724.890039708 5820084.36824474 31.1499996185303 389725.722244389 5820119.32276668 31.1499996185303 389723.273609132 5820119.38216699 31.1499996185303 389722.572661731 5820116.2536626 31.1499996185303 389720.727248643 5820113.63090112 31.1499996185303 389718.019015007 5820111.9143751 31.1499996185303 389714.860007906 5820111.36539021 31.1499996185303 389711.730881455 5820112.06634813 31.1499996185303 389709.10872101 5820113.91174341 31.1499996185303 389707.392188436 5820116.61996669 31.1499996185303 389706.842591477 5820119.77897526 31.1499996185303 389704.36345244 5820119.83895489 31.1499996185303 389703.54734906 5820084.86459548 31.1499996185303 389689.31768395 5820085.19633251 31.1499996185303 389690.129883263 5820120.15795135 31.1499996185303 389687.679993487 5820120.21554467 31.1499996185303 389686.975542697 5820117.06330458 31.1499996185303 389685.118764221 5820114.42061871 31.1499996185303 389682.391340063 5820112.6898108 31.1499996185303 389679.209630892 5820112.13454529 31.1499996185303 389676.057379312 5820112.83899396 31.1499996185303 389673.41469623 5820114.69637589 31.1499996185303 389671.683870105 5820117.42318087 31.1499996185303 389671.128612804 5820120.60548853 31.1499996185303 389668.800106487 5820120.65955073 31.1499996185303 389667.970767536 5820085.6952045 31.1499996185303 389653.721821351 5820086.04012535 31.1499996185303 389656.113168984 5820188.00066992 31.1499996185303 389670.339367799 5820187.64702423 31.1499996185303 389669.538155839 5820152.68580477 31.1499996185303 389671.996000476 5820152.62928083 31.1499996185303 389672.69451992 5820155.75844267 31.1499996185303 389674.537493569 5820158.38125067 31.1499996185303 389677.243321575 5820160.09965221 31.1499996185303 389680.401777362 5820160.65169904 31.1499996185303 389683.530339706 5820159.95319326 31.1499996185303 389686.153767021 5820158.1102143 31.1499996185303 389687.871554721 5820155.4037977 31.1499996185303 389688.423614602 5820152.24596299 31.1499996185303 389690.881435895 5820152.18821859 31.1499996185303 389691.687481216 5820187.14690359 31.1499996185303 389705.932683232 5820186.79777902 31.1499996185303 389705.119607338 5820151.8544867 31.1499996185303 389707.57742754 5820151.79674333 31.1499996185303 389708.277847726 5820154.92952946 31.1499996185303 389710.124562845 5820157.55653873 31.1499996185303 389712.835305267 5820159.27667749 31.1499996185303 389715.998031877 5820159.82864303 31.1499996185303 389719.131439964 5820159.12821427 31.1499996185303 389721.75846939 5820157.28211518 31.1499996185303 389723.478615606 5820154.57138126 31.1499996185303 389724.031193335 5820151.40865535 31.1499996185303 389726.460339956 5820151.35145647 31.1499996185303 389727.27504245 5820186.31607644 31.1499996185303 389741.51025566 5820185.98728234 31.1499996185303 389740.686401231 5820151.02283761 31.1499996185303 389743.155824716 5820150.96548345 31.1499996185303 389743.854355596 5820154.09525352 31.1499996185303 389745.696753985 5820156.7199039 31.1499996185303 389748.403824148 5820158.43950242 31.1499996185303 389751.562291924 5820158.99215889 31.1499996185303 389754.692695467 5820158.29422815 31.1499996185303 389757.316733015 5820156.45123789 31.1499996185303 389759.036349593 5820153.74478693 31.1499996185303 389759.589606625 5820150.58570906 31.1499996185303 389762.058431869 5820150.52897724 31.1499996185303 389762.853327822 5820185.44697792 31.1499996185303 389777.098602129 5820185.13385917 31.1499996185303 389776.288646831 5820150.19417588 31.1499996185303 389778.708664338 5820150.13837159 31.1499996185303 389779.40924687 5820153.27969844 31.1499996185303 389781.25913896 5820155.91336031 31.1499996185303 389783.975488521 5820157.63949563 31.1499996185303 389787.146203062 5820158.19435987 31.1499996185303 389790.287530473 5820157.49317062 31.1499996185303 389792.921822153 5820155.64388191 31.1499996185303 389794.647342587 5820152.92694369 31.1499996185303 389795.20221026 5820149.75624036 31.1499996185303 389797.650902074 5820149.69989057 31.1499996185303 389798.438481513 5820184.65037401 31.1499996185303 389812.675684655 5820184.33008543 31.1499996185303 389812.655946841 5820183.83794732 31.1499996185303 389811.880968399 5820149.67817703 31.1499996185303 389811.135300398 5820116.99478293 31.1499996185303 389810.82027009 5820103.16216092 31.1499996185303 389813.279061669 5820103.03498423 31.1499996185303 389813.713574074 5820105.07900651 31.1499996185303 389815.485438542 5820108.71199796 31.1499996185303 389817.472329445 5820111.00799631 31.1499996185303 389819.457296224 5820108.87201279 31.1499996185303 389821.656034205 5820110.49814117 31.1499996185303 389825.08641429 5820111.70963485 31.1499996185303 389828.725047058 5820111.70937631 31.1499996185303 389832.155705097 5820110.49735262 31.1499996185303 389834.986134601 5820108.21101869 31.1499996185303 389836.8935969 5820105.11284186 31.1499996185303 389837.659608533 5820101.55607165 31.1499996185303 389837.197301966 5820097.94699222 31.1499996185303 389835.559082727 5820094.69839145 31.1499996185303 389834.352682907 5820093.32802068 31.1499996185303 389836.334093759 5820091.19759854 31.1499996185303 389835.613123863 5820090.54547367 31.1499996185303 389833.199542362 5820088.91855084 31.1499996185303 389830.495497074 5820087.83910145 31.1499996185303 389827.625196932 5820087.35603044 31.1499996185303 389824.717238568 5820087.49191137 31.1499996185303 389824.663662359 5820085.25311982 31.1499996185303 389851.170736893 5820084.62800567 31.1499996185303 389851.081600166 5820081.93155346 31.1499996185303 389853.795330067 5820081.8518534 31.1499996185303 389854.267942058 5820081.35095776 31.1499996185303 389820.98427341 5820082.1165865 31.1499996185303 389821.045561593 5820084.56822731 31.1499996185303 389819.196489132 5820084.6082859 31.1499996185303 389819.155477958 5820082.70919409 31.1499996185303 389812.178893248 5820082.85961604 31.1499996185303 389812.219904442 5820084.75870888 31.1499996185303 389810.361082835 5820084.79956389 31.1499996185303 389810.303013189 5820082.38875151 31.1499996185303 389796.060481783 5820082.71768246 31.1499996185303 389796.712197757 5820110.55889232 31.1499996185303 389796.886525843 5820117.66926515 31.1499996185303 389794.448816187 5820117.72540484 31.1499996185303 389793.748902707 5820114.58711707 31.1499996185303 389791.901484677 5820111.95523889 31.1499996185303 389789.187611104 5820110.23088708 31.1499996185303 389786.019957904 5820109.6765742 31.1499996185303 389782.881647245 5820110.37587587 31.1499996185303 389780.249771995 5820112.22389647 31.1499996185303 389778.52541369 5820114.93776175 31.1499996185303 389777.971086835 5820118.10479316 31.1499996185303 389775.540592412 5820118.15530402 31.1499996185303 389774.715400728 5820083.21651938 31.1499996185303 389760.48027098 5820083.54958121 31.1499996185303 389761.300987075 5820118.50981242 31.1499996185303 389758.862654853 5820118.56535401 31.1499996185303 389758.164594698 5820115.42825063 31.1499996185303 389756.319618435 5820112.79632678 31.1499996185303 389753.608172928 5820111.07131869 31.1499996185303 389750.442314784 5820110.51513976 31.1499996185303 389747.305200071 5820111.21319784 31.1499996185303 389744.673266554 5820113.05816894 31.1499996185303 389742.948262537 5820115.77021587 31.1499996185303 389742.392681352 5820118.93544054 31.1499996185303 389739.953738273 5820118.99099375 31.1499996185303 389739.130122322 5820084.03875036 31.1499996185303</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
		</bldg:Building>
	</cityObjectMember>
</CityModel>