<?xml version="1.0" encoding="UTF-8"?>
<core:CityModel xmlns:smil20="http://www.w3.org/2001/SMIL20/" xmlns:grp="http://www.opengis.net/citygml/cityobjectgroup/1.0" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:smil20lang="http://www.w3.org/2001/SMIL20/Language" xmlns:base="http://www.citygml.org/citygml/profiles/base/1.0" xmlns:luse="http://www.opengis.net/citygml/landuse/1.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:frn="http://www.opengis.net/citygml/cityfurniture/1.0" xmlns:tex="http://www.opengis.net/citygml/texturedsurface/1.0" xmlns:xAL="urn:oasis:names:tc:ciq:xsdschema:xAL:2.0" xmlns:bldg="http://www.opengis.net/citygml/building/1.0" xmlns:sch="http://www.ascc.net/xml/schematron" xmlns:core="http://www.opengis.net/citygml/1.0" xmlns:app="http://www.opengis.net/citygml/appearance/1.0" xmlns:veg="http://www.opengis.net/citygml/vegetation/1.0" xmlns:gml="http://www.opengis.net/gml" xmlns:gen="http://www.opengis.net/citygml/generics/1.0" xmlns:dem="http://www.opengis.net/citygml/relief/1.0" xmlns:wtr="http://www.opengis.net/citygml/waterbody/1.0" xmlns:tran="http://www.opengis.net/citygml/transportation/1.0">
<gml:boundedBy>
<gml:Envelope srsDimension="3">
<gml:lowerCorner>390816.40383755 5819139.61584261 33.7799987792969</gml:lowerCorner>
<gml:upperCorner>390816.40446077 5819139.61676511 62.97</gml:upperCorner>
</gml:Envelope>
</gml:boundedBy>
<core:cityObjectMember>
<gen:GenericCityObject>
<gen:stringAttribute name="xyz_type">
<gen:value>xyz_point</gen:value>
</gen:stringAttribute>
<gen:lod4Geometry>
<gml:Point srsDimension="3">
<gml:pos>390816.40383755 5819139.61584261 33.7799987792969</gml:pos>
</gml:Point>
</gen:lod4Geometry>
</gen:GenericCityObject>
</core:cityObjectMember>
<core:cityObjectMember>
<gen:GenericCityObject>
<gen:stringAttribute name="xyz_type">
<gen:value>xyz_point</gen:value>
</gen:stringAttribute>
<gen:lod4Geometry>
<gml:Point srsDimension="3">
<gml:pos>390816.404460692 5819139.61676499 62.9663112147753</gml:pos>
</gml:Point>
</gen:lod4Geometry>
</gen:GenericCityObject>
</core:cityObjectMember>
<core:cityObjectMember>
<gen:GenericCityObject>
<gen:stringAttribute name="xyz_type">
<gen:value>xyz_point</gen:value>
</gen:stringAttribute>
<gen:lod4Geometry>
<gml:Point srsDimension="3">
<gml:pos>390816.40446077 5819139.61676511 62.97</gml:pos>
</gml:Point>
</gen:lod4Geometry>
</gen:GenericCityObject>
</core:cityObjectMember>
</core:CityModel>
