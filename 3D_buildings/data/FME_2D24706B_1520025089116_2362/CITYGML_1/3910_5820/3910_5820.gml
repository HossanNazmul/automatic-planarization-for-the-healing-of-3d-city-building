<?xml version="1.0" encoding="UTF-8"?>
<CityModel xmlns:bldg="http://www.opengis.net/citygml/building/1.0" xmlns:app="http://www.opengis.net/citygml/appearance/1.0" xmlns:smil20="http://www.w3.org/2001/SMIL20/" xmlns:grp="http://www.opengis.net/citygml/cityobjectgroup/1.0" xmlns:base="http://www.citygml.org/citygml/profiles/base/1.0" xmlns:frn="http://www.opengis.net/citygml/cityfurniture/1.0" xmlns:smil20lang="http://www.w3.org/2001/SMIL20/Language" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:wtr="http://www.opengis.net/citygml/waterbody/1.0" xmlns:tex="http://www.opengis.net/citygml/texturedsurface/1.0" xmlns:veg="http://www.opengis.net/citygml/vegetation/1.0" xmlns:sch="http://www.ascc.net/xml/schematron" xmlns:gen="http://www.opengis.net/citygml/generics/1.0" xmlns:dem="http://www.opengis.net/citygml/relief/1.0" xmlns:gml="http://www.opengis.net/gml" xmlns:tran="http://www.opengis.net/citygml/transportation/1.0" xmlns:xAL="urn:oasis:names:tc:ciq:xsdschema:xAL:2.0" xmlns:luse="http://www.opengis.net/citygml/landuse/1.0" xmlns="http://www.opengis.net/citygml/1.0">
	<gml:boundedBy>
		<gml:Envelope srsName="EPSG:25833" srsDimension="3">
			<gml:lowerCorner>391890.897973695 5820113.35641551 34.4300003051758</gml:lowerCorner>
			<gml:upperCorner>391970.721147864 5820147.19715813 122.469078063965</gml:upperCorner>
		</gml:Envelope>
	</gml:boundedBy>
	<cityObjectMember>
		<bldg:Building gml:id="BLDG_0003000e0080fce9">
			<gml:name>Marienkirche</gml:name>
			<bldg:roofType/>
			<bldg:boundedBy>
				<bldg:GroundSurface gml:id="GEOM_857360">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391939.495056784 5820122.47244254 34.4300003051758 391939.808611006 5820114.87543902 34.4300003051758 391931.551692329 5820114.36177419 34.4300003051758 391931.152276033 5820121.99391596 34.4300003051758 391939.495056784 5820122.47244254 34.4300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface gml:id="GEOM_857378">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391930.258982915 5820139.06318292 34.4300003051758 391929.941664042 5820145.12659392 34.4300003051758 391938.541630757 5820145.57271202 34.4300003051758 391938.792031328 5820139.5058298 34.4300003051758 391930.258982915 5820139.06318292 34.4300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface gml:id="GEOM_857384">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391945.785755169 5820139.86862512 34.4300003051758 391945.331791917 5820145.92494755 34.4300003051758 391953.550256295 5820146.35127544 34.4300003051758 391953.650474438 5820140.2766028 34.4300003051758 391945.785755169 5820139.86862512 34.4300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface gml:id="GEOM_857369">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391946.687525568 5820127.83811214 34.4300003051758 391947.05720717 5820122.90619359 34.4300003051758 391939.495056784 5820122.47244254 34.4300003051758 391939.292584835 5820127.37807138 34.4300003051758 391946.687525568 5820127.83811214 34.4300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface gml:id="GEOM_857383">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391953.650474438 5820140.2766028 34.4300003051758 391953.550256295 5820146.35127544 34.4300003051758 391969.853030076 5820147.196972 34.4300003051758 391970.14761791 5820141.1323824 34.4300003051758 391953.650474438 5820140.2766028 34.4300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface gml:id="GEOM_857364">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391905.193368559 5820137.76291885 34.4300003051758 391914.1847822 5820138.22934318 34.4300003051758 391914.784967758 5820125.85344751 34.4300003051758 391905.455155887 5820125.27303799 34.4300003051758 391905.193368559 5820137.76291885 34.4300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface gml:id="GEOM_857380">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391953.848331889 5820128.28358734 34.4300003051758 391953.930542647 5820123.30043548 34.4300003051758 391947.05720717 5820122.90619359 34.4300003051758 391946.687525568 5820127.83811214 34.4300003051758 391953.848331889 5820128.28358734 34.4300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface gml:id="GEOM_857359">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391947.05720717 5820122.90619359 34.4300003051758 391947.622730258 5820115.36155718 34.4300003051758 391939.808611006 5820114.87543902 34.4300003051758 391939.495056784 5820122.47244254 34.4300003051758 391947.05720717 5820122.90619359 34.4300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface gml:id="GEOM_857358">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391931.152276033 5820121.99391596 34.4300003051758 391931.551692329 5820114.36177419 34.4300003051758 391923.819962631 5820113.88078148 34.4300003051758 391923.353243116 5820121.54657774 34.4300003051758 391931.152276033 5820121.99391596 34.4300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface gml:id="GEOM_857361">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391923.353243116 5820121.54657774 34.4300003051758 391923.819962631 5820113.88078148 34.4300003051758 391915.391028338 5820113.35641551 34.4300003051758 391915.017023775 5820121.0684275 34.4300003051758 391923.353243116 5820121.54657774 34.4300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface gml:id="GEOM_857367">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391891.192085195 5820137.03661045 34.4300003051758 391890.897973695 5820143.10122585 34.4300003051758 391905.066073256 5820143.83618755 34.4300003051758 391905.193368559 5820137.76291885 34.4300003051758 391891.192085195 5820137.03661045 34.4300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface gml:id="GEOM_857365">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391891.192085195 5820137.03661045 34.4300003051758 391905.193368559 5820137.76291885 34.4300003051758 391905.455155887 5820125.27303799 34.4300003051758 391891.803761172 5820124.42378189 34.4300003051758 391891.192085195 5820137.03661045 34.4300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface gml:id="GEOM_857366">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391905.193368559 5820137.76291885 34.4300003051758 391905.066073256 5820143.83618755 34.4300003051758 391913.890670736 5820144.29395826 34.4300003051758 391914.1847822 5820138.22934318 34.4300003051758 391905.193368559 5820137.76291885 34.4300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface gml:id="GEOM_857363">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391905.455155887 5820125.27303799 34.4300003051758 391905.554660308 5820120.52568368 34.4300003051758 391892.030424586 5820119.74995831 34.4300003051758 391891.803761172 5820124.42378189 34.4300003051758 391905.455155887 5820125.27303799 34.4300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface gml:id="GEOM_857362">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391914.784967758 5820125.85344751 34.4300003051758 391915.017023775 5820121.0684275 34.4300003051758 391905.554660308 5820120.52568368 34.4300003051758 391905.455155887 5820125.27303799 34.4300003051758 391914.784967758 5820125.85344751 34.4300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface gml:id="GEOM_857375">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391914.1847822 5820138.22934318 34.4300003051758 391922.311872907 5820138.65093125 34.4300003051758 391923.059685389 5820126.36821939 34.4300003051758 391914.784967758 5820125.85344751 34.4300003051758 391914.1847822 5820138.22934318 34.4300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface gml:id="GEOM_857373">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391938.792031328 5820139.5058298 34.4300003051758 391945.785755169 5820139.86862512 34.4300003051758 391946.687525568 5820127.83811214 34.4300003051758 391939.292584835 5820127.37807138 34.4300003051758 391938.792031328 5820139.5058298 34.4300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface gml:id="GEOM_857374">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391930.258982915 5820139.06318292 34.4300003051758 391938.792031328 5820139.5058298 34.4300003051758 391939.292584835 5820127.37807138 34.4300003051758 391930.897835255 5820126.8558323 34.4300003051758 391930.258982915 5820139.06318292 34.4300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface gml:id="GEOM_857371">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391923.059685389 5820126.36821939 34.4300003051758 391923.353243116 5820121.54657774 34.4300003051758 391915.017023775 5820121.0684275 34.4300003051758 391914.784967758 5820125.85344751 34.4300003051758 391923.059685389 5820126.36821939 34.4300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface gml:id="GEOM_857372">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391922.311872907 5820138.65093125 34.4300003051758 391930.258982915 5820139.06318292 34.4300003051758 391930.897835255 5820126.8558323 34.4300003051758 391923.059685389 5820126.36821939 34.4300003051758 391922.311872907 5820138.65093125 34.4300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface gml:id="GEOM_857382">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391945.785755169 5820139.86862512 34.4300003051758 391953.650474438 5820140.2766028 34.4300003051758 391953.848331889 5820128.28358734 34.4300003051758 391946.687525568 5820127.83811214 34.4300003051758 391945.785755169 5820139.86862512 34.4300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface gml:id="GEOM_857377">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391938.792031328 5820139.5058298 34.4300003051758 391938.541630757 5820145.57271202 34.4300003051758 391945.331791917 5820145.92494755 34.4300003051758 391945.785755169 5820139.86862512 34.4300003051758 391938.792031328 5820139.5058298 34.4300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface gml:id="GEOM_857379">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391914.1847822 5820138.22934318 34.4300003051758 391913.890670736 5820144.29395826 34.4300003051758 391921.942875549 5820144.71166156 34.4300003051758 391922.311872907 5820138.65093125 34.4300003051758 391914.1847822 5820138.22934318 34.4300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface gml:id="GEOM_857376">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391922.311872907 5820138.65093125 34.4300003051758 391921.942875549 5820144.71166156 34.4300003051758 391929.941664042 5820145.12659392 34.4300003051758 391930.258982915 5820139.06318292 34.4300003051758 391922.311872907 5820138.65093125 34.4300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface gml:id="GEOM_857370">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391939.292584835 5820127.37807138 34.4300003051758 391939.495056784 5820122.47244254 34.4300003051758 391931.152276033 5820121.99391596 34.4300003051758 391930.897835255 5820126.8558323 34.4300003051758 391939.292584835 5820127.37807138 34.4300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface gml:id="GEOM_857368">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391930.897835255 5820126.8558323 34.4300003051758 391931.152276033 5820121.99391596 34.4300003051758 391923.353243116 5820121.54657774 34.4300003051758 391923.059685389 5820126.36821939 34.4300003051758 391930.897835255 5820126.8558323 34.4300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface gml:id="GEOM_857381">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391970.720763008 5820129.33322466 34.4300003051758 391953.848331889 5820128.28358734 34.4300003051758 391953.650474438 5820140.2766028 34.4300003051758 391970.14761791 5820141.1323824 34.4300003051758 391970.720763008 5820129.33322466 34.4300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_857391">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391923.819962631 5820113.88078148 34.4300003051758 391923.82010322 5820113.88098948 41.0079400725952 391919.605750477 5820113.61897573 46.3596332160857 391915.391168926 5820113.35662351 41.0079400725952 391915.391028338 5820113.35641551 34.4300003051758 391923.819962631 5820113.88078148 34.4300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_857432">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391953.550382104 5820146.35146158 40.3162390393235 391953.550622679 5820146.35181751 51.5720678737949 391953.651036856 5820140.2774349 60.7439746313207 391953.650665236 5820140.27688509 43.3568873990314 391953.550382104 5820146.35146158 40.3162390393235</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_857403">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391890.897973695 5820143.10122585 34.4300003051758 391890.898340058 5820143.10176792 51.5720678737949 391905.066439623 5820143.83672962 51.5720678737949 391905.066073256 5820143.83618755 34.4300003051758 391890.897973695 5820143.10122585 34.4300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_857388">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391947.05720717 5820122.90619359 34.4300003051758 391947.057347762 5820122.90640159 41.0079400725952 391947.622870851 5820115.36176518 41.0079400725952 391947.622730258 5820115.36155718 34.4300003051758 391947.05720717 5820122.90619359 34.4300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_857422">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391953.848331889 5820128.28358734 34.4300003051758 391953.848898213 5820128.28442522 60.9267381515615 391953.930956313 5820123.30104749 53.7842916909644 391953.930542647 5820123.30043548 34.4300003051758 391953.848331889 5820128.28358734 34.4300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_857431">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391953.550256295 5820146.35127544 34.4300003051758 391953.550382104 5820146.35146158 40.3162390393233 391969.853155886 5820147.19715813 40.3162390393233 391969.853030076 5820147.196972 34.4300003051758 391953.550256295 5820146.35127544 34.4300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_857430">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391969.853030076 5820147.196972 34.4300003051758 391969.853155886 5820147.19715813 40.3162390393233 391970.147808711 5820141.13266468 43.3568873990319 391970.14761791 5820141.1323824 34.4300003051758 391969.853030076 5820147.196972 34.4300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_857416">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391914.185344598 5820138.23017527 60.7439746313207 391914.185349559 5820138.23018261 60.9761183063594 391922.312440269 5820138.65177069 60.9761183063594 391922.312435308 5820138.65176335 60.7439746313207 391914.185344598 5820138.23017527 60.7439746313207</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_857426">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391953.848898213 5820128.28442522 60.9267381515615 391953.848899269 5820128.28442678 60.9761183063594 391946.688092944 5820127.83895157 60.9761183063594 391946.688091888 5820127.83895001 60.9267381515615 391953.848898213 5820128.28442522 60.9267381515615</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_857402">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391905.066073256 5820143.83618755 34.4300003051758 391905.066439623 5820143.83672962 51.5720678737949 391913.891037107 5820144.29450033 51.5720678737949 391913.890670736 5820144.29395826 34.4300003051758 391905.066073256 5820143.83618755 34.4300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_857433">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391945.331791917 5820145.92494755 34.4300003051758 391945.332158299 5820145.92548962 51.5720678737949 391953.550622679 5820146.35181751 51.5720678737949 391953.550256295 5820146.35127544 34.4300003051758 391945.331791917 5820145.92494755 34.4300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_857421">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391953.930542647 5820123.30043548 34.4300003051758 391953.930956313 5820123.30104749 53.7842916909644 391947.057620834 5820122.9068056 53.7842916909644 391947.05720717 5820122.90619359 34.4300003051758 391953.930542647 5820123.30043548 34.4300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_857409">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391930.898401567 5820126.85667017 60.9267381515615 391930.898402622 5820126.85667173 60.9761183063594 391923.060252753 5820126.36905882 60.9761183063594 391923.060251697 5820126.36905726 60.9267381515615 391930.898401567 5820126.85667017 60.9267381515615</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_857398">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391905.193935913 5820137.76375829 60.9761183063625 391905.194258498 5820137.76423558 76.0697555541992 391905.40470886 5820127.72364145 76.0697555541992 391905.193935913 5820137.76375829 60.9761183063625</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_857400">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391891.192647579 5820137.03744254 60.7439746313207 391891.192975121 5820137.03792717 76.0697555541992 391905.194258498 5820137.76423558 76.0697555541992 391905.193930951 5820137.76375095 60.7439746313207 391891.192647579 5820137.03744254 60.7439746313207</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_857401">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391891.803761172 5820124.42378189 34.4300003051758 391891.804651099 5820124.42509861 76.0697555541992 391891.192975121 5820137.03792717 76.0697555541992 391891.192085195 5820137.03661045 34.4300003051758 391891.803761172 5820124.42378189 34.4300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_857399">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391905.32503017 5820131.51911471 70.3638960003392 391905.40470886 5820127.72364145 76.0697555541992 391905.456045825 5820125.27435471 76.0697555541992 391905.455723241 5820125.27387742 60.9761183063577 391905.32503017 5820131.51911471 70.3638960003392</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_857404">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391891.192085195 5820137.03661045 34.4300003051758 391891.192647579 5820137.03744254 60.7439746313207 391890.898340058 5820143.10176792 51.5720678737949 391890.897973695 5820143.10122585 34.4300003051758 391891.192085195 5820137.03661045 34.4300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_857413">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391939.293151151 5820127.37890925 60.9267381515615 391939.293152207 5820127.37891081 60.9761183063594 391930.898402622 5820126.85667173 60.9761183063594 391930.898401567 5820126.85667017 60.9267381515615 391939.293151151 5820127.37890925 60.9267381515615</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_857408">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391923.353383705 5820121.54678575 41.0079400725952 391923.353656771 5820121.54718976 53.7842916909644 391915.017437427 5820121.06903951 53.7842916909644 391915.017164363 5820121.0686355 41.0079400725952 391923.353383705 5820121.54678575 41.0079400725952</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_857389">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391939.808611006 5820114.87543902 34.4300003051758 391939.808751598 5820114.87564702 41.0079400725952 391935.680406663 5820114.61898384 46.3596332160857 391931.551832919 5820114.36198219 41.0079400725952 391931.551692329 5820114.36177419 34.4300003051758 391939.808611006 5820114.87543902 34.4300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_857429">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391945.786317582 5820139.86945721 60.7439746313207 391945.786322544 5820139.86946456 60.9761183063594 391953.651041817 5820140.27744224 60.9761183063594 391953.651036856 5820140.2774349 60.7439746313207 391945.786317582 5820139.86945721 60.7439746313207</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_857405">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391931.152416622 5820121.99412396 41.0079400725952 391931.15268969 5820121.99452797 53.7842916909644 391923.353656771 5820121.54718976 53.7842916909644 391923.353383705 5820121.54678575 41.0079400725952 391931.152416622 5820121.99412396 41.0079400725952</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_857407">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391939.495197375 5820122.47265054 41.0079400725952 391939.495470444 5820122.47305455 53.7842916909644 391931.15268969 5820121.99452797 53.7842916909644 391931.152416622 5820121.99412396 41.0079400725952 391939.495197375 5820122.47265054 41.0079400725952</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_857396">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391905.193930951 5820137.76375095 60.7439746313207 391905.193935913 5820137.76375829 60.9761183063594 391914.185349559 5820138.23018261 60.9761183063594 391914.185344598 5820138.23017527 60.7439746313207 391905.193930951 5820137.76375095 60.7439746313207</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_857423">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391953.650665236 5820140.27688509 43.3568873990319 391953.650859287 5820140.27717219 52.436030167115 391970.148002766 5820141.13295178 52.436030167115 391970.147808711 5820141.13266468 43.3568873990319 391953.650665236 5820140.27688509 43.3568873990319</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_857406">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391947.057347762 5820122.90640159 41.0079400725952 391947.057620834 5820122.9068056 53.7842916909644 391939.495470444 5820122.47305455 53.7842916909644 391939.495197375 5820122.47265054 41.0079400725952 391947.057347762 5820122.90640159 41.0079400725952</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_857410">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391922.312435308 5820138.65176335 60.7439746313207 391922.312440269 5820138.65177069 60.9761183063594 391930.259550282 5820139.06402236 60.9761183063594 391930.25954532 5820139.06401502 60.7439746313207 391922.312435308 5820138.65176335 60.7439746313207</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_857411">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391946.688091888 5820127.83895001 60.9267381515615 391946.688092944 5820127.83895157 60.9761183063594 391939.293152207 5820127.37891081 60.9761183063594 391939.293151151 5820127.37890925 60.9267381515615 391946.688091888 5820127.83895001 60.9267381515615</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_857386">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391931.551692329 5820114.36177419 34.4300003051758 391931.551832919 5820114.36198219 41.0079400725952 391927.686082471 5820114.12165507 46.3596332160857 391923.82010322 5820113.88098948 41.0079400725952 391923.819962631 5820113.88078148 34.4300003051758 391931.551692329 5820114.36177419 34.4300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_857392">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391915.017023775 5820121.0684275 34.4300003051758 391915.017437427 5820121.06903951 53.7842916909644 391905.555073956 5820120.52629569 53.7842916909644 391905.554660308 5820120.52568368 34.4300003051758 391915.017023775 5820121.0684275 34.4300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_857419">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391929.941664042 5820145.12659392 34.4300003051758 391929.942030417 5820145.12713599 51.5720678737949 391938.541997136 5820145.57325409 51.5720678737949 391938.541630757 5820145.57271202 34.4300003051758 391929.941664042 5820145.12659392 34.4300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_857420">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391913.890670736 5820144.29395826 34.4300003051758 391913.891037107 5820144.29450033 51.5720678737949 391921.943241923 5820144.71220363 51.5720678737949 391921.942875549 5820144.71166156 34.4300003051758 391913.890670736 5820144.29395826 34.4300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_857387">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391947.622730258 5820115.36155718 34.4300003051758 391947.622870851 5820115.36176518 41.0079400725952 391943.715925627 5820115.11887533 46.3596332160857 391939.808751598 5820114.87564702 41.0079400725952 391939.808611006 5820114.87543902 34.4300003051758 391947.622730258 5820115.36155718 34.4300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_857412">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391938.792593738 5820139.5066619 60.7439746313207 391938.7925987 5820139.50666924 60.9761183063594 391945.786322544 5820139.86946456 60.9761183063594 391945.786317582 5820139.86945721 60.7439746313207 391938.792593738 5820139.5066619 60.7439746313207</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_857414">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391930.25954532 5820139.06401502 60.7439746313207 391930.259550282 5820139.06402236 60.9761183063594 391938.7925987 5820139.50666924 60.9761183063594 391938.792593738 5820139.5066619 60.7439746313207 391930.25954532 5820139.06401502 60.7439746313207</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_857394">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391905.554660308 5820120.52568368 34.4300003051758 391905.555073956 5820120.52629569 53.7842916909644 391892.030838228 5820119.75057032 53.7842916909644 391892.030424586 5820119.74995831 34.4300003051758 391905.554660308 5820120.52568368 34.4300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_857427">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391953.650859287 5820140.27717219 52.436030167113 391953.651041817 5820140.27744224 60.9761183063594 391953.750171149 5820134.28123136 70.3638960003395 391953.787066661 5820132.04017547 66.855605175499 391953.650859287 5820140.27717219 52.436030167113</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_857428">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391953.750012324 5820134.28099638 62.9328544064414 391953.787066661 5820132.04017547 66.855605175499 391953.848899269 5820128.28442678 60.9761183063594 391953.848716739 5820128.28415673 52.4360301671057 391953.750012324 5820134.28099638 62.9328544064414</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_857424">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391970.720763008 5820129.33322466 34.4300003051758 391970.721147864 5820129.33379405 52.436030167115 391953.848716739 5820128.28415673 52.436030167115 391953.848331889 5820128.28358734 34.4300003051758 391970.720763008 5820129.33322466 34.4300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_857425">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391970.14761791 5820141.1323824 34.4300003051758 391970.148002766 5820141.13295178 52.436030167115 391970.721147864 5820129.33379405 52.436030167115 391970.720763008 5820129.33322466 34.4300003051758 391970.14761791 5820141.1323824 34.4300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_857415">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391923.060251697 5820126.36905726 60.9267381515615 391923.060252753 5820126.36905882 60.9761183063594 391914.785535116 5820125.85428694 60.9761183063594 391914.785534061 5820125.85428538 60.9267381515615 391923.060251697 5820126.36905726 60.9267381515615</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_857417">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391921.942875549 5820144.71166156 34.4300003051758 391921.943241923 5820144.71220363 51.5720678737949 391929.942030417 5820145.12713599 51.5720678737949 391929.941664042 5820145.12659392 34.4300003051758 391921.942875549 5820144.71166156 34.4300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_857397">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391905.455722185 5820125.27387586 60.9267381515615 391905.456045825 5820125.27435471 76.0697555541992 391891.804651099 5820124.42509861 76.0697555541992 391891.804327463 5820124.42461976 60.9267381515615 391905.455722185 5820125.27387586 60.9267381515615</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_857390">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391915.391028338 5820113.35641551 34.4300003051758 391915.391168926 5820113.35662351 41.0079400725952 391915.017164363 5820121.0686355 41.0079400725952 391915.017023775 5820121.0684275 34.4300003051758 391915.391028338 5820113.35641551 34.4300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_857418">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391938.541630757 5820145.57271202 34.4300003051758 391938.541997136 5820145.57325409 51.5720678737949 391945.332158299 5820145.92548962 51.5720678737949 391945.331791917 5820145.92494755 34.4300003051758 391938.541630757 5820145.57271202 34.4300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_857395">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391914.785534061 5820125.85428538 60.9267381515615 391914.785535116 5820125.85428694 60.9761183063594 391905.455723241 5820125.27387742 60.9761183063594 391905.455722185 5820125.27387586 60.9267381515615 391914.785534061 5820125.85428538 60.9267381515615</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="GEOM_857393">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391892.030424586 5820119.74995831 34.4300003051758 391892.030838228 5820119.75057032 53.7842916909644 391891.804327463 5820124.42461976 60.9267381515615 391891.803761172 5820124.42378189 34.4300003051758 391892.030424586 5820119.74995831 34.4300003051758</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="GEOM_857448">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391892.030838228 5820119.75057032 53.7842916909644 391905.555073956 5820120.52629569 53.7842916909644 391905.455722185 5820125.27387586 60.9267381515615 391891.804327463 5820124.42461976 60.9267381515615 391892.030838228 5820119.75057032 53.7842916909644</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="GEOM_857440">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391939.495197375 5820122.47265054 41.0079400725952 391943.499584321 5820118.84523916 46.3596332160857 391947.057347762 5820122.90640159 41.0079400725952 391939.495197375 5820122.47265054 41.0079400725952</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="GEOM_857445">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391919.605750477 5820113.61897573 46.3596332160857 391923.82010322 5820113.88098948 41.0079400725952 391923.353383705 5820121.54678575 41.0079400725952 391919.414597826 5820117.11537679 46.3596332160857 391919.605750477 5820113.61897573 46.3596332160857</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="GEOM_857446">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391915.017164363 5820121.0686355 41.0079400725952 391919.414597826 5820117.11537679 46.3596332160857 391923.353383705 5820121.54678575 41.0079400725952 391915.017164363 5820121.0686355 41.0079400725952</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="GEOM_857449">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391914.785535116 5820125.85428694 60.9761183063594 391914.485642934 5820132.04253163 70.3638960003395 391905.32503017 5820131.51911471 70.3638960003395 391905.455723241 5820125.27387742 60.9761183063594 391914.785535116 5820125.85428694 60.9761183063594</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="GEOM_857447">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391905.555073956 5820120.52629569 53.7842916909644 391915.017437427 5820121.06903951 53.7842916909644 391914.785534061 5820125.85428538 60.9267381515615 391905.455722185 5820125.27387586 60.9267381515615 391905.555073956 5820120.52629569 53.7842916909644</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="GEOM_857439">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391943.715925627 5820115.11887533 46.3596332160857 391947.622870851 5820115.36176518 41.0079400725952 391947.057347762 5820122.90640159 41.0079400725952 391943.499584321 5820118.84523916 46.3596332160857 391943.715925627 5820115.11887533 46.3596332160857</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="GEOM_857452">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391905.194258498 5820137.76423558 76.0697555541992 391898.412974295 5820131.12687122 122.469078063965 391905.456045825 5820125.27435471 76.0697555541992 391905.194258498 5820137.76423558 76.0697555541992</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="GEOM_857462">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391930.579177052 5820132.9606439 70.3638960003395 391930.259550282 5820139.06402236 60.9761183063594 391922.312440269 5820138.65177069 60.9761183063594 391922.686547109 5820132.51071161 70.3638960003395 391930.579177052 5820132.9606439 70.3638960003395</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="GEOM_857451">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391905.456045825 5820125.27435471 76.0697555541992 391898.412974295 5820131.12687122 122.469078063965 391891.804651099 5820124.42509861 76.0697555541992 391905.456045825 5820125.27435471 76.0697555541992</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="GEOM_857468">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391922.686547109 5820132.51071161 70.3638960003395 391922.312440269 5820138.65177069 60.9761183063594 391914.185349559 5820138.23018261 60.9761183063594 391914.485642934 5820132.04253163 70.3638960003395 391922.686547109 5820132.51071161 70.3638960003395</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="GEOM_857450">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391914.485642934 5820132.04253163 70.3638960003395 391914.185349559 5820138.23018261 60.9761183063594 391905.193935913 5820137.76375829 60.9761183063594 391905.32503017 5820131.51911471 70.3638960003395 391914.485642934 5820132.04253163 70.3638960003395</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="GEOM_857461">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391930.898402622 5820126.85667173 60.9761183063594 391930.579177052 5820132.9606439 70.3638960003395 391922.686547109 5820132.51071161 70.3638960003395 391923.060252753 5820126.36905882 60.9761183063594 391930.898402622 5820126.85667173 60.9761183063594</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="GEOM_857454">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391891.804651099 5820124.42509861 76.0697555541992 391898.412974295 5820131.12687122 122.469078063965 391891.192975121 5820137.03792717 76.0697555541992 391891.804651099 5820124.42509861 76.0697555541992</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="GEOM_857464">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391946.237408348 5820133.85450492 70.3638960003395 391945.786322544 5820139.86946456 60.9761183063594 391938.7925987 5820139.50666924 60.9761183063594 391939.043076056 5820133.44308688 70.3638960003395 391946.237408348 5820133.85450492 70.3638960003395</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="GEOM_857466">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391939.043076056 5820133.44308688 70.3638960003395 391938.7925987 5820139.50666924 60.9761183063594 391930.259550282 5820139.06402236 60.9761183063594 391930.579177052 5820132.9606439 70.3638960003395 391939.043076056 5820133.44308688 70.3638960003395</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="GEOM_857460">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391915.017437427 5820121.06903951 53.7842916909644 391923.353656771 5820121.54718976 53.7842916909644 391923.060251697 5820126.36905726 60.9267381515615 391914.785534061 5820125.85428538 60.9267381515615 391915.017437427 5820121.06903951 53.7842916909644</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="GEOM_857465">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391939.293152207 5820127.37891081 60.9761183063594 391939.043076056 5820133.44308688 70.3638960003395 391930.579177052 5820132.9606439 70.3638960003395 391930.898402622 5820126.85667173 60.9761183063594 391939.293152207 5820127.37891081 60.9761183063594</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="GEOM_857470">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391945.332158299 5820145.92548962 51.5720678737949 391938.541997136 5820145.57325409 51.5720678737949 391938.792593738 5820139.5066619 60.7439746313207 391945.786317582 5820139.86945721 60.7439746313207 391945.332158299 5820145.92548962 51.5720678737949</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="GEOM_857453">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391891.192975121 5820137.03792717 76.0697555541992 391898.412974295 5820131.12687122 122.469078063965 391905.194258498 5820137.76423558 76.0697555541992 391891.192975121 5820137.03792717 76.0697555541992</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="GEOM_857475">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391953.750012324 5820134.28099638 62.9328544064475 391953.848716739 5820128.28415673 52.436030167115 391970.721147864 5820129.33379405 52.436030167115 391964.493296341 5820134.89444248 62.9328544064475 391953.750012324 5820134.28099638 62.9328544064475</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="GEOM_857441">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391931.551832919 5820114.36198219 41.0079400725952 391935.680406663 5820114.61898384 46.3596332160857 391935.518338329 5820118.08078469 46.3596332160857 391931.152416622 5820121.99412396 41.0079400725952 391931.551832919 5820114.36198219 41.0079400725952</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="GEOM_857463">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391946.688092944 5820127.83895157 60.9761183063594 391946.237408348 5820133.85450492 70.3638960003395 391939.043076056 5820133.44308688 70.3638960003395 391939.293152207 5820127.37891081 60.9761183063594 391946.688092944 5820127.83895157 60.9761183063594</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="GEOM_857455">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391913.891037107 5820144.29450033 51.5720678737949 391905.066439623 5820143.83672962 51.5720678737949 391905.193930951 5820137.76375095 60.7439746313207 391914.185344598 5820138.23017527 60.7439746313207 391913.891037107 5820144.29450033 51.5720678737949</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="GEOM_857456">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391905.066439623 5820143.83672962 51.5720678737949 391890.898340058 5820143.10176792 51.5720678737949 391891.192647579 5820137.03744254 60.7439746313207 391905.193930951 5820137.76375095 60.7439746313207 391905.066439623 5820143.83672962 51.5720678737949</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="GEOM_857437">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391923.353383705 5820121.54678575 41.0079400725952 391927.472883658 5820117.88723308 46.3596332160857 391931.152416622 5820121.99412396 41.0079400725952 391923.353383705 5820121.54678575 41.0079400725952</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="GEOM_857474">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391953.650859287 5820140.27717219 52.436030167115 391953.750012324 5820134.28099638 62.9328544064475 391964.493296341 5820134.89444248 62.9328544064475 391970.148002766 5820141.13295178 52.436030167115 391953.650859287 5820140.27717219 52.436030167115</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="GEOM_857471">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391938.541997136 5820145.57325409 51.5720678737949 391929.942030417 5820145.12713599 51.5720678737949 391930.25954532 5820139.06401502 60.7439746313207 391938.792593738 5820139.5066619 60.7439746313207 391938.541997136 5820145.57325409 51.5720678737949</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="GEOM_857477">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391953.848899269 5820128.28442678 60.9761183063594 391953.750171149 5820134.28123136 70.3638960003395 391946.237408348 5820133.85450492 70.3638960003395 391946.688092944 5820127.83895157 60.9761183063594 391953.848899269 5820128.28442678 60.9761183063594</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="GEOM_857472">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391921.943241923 5820144.71220363 51.5720678737949 391913.891037107 5820144.29450033 51.5720678737949 391914.185344598 5820138.23017527 60.7439746313207 391922.312435308 5820138.65176335 60.7439746313207 391921.943241923 5820144.71220363 51.5720678737949</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="GEOM_857435">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391923.82010322 5820113.88098948 41.0079400725952 391927.686082471 5820114.12165507 46.3596332160857 391927.472883658 5820117.88723308 46.3596332160857 391923.353383705 5820121.54678575 41.0079400725952 391923.82010322 5820113.88098948 41.0079400725952</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="GEOM_857458">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391939.495470444 5820122.47305455 53.7842916909644 391947.057620834 5820122.9068056 53.7842916909644 391946.688091888 5820127.83895001 60.9267381515615 391939.293151151 5820127.37890925 60.9267381515615 391939.495470444 5820122.47305455 53.7842916909644</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="GEOM_857459">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391931.15268969 5820121.99452797 53.7842916909644 391939.495470444 5820122.47305455 53.7842916909644 391939.293151151 5820127.37890925 60.9267381515615 391930.898401567 5820126.85667017 60.9267381515615 391931.15268969 5820121.99452797 53.7842916909644</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="GEOM_857457">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391923.353656771 5820121.54718976 53.7842916909644 391931.15268969 5820121.99452797 53.7842916909644 391930.898401567 5820126.85667017 60.9267381515615 391923.060251697 5820126.36905726 60.9267381515615 391923.353656771 5820121.54718976 53.7842916909644</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="GEOM_857478">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391953.750171149 5820134.28123136 70.3638960003395 391953.651041817 5820140.27744224 60.9761183063594 391945.786322544 5820139.86946456 60.9761183063594 391946.237408348 5820133.85450492 70.3638960003395 391953.750171149 5820134.28123136 70.3638960003395</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="GEOM_857479">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391969.853155886 5820147.19715813 40.3162390393233 391953.550382104 5820146.35146158 40.3162390393233 391953.650665236 5820140.27688509 43.3568873990319 391970.147808711 5820141.13266468 43.3568873990319 391969.853155886 5820147.19715813 40.3162390393233</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="GEOM_857442">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391935.680406663 5820114.61898384 46.3596332160857 391939.808751598 5820114.87564702 41.0079400725952 391939.495197375 5820122.47265054 41.0079400725952 391935.518338329 5820118.08078469 46.3596332160857 391935.680406663 5820114.61898384 46.3596332160857</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="GEOM_857444">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391915.391168926 5820113.35662351 41.0079400725952 391919.605750477 5820113.61897573 46.3596332160857 391919.414597826 5820117.11537679 46.3596332160857 391915.017164363 5820121.0686355 41.0079400725952 391915.391168926 5820113.35662351 41.0079400725952</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="GEOM_857473">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391947.057620834 5820122.9068056 53.7842916909644 391953.930956313 5820123.30104749 53.7842916909644 391953.848898213 5820128.28442522 60.9267381515615 391946.688091888 5820127.83895001 60.9267381515615 391947.057620834 5820122.9068056 53.7842916909644</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="GEOM_857436">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391927.686082471 5820114.12165507 46.3596332160857 391931.551832919 5820114.36198219 41.0079400725952 391931.152416622 5820121.99412396 41.0079400725952 391927.472883658 5820117.88723308 46.3596332160857 391927.686082471 5820114.12165507 46.3596332160857</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="GEOM_857469">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391929.942030417 5820145.12713599 51.5720678737949 391921.943241923 5820144.71220363 51.5720678737949 391922.312435308 5820138.65176335 60.7439746313207 391930.25954532 5820139.06401502 60.7439746313207 391929.942030417 5820145.12713599 51.5720678737949</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="GEOM_857438">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391939.808751598 5820114.87564702 41.0079400725952 391943.715925627 5820115.11887533 46.3596332160857 391943.499584321 5820118.84523916 46.3596332160857 391939.495197375 5820122.47265054 41.0079400725952 391939.808751598 5820114.87564702 41.0079400725952</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="GEOM_857476">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391970.148002766 5820141.13295178 52.436030167115 391964.493296341 5820134.89444248 62.9328544064475 391970.721147864 5820129.33379405 52.436030167115 391970.148002766 5820141.13295178 52.436030167115</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="GEOM_857480">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391953.550622679 5820146.35181751 51.5720678737949 391945.332158299 5820145.92548962 51.5720678737949 391945.786317582 5820139.86945721 60.7439746313207 391953.651036856 5820140.2774349 60.7439746313207 391953.550622679 5820146.35181751 51.5720678737949</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="GEOM_857467">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391923.060252753 5820126.36905882 60.9761183063594 391922.686547109 5820132.51071161 70.3638960003395 391914.485642934 5820132.04253163 70.3638960003395 391914.785535116 5820125.85428694 60.9761183063594 391923.060252753 5820126.36905882 60.9761183063594</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="GEOM_857443">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsName="EPSG:25833" srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon>
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>391931.152416622 5820121.99412396 41.0079400725952 391935.518338329 5820118.08078469 46.3596332160857 391939.495197375 5820122.47265054 41.0079400725952 391931.152416622 5820121.99412396 41.0079400725952</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
		</bldg:Building>
	</cityObjectMember>
</CityModel>