/*
* LinearRing.cpp
*
*  Created on: Jun 29, 2016
*      Author: Md.Nazmul
*/

#include "LinearRing.h"


using namespace std;

LinearRing::LinearRing() {
	// TODO Auto-generated constructor stub
}

LinearRing::~LinearRing() {
	// TODO Auto-generated destructor stub
}
vector<std::pair<string, vector<double>>> LinearRing::getLinearRings(vector<string>& Surface) {
	Building B;
	//now parse the linear rings from the Surface
	vector<string> LinearRings;
	vector<string> Polygon_id;
	vector<string> LinearRing_id;
	for (unsigned int i = 0;i < Surface.size(); i++) {		

		//line contains the coords 
		if (Surface[i].find("<gml:LinearRing") != std::string::npos) {
			//if (Surface[i].find("<gml:posList srsDimension=\"3\">") != std::string::npos) {
				LinearRings.push_back(Surface[i+1]);

				L_Rings.push_back(Surface[i+1]); //delete me later

		}

		//line contains the gml:id of the linear ring / polygon
		if (Surface[i].find("<gml:Polygon gml:id=") != std::string::npos) {
			Polygon_id.push_back(Surface[i]);
		}

		if (Surface[i].find("<gml:LinearRing gml:id=") != std::string::npos) {
			LinearRing_id.push_back(Surface[i]);
		}

	}

	//delete me later -NOT used-
	getPolygonID(Polygon_id);
	getLRID(LinearRing_id);

	
	if (LinearRing_id.size() == LinearRings.size()) {
			for (unsigned int i = 0;i < LinearRings.size();i++) {
				GMLID_LinearRingCoords.push_back(std::make_pair(LinearRing_id[i], getRingCoords(LinearRings[i])));
			}
	}
	else if (Polygon_id.size() == LinearRings.size()) {
		for (unsigned int i = 0;i < LinearRings.size();i++) {
			GMLID_LinearRingCoords.push_back(std::make_pair(Polygon_id[i], getRingCoords(LinearRings[i])));
		}
	}

	else {
		for (unsigned int i = 0;i < LinearRings.size();i++) {
			GMLID_LinearRingCoords.push_back(std::make_pair("NO/inconsistent GMLID", getRingCoords(LinearRings[i])));
		}
	}

	return GMLID_LinearRingCoords;
}

vector<double> LinearRing::getRingCoords(string LinearRings) {


	vector<double> RingCoords;
	vector<double> RingCoords_test; //delete me later
	vector<string> String_RingCoords; //delete me later
	string buf; // Have a buffer string
	std::string delimiter1 = "srsDimension=\"3\">";
	std::string delimiter2 = "</gml:posList>";

	//	for(unsigned int i=0; i<LinearRing.size();i++){

	stringstream ss(LinearRings); // Insert the string into a stream
	SplitAndConvert conv;

	while (ss >> buf)
	{
		if (buf == "<gml:posList") {
			//			  cout<<"Ignoring the first index"<<endl;
			//do nothing
		}
		else if (buf.find(delimiter1) != std::string::npos) {
			faces.push_back(conv.SplitStartTagAndCoord(buf, delimiter1));

			buf = conv.SplitStartTagAndCoord(buf, delimiter1);
			RingCoords.push_back(conv.StringToCoord(buf));
			
			
			String_RingCoords.push_back(buf);

			//cout<<"---------the dilimiter1 is found--------------"<<endl;
		}

		else if (buf.find(delimiter2) != std::string::npos) {
			faces.push_back(conv.SplitEndTagAndCoord(buf, delimiter2));

			buf = conv.SplitEndTagAndCoord(buf, delimiter2);
			RingCoords.push_back(conv.StringToCoord(buf));
			
			
			String_RingCoords.push_back(buf);

			//cout<<"xxxxxxxxxxxxxxxxx the dilimiter2 is found xxxxxxxxxxxx"<<endl;
		}
		else {
			RingCoords.push_back(conv.StringToCoord(buf));
			
			
			String_RingCoords.push_back(buf);
			faces.push_back(buf);
			//					cout<<"RingCoords :"<< buf<<endl;
		}
	}
	ss.clear();

	return RingCoords;
}
//delete me later -NOT used-
vector<string> LinearRing::getPolygonID(vector<string> &polyID) {
	string buf; // Have a buffer string
	std::string delimiter1 = "gml:id=\"";
	std::string delimiter2 = "\">";

	for (unsigned int i = 0; i < polyID.size();i++) {

		stringstream ss(polyID[i]); // Insert the string into a stream
		SplitAndConvert conv;

		while (ss >> buf)
		{
			if (buf == "<gml:Polygon") {
				//	cout<<"Ignoring the first index"<<endl;
				//do nothing
			}
			else if (buf.find(delimiter1) != std::string::npos) {
				buf = conv.SplitStartTagAndCoord(buf, delimiter1);
				PolygonID.push_back(buf);
				//cout<<"---------the dilimiter1 is found--------------"<<endl;
			}

			else if (buf.find(delimiter2) != std::string::npos) {
				buf = conv.SplitEndTagAndCoord(buf, delimiter2);
				PolygonID.push_back(buf);

				//cout<<"xxxxxxxxxxxxxxxxx the dilimiter2 is found xxxxxxxxxxxx"<<endl;
			}
			else {

				PolygonID.push_back(buf);
				//	cout<<"RingCoords :"<< buf<<endl;
			}
		}
		ss.clear();
	}

	return PolygonID;
}
//delete me later -NOT used-
vector<string> LinearRing::getLRID(vector<string> &LRID) {
	string buf; // Have a buffer string
	std::string delimiter1 = "gml:id=\"";
	std::string delimiter2 = "\">";

	for (unsigned int i = 0; i < LRID.size();i++) {

		stringstream ss(LRID[i]); // Insert the string into a stream
		SplitAndConvert conv;

		while (ss >> buf)
		{
			if (buf == "<gml:LinearRing") {
				//			  cout<<"Ignoring the first index"<<endl;
				//do nothing
			}
			else if (buf.find(delimiter1) != std::string::npos) {
				buf = conv.SplitStartTagAndCoord(buf, delimiter1);
				LinearRingID.push_back(buf);
				//cout<<"---------the dilimiter1 is found--------------"<<endl;
			}

			else if (buf.find(delimiter2) != std::string::npos) {
				buf = conv.SplitEndTagAndCoord(buf, delimiter2);
				LinearRingID.push_back(buf);

				//cout<<"xxxxxxxxxxxxxxxxx the dilimiter2 is found xxxxxxxxxxxx"<<endl;
			}
			else {

				LinearRingID.push_back(buf);
				//	cout<<"RingCoords :"<< buf<<endl;
			}
		}
		ss.clear();
	}
	return LinearRingID;
}
//delete me later -NOT used-
vector<string> LinearRing::Writevectors() {
	Building B;

	//B.WriteInFile(L_Rings, "C:/dev/CGAL-4.10/build-examples/Repair_3D_Buildings/3D_buildings/data/LinearRings-string.txt");
	//B.WriteInFile(faces, "C:/dev/CGAL-4.10/build-examples/Repair_3D_Buildings/3D_buildings/data/String_RingCoords_plain.txt");

	return L_Rings;
}
