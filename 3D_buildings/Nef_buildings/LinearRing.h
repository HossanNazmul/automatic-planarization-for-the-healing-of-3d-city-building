/*
* LinearRing.hpp
*
*  Created on: Jun 29, 2016
*      Author: Md.Nazmul
*/

#ifndef GEOMETRY_LINEARRING_H_
#define GEOMETRY_LINEARRING_H_

#include <fstream>      // std::ifstream class to read from file
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <limits>
#include <iomanip>
#include "SplitAndConvert.h"
#include "Building.h"

using namespace std;

class LinearRing {
public:
	LinearRing();
	virtual ~LinearRing();
	vector<string> L_Rings; //delete me later 
	vector<string> faces; //delete me later
	vector<string> PolygonID, LinearRingID;
	vector<std::pair<string, vector<double>>> GMLID_LinearRingCoords;
	vector<std::pair<string, vector<double>>> getLinearRings(vector<string>& Surface);
	vector<double> getRingCoords(string LinearRing);
	vector<string> getPolygonID(vector<string> &polyID);
	vector<string> getLRID(vector<string> &LRID);
	vector<string>  Writevectors();
	
};

#endif /* GEOMETRY_LINEARRING_H_ */
#pragma once
