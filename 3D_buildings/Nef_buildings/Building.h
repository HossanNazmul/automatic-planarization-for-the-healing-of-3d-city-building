/*
* building.hpp
*
*  Created on: Jun 26, 2016
*      Author: Md.Nazmul
*/

#ifndef GEOMETRY_BUILDING_H_
#define GEOMETRY_BUILDING_H_

#include <iostream>     // std::cout
#include <fstream>      // std::ifstream class to read from file
#include <sstream>
#include <string>
#include <vector>
#include <iomanip>
using namespace std;

class Building {
public:
	Building();
	virtual ~Building();
	void readGMLFile(string filePath);
	void WriteInFile(vector<string> v, string inputFile);
	vector<string> cityObjectMember, buildingGeom, GroundSurface, RoofSurface, WallSurface;
	void WriteInOFF(vector<double> v, string inputFile);
};

#endif /* GEOMETRY_BUILDING_H_ */
