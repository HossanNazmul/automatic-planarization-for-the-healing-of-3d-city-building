
#pragma once
#include "definitions.h"

class CityGMLOutput {
public:
	CityGMLOutput();
	virtual ~CityGMLOutput();
	std::vector<std::string> GML_ID;
	std::string doubleToString(double d, int ndec);
	int add(int& a, int& b);
	std::vector<std::pair<std::string, std::string>> new_linear_rings(std::vector<std::pair<face_index, std::pair<std::string, std::vector<Point_3>>>>& face_gmlids_rings);
	void writeToCityGML(std::vector<std::string>& origin_building, std::vector<std::pair<std::string, std::string>>& gmlid_linearRings_pair, std::string filepath, std::string filename);
	std::vector<std::pair<std::string, std::string>> writeRepairedGeom(std::vector < std::pair<std::string, Surface_Mesh> >& splitted_faces);
};
