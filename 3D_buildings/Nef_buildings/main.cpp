/*
*
*  main.cpp  updated
*  Last modified on: Mar 11, 2018
*  Author: Md. Nazmul Hossan
*  Description: Automatic healing of planarity error in 3D building models.
*  Git repository:  https://HossanNazmul@bitbucket.org/HossanNazmul/automatic-planarization-for-the-healing-of-3d-city-building.git
*/
#define _CRT_SECURE_NO_WARNINGS
#define _SCL_SECURE_NO_WARNINGS //to avoid VS 2014 compiler warning for CGAL transform parameter

// headers
#include "definitions.h"
#include "Building.h"
#include "LinearRing.h"
#include "CityGMLOutput.h"

using namespace std;

//global variables
Surface_Mesh polygon_mesh;
std::vector<std::pair<face_index, std::pair<string, vector<Point_3>>>> face_gmlids_rings; //stores face index with gmlid and linear rings
std::vector < std::pair<std::string, Surface_Mesh> > splitted_new_faces; //stores the splitted new faces
std::vector<std::pair<Point_3, Point_3>> dup_points; //used in finding and repairing floating point number error
std::vector<face_points_pair> face_vertices; //face and incident vertices //typedef std::pair<face_index, std::vector<Point_3>> face_points_pair;
vector<std::pair<std::string, std::string>> gmlid_linearRings_repaired; //stores the face ids and vertices after floating point repair
std::vector< std::pair<face_index, std::vector<vertex_index>>> non_planar_face_vertices; //stores non-planar faces with all vertices
int no_of_non_planar_polygons = 0; //number of non-planar polygons
std::vector<string> GMLID; //gml id's of linear rings
std::vector<string> origin_building; //input building
std::vector<string> Test_Report; //error and repair reports


//functions
bool check_LR_validity(vector<Point_3>& linearRingPoints);
face_index add_face(vector<Point_3>& pts, Surface_Mesh& pmesh, std::map<Point_3, vertex_index>& point_to_vertex);
vector<Point_3> check_and_repair_small_folds(vector<Point_3>& points, double& tol);
bool floating_point_number_error(Surface_Mesh& pmesh, double& tol);
Surface_Mesh repair_floating_point_number_error(Surface_Mesh& pmesh);
bool check_planarity_on_faces(vector<double>& linear_rings, face_index& fi, Surface_Mesh& pmesh);
Plane_3 plane_equation(Surface_Mesh& mesh, face_index& fi);
void check_planarity_error(Surface_Mesh& pmesh);
void healing(Surface_Mesh& mesh);
void write_err_report(std::string text, double val);
void write_err_report(std::string text, int val);
void write_err_report(std::string text, unsigned int val);
void write_err_report(std::string text, std::string val);
//optional
void test_cases();
void write_off_format(Surface_Mesh& m);


//location of the input datasets
std::string format = ".gml";
string filepath = "C:/dev/CGAL-4.10/build-examples/Repair_3D_Buildings/3D_buildings/Test-datasets/Imaginary/";
std::string filename = "Gendarmenmarkt"; //Test-dataset-1,	Gendarmenmarkt, Hedwigskathedrale, Bodemuseum, Hauptgebaeude-TU-Berlin, Fernsehtrum

//0.01 = 1cm; 0.0001 = 1mm; 0.00001 = 0.01 mm
//distance to plane tolerance
double distance_plane_tol = 0.00001;
//tolerance to detect small folds
double folds_tol = 0.0000001;
//snap tolerance
double snap_tolerence = 0.000001; //in meter
//repair tolerance
double repair_tol = 0.0000001; //in meter 0.000001

int main()
{
	//test_cases();
	Building building;
	LinearRing LR;
	vector<double> linearRingCoords;

	std::string inputFile = filepath + filename + format;

	// ---- Parse LinearRings from CityGML LOD2 building model ----
	building.readGMLFile(inputFile);
	vector<string> Geom = building.buildingGeom;
	vector<std::pair<string, vector<double>>> L_rings = LR.getLinearRings(Geom);
	//LR.Writevectors();
	if (L_rings.size() == LR.PolygonID.size()) {
		GMLID = LR.PolygonID;
	}
	else {
		GMLID = LR.LinearRingID;
	}

	// ---- CityGML LinearRings to CGAL Surface_Mesh ----

	origin_building = building.cityObjectMember;
	std::map<Point_3, vertex_index> point_to_vertex;
	//iterate through Vector<string> _surfaces and its Linear_Rings 
	for (unsigned int index = 0; index < L_rings.size(); index++) {
		GMLID.push_back(L_rings[index].first);
		linearRingCoords = L_rings[index].second;
		vector<Point_3> linearRingPoints;
		for (unsigned int i = 0; i < linearRingCoords.size(); i = i + 3)
		{
			Point_3 p(linearRingCoords[i], linearRingCoords[i + 1], linearRingCoords[i + 2]);
			linearRingPoints.push_back(p);
		}
		//check if the Linear ring is valid 
		check_LR_validity(linearRingPoints);
		
		// ---- Check small folds and consecutive points in the polygon ----
		linearRingPoints = check_and_repair_small_folds(linearRingPoints, folds_tol);

		std::cout << ">>Processing Linear Ring << : " << L_rings[index].first << endl;
		face_index face_id = add_face(linearRingPoints, polygon_mesh, point_to_vertex);
		face_vertices.push_back(std::make_pair(face_id, linearRingPoints)); //this is used for repairing floating point error
		//face_count++;
		//check_planarity_on_faces(linearRingCoords);
		//std::cout << ">>>>>>>>>>>>Processing GMLID <<<<<<<<<<<<<<< : " << GMLID [index] << endl;

		face_gmlids_rings.push_back(std::make_pair(face_id, std::make_pair(L_rings[index].first, linearRingPoints)));
	}

	// ---- Check floating point number error ----
	Test_Report.push_back("----------------------Error Report------------------------------");
	std::cout << "Snapping Tolerance is: " << snap_tolerence << endl;
	write_err_report("Snapping Tolerance: ", snap_tolerence);
	write_err_report("Folds tolerance: ", folds_tol);
	write_err_report("Distnace to plane Tolerance: ", distance_plane_tol);

	bool has_floating_point_error = floating_point_number_error(polygon_mesh, snap_tolerence);
	if (has_floating_point_error) {
		std::cout << "Error: floating point number errors are present in the dataset! Repairing in progress....." << endl;
		Test_Report.push_back("Error: floating point number errors are present in the dataset!");

		Surface_Mesh newMesh;
		newMesh = repair_floating_point_number_error(polygon_mesh);
		polygon_mesh.clear();
		polygon_mesh = newMesh;
	}
	else {

		std::cout << "No duplicate points are present in the dataset." << endl;
		Test_Report.push_back("No duplicate points are present in the dataset.");
	}

	// ---- Check Planarity Error ----
	check_planarity_error(polygon_mesh);

	if (non_planar_face_vertices.size() != 0)
	{
		healing(polygon_mesh);
	}
	else {
		std::cout << "There is no planarity errors in the data" << endl;
		Test_Report.push_back("There is no planarization errors in the data.");
	}

	//---- Write Error and Repair Report ----
	std::string Err_report_filepath = filepath + filename + "_Test_Report.txt";
	building.WriteInFile(Test_Report, Err_report_filepath);


	//Exit
	std::cout << ">>>>>>>>>>>> Press any key to exit <<<<<<<<<<<<<<<  " << endl;
	std::getchar();
	return 0;
}

//--------------All member functions ------------------------------
bool check_LR_validity(vector<Point_3>& linearRingPoints) {
	//check if the linear ring consist of a min. 4 points (first point is identical to the last point)
	bool valid = true;
	if (linearRingPoints.size() < 4) {
		std::cout << "ERROR: The linear ring contains less than three points" << endl;
		valid = false;
	}
	//check if the first point is identical to the last point of the linear ring
	if (linearRingPoints.at(0) != linearRingPoints.at(linearRingPoints.size() - 1)) {
		std::cout << "first point: " << linearRingPoints.at(0) << " last point: " << linearRingPoints.at(linearRingPoints.size() - 1) << endl;
		std::cout << "ERROR: first and the last points of Linear Rings are NOT identical" << endl;
		valid = false;
	}
	return valid;
}

face_index add_face(vector<Point_3>& pts, Surface_Mesh& pmesh, std::map<Point_3, vertex_index>& point_to_vertex2)
{
	//std::map<Kernel::Point_3, Surface_Mesh::Vertex_index> point_to_vertex2;
	std::vector<Surface_Mesh::Vertex_index> vertices;

	for (unsigned int i = 0; i < pts.size(); i++)
	{
		Point_3 p(pts[i]);

		std::pair<std::map<Kernel::Point_3, Surface_Mesh::Vertex_index>::iterator, bool> insert_res
			= point_to_vertex2.insert(std::make_pair(p, Surface_Mesh::Vertex_index()));

		if (insert_res.second) {
			insert_res.first->second = pmesh.add_vertex(p);
		}
		vertices.push_back(insert_res.first->second);
	}
	if (vertices.front() == vertices.back())
		vertices.pop_back();
	//insert by range
	face_index fi = pmesh.add_face(vertices);
	//--delete me--
	//faces.push_back(fi);
	//BOOST_FOREACH(vertex_index vd, polygon_mesh.vertices()) {
	//	std::cout << "iteratining through vertex index: " << vd << std::endl;
	//}
	return fi;
}

// ---- Check folds in the polygon ----
vector<Point_3> check_and_repair_small_folds(vector<Point_3>& linearRingPoints, double& tol) {
	
	if (linearRingPoints.size() > 5) {

		for (unsigned int i = 0; i < linearRingPoints.size()-4; i++) {

			if (linearRingPoints[i + 1] == linearRingPoints[i + 2]) {
				cout << "Consecutive same points! Remove one of them! " << endl;
				Test_Report.push_back("Error: Consecutive same points!");
				linearRingPoints.erase(linearRingPoints.begin() + (i + 1));
			}
			else {
				double dist = sqrt(CGAL::to_double(squared_distance(linearRingPoints[i + 1], linearRingPoints[i + 2])));
				if (dist < folds_tol) {
					linearRingPoints.erase(linearRingPoints.begin() + (i + 1));
					cout << "small folds found! Remove one of them! " << endl;
					Test_Report.push_back("Error: Small folds found! one of them is removed!");
					//cout << "dist: " << dist << endl;
					//cout << "fold_tol: " << folds_tol << endl;
				}
			}
		}
	}
	return linearRingPoints;
}

//Check floating point number error
bool floating_point_number_error(Surface_Mesh& pmesh, double& tol) {
	bool _floating_point_error = false;
	//duplicate points
	std::vector<Point_3> base_pts, target_pts;
	BOOST_FOREACH(vertex_index vi, pmesh.vertices()) {
		target_pts.push_back(pmesh.point(vi));
	}

	//---------------- start -- Approach 01: each point to all points ---------------
	int it1 = 0, it2 = 0;
	//std::cout << "size of vertices: " << target_pts.size() << endl;
	for (unsigned int i = 0; i < target_pts.size(); i++) {
		it1++;

		Point_3 base_point(target_pts[i]);
		for (unsigned int j = 1; j < target_pts.size(); j++) {
			it2++;
			if (base_point != target_pts[j]) {
				double dist = sqrt(CGAL::to_double(squared_distance(base_point, target_pts[j])));
				//std::cout << "dist2plane: " << std::setprecision(15) << dist << endl;

				if (dist < tol) {
					_floating_point_error = true;
					dup_points.push_back(std::make_pair(base_point, target_pts[j]));
					std::cout << "Error: floating point number errors are present in the data" << endl;
					//std::cout << std::setprecision(15) << "basepoint: " << base_point << " targetpoint: " << target_pts[j] << endl;
					//std::cout << "squared_distance: " << std::setprecision(15) << dist << endl;
				}
			}
		}
	}

	//---------------- start -- Approach 02: each point to all points from adjacent faces -------------
		//check performance for adjacent faces
		//std::vector<Point_3> adj_face_vertices;
		//BOOST_FOREACH(vertex_index vi, polygon_mesh.vertices()) {
		//	pts.push_back(polygon_mesh.point(vi));
		//}
		//for (unsigned int i = 0; i < faces.size(); i++) {
		//	BOOST_FOREACH(face_index fi, faces_around_face(polygon_mesh.halfedge(faces[i]), polygon_mesh)) {
		//		//std::cout << "Adjacent faces: " << fi << std::endl;
		//		//vertices around that adjacent face
		//		BOOST_FOREACH(vertex_index vd, vertices_around_face(polygon_mesh.halfedge(faces[i]), polygon_mesh)) {
		//			//std::cout << "Face: " << fi2  << " Vertex: " << polygon_mesh.point(vd) << endl;
		//			//to check type1error? computationally expensive
		//			pts.push_back(polygon_mesh.point(vd));
		//		}
		//	}
	//---------------- end -- Approach 02: each point to all points from adjacent faces ---------------	

	//remove duplicate entries
	for (unsigned int i = 0; i < dup_points.size(); i++) {
		Point_3 p1(dup_points[i].first);
		Point_3 p2(dup_points[i].second);
		for (unsigned int j = 1; j < dup_points.size();j++) {
			if (p1 == dup_points[j].second && p2 == dup_points[j].first) {
				//std::cout << "erasing points " << dup_points[j].first << " " << dup_points[j].second << endl;
				//erase doubly occured points
				dup_points.erase(dup_points.begin() + j);
			}
		}
	}
	std::cout << "Number of duplicate points: " << dup_points.size() << endl;
	write_err_report("Number of floating points error: ", (unsigned int)dup_points.size());

	return _floating_point_error;
}
//Repair Duplicate Points Error
Surface_Mesh repair_floating_point_number_error(Surface_Mesh& pmesh) {
	std::vector<std::pair<Point_3, Point_3>> _erroneous_points;
	bool _is_successful = true;
	std::vector<std::pair<vertex_index, int>> vertex_occurance;
	std::vector<vertex_index> dup_vertex;
	std::vector<Point_3> unique_pts;
	std::vector<vertex_index> point2vertex;

	for (unsigned int i = 0; i < dup_points.size(); i++) {

		BOOST_FOREACH(vertex_index v, pmesh.vertices()) {
			if (dup_points[i].first == pmesh.point(v)) {
				point2vertex.push_back(v);
				//std::cout << "duplicate_points first: " << v << " :" << pmesh.point(v) << endl;
			}
			if (dup_points[i].second == pmesh.point(v)) {
				point2vertex.push_back(v);
				//std::cout << "duplicate_points second: " << v << " :" << pmesh.point(v) << endl;
			}
		}
	}
	int counter = 0;
	for (unsigned int i = 0; i < point2vertex.size(); i++) {
		//std::cout << "vertices around duplicate_points vertex " << point2vertex[i] << std::endl;
		CGAL::Vertex_around_target_circulator<Surface_Mesh> vbegin(pmesh.halfedge(point2vertex[i]), pmesh), done(vbegin);
		do {
			if (*vbegin != pmesh.null_vertex()) {
				counter++;
				//std::cout << "adjacent vertex: " << *vbegin++ << endl;
			}
			//std::cout << "adjacent vertex: " << *vbegin++ << endl;
		} while (vbegin != done);

		//std::cout << "point2vertex[i]: " << point2vertex[i] << " occurance; " << counter << endl;
		vertex_occurance.push_back(std::make_pair(point2vertex[i], counter));
		counter = 0;
	}

	for (unsigned int i = 0; i < vertex_occurance.size(); i = i + 2) {
		//std::cout << vertex_occurance[i].first << " =>first => second: " << vertex_occurance[i].second << endl;
		if (vertex_occurance[i].second > vertex_occurance[i + 1].second) {
			dup_vertex.push_back(vertex_occurance[i + 1].first);
			//std::cout << vertex_occurance[i + 1].first << " is the possible duplicate point! Replace it" << endl;
		}
		else if (vertex_occurance[i].second < vertex_occurance[i + 1].second) {
			dup_vertex.push_back(vertex_occurance[i].first);
			//std::cout << vertex_occurance[i].first << " is the possible duplicate point! Replace it" << endl;
		}
		else if (vertex_occurance[i + 1].second == vertex_occurance[i].second) {
			dup_vertex.push_back(vertex_occurance[i + 1].first); //chose any one of them doesn't matter if i or i+1
			//std::cout << "same number of occurance! chose any one of those and replace other one" << endl;
			//_is_successful = false;


		}
		else {
			std::cout << "difficult to decide! try to planarize the whole face! no further processing here!" << endl;
			_is_successful = false;
		}
	}
	std::vector< std::pair< vertex_index, std::vector<face_index> >> vertex2incident_faces; //row
	if (!dup_vertex.empty()) {
		//vertex2point
		for (unsigned int i = 0; i < dup_vertex.size(); i++) {
			BOOST_FOREACH(vertex_index v, pmesh.vertices()) {
				//if (v != pmesh.null_vertex()) {
					//std::cout << "null vertex \n";
				if (dup_vertex[i] == (v)) {
					for (unsigned int j = 0; j < dup_points.size();j++) {
						if (pmesh.point(v) == dup_points[j].first) {
							std::cout << dup_points[j].first << " has to be replaced by: " << dup_points[j].second << endl;
							//_erroneous_points(erroneous point, right point)
							_erroneous_points.push_back(std::make_pair(dup_points[j].first, dup_points[j].second));
						}
						else if (pmesh.point(v) == dup_points[j].second) {
							std::cout << dup_points[j].second << " has to be replaced by: " << dup_points[j].first << endl;
							_erroneous_points.push_back(std::make_pair(dup_points[j].second, dup_points[j].first));
						}
					}

					std::vector<face_index> fa; //column
					CGAL::Face_around_target_circulator<Surface_Mesh> vbegin(pmesh.halfedge(dup_vertex[i]), pmesh), done(vbegin);
					do {
						//avoid null faces
						if (*vbegin != pmesh.null_face()) {
							//std::cout << *vbegin << " - is the incident face " << std::endl;
							fa.push_back(*vbegin);
						}
						*vbegin++;
					} while (vbegin != done);

					vertex2incident_faces.push_back(std::make_pair(dup_vertex[i], fa));
					fa.clear();
				}
				//}
			}
		}
	}
	//replacing part 
	vector<face_index> faces2update;

	for (unsigned int i = 0; i < vertex2incident_faces.size(); i++) {

		//std::cout << "vertex2incident_faces first index: " << vertex2incident_faces[i].first << endl;
		//iterate over 2nd pair index <vector>
		for (unsigned int j = 0; j < vertex2incident_faces[i].second.size(); j++) {

			//std::cout << "vertex2incident_faces second vectors: " << vertex2incident_faces[i].second[j] << endl;

			for (unsigned int k = 0; k < face_vertices.size(); k++) {
				//std::cout << "first index: " << face_vertices[k].first << endl;
				if (vertex2incident_faces[i].second[j] == face_vertices[k].first) {
					for (unsigned int ik = 0; ik < face_vertices[k].second.size(); ik++) {
						//std::cout << "second vectors: " << face_vertices[k].second[ik] << endl;

						if (_erroneous_points[i].first == face_vertices[k].second[ik]) {
							//polygon_mesh.remove_vertex(vd);
							//vertex_index v = polygon_mesh.add_vertex(_erroneous_points[i].second);
							face_vertices[k].second.at(ik) = _erroneous_points[i].second;
							faces2update.push_back(face_vertices[k].first);
							//coords.push_back(_erroneous_points[i].second);
							//std::cout << "Replacing : " << _erroneous_points[i].first << " by => " << _erroneous_points[i].second << endl;

						}
					}
				}
			}

		}
	}

	//--not used--//if more than one duplicate points in one face 
	for (unsigned int i = 0; i < faces2update.size(); i++) {
		//std::cout << "faces2update: " << faces2update[i] << endl;
		//face_index fi = faces2update[i];
		for (unsigned int j = 1;j < faces2update.size();j++) {
			if (i != j) {
				if (faces2update[i] == faces2update[j]) {
					faces2update.erase(faces2update.begin() + j);
				}
			}
		}
	}

	Surface_Mesh NewMesh;
	std::map<Point_3, vertex_index> point_to_vertex2;
	std::vector<Point_3> NewPoints;

	for (unsigned int i = 0; i < face_vertices.size(); i++) {
		//std::cout << "face_vertices first index: " << face_vertices[i].first << endl;
		//iterate over 2nd pair index <vector>
		//polygon_mesh.remove_face(face2new_vertices[i].first);

		//I updated the face_gmlid_rings vector to write the final citygml output
		for (unsigned int k = 0; k < face_gmlids_rings.size(); k++) {
			if (face_vertices[i].first == face_gmlids_rings[k].first) {
				//std::cout << "face_index: face_gmlids_rings: " << face_gmlids_rings[k].first << endl;
				//std::cout << "gml id: " << face_gmlids_rings[k].second.first << endl;
				face_gmlids_rings[k].second.second.clear();
				//std::cout << "LinearRing Points: " << face_gmlids_rings[i].second.second.size() - 1 << endl; //as it is in input file
				//for (unsigned int j = 0; j < face_gmlids_rings[i].second.second.size(); j++) {
				//	std::cout << face_gmlids_rings[i].second.second[j] << "\n";
				//}

				for (unsigned int j = 0; j < face_vertices[i].second.size(); j++) {

					//if(face_vertices[i].first!=pmesh.null_face()){

					//std::cout << "face_vertices second vectors: " << face_vertices[i].second[j] << endl;
					NewPoints.push_back(face_vertices[i].second[j]);
					face_gmlids_rings[k].second.second.push_back(face_vertices[i].second[j]);

					//}
				}
				//NewPoints.pop_back();
				add_face(NewPoints, NewMesh, point_to_vertex2);
				NewPoints.clear();
			}
		}

	}

	CityGMLOutput repaired_lrings;

	gmlid_linearRings_repaired = repaired_lrings.new_linear_rings(face_gmlids_rings);

	if (_is_successful) {
		std::cout << "Number of repaired points: " << _erroneous_points.size() << " and no of updated faces: " << faces2update.size() << endl;
		write_err_report("Number of repaired points: ", CGAL::to_double(_erroneous_points.size()));

		std::cout << "--------------------All duplicate points are replaced successfully!--------------------" << endl;
		Test_Report.push_back("--------------------All duplicate points are replaced successfully!--------------------");
	}
	else
		std::cout << "xxxxxxxxxxx -- Error: Repair failed! -- xxxxxxxxxxx" << endl;
		Test_Report.push_back("xxxxxxxxxxx -- Error: Floating point error repair failed! -- xxxxxxxxxxx");

	return NewMesh;
}

void check_planarity_error(Surface_Mesh& pmesh)
{
	//face_vertices.clear();
	//non_planar_face_vertices.clear();
	// True= Planar face; False= Non planar face
	bool is_planar;
	vector<face_index> non_planar_faces;
	BOOST_FOREACH(face_index fi, pmesh.faces()) {
		std::cout << "Checking faces: " << fi << std::endl;
		//vector of face indexes
		//faces.push_back(fi);
		std::vector<double> face_x; //contains coordinates of all vertices around a particular face
									//iterate through vertices around face
		vector<Point_3> pt;
		BOOST_FOREACH(vertex_index vi, vertices_around_face(pmesh.halfedge(fi), pmesh)) {
			//std::cout << " Vertices: " << vi << " Coords: " << std::setprecision(15) << polygon_mesh.point(vi) << endl;
			Point_3 P = pmesh.point(vi);
			pt.push_back(P);
			//convertion bcoz of Exact_predicates_exact_constructions_kernel
			face_x.push_back(CGAL::to_double(P.x()));
			face_x.push_back(CGAL::to_double(P.y()));
			face_x.push_back(CGAL::to_double(P.z()));
		}
		//check planarity for the current face
		is_planar = check_planarity_on_faces(face_x, fi, pmesh);
		//Contains non_planar_faces as face_index
		if (!is_planar) {
			non_planar_faces.push_back(fi);
		}
		//face_vertices.push_back(std::make_pair(fi, pt));
		//pt.clear();
	}

	if (non_planar_faces.empty()) {
		std::cout << "------------------ No Planarization Error! ------------------!" << endl;
	}
	else {
		std::cout << "Planarity Error! Healing required!" << endl;
		Test_Report.push_back("Planarity Error! Healing required!");

		std::cout << "Planarity Tolerance: " << distance_plane_tol << endl;
		
		std::cout << "------------------Total number of polygons: " << pmesh.number_of_faces() << " ------------------ " << endl;
		write_err_report("Total number of polygons: ", pmesh.number_of_faces());

		std::cout <<  "------------------ Number of non-planar polygons: "<< no_of_non_planar_polygons << " ------------------ " << endl;
		write_err_report("Total number of non-planar polygons: ", no_of_non_planar_polygons);
	}
}

bool check_planarity_on_faces(vector<double>& linearRingCoords, face_index& fi, Surface_Mesh& pmesh) {
	// True= Planar face; False= Non planar face
	bool is_planar = true;
	//number of non-planar faces
	int plane_no = 0;
	//find least squares plane fitting 
	Plane_3 plane = plane_equation(pmesh, fi); 	//Linear least squares plane fitting part										

	vector<vertex_index> v_indexes;
	//make it a new method
	BOOST_FOREACH(vertex_index vi, vertices_around_face(pmesh.halfedge(fi), pmesh)) {
		//Set newPoint and check if it is on the plane
		Point_3 point_of_interest = pmesh.point(vi);

		//perpendicular distance(squared) between the point and the fitted plane
		double dist2plane = sqrt(CGAL::to_double(squared_distance(point_of_interest, plane)));

		v_indexes.push_back(vi);
		//if checkPlanar = 0 (>D) then it's on the plane otherwise not and threshold is a user defined value
		if (dist2plane > distance_plane_tol) {
			std::cout << std::setprecision(15) << "the point (" << point_of_interest.x() << " " << point_of_interest.y() << " " << point_of_interest.z() << ") is NOT on the Plane" << endl;
			std::cout << std::setprecision(15) << "distance2plane: " << dist2plane << endl;
			//std::cout << "----------------------------------------" << endl;
			plane_no++;
			//if (vi != pmesh.null_vertex()) {
			//non_planar_vertices.push_back(vi);
			//}
			//vertex_map.insert(std::pair<vertex_index, Point_3>(vi, point_of_interest));
			//non_planar_vertex
			
			//a plane can have more than 1 non planar points
			//number of non planar polygons
			if (plane_no == 1) {
				//std::cout << " Name of non planar face: " << fi << endl;
				no_of_non_planar_polygons++;
				is_planar = false;
			}
		}
	}
	if (!is_planar) {
		non_planar_face_vertices.push_back(std::make_pair(fi, v_indexes));
		v_indexes.clear();
	}
	return is_planar;
}

void healing(Surface_Mesh& mesh2) {
	Surface_Mesh copied_sm = mesh2;
	std::vector<Point_3> points;
	std::vector<Point_3> new_face;
	std::vector<std::vector<Point_3>> new_faces;

	std::cout << "Repair Tolerance: " << repair_tol << endl;
	write_err_report("Repair tolerance: ", repair_tol);
	Test_Report.push_back("----------------------Repair Report------------------------------");

	cout << "------------------------------------------------------------------" << endl;
	cout << "----------------------Healing Starts------------------------------" << endl;
	cout << "------------------------------------------------------------------" << endl;

	for (unsigned int index = 0; index < non_planar_face_vertices.size(); index++) {
		//cout << "vertices: " << endl;
		BOOST_FOREACH(vertex_index v, vertices_around_face(copied_sm.halfedge(non_planar_face_vertices[index].first), copied_sm)) {
			points.push_back(copied_sm.point(v));
			//cout << mesh2.point(v) << " "<< endl;
		}
		mesh2.clear();

		//healing starts
		vector<Point_3> pts; //only three points
		unsigned int mid_point_index = 1;
		unsigned int last_point_index;
		unsigned int next_point_index;

		//maximum no of faces = points.size()-2;
		for (unsigned int i = 0; i < points.size(); i++) {

			i = mid_point_index;
			last_point_index = mid_point_index + 1;
			next_point_index = last_point_index + 1;


			Point_3 P(points.at(0));
			Point_3 Q(points[mid_point_index]);

			if (next_point_index < points.size()) {

				Point_3 R(points[last_point_index]);
				pts.push_back(P);
				pts.push_back(Q);
				pts.push_back(R);

				//create a mesh with only 3 vertices
				Surface_Mesh temp_sm;
				std::map<Point_3, vertex_index> temp_point_to_vertex;
				face_index temp_face = add_face(pts, temp_sm, temp_point_to_vertex);

				//std::cout << std::setprecision(15) << "plane defining points = " << "(" << P << "," << Q << "," << R << ")" << endl;
				Plane_3 lsp = plane_equation(temp_sm, temp_face);
				pts.clear();

				new_face.push_back(P);
				new_face.push_back(Q);
				new_face.push_back(R);

				double dist2plane = sqrt(CGAL::to_double(squared_distance(points[next_point_index], lsp)));

				
				if (dist2plane < repair_tol /*|| dist2plane == tol*/)
				{
					new_face.push_back(points[next_point_index]);
					
					mid_point_index = next_point_index - 1;

					for (unsigned int j = mid_point_index + 2; j < points.size(); j++) {						
						double d2p = sqrt(CGAL::to_double(squared_distance(points[j], lsp)));

						if (d2p < repair_tol /*|| d2p == tol*/)
						{
							new_face.push_back(points[j]);
							mid_point_index = j - 1;

							//J can be the last point
							if (mid_point_index + 1 == points.size()) {
								i = (unsigned int)points.size();
							}

						}
						else {
							mid_point_index = j - 1;
							j = (unsigned int)points.size(); //(unsigned int) - due to convertion waring
						}
					}
					//}
					new_faces.push_back(new_face);
					new_face.clear();
					//all points are finished
				}
				else {
					new_faces.push_back(new_face);
					new_face.clear();
					mid_point_index = last_point_index;

				}
				//}
			}
			//perhaps the last face
			else {
				Point_3 P1(points.at(0));
				Point_3 Q1(points[mid_point_index]);
				Point_3 R1(points[last_point_index]);

				new_face.push_back(P1);
				new_face.push_back(Q1);
				new_face.push_back(R1);

				new_faces.push_back(new_face);
				new_face.clear();
				i = (unsigned int)points.size(); //(unsigned int) - due to convertion waring
			}

		} //end of the points i for loop

		Surface_Mesh repaired_mesh;
		std::map<Point_3, vertex_index> point2vertex, point2vertex_poly;
		for (unsigned int i = 0; i < new_faces.size();i++) {
			//std::cout << "faces:" << i << " Vertices: " << endl;
			for (unsigned int j = 0; j < new_faces[i].size(); j++) {
				//std::cout << new_faces[i][j] << endl;
			}
			//std::cout << "\n";
			add_face(new_faces[i], repaired_mesh, point2vertex);
			//polygon_mesh.collect_garbage();
			add_face(new_faces[i], polygon_mesh, point2vertex_poly); //add new faces to polygon mesh to do a final check
		}

		//to find the gmlid of the non-planar faces
		for (unsigned int i = 0; i < face_gmlids_rings.size(); i++) {
			if (non_planar_face_vertices[index].first == face_gmlids_rings[i].first) {
				write_err_report("GML id of non-planar polygon: ", face_gmlids_rings[i].second.first);
				splitted_new_faces.push_back(std::make_pair(face_gmlids_rings[i].second.first, repaired_mesh));
			}
		}

		repaired_mesh.clear();
		points.clear();
		new_faces.clear();
		//healing ends
	}//end of for loop 

	 //show me the final outputs
	for (unsigned int i = 0; i < splitted_new_faces.size(); i++) {

		std::cout << "Total Number of vertices before Repair Process: " << splitted_new_faces[i].second.number_of_vertices() << endl;
		write_err_report("Number of vertices before repair (non-planar polygon): ", splitted_new_faces[i].second.number_of_vertices());

		std::cout << "Total Number of split faces after Repair Process: " << splitted_new_faces[i].second.number_of_faces() << endl;
		write_err_report("Number of split faces after Repair Process: ", splitted_new_faces[i].second.number_of_faces());

		BOOST_FOREACH(face_index fi, splitted_new_faces[i].second.faces()) {
			//std::cout << "face: " << fi<<endl;
			int counter = 0;
			BOOST_FOREACH(vertex_index v, vertices_around_face(splitted_new_faces[i].second.halfedge(fi), splitted_new_faces[i].second)) {
				//std::cout<<splitted_new_faces[i].second.point(v)<<" ";
				counter++;
			}
			//std::cout << "\n";
			//if (counter != 3) {
			std::cout << "Split face contains : " << counter << " vertices" << endl;
			write_err_report("Split face contains (vertices): ", counter);
			//}
		}
	}

	cout << "------------- Total number of repaired faces-------------: " << splitted_new_faces.size() << endl;
	write_err_report("------------- Total number of repaired faces-------------: ", (unsigned int)splitted_new_faces.size());

	CityGMLOutput citygmloutput;
	vector<std::pair<std::string, std::string>> new_gmlid_splitted_Lrings = citygmloutput.writeRepairedGeom(splitted_new_faces);

	vector<std::pair<std::string, std::string>> final_Lrings;
	//replace the original geomerty with all changes 
	//vector<std::pair<std::string, std::string>> citygmloutput.new_linear_rings(face_gmlids_rings);

	if (!gmlid_linearRings_repaired.empty()) {
		for (unsigned int i = 0; i < gmlid_linearRings_repaired.size(); i++) {

			for (unsigned int j = 0; j < new_gmlid_splitted_Lrings.size(); j++) {

				if (gmlid_linearRings_repaired[i].first == new_gmlid_splitted_Lrings[j].first) {
					//cout << "gmlid_linearRings_repaired: " << gmlid_linearRings_repaired[i].second << endl;
					gmlid_linearRings_repaired[i].second.clear();
					gmlid_linearRings_repaired[i].second = new_gmlid_splitted_Lrings[j].second;
				}

			}
		}

		//check all updated faces for (repair floating point number error and repair planarity error)
		citygmloutput.writeToCityGML(origin_building, gmlid_linearRings_repaired, filepath, filename);
		Test_Report.push_back("-----------------Repair successfully executed!-----------------");
		check_planarity_error(polygon_mesh);
	}
	else {

		//check all updated faces for (repair floating point number error and repair planarity error)
		citygmloutput.writeToCityGML(origin_building, new_gmlid_splitted_Lrings, filepath, filename);
		Test_Report.push_back("-----------------Repair successfully executed!-----------------");
		non_planar_face_vertices.clear();
		check_planarity_error(polygon_mesh);
	}


}

void write_err_report(std::string t, double v) {
	std::string text = t + " " + std::to_string(v);
	Test_Report.push_back(text);
}

void write_err_report(std::string t, int v) {
	std::string text = t + " " + std::to_string(v);
	Test_Report.push_back(text);
}

void write_err_report(std::string t, unsigned int v) {
	std::string text = t + " " + std::to_string(v);
	Test_Report.push_back(text);
}

void write_err_report(std::string t, std::string v) {
	std::string text = t + " " + v;
	Test_Report.push_back(text);
}

void write_off_format(Surface_Mesh& m) {
	std::string inputFile = filepath + "sm.off";
	std::ofstream out(inputFile);
	out << std::setprecision(15) << m;
	out.close();
}

Plane_3 plane_equation(Surface_Mesh& mesh, face_index& fi) {
	//Linear least squares plane fitting part
	vector<Point_3> points, pts; //later replace this with face_x 
	Plane_3 plane;
	BOOST_FOREACH(vertex_index vi, vertices_around_face(mesh.halfedge(fi), mesh)) {
		pts.push_back(mesh.point(vi));
	}

	//------------------- first 3 non collinear points defines the plane------------------------------
		//to use first three points to define the plane
		/*
		for (int i = 0; i < 1;i++) {
			Point_3 X(pts[i].x(), pts[i].y(), pts[i].z());
			Point_3 Y(pts[i+1].x(), pts[i+1].y(), pts[i+1].z());
			Point_3 Z(pts[i+2].x(), pts[i+2].y(), pts[i+2].z());
			if (CGAL::collinear(X, Y, Z)) {

				Point_3 Z (pts[i + 3].x(), pts[i + 3].y(), pts[i + 3].z());
				if (CGAL::collinear(X, Y, Z)) {
					//check if it is again co-linear
					std::cout << " collinear --- choose another point" << endl;
				}
				else
				{
					points.push_back(X);points.push_back(Y);points.push_back(Z);
				}
			}
			else {
				points.push_back(X);points.push_back(Y);points.push_back(Z);
			}
		}

		Plane_3 pln(points.at(0), points.at(1), points.at(2));
		//std::cout << std::setprecision(15) <<"plane defining points: "<< "(" << points.at(0) << "," << points.at(1) << "," << points.at(2) << ")" << endl;
		//std::cout << "plane fitting() = " << "(" << pln.a() << "," << pln.b() << "," << pln.c() << "," << pln.d() << ")"<< endl;

		return pln;

		*/

		//------------------- least squares plane fitting -------------------------
	double plane_quality = linear_least_squares_fitting_3(pts.begin(), pts.end(), plane, CGAL::Dimension_tag<0>());
	//std::cout << "least square plane fitting() = " << "(" << plane.a() << "," << plane.b() << "," << plane.c() << "," << plane.d() << ")" << "(1=Best fit,0=NOT best) : " << plane_quality << endl;

	//additional code (Adapted from val3dity)
	double zero = 1e-12;
	bool Iszero = false;
	double a = CGAL::to_double(plane.a());
	double b = CGAL::to_double(plane.b());
	double c = CGAL::to_double(plane.c());

	if (CGAL::abs(plane.a()) < zero) {
		a = 0;
		Iszero = true;
	}
	if (CGAL::abs(plane.b()) < zero) {
		b = 0;
		Iszero = true;
	}
	if (CGAL::abs(plane.c()) < zero) {
		c = 0;
		Iszero = true;
	}

	if (Iszero == false) {
		return plane;
	}
	else {
		Plane_3 pl(a, b, c, plane.d());
		//std::cout << "the plane parameters are changed to zero!" << endl;
		return pl;
	}
}

void test_cases()
{
	std::cout << " --test_cases() method-- " << endl;
	std::vector<Point_3> pt;


	//test_set 1
	//test_set 2
	//391084.16934711 5819634.61678903 48.1121807963131
	//391082.052076655 5819635.13460487 48.1126612001999
	//391082.052074473 5819635.13460325 48.1126612001999
	pt.push_back(Point_3(391084.16934711, 5819634.61678903, 48.1121807963131));
	pt.push_back(Point_3(391082.052076655, 5819635.13460487, 48.1126612001999));
	pt.push_back(Point_3(391082.052074473, 5819635.13460325, 48.1126612001999));
	////pt.push_back(Point_3(458885.0, 5438350.0, 112.0));
	//Point_3 S(458885.0, 5438350.0, 112.01);
	std::cout << "distance: " << sqrt(CGAL::to_double(squared_distance(pt.at(1), pt.at(2)))) << endl;
	std::cout << "distance: " << sqrt(CGAL::to_double(squared_distance(pt.at(2), pt.at(3)))) << endl;

	//test_set 3
	pt.push_back(Point_3(458885, 5438350, 112));
	pt.push_back(Point_3(458885, 5438355, 115));
	pt.push_back(Point_3(458885, 5438352.5, 117));

	Point_3 S(458885, 5438350, 115);

	std::map<Point_3, vertex_index> p2v;
	Surface_Mesh dmesh;

	add_face(pt, dmesh, p2v);
	face_index fi;
	BOOST_FOREACH(face_index ff, dmesh.faces()) {
		fi = ff;
		cout << "faces \n";
	}

	BOOST_FOREACH(vertex_index v, vertices_around_face(dmesh.halfedge(fi), dmesh)) {
		halfedge_index hi = dmesh.halfedge(v);
		halfedge_index hi_opp = dmesh.opposite(hi);
		halfedge_index hi_nnext = dmesh.next(hi_opp);
		vertex_index vi_nnext = dmesh.target(hi_nnext);
		halfedge_index hi_nnnext = dmesh.next(hi_nnext);
		vertex_index vi_nnnext = dmesh.target(hi_nnnext);
		vertex_index vi_next = dmesh.target(hi_opp);
		halfedge_index hi_next = dmesh.next(hi);
		halfedge_index hi_prev = dmesh.prev(hi);
		vertex_index vi_prev = dmesh.target(hi_next);
		Point_3 P(dmesh.point(v));
		Point_3 Q(dmesh.point(dmesh.target(hi_opp)));
		Point_3 R(dmesh.point(dmesh.target(hi_nnext)));
		Point_3 Next_vertex(dmesh.point(dmesh.target(hi_nnnext)));
		std::cout << std::setprecision(15) << P << Q << R << Next_vertex << endl;

		std::cout << "v: " << v << " vi_next " << vi_next << " vi_prev " << vi_prev << endl;
		std::cout << "hi: " << hi << " Next halfedge " << hi_next << " Prev halfedge " << hi_prev << " Opposite halfedge " << hi_opp << endl;

		if (CGAL::collinear(P, Q, R)) {
			std::cout << "collinear" << endl;
		}

		Plane_3 plane(P, Q, R);
		std::cout << std::setprecision(15) << "plane defining points: " << "(" << P << "," << Q << "," << R << ")" << endl;
		std::cout << "plane fitting() = " << "(" << plane.a() << "," << plane.b() << "," << plane.c() << "," << plane.d() << ")" << endl;
		std::cout << std::setprecision(15) << "dist to P: " << (CGAL::to_double(squared_distance(P, plane))) << endl;
		std::cout << std::setprecision(15) << "dist to Q: " << (CGAL::to_double(squared_distance(Q, plane))) << endl;
		std::cout << std::setprecision(15) << "dist to R: " << (CGAL::to_double(squared_distance(R, plane))) << endl;
		std::cout << std::setprecision(15) << "dist to : " << (CGAL::to_double(squared_distance(S, plane))) << endl;
		Plane_3 pln;
		double plane_quality = linear_least_squares_fitting_3(pt.begin(), pt.end(), pln, CGAL::Dimension_tag<0>());
		std::cout << "LS plane fitting() = " << "(" << pln.a() << "," << pln.b() << "," << pln.c() << "," << pln.d() << ")" << "(1=Best fit,0=NOT best) : " << plane_quality << endl;
		std::cout << std::setprecision(15) << "LS dist to P: " << (CGAL::to_double(squared_distance(P, pln))) << endl;
		std::cout << std::setprecision(15) << "LS dist to Q: " << (CGAL::to_double(squared_distance(Q, pln))) << endl;
		std::cout << std::setprecision(15) << "LS dist to R: " << (CGAL::to_double(squared_distance(R, pln))) << endl;
		std::cout << std::setprecision(15) << "dist to : " << (CGAL::to_double(squared_distance(S, pln))) << endl;

		double A, B, C, D;
		A = CGAL::to_double(plane.a());
		B = CGAL::to_double(plane.b());
		C = CGAL::to_double(plane.c());
		D = CGAL::to_double(plane.d());

		double checkPlanar = (A* CGAL::to_double(S.x())) + (B*CGAL::to_double(S.y())) + (C*CGAL::to_double(S.z())) + D;
		std::cout << "the checkPlanar value is : " << std::setprecision(15) << (checkPlanar) << endl;
	}

	Surface_Mesh mesh;
	//SM_to_Nef SmToNef;
	//Nef_polyhedron n = SmToNef.get_Nef();
	vertex_index u1 = mesh.add_vertex(Kernel::Point_3(0, 0, 0));
	vertex_index v1 = mesh.add_vertex(Kernel::Point_3(1, 0, 0));
	vertex_index w1 = mesh.add_vertex(Kernel::Point_3(1, 1, 0));
	vertex_index x1 = mesh.add_vertex(Kernel::Point_3(0, 1, 0));
	vertex_index z1 = mesh.add_vertex(Kernel::Point_3(0.5, 0.5, 0));

	face_index face1 = mesh.add_face(u1, v1, w1, x1);
	//face_index face2 = mesh.add_face(x1, z1, w1);
	//face_index face3 = mesh.add_face(x1, u1, z1);
	//face_index face4 = mesh.add_face(u1, v1, z1);

	BOOST_FOREACH(vertex_index v, vertices_around_face(mesh.halfedge(face1), mesh)) {
		halfedge_index hi = mesh.halfedge(v);
		halfedge_index hi_opp = mesh.opposite(hi);
		halfedge_index hi_nnext = mesh.next(hi_opp);
		vertex_index vi_nnext = mesh.target(hi_nnext);
		halfedge_index hi_nnnext = mesh.next(hi_nnext);
		vertex_index vi_nnnext = mesh.target(hi_nnnext);
		vertex_index vi_next = mesh.target(hi_opp);
		halfedge_index hi_next = mesh.next(hi);
		halfedge_index hi_prev = mesh.prev(hi);
		vertex_index vi_prev = mesh.target(hi_next);
		Point_3 P(mesh.point(v));
		Point_3 Q(mesh.point(mesh.target(hi_opp)));
		Point_3 R(mesh.point(mesh.target(hi_nnext)));
		Point_3 Next_vertex(mesh.point(mesh.target(hi_nnnext)));
		std::cout << P << Q << R << Next_vertex << endl;
		std::cout << "v: " << v << " vi_next " << vi_next << " vi_nnext " << vi_nnext << " vi_nnnext " << vi_nnnext << endl;
		std::cout << "hi: " << hi << " Next halfedge " << hi_next << " Prev halfedge " << hi_prev << " Opposite halfedge " << hi_opp << endl;

		if (CGAL::collinear(P, Q, R)) {
			std::cout << "collinear" << endl;
		}
	}
}

//unused codes
//-----------------------------------------------------------------------------------------------------------------
/*
//How to read and write off format
std::ifstream stream(inputFile);
Surface_Mesh polygon_mesh;
stream >> polygon_mesh;



write_off_format(polygon_mesh);
*/
//---------------------------------hwo to calculate intersection of three planes--------------------------------------------------------------------------------
//
//Test if the a point is incident to exactly three faces 
//precondition to find intersection of three plane

//	int no_of_incident_vertices;
//	bool count = true;
//	bool has_3incident_face = false;
//	for (unsigned int i = 0; i < non_planar_vertices.size(); i++) {

//		std::cout << "vertices around vertex " << non_planar_vertices[i] << " --  " << pmesh.point(non_planar_vertices[i]) << std::endl;
//		CGAL::Vertex_around_target_circulator<Surface_Mesh> vbegin(pmesh.halfedge(non_planar_vertices[i]), pmesh), done(vbegin);
//		no_of_incident_vertices = 0;
//		do {
//			if (*vbegin != pmesh.null_vertex()) {
//				std::cout << *vbegin++ << " ---- " << pmesh.point(*vbegin++) << std::endl;
//				no_of_incident_vertices++;
//			}
//		} while (vbegin != done);

//		if (no_of_incident_vertices == 3) {
//			if (count) {
//				has_3incident_face = true;
//			}

//		}
//		else {
//			has_3incident_face = false;
//			count = false;
//		}
//	}

//	if (has_3incident_face) {
//		std::cout << "+++++ Each non planar vertex is incident to exactly three faces +++++" << endl;


//		//adjacent faces to a vertex 
//		for (unsigned int k = 0; k < non_planar_vertices.size(); k++) {
//			vector<face_index> adj_faces;
//			vector<Plane_3> adj_planes;
//			std::cout << "faces around vertex " << non_planar_vertices[k] << " --  " << pmesh.point(non_planar_vertices[k]) << std::endl;
//			CGAL::Face_around_target_circulator<Surface_Mesh> vbegin(pmesh.halfedge(non_planar_vertices[k]), pmesh), done(vbegin);
//			do {
//				//std::cout << *vbegin++ << " ---- " << std::endl;
//				adj_faces.push_back(*vbegin++);
//				adj_planes.push_back(plane_equation(pmesh, *vbegin++));

//			} while (vbegin != done);

//			// find intersection point of three planes
//			//std::cout << "size of adj_faces " << adj_faces.size() << endl;
//			//std::cout << "size of adj_planes " << adj_planes.size() << endl;
//			for (unsigned int i = 0; i < adj_planes.size(); i = i + 3) {
//				Vector_3 n1, n2, n3;
//				n1 = adj_planes[i].orthogonal_vector();
//				n2 = adj_planes[i + 1].orthogonal_vector();
//				n3 = adj_planes[i + 2].orthogonal_vector();
//				double d1, d2, d3;
//				d1 = -(adj_planes[i].d()); //d = -D 
//				d2 = -(adj_planes[i + 1].d());
//				d3 = -(adj_planes[i + 2].d());
//				Vector_3 cross_n23 = CGAL::cross_product(n2, n3);
//				Vector_3 cross_n31 = CGAL::cross_product(n3, n1);
//				Vector_3 cross_n12 = CGAL::cross_product(n1, n2);
//				Vector_3 mul1 = (d1 * cross_n23);
//				Vector_3 mul2 = (d2 * cross_n31);
//				Vector_3 mul3 = (d3 * cross_n12);
//				Vector_3 part1 = (mul1 + mul2 + mul3);
//				double dot_n23 = CGAL::to_double(CGAL::scalar_product(n1, cross_n23));
//				if (dot_n23 == 0) {
//					std::cout << "Two planes are parallel!! Determinant is zero! => n1.(n2*n3)!=0)" << endl;
//				}
//				else
//				{
//					Vector_3 pt = (part1 / dot_n23);
//					std::cout << pmesh.point(non_planar_vertices[k]) << " can be replaced by intersection point: " << pt << endl;
//					intersection_points.insert(std::pair<vertex_index, Vector_3>(non_planar_vertices[k], pt));
//				}

//			}

//		}
//	}
//	else
//	{
//		std::cout << "----- Warning: Non planar vertex is NOT incident to exactly three faces. ----" << endl;
//	}

//-----------------------------------------------------------------------------------------------------------------