/*
* building.cpp
*
*  Created on: Jun 26, 2016
*      Author: Md.Nazmul
*/

#include "Building.h"

Building::Building() {
	// TODO Auto-generated constructor stub

}

Building::~Building() {
	// TODO Auto-generated destructor stub
}

void Building::readGMLFile(string filePath) {
	ifstream file_(filePath.c_str());
	std::string line;
	//to find the position of the <bldg:Building
	int pos = 0;
	int B_foundStartPos, B_foundEndPos;
	int GS_foundStartPos, GS_foundEndPos;
	int RS_foundStartPos, RS_foundEndPos;
	int WS_foundStartPos, WS_foundEndPos;

	if (file_.is_open()) {
		while (getline(file_, line)) {
			cityObjectMember.push_back(line);
			if (line.find("<bldg:Building") != std::string::npos) {
				B_foundStartPos = pos++;
			}
			else if (line.find("</bldg:Building>") != std::string::npos) {
				B_foundEndPos = pos;
			}
			else if (line.find("<bldg:GroundSurface") != std::string::npos) {
				GS_foundStartPos = pos++;
			}
			else if (line.find("</bldg:GroundSurface>") != std::string::npos) {
				GS_foundEndPos = pos;
			}
			else if (line.find("<bldg:RoofSurface") != std::string::npos) {
				RS_foundStartPos = pos++;
			}
			else if (line.find("</bldg:RoofSurface>") != std::string::npos) {
				RS_foundEndPos = pos;
			}
			else if (line.find("<bldg:WallSurface") != std::string::npos) {
				WS_foundStartPos = pos++;
			}
			else if (line.find("</bldg:WallSurface>") != std::string::npos) {
				WS_foundEndPos = pos;
			}
			else {
				pos++;
			}
		}
		file_.clear();
		file_.close();
	}

	else {
		std::cerr << "The file is NOT open! Error reading file from " << filePath << std::endl;
	}

	for (int i = B_foundStartPos; i<B_foundEndPos; i++) {
		buildingGeom.push_back(cityObjectMember[i]);
	}
}

void Building::WriteInFile(vector<string> v, string inputFile) {
	std::ofstream fs;
	fs.open(inputFile.c_str());

	for (unsigned int i = 0; i<v.size();i++) {
		fs <<  v[i] << endl;
	}
	fs.clear();
	fs.close();
}

void Building::WriteInOFF(vector<double> v, string inputFile) {
	std::ofstream fs;
	fs.open(inputFile.c_str());

	for (unsigned int i = 0; i<v.size();i++) {
		fs << std::fixed << std::setprecision(15) << v[i] << endl;
	}
	fs.clear();
	fs.close();
}

