#define _CRT_SECURE_NO_WARNINGS


#include "CityGMLOutput.h"

using namespace std;

CityGMLOutput::CityGMLOutput() {
	// TODO Auto-generated constructor stub
}

CityGMLOutput::~CityGMLOutput() {
	// TODO Auto-generated destructor stub
}
int CityGMLOutput::add(int& a, int& b) {
	return a + b;
}

std::string  CityGMLOutput::doubleToString(double d, int ndec) {
	int i = 0;
	stringstream ss;
	ss.precision(50);
	bool pointFound = false;
	ss << d;
	std::string ds = ss.str(), ns;
	std::string::iterator si = ds.begin();
	while (i<ndec) {
		ns.push_back(*si);
		if (*si == '.' && !pointFound) {
			pointFound = true;
		}
		else if (pointFound) ++i;
		++si;
		if (si == ds.end()) {
			break;
		}
	}
	//cout << ns << endl;
	return ns;
}
//
std::vector<std::pair<std::string, std::string>> CityGMLOutput::new_linear_rings(std::vector<std::pair<face_index, std::pair<std::string, vector<Point_3>>>>& face_gmlids_rings) {
	
	vector<std::pair<std::string, std::string>> gmlid_linearRing;

	//write only the affected faces

	for (unsigned int i = 0; i < face_gmlids_rings.size(); i++) {
		//cout << "first index: " << face_gmlids_rings[i].first << endl;
		std::string NewLinearRing = "";
		std::string coords = "first";
		std::string lastPoint;

		//iterate over 2nd pair index <vector>
		//polygon_mesh.remove_face(face2new_vertices[i].first);
		for (unsigned int j = 0; j < face_gmlids_rings[i].second.second.size(); j++) {
			//cout << "second vectors: " << face_gmlids_rings[i].second.second[j] << endl;
			if (coords == "first") {
				lastPoint = doubleToString(CGAL::to_double(face_gmlids_rings[i].second.second[j].x()), 15) + " " + doubleToString(face_gmlids_rings[i].second.second[j].y(), 15) + " " + doubleToString(face_gmlids_rings[i].second.second[j].z(), 15);
			}

			coords = doubleToString(face_gmlids_rings[i].second.second[j].x(), 15) + " " + doubleToString(face_gmlids_rings[i].second.second[j].y(), 15) + " " + doubleToString(face_gmlids_rings[i].second.second[j].z(), 15) + " ";
			NewLinearRing = NewLinearRing + coords;
		}
		NewLinearRing.append(lastPoint);

		//cout << NewLinearRing << endl;
		std::string linearRingTag = "<gml:posList srsDimension=\"3\">" + NewLinearRing + "</gml:posList>";

		//cout << linearRingTag << endl;

		gmlid_linearRing.push_back(std::make_pair(face_gmlids_rings[i].second.first, linearRingTag));
		//write in output file
		//output << linearRingTag << "\n";

	}



	//for (unsigned int i = 0; i < gmlid_linearRing.size(); i++) {
	//	cout << "GMLid: \n" << gmlid_linearRing[i].first << " LinearRing: " << gmlid_linearRing[i].second << endl;
	//}

	return gmlid_linearRing;
}
//
void CityGMLOutput::writeToCityGML(std::vector<std::string>& origin_building, vector<std::pair<std::string, std::string>>& gmlid_linearRings_pair, std::string filepath, std::string filename) {
	
	std::vector<std::string> final_output;
	std::string findGMLID;
	int k;

	for (unsigned int i = 0; i < gmlid_linearRings_pair.size(); i++) {
		findGMLID = gmlid_linearRings_pair[i].first;

		if (i == 0) {
			std::string buffer;
			stringstream sstream(gmlid_linearRings_pair[0].first);

			while (sstream >> buffer) {
				if (buffer == "<gml:Polygon") {
					k = 3;
				}
				else if (buffer == "<gml:LinearRing") {
					k = 1;
				}
			}
		}
		for (unsigned int j = 0; j < origin_building.size();j++) {
			//cout << origin_building[j] << endl;
			//working
			if (origin_building[j].find(findGMLID) != std::string::npos) {
				std::string buf;
				stringstream ss(origin_building.at(j + k));
				while (ss >> buf) {
					if(buf == "<gml:posList"){ //can have multiple times of GML_ID //replace where <gml:Polygon gml:id= or <gml:LinearRing gml:id=
						origin_building.at(j + k) = (gmlid_linearRings_pair[i].second); //replace the old linear ring by new
					}
				}
				//origin_building.at(j + 3) = (gmlid_linearRings_pair[i].second); //replace the old linear ring by new
			}
		}

	}	
	//write in file 
	std::string output_inputFile = filepath + "planarized_"+filename+".gml";
	std::ofstream writeGML(output_inputFile);

	for (unsigned int j = 0; j < origin_building.size();j++) {
		writeGML << origin_building[j] << "\n";
	}
	writeGML.clear();
	writeGML.close();

	cout << "The planarized building model is successfully generated!" << endl;
	cout << "Find it here: " << filepath + "planarized_" + filename + ".gml" << endl;

}


std::vector<std::pair<std::string, std::string>> CityGMLOutput::writeRepairedGeom(std::vector < std::pair<std::string, Surface_Mesh> >& splitted_faces) {
	

	vector<std::pair<std::string, std::string>> new_gmlid_linearRings;
	//show me the final outputs//splitted_faces.second is the new surface_mesh
	for (unsigned int i = 0; i < splitted_faces.size(); i++) {
		//std::cout << "gml id: " << splitted_faces[i].first << endl;
		//std::cout << " Splitted faces: " << endl;
		std::vector<std::string> combine_splitted_faces;
		
		BOOST_FOREACH(face_index fi, splitted_faces[i].second.faces()) {
			//std::cout << "face: " << fi << endl;
			std::string NewLinearRing = "";
			std::string coords = "first";
			std::string lastPoint;
			BOOST_FOREACH(vertex_index v, vertices_around_face(splitted_faces[i].second.halfedge(fi), splitted_faces[i].second)) {
				
				if (coords == "first") {
					lastPoint = doubleToString((splitted_faces[i].second.point(v)).x(), 15) + " " + doubleToString((splitted_faces[i].second.point(v)).y(), 15) + " " + doubleToString((splitted_faces[i].second.point(v)).z(), 15);
				}
				
				coords = doubleToString((splitted_faces[i].second.point(v)).x(), 15) + " " + doubleToString((splitted_faces[i].second.point(v)).y(), 15) + " " + doubleToString((splitted_faces[i].second.point(v)).z(), 15) + " ";
				NewLinearRing = NewLinearRing + coords;
			}
			NewLinearRing.append(lastPoint);
			//cout << NewLinearRing << endl;
			std::string new_linearRing_Tag = "<gml:posList srsDimension=\"3\">" + NewLinearRing + "</gml:posList>";
			combine_splitted_faces.push_back(new_linearRing_Tag);
			
			//cout << new_linearRing_Tag << endl;
		}



		std::string new_gml_id = "_planarized_face_" + std::to_string(i);
		std::string indents = "\t\t\t\t\t";
		std::string LR_tags = indents + "\t\t\t\t" + "</gml:LinearRing>" + "\n";
		LR_tags += indents + "\t\t\t" + "</gml:exterior>" + "\n";
		LR_tags += indents + "\t\t" + "</gml:Polygon>" + "\n";
		LR_tags += indents + "\t" + "</gml:surfaceMember>" + "\n";
		//LR_tags += indents + "" + "</gml:MultiSurface>" + "\n";
		//LR_tags += indents + "" + "<gml:MultiSurface>" + "\n";
		LR_tags += indents + "\t" + "<gml:surfaceMember>" + "\n";
		LR_tags += indents + "\t\t" + "<gml:Polygon " + "gml:id=\"" + new_gml_id + "\">" + "\n";
		LR_tags += indents + "\t\t\t" + "<gml:exterior>" + "\n";
		LR_tags += indents + "\t\t\t\t" + "<gml:LinearRing>" + "\n";

		//std::cout << LR_tags << endl;


		//adding gml tags in between or polygons
		std::string splitedRings="";
		for (unsigned int ii = 0; ii < combine_splitted_faces.size(); ii++) {
			if (ii == combine_splitted_faces.size() - 1) {
				splitedRings += indents + "\t\t\t\t\t" + combine_splitted_faces[ii];
			}
			else {
				splitedRings += indents + "\t\t\t\t\t" + combine_splitted_faces[ii] + "\n" + LR_tags;
			}

		}
		combine_splitted_faces.clear();
		LR_tags.clear();
		new_gmlid_linearRings.push_back(std::make_pair(splitted_faces[i].first, splitedRings));
	}



	//for (unsigned int i = 0; i < new_gmlid_linearRings.size(); i++) {
		//cout << "GMLid: " << new_gmlid_linearRings[i].first<<endl;
		//cout <<" LinearRing: " << new_gmlid_linearRings[i].second << endl;
	//}

	return new_gmlid_linearRings;
}
