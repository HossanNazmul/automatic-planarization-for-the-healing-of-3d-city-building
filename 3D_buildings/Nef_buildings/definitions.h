#pragma once
//C++ standard header files and custom header files
//CGAL header files

#define CGAL_EIGEN3_ENABLED

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <iostream>
#include <cmath>
#include <string>
#include <fstream>
#include <sstream>
#include <map>
#include <CGAL/Vector_3.h>
#include <CGAL/Plane_3.h>
#include <boost/foreach.hpp>
#include <CGAL/Surface_mesh.h>
#include <CGAL/squared_distance_3.h> //for 3D functions
#include <CGAL/linear_least_squares_fitting_3.h> //NOTE: doesn't work with CGAL::Exact_predicates_exact_constructions_kernel :(

#include <CGAL/Eigen_diagonalize_traits.h>
#include <ctime>

//Polyhedron and Nef_polyhedron related headers here
#include <CGAL/Polyhedron_3.h>
//#include <CGAL/Polyhedron_items_with_id_3.h>
//#include <CGAL/Nef_polyhedron_3.h>
//#include <CGAL/IO/Nef_polyhedron_iostream_3.h>
//#include <CGAL/OFF_to_nef_3.h>

//CGAL Kernels
typedef CGAL::Exact_predicates_inexact_constructions_kernel      Kernel;
//typedef CGAL::Simple_cartesian<double> Kernel;
typedef Kernel::Point_3             Point_3;
typedef Kernel::Plane_3             Plane_3;
typedef Kernel::Vector_3			Vector_3;

//Surface_mesh typedefs
typedef CGAL::Surface_mesh<Point_3> Surface_Mesh;
typedef Surface_Mesh::Vertex_index vertex_index;
typedef Surface_Mesh::Halfedge_index halfedge_index;
typedef Surface_Mesh::Face_index face_index;
typedef Surface_Mesh::Vertex_range vertex_range;


//Important: Nef needs Exact_predicates_exact_constructions_kernel otherwise error?
//Polyhedron typedefs
typedef CGAL::Polyhedron_3<Kernel>  Polyhedron;
typedef Polyhedron::Vertex_iterator vertex_iterator;
typedef Polyhedron::Facet_iterator facet_iterator;
typedef Polyhedron::Halfedge_handle halfedge_handle;
//
////Nef Polyhedron typedefs
//typedef CGAL::Nef_polyhedron_3<Kernel> Nef_polyhedron;

//my own definitions
typedef std::pair<face_index, std::vector<Point_3>> face_points_pair;